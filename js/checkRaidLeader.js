var checkRaidLeaderApp = angular.module('checkRaidLeaderApp', ['ngSanitize'])

  .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {

    $scope.data = {
      loading: false,
      name: null,
      errormsg: null
    };

    $scope.searchPlayer = function () {

      var platformBool = $("#checkPlatform").is(':checked');
      var platform = 1;
      if (platformBool) platform = 2;
      if (!$scope.data.name || $scope.data.name.trim().length==0) return;
      console.log(platform+" "+$scope.data.name);
      ReadService.load(platform, $scope.data.name, $scope);
    }

    console.log("loaded");
  }]);

checkRaidLeaderApp.service("ReadService", ["$http", "$q", function ($http, $q) {
  return ({
    load: load
  });

  function load(platform, name, $scope) {
    $scope.data.loading = true;
    $scope.data.errormsg = "";
    var request = $http({
      method: "get",
      url: "/api/checkRaidLeader/" + platform + "/" + encodeURIComponent(name),
      params: {nocache: true}
    });
    (request.then(function (response) {
      if (!response.data.chars) {
        if (response.data.message){

          $scope.data.errormsg = response.data.message;
        }
        else{
          $scope.data.errormsg = "Problem loading data from Bungie";
        }
        $scope.data.loading = false;
        return;
      }
      $scope.data.chars = response.data.chars;
      $scope.data.loading = false;
      console.log(JSON.stringify(response.data));
    }, handleError));
  }

  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return ($q.reject("An unknown error occurred."));
    }
    return ($q.reject(response.data));
  }

}]);