function addFav(platform, name) {
    var cookieString = localStorage.getItem("favorites");
    var favs = parseFavs(cookieString);
    for (var cntr = 0; cntr < favs.length; cntr++) {
        if (favs[cntr].name == name) {
            return false;
        }
    }
    favs.push({
        "platform": platform,
        "name": name
    });
    cookieString = serializeFavs(favs);
    localStorage.setItem("favorites", cookieString);
    return true;
}

function serializeFavs(favs) {
    var returnMe = "";
    for (var cntr = 0; cntr < favs.length; cntr++) {
        returnMe += favs[cntr].platform + "^#^" + favs[cntr].name + "|^|";
    }
    return returnMe;
}

function parseFavs(favs) {
    if (favs == null)
        return [];
    if (favs.trim().length == 0)
        return [];
    var listFavs = favs.split("|^|");
    var returnMe = [];
    for (var cntr = 0; cntr < listFavs.length; cntr++) {
        var fav = listFavs[cntr];
        if (fav.trim().length == 0)
            continue;
        var aFav = fav.split("^#^");
        returnMe.push({
            "platform": aFav[0],
            "name": aFav[1]
        });
    }
    return returnMe;
}
