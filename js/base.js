$.fn.pressEnter = function(fn) {

    return this.each(function() {
        $(this).bind('enterPress', fn);
        $(this).keyup(function(e){
            if(e.keyCode == 13)
            {
                $(this).trigger("enterPress");
            }
        })
    });
};


function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

(function($) {
	$.QueryString = (function(a) {
		if (a == "") return {};
		var b = {};
		for (var i = 0; i < a.length; ++i)
		{
			var p=a[i].split('=');
			if (p.length != 2) continue;
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	})(window.location.search.substr(1).split('&'))
})(jQuery);

var NO_CACHE = false;
function isCache(){
    NO_CACHE = $.QueryString["nocache"];
}

//isCache();
$(document).ready(isCache);

function tagSearchClick(tag){
  var platformBool = $("#"+tag+"Platform").is(':checked');
  var platform = 1;
  if (platformBool) platform = 2;

  var name = $("#"+tag+"Name").val();
  localStorage.setItem(tag+"platform", platform);
  localStorage.setItem(tag+"Tag", name);

  if (platform < 1 || platform > 2)
    return false;
  if (name == null || name.trim().length == 0)
    return false;
  
  var dest = tag;
  if (dest=="layout") dest = "tabs";
  window.location.href = "/"+dest+"/" + platform + "/" + encodeURIComponent(name);
  return false; 
}

function tagSetupSearch(tag){
  var savedPlatform = localStorage.getItem(tag+"platform");
  var savedGt = localStorage.getItem(tag+"Tag");
  if (savedPlatform==2){
    $("#"+tag+"Platform").prop('checked', true).change();
  }
  if (savedGt){
    $("#"+tag+"Name").val(savedGt);
  }
  var searchClick = tagSearchClick.bind(undefined, tag);
  $("#"+tag+"ViewBtn").bind("click", searchClick);
  $('#'+tag+'Name').pressEnter(searchClick);
}



function searchNavBarClick() {
    var platformBool = $("#layoutPlatform").is(':checked');
    var platform = 1;
    if (platformBool) platform = 2;

    var name = $("#layoutName").val();
    localStorage.setItem("navplatform", platform);
    localStorage.setItem("navTag", name);

    console.log(platform+" "+name);
    if (platform < 1 || platform > 2)
        return false;
    if (name == null || name.trim().length == 0)
        return false;
    window.location.href = "/tabs/" + platform + "/" + encodeURIComponent(name);
    return false;
}


function setupSpecificSearch(){
  if (PATH){
    tagSetupSearch(PATH);
  }
}

function setupSearch(){
    var savedPlatform = localStorage.getItem("navplatform");
    var savedGt = localStorage.getItem("navTag");
    if (savedPlatform==2){
        $("#layoutPlatform").prop('checked', true).change();
    }
    if (savedGt){
        $("#layoutName").val(savedGt);
    }
    $("#layoutViewBtn").bind("click", searchNavBarClick);
    $('#layoutName').pressEnter(searchNavBarClick);
}

function setupCss(){
	$(".cssSelector").click ( function() {
	    var name = $(this).attr ( "data-name" );
	    var url = $(this).attr ( "data-url" );
	    changeTheme(name, url);
	});
}

//used for Leaderboard only since it's static
function initTheme(){
	var storedTheme = $.cookie('csstheme');

	if (storedTheme){
		var curTheme = $('#csstheme').attr('href');
		if (storedTheme != curTheme) {
			var storedName = $.cookie('cssname');
			changeTheme(storedName, storedTheme);
		}	
	}
	
}

function changeTheme(name, newCss) {
	var curTheme = $('#csstheme').attr('href');
	if (curTheme != newCss) {
		console.log(curTheme + "->" + newCss);
		$('#csstheme').attr('href', newCss);
        $.cookie('csstheme', newCss, { expires: 365*10, path: '/' });
        $.cookie('cssname', name, { expires: 365*10, path: '/' });
	}
	
	$(".cssSelector").each ( function(i, e) {
		e = $(e);
	    var cssUrl = e.attr ( "data-url" );
	    if (cssUrl==newCss){
	    	console.log("Selected "+newCss);
	    	e.parent().addClass("active");
	    }
	    else{
	    	e.parent().removeClass("active");
	    }
	});

	
};

(function(i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function() {
		(i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o), m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', GOOG_ANAL, 'auto');
ga('send', 'pageview');

function trunc(num){
	if (num==null) return 0;
	if (isNaN(num)) return 0;
	return Number(Number(num).toFixed(0));
}


$(document).ready(setupSearch);
$(document).ready(setupSpecificSearch);
$(document).ready(initMe);
$(document).ready(setupCss);