var ORACLE_JS = (function () {
    var exports = {};

    var ROUND = 1;

    function update() {

        if (ROUND == 1) {
            $("#prevBtn").attr("disabled", true);
        }
        else {
            $("#prevBtn").removeAttr("disabled");
        }

        if (ROUND == 7) {
            $("#nextBtn").attr("disabled", true);
        }
        else {
            $("#nextBtn").removeAttr("disabled");
        }

        var tbody = $("#rounds");

        var cntr;
        var children = tbody.children();
        for (cntr = 0; cntr < 7; cntr++) {
            var row = $(children[cntr]);
            if (ROUND == (cntr + 1)) {
                row.addClass("highlighted");
            }
            else {
                row.removeClass("highlighted");
            }
        }
    }

    function prev() {
        if (ROUND > 1) {
            ROUND--;
        }
        update();
    }

    function next() {
        if (ROUND < 7) {
            ROUND++;
        }
        update();

    }
    
    function playSound(oracleNumber, event){
      var player = $("#oraclePlayer"+oracleNumber);
      player[0].play();
      
    }

    function initMe() {
      $(".asdf").bind("click", function(){
        player = document.getElementById('asdf');
        player.play();
      });
        $(".oracleMid").bind("click", playSound.bind(null, 1));
        $(".oracleL1").bind("click", playSound.bind(null, 2));
        $(".oracleR1").bind("click", playSound.bind(null, 3));
        $(".oracleL2").bind("click", playSound.bind(null, 4));
        $(".oracleR2").bind("click", playSound.bind(null, 5));
        $(".oracleL3").bind("click", playSound.bind(null, 6));
        $(".oracleR3").bind("click", playSound.bind(null, 7));
      
        $("#prevBtn").bind("click", prev);
        $("#nextBtn").bind("click", next);
        update();
        $(document).on('keydown', function (e) {

            //right down space enter
            if (e.which == 39 || e.which == 40 || e.which == 32 || e.which == 13) {
                next();
                e.preventDefault();
            }
            //left up backspace
            else if (e.which == 37 || e.which == 38) {
                prev();
                e.preventDefault();
            }
        });
    }

    exports.initMe = initMe;
    return exports;
});