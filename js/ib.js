var ibApp = angular.module('ibApp', ['ui.bootstrap-slider'])
    
    .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {
      tagSetupSearch()
        var lastRankXp, nextRankXp;
        var weekday = new Array(7);
        weekday[0] = "Tue";
        weekday[1] = "Wed";
        weekday[2] = "Thu";
        weekday[3] = "Fri";
        weekday[4] = "Sat";
        weekday[5] = "Sun";
        weekday[6] = "Mon";

        var btyWeekday = new Array(7);
        btyWeekday[0] = "Skip";
        btyWeekday[1] = "Tue";
        btyWeekday[2] = "Wed";
        btyWeekday[3] = "Thu";
        btyWeekday[4] = "Fri";
        btyWeekday[5] = "Sat";
        btyWeekday[6] = "Sun";
        btyWeekday[7] = "Mon";

        function getRankInfo(xp) {
            var nextRank, lastRank, rank, pctToNextRank;
            if (xp > 8499) {
                rank = 5;
                lastRankXp = 8500;
                nextRankXp = 0;
            }
            else if (xp > 6099) {
                rank = 4;
                lastRankXp = 6100;
                nextRankXp = 8500;
            }
            else if (xp > 3699) {
                rank = 3;
                lastRankXp = 3700;
                nextRankXp = 6100;
            }
            else if (xp > 1299) {
                rank = 2;
                lastRankXp = 1300;
                nextRankXp = 3700;
            }
            else if (xp > 99) {
                rank = 1;
                lastRankXp = 100;
                nextRankXp = 1300;
            }
            else {
                rank = 0;
                lastRankXp = 0;
                nextRankXp = 100;
            }
            var nextRank;
            if (nextRankXp > 0) {
                pctToNextRank = parseInt((100 * (xp - lastRankXp) / (nextRankXp - lastRankXp)).toFixed(0));
                nextRank = rank + 1;
            }
            else {
                //handle "you're done" case
                pctToNextRank = 100;
                lastRankXp = 6100;
                nextRankXp = 8500;
                nextRank = 5;
            }

            var pctToTotal = parseInt((100 * (xp / 8500)).toFixed(0));

            return {
                xp: xp,
                lastRankXp: lastRankXp,
                nextRankXp: nextRankXp,
                rank: rank,
                rankProgress: xp - lastRankXp,
                rankTotal: nextRankXp - lastRankXp,
                nextRank: nextRank,
                pctToNextRank: pctToNextRank,
                pctToTotal: pctToTotal,
                pctToTotalStyle: {
                    width: pctToTotal + '%'
                },
                pctToNextRankStyle: {
                    width: pctToNextRank + '%'
                }
            };

        };
        
        var WIN_VAL = 250;
        var MED_VAL = 150;

        function buildRow(iDay, startXp, data) {
            var startRank = $scope.getRank(startXp);
            
            var temperBonus = 0; //$scope.getTemper(iDay);
            var nfBonus = 0;
            var altBonus = 0;
            var gearBonus = data.calculated.gearBonus;
            if (startRank < data.calculated.maxAlt && data.calculated.maxAlt > 1)
                altBonus = 1;
            if ($scope.hasNightfallBonus()){
              nfBonus = 0.25;
            }
            
            var totalBonus = (1 + gearBonus) * (1 + temperBonus) * (1 + altBonus) * (1+nfBonus);
            var winValue = parseInt((WIN_VAL * totalBonus).toFixed(0));
            //var bountyValue = parseInt((125 * totalBonus).toFixed(0));
            var medallionValue = parseInt((MED_VAL * totalBonus).toFixed(0));
            var wkBtyValue = parseInt((750 * totalBonus).toFixed(0));
            var matchCnt = data.assumptions.matches;
            var btyCnt = data.assumptions.bounty;
            if (data.assumptions.advanced.overrideDays[iDay].match != null) matchCnt = data.assumptions.advanced.overrideDays[iDay].match;
            if (data.assumptions.advanced.overrideDays[iDay].bounty != null) btyCnt = data.assumptions.advanced.overrideDays[iDay].bounty;
            var wkBtyCnt = 0;
            if ((data.assumptions.wkBty1-1) == iDay) wkBtyCnt++;
            if ((data.assumptions.wkBty2-1) == iDay) wkBtyCnt++;
            if ((data.assumptions.wkBty3-1) == iDay) wkBtyCnt++;
            if ((data.assumptions.wkBty4-1) == iDay) wkBtyCnt++;
            
            var winPct = data.assumptions.winPct / 100;

            var winCnt = matchCnt * winPct;
            var medCnt = matchCnt * (1 - winPct);
            var winXp = winCnt * winValue;
            var medXp = medCnt * medallionValue;
            //var btyXp = btyCnt * bountyValue;
            var btyXp = 0;
            var wkBtyXp = wkBtyCnt * wkBtyValue;
            data.calculated.totalWeeklyBountyXp+=wkBtyXp;
            
            var finalXp = startXp + winXp + medXp + btyXp + wkBtyXp;
            //if (finalXp > 8500) finalXp = 8500;
            var final = getRankInfo(finalXp);

            var rankMsg = null;
            if (final.rank == 5 && startRank!=5) {
                data.calculated.lastDay = iDay;
                rankMsg = "Complete as of "+weekday[iDay];
                data.calculated.finalMsg = rankMsg;
            }
            else if (final.rank==5){
                rankMsg = data.calculated.finalMsg;
            }
            else {
                rankMsg = final.pctToNextRank + "% of the way to Rank " + (final.rank + 1);
            }
            data.calculated.totalMatches += matchCnt;
            data.calculated.totalBounties += btyCnt;
            var row = {
                day: weekday[iDay],
                rank: {
                    rank: final.rank,
                    msg: rankMsg
                },
                bonus: {
                    gear: gearBonus,
                    temper: temperBonus,
                    alt: altBonus,
                    total: totalBonus-1
                },
                value: {
                    win: winValue,
                    //bounty: bountyValue,
                    medallion: medallionValue,
                    wkBty: wkBtyValue
                },
                earned: {
                    win: {
                        cnt: winCnt.toFixed(1),
                        val: winXp
                    },
                    bounty: {
                        cnt: btyCnt.toFixed(1),
                        val: btyXp
                    },
                    wkBounty: {
                        cnt: wkBtyCnt,
                        val: wkBtyXp
                    },
                    med: {
                        cnt: medCnt.toFixed(1),
                        val: medXp
                    }
                }
            };
            
            data.calculated.table.push(row);
            return finalXp;

        };

        isCache();

        var dayOfWeek = new Date().getDay() - 2;
        if (dayOfWeek < -1)
            dayOfWeek = 5;

        if (dayOfWeek < 0)
            dayOfWeek = 6;

        $scope.showBountyTab = function(){
            $('.nav-tabs a[href="#bounty"]').tab('show');
        }

        $scope.calcBounties = function(data){
            if (!data.selectedChar || !data.selectedChar.publicAdv) return;
            var btys = data.selectedChar.publicAdv.ib.bounties;
            var totalWkVal =  btys.length * data.calculated.table[0].value.wkBty;
            var cmpWkly=0;
            
            for (var cntr=0; cntr<btys.length; cntr++){
                if (btys[cntr].isFullyComplete){
                    cmpWkly++;
                }
            }
            var cmpXpWkly = cmpWkly * data.calculated.table[0].value.wkBty;

            var curXP =  data.calculated.current.xp;
            var isDone=false;
            if (curXP==8500){
                isDone=true;
            }
            
            var hasCompleted = cmpWkly>0;
            var hasHeld = (data.selectedChar.publicAdv.ib.bounties.length)>0;
            var moreForCompleted = (8500-(curXP+cmpXpWkly));
            var moreForHeld = (8500-(curXP+totalWkVal));
            
            console.log(hasCompleted + " "+ hasHeld +" " + moreForCompleted+ " " +moreForHeld);

            data.calculated.btyInfo = {
                completed: {
                    weekly: cmpWkly,
                    weeklyXp: cmpXpWkly
                },
                rank5: {
                    done: isDone,
                    hasCompleted: hasCompleted,
                    hasHeld: hasHeld,
                    moreForCompleted: moreForCompleted,
                    moreForHeld: moreForHeld
                }
            };

        }

        $scope.neededForR5 = function(data){
            var curXP =  data.calculated.current.xp;
            if (curXP>=8500) return 0;
            var btys = data.selectedChar.publicAdv.ib.bounties;
            
            for (var cntr=0; cntr<btys.length; cntr++){
                if (btys[cntr].isFullyComplete){

                }
            }

            var wkVal =  data.selectedChar.publicAdv.ib.bounties.length * data.calculated.table[0].value.wkBty;
            var total = curXP + wkVal;
            return 8500-total;
        }

        $scope.getRank = function (xp) {
            if (xp > 8499)
                return 5;
            else if (xp > 6099)
                return 4;
            else if (xp > 3699)
                return 3;
            else if (xp > 1299)
                return 2;
            else if (xp > 99)
                return 1;
            else
                return 0;

        }

        //$scope.getTemper = function (iDay) {
        //    if (iDay == 0)
        //        return .1;
        //    else if (iDay == 1)
        //        return .15;
        //    else if (iDay == 2)
        //        return .25;
        //    else if (iDay == 3)
        //        return .4;
        //    else if (iDay == 4)
        //        return .6;
        //    else if (iDay == 5)
        //        return 1.0;
        //    else if (iDay == 6)
        //        return 1.5;
        //    else
        //        throw "Day " + iDay + " makes no sense";
        //
        //};

        $scope.calcTable = function () {
            console.log("Calc table");
            $scope.data.calculated = {
                totalMatches: 0,
                totalBounties: 0,
                totalWeeklyBountyXp: 0,
                pctToNextRankStyle: {},
                pctToTotalStyle: {}
            };

            var xp;
            if ($scope.data.assumptions.advanced.overrideXP != null)
                xp = $scope.data.assumptions.advanced.overrideXP;
            else
                xp = $scope.data.selectedChar.ibXp;

            $scope.data.calculated.current = getRankInfo(xp);
            
            var bonus = 1;
            if ($scope.data.selectedChar.ibEmblem)
                bonus = bonus * 1.1;
            if ($scope.data.selectedChar.ibClass)
                bonus = bonus * 1.1;
            if ($scope.data.selectedChar.ibShader)
                bonus = bonus * 1.1;
            $scope.data.calculated.gearBonus = bonus - 1;
            $scope.data.calculated.maxAlt = $scope.data.maxAltRank;
            if ($scope.data.assumptions.advanced.overrideAlt != null) {
                $scope.data.calculated.maxAlt = $scope.data.assumptions.advanced.overrideAlt;
            }
            $scope.data.calculated.altBonusMsg = "";
            $scope.data.calculated.displayAltBonus = (100 * $scope.data.calculated.altBonus).toFixed(0);
            $scope.data.calculated.totalBonus = (1 + $scope.data.calculated.gearBonus) * (1 + $scope.data.calculated.temperBonus) * (1 + $scope.data.calculated.altBonus) - 1;
            $scope.data.calculated.displayTotalBonus = (100 * $scope.data.calculated.totalBonus).toFixed(0);

            $scope.data.calculated.table = [];
            var cntr;
            //repeat for remaning days in week
            var xp = $scope.data.calculated.current.xp;
            for (cntr = $scope.data.dayOfWeek; cntr < 7; cntr++) {
                xp = buildRow(cntr, xp, $scope.data);
            }
            $scope.data.calculated.final = getRankInfo(xp);
            $scope.calcBounties($scope.data);
            localStorage.setItem("ibSaved", JSON.stringify($scope.data.assumptions));
            console.log("Done build");


        };

        var dummy = {
            label: "Loading...",
            ibEmblem: false,
            ibClass: false,
            ibShader: false,
            ibXp: 0,
            medallions: 0,
            history: []
        };
        
        $scope.hasNightfallBonus = function(){
          return $scope.data.assumptions.advanced.overrideNf || ($scope.data.selectedChar.publicAdv && $scope.data.selectedChar.publicAdv.nightfall);
        };

        $scope.buildDefaultOverrides = function () {
            return {
                overrideXp: null,
                overrideNf: false,
                overrideDays: [{bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null}]
            }
        };

        var assumptions = {
            winPct: 50,
            matches: 5,
            bounty: 2,
            wkBty1: 7,
            wkBty2: 7,
            wkBty3: 0,
            wkBty4: 0,
            advanced: $scope.buildDefaultOverrides()
        };

        var localSettings = localStorage.getItem("ibSaved");
        if (localSettings) {
            assumptions = JSON.parse(localSettings);
            if (!assumptions.wkBty1) assumptions.wkBty1 = 7;
            if (!assumptions.wkBty2) assumptions.wkBty2 = 7;
            if (!assumptions.wkBty3) assumptions.wkBty3 = 0;
            if (!assumptions.wkBty4) assumptions.wkBty4 = 0;
        }

        $scope.data = {
            loading: true,
            selectedChar: dummy,
            weekdays: weekday,
            btyWeekdays: btyWeekday,
            dayOfWeek: dayOfWeek,
            maxAltRank: 0,
            chars: [dummy],
            calculated: {
                final: getRankInfo(0),
                current: getRankInfo(0),
                gearBonus: 0,
                maxAlt: 0
            },
            assumptions: assumptions
        };
        $scope.calcTable();
        if (PLAYER_PLATFORM!=0) {
          ReadService.loadIb(PLAYER_PLATFORM, MEMBER_ID, $scope);
        }
        else{
          $scope.data.loading = false;
        }
        console.log("loaded");
    }]);

ibApp.service("ReadService", ["$http", "$q", function ($http, $q) {
    return ({
        loadIb: loadIb
    });

    function loadIb(platform, memberId, $scope) {
        var request = $http({
            method: "get",
            url: "/api/ibChars/" + platform + "/" + memberId,
            params: {nocache: NO_CACHE}
        });
        (request.then(function (response) {
            if (!response.data.chars) {
                $scope.data.errormsg = "Problem loading data from Bungie. You'll have to enter your own data below for XP and IB gear, but the rest of the calculator should work!";
                $scope.data.loading = false;
                return;
            }
            $scope.data.chars = response.data.chars;
            if ($scope.data.chars.length >= 1) $scope.data.selectedChar = $scope.data.chars[0];
            if ($scope.data.selectedChar.privacy==true){
                $scope.data.errormsg = " This user has selected to hide parts of their data on Bungie.net which prevents this page from being properly displayed." +
                    " If you are the user in question, you can fix this by: Going to Bungie.net, clicking the settings gear icon, " +
                    "clicking 'Privacy' and checking the 'Show My Advisors' box.";
            }
            $scope.data.loading = false;
            var maxAltRank = 0;
            var cntr;
            for (cntr = 0; cntr < $scope.data.chars.length; cntr++) {
                var char = $scope.data.chars[cntr];
                if (char.stale) char.ibXp = 0;

            }
            
            for (cntr = 0; cntr < $scope.data.chars.length; cntr++) {
                var char = $scope.data.chars[cntr];
                var altRank = $scope.getRank(char.ibXp);
                if (altRank > maxAltRank) maxAltRank = altRank;
            }
            $scope.data.maxAltRank = maxAltRank;

            $scope.calcTable();
        }, handleError));
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
        }
        return ($q.reject(response.data));
    }

}]);