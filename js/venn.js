var vennApp = angular.module('vennApp', ['toaster','ngSanitize','ngAnimate'])
  .controller('mainController', ['$rootScope', '$scope', 'toaster', 'ReadService', function ($rootScope, $scope, toaster, ReadService) {
    $scope.data = {
      loading: false,
      query: {
        platform: 1,
        gt1: {
          value: null
        },
        gt2: {
          value: null
        },
        gt3: {
          value: null
        },
        filter: "someQuests",
        btyFilter: "someBounties"
      }
    };


    var loadedPlatform = VENN_PLATFORM;
    if (loadedPlatform) {
      $scope.data.query.platform = loadedPlatform;
      $scope.data.query.gt1.value = VENN_NAME != '' ? VENN_NAME : null;
      $scope.data.query.gt2.value = VENN_NAME2 != '' ? VENN_NAME2 : null;
      $scope.data.query.gt3.value = VENN_NAME3 != '' ? VENN_NAME3 : null;
    }
    else {
      var savedPlatform = localStorage.getItem("vennPlatform");
      if (savedPlatform != 2) savedPlatform = 1;
      $scope.data.query.platform = savedPlatform;
      $scope.data.query.gt1.value = localStorage.getItem("vennGt1");
      $scope.data.query.gt2.value = localStorage.getItem("vennGt2");
      $scope.data.query.gt3.value = localStorage.getItem("vennGt3");
    }

    if ($scope.data.query.platform == 2) $("#vennPlatform").prop('checked', true).change();

    $('#vennPlatform').change(function () {
      console.log("Change");
      var val = $(this).prop('checked');
      var platform = val ? 2 : 1;
      $scope.data.query.platform = platform;
      $scope.$digest();
    });

    function submitQuery(query) {
      localStorage.setItem("vennPlatform", query.platform);
      localStorage.setItem("vennGt1", query.gt1.value);
      localStorage.setItem("vennGt2", query.gt2.value);
      if (query.gt3.value == null)
        localStorage.setItem("vennGt3", null);
      else
        localStorage.setItem("vennGt3", query.gt3.value);
      ReadService.load(query.platform, query.gt1, query.gt2, query.gt3, query.refresh, $scope, loadHidden());
    }

    $scope.submitVenn = function (query) {
      submitQuery(query);
    }
    
    $scope.hideQuest = function(quest){
      quest.hidden = true;
      $scope.data.questsHidden = true;
      saveHidden();
    }
    
    function saveHidden(){
      if (!$scope.data.quests) return;
      var oHash = {};
      $scope.data.quests.forEach(function(q){
        if (q.hidden) 
          oHash[q.questHash] = true;
      });
      localStorage.setItem("hiddenQuests", JSON.stringify(oHash));
    }
    
    function loadHidden(quests){
      var oHash = {};
      try{
        var sQuests = localStorage.getItem("hiddenQuests");
        oHash = JSON.parse(sQuests);
      }
      catch (e)
      {
        //ignore
      }
      return oHash;
    }

    $scope.showAllQuests = function(){
      $scope.data.quests.forEach(function(quest){
        delete quest.hidden;
      });
      delete $scope.data.questsHidden;
      saveHidden();
    }

    $scope.clickToOpen = function (q) {
      $scope.data.selectedQuest = q;
      $('#questModal').modal();
    };

    $scope.clickToOpenBty = function (q) {
      $scope.data.selectedBty = q;
      $('#btyModal').modal();
    };

    $scope.getDirectLink = function(query){
      var val = "/venn/"+query.platform+"/"+query.gt1.value+"/"+query.gt2.value;
      if (query.gt3.value) {
        val += "/" + query.gt3.value;
      }
      return val;
    };

    $scope.filterBounties= function(bty) {
      switch($scope.data.query.btyFilter) {
        case 'someBounties':
          return bty.members && bty.members.length>1;
          break;
        case 'allBounties':
          return bty.members && bty.members>=$scope.data.goodPlayers;
          break;
        default:
          return true;
      }
    }
    
    $scope.filterQuests = function(quest) {
      switch($scope.data.query.filter) {
        case 'someQuests':
          return quest.members && quest.members.length>1;
          break;
        case 'allQuests':
          return quest.members && quest.members>=$scope.data.goodPlayers;
          break;
        case 'someSteps':
          return quest.membersOnSameStep && quest.membersOnSameStep>1;
          break;
        case 'allSteps':
          return quest.membersOnSameStep && quest.membersOnSameStep>=$scope.data.goodPlayers.length;
          break;
        default:
          return true;
      }
    }

      console.log("loaded");

    //auto search if we're full
    if ($scope.data.query.platform && $scope.data.query.gt1.value && $scope.data.query.gt2.value){
      submitQuery($scope.data.query);
    }
  }]);

vennApp.service("ReadService", ["$http", "$q", "toaster", function ($http, $q, toaster) {
  return ({
    load: load
  });
  
  function cookRaid(raid){
    if (!raid) return;
    if (raid.hm && raid.hm.steps) {
      if (raid.hm.steps[raid.hm.steps.length - 1].isComplete == true) {
        raid.hmComplete = true;
      }
    }
    else if (raid.nm.steps[raid.nm.steps.length-1].isComplete==true){
      raid.nmComplete = true;
    }
    return raid;
  }

  function cookWeekly(allChars){
   var weeklyChars = [];

    allChars.forEach(function (char){
      var wkly = {};
      if (!char.publicAdv) return;
      wkly.coe = char.publicAdv.coe;
      if (wkly.coe){
        if (wkly.coe.high.complete && wkly.coe.cum.complete)
          wkly.coe.done = true;
        else wkly.coe.done = false;
      }
      wkly.kf = cookRaid(char.publicAdv.kf);
      wkly.ce = cookRaid(char.publicAdv.ce);
      wkly.vog = cookRaid(char.publicAdv.vog);
      wkly.wrath = cookRaid(char.publicAdv.wrath);
      

      wkly.gunsmith = char.publicAdv.gunsmith;
      wkly.nightfall = char.publicAdv.nightfall;
      wkly.dailyHeroic = char.publicAdv.dailyHeroic;
      wkly.dailyPvp = char.publicAdv.dailyPvp;
      wkly.weeklyHeroic = char.publicAdv.weeklyHeroic;
      wkly.weeklyCrucible = char.publicAdv.weeklyCrucible;
      wkly.nightfall = char.publicAdv.nightfall;
      weeklyChars.push(wkly);
    });
    return weeklyChars;
  }

  function cookBounties(allChars){
    var bounties = {};

    //load up all the quests
    allChars.forEach(function (char){
      if (char.publicAdv){
        //if the char has any quests, iterate them
        char.publicAdv.bounties.forEach(function(bounty){
          //if it's complete we don't care about it
          if (bounty.completed) return;
          //if all the objectives are met, it's also done
          
          if (!bounty.objectives) bounty.objectives = bounty.steps;
          if (!bounty.objectives) bounty.objectives = [];
          
          var done = true;
          if (bounty.objectives) {
            if (bounty.objectives.length==0){
              done = false;
            }
            else {
              bounty.objectives.forEach(function (obj) {
                if (obj.progress < obj.value) done = false;
              });
            }
          }
          if (done) return;
          //try to grab the globally cached quest, if it's not there copy this one
          if (!bounties[bounty.questHash]){
            bounties[bounty.questHash] =  angular.copy(bounty);

            //delete started, completed, objects[].progress
            delete bounties[bounty.questHash].started;
            delete bounties[bounty.questHash].completed;
            if (bounties[bounty.questHash].objectives){
              bounties[bounty.questHash].objectives.forEach(function(obj){
                delete obj.progress;
                obj.progress = [];
              });
            }
             //associate array of all chars holding the quest, we'll delete this
            bounties[bounty.questHash].holdingChars = {};
            //and we'll push the chars to this array instead
            bounties[bounty.questHash].chars = [];
            bounties[bounty.questHash].holdingMembers = {};
            //same plan here, holdingMembers goes away and gets pushed to this array
            bounties[bounty.questHash].members = [];
          }
          var target = bounties[bounty.questHash];
          bounty.objectives.forEach(function(obj, index){
            bounties[bounty.questHash].objectives[index].progress.push({
              char: char, 
              progress: obj.progress
            });
          });
          target.holdingChars[char.id] = true;
          target.holdingMembers[char.parent.id] = char.parent;
        });
      }
    });
    //now that we have a superset of quests, iterate through each char to mark them
    var aBounties = [];
    angular.forEach(bounties, function(bounty){
      allChars.forEach(function (char){
        //this char has the bounty
        if (bounty.holdingChars[char.id]){
          bounties[bounty.questHash].chars.push(bounty.holdingChars[char.id]);
        }
        else{
          bounties[bounty.questHash].chars.push(null);
        }
      });
      angular.forEach(bounty.holdingMembers, function(member){
        bounty.members.push(member);
      });
      delete bounty.holdingMembers;
      delete bounty.holdingChars;
      aBounties.push(bounty);
    });

    return aBounties;
  }



  function cookQuests(allChars){
    var quests = {};
   
    //load up all the quests
    allChars.forEach(function (char){
      if (char.publicAdv){
        //if the char has any quests, iterate them
        char.publicAdv.quests.forEach(function(quest){
          //try to grab the globally cached quest, if it's not there copy this one
          if (!quests[quest.questHash]){
            quests[quest.questHash] =  angular.copy(quest);
            //there is no shared current step, delete it
            delete quests[quest.questHash].currentStep;
            //associate array of all chars holding the quest, we'll delete this
            quests[quest.questHash].holdingChars = {};
            //and we'll push the chars to this array instead
            quests[quest.questHash].chars = [];
            quests[quest.questHash].holdingMembers = {};
            //same plan here, holdingMembers goes away and gets pushed to this array
            quests[quest.questHash].members = [];
          }
          var target = quests[quest.questHash];
          target.holdingChars[char.id] = quest.currentStep;
          if (target.steps[quest.currentStep-1].chars == null){
            target.steps[quest.currentStep-1].chars = [];

            target.steps[quest.currentStep-1].holdingMembers = {};
          }
          target.steps[quest.currentStep-1].chars.push(char);
          target.steps[quest.currentStep-1].holdingMembers[char.parent.id] = true;
          target.holdingMembers[char.parent.id] = char.parent;
          
        });
      }
    });
    //now that we have a superset of quests, iterate through each char to mark them
    var aQuests = [];
    angular.forEach(quests, function(quest){
      allChars.forEach(function (char){
        //this char has the quest
        if (quest.holdingChars[char.id]>0){
          quests[quest.questHash].chars.push(quest.holdingChars[char.id]);
        }
        else{
          quests[quest.questHash].chars.push(-1);
        }
      });
      angular.forEach(quest.holdingMembers, function(member){
        quest.members.push(member);
      });
      
      var maxMaxMembers = 0;
      quest.steps.forEach(function(step, stepIndex){
        if (!step.chars || step.chars.length==0) return;
        step.maxMembers = 0;
        angular.forEach(step.holdingMembers, function(member){
          step.maxMembers++;
        });
        if (step.maxMembers>maxMaxMembers) maxMaxMembers = step.maxMembers;
        delete step.holdingMembers;
      });
      
      quest.membersOnSameStep = maxMaxMembers;
      
      delete quest.holdingMembers;
      delete quest.holdingChars;
      aQuests.push(quest);
    });
    
    return aQuests;
  }

  function load(platform, gt1, gt2, gt3, refresh, $scope, saveHiddenHashes) {
    var url = "/api/venn/" + platform + "/" + encodeURIComponent(gt1.value) + "/" + encodeURIComponent(gt2.value);
    if (gt3.value) url += "/" + encodeURIComponent(gt3.value);
    gt1.validclass = null;
    gt2.validclass = null;
    gt3.validclass = null;
    var gts = [gt1, gt2, gt3];
    $scope.data.loading = true;
    $http({
      method: "get",
      url: url,
      params: {nocache: refresh}
    }).then(function (response) {
      $scope.data.loading = false;
      if (!response.data) {
        $scope.data.errormsg = "Problem loading data from Bungie";
        return;
      }
      
      $scope.data.goodPlayers = [];
      //should have at least 2 items in the array
      if (response.data.length > 1) {
        response.data.forEach(function (member, index) {
          var gt = gts[index];
          if (member.memberId) {
            gt.chars = member.chars
            gt.id = member.memberId;
            gt.validclass = "has-success";
            gt.encodeName = encodeURIComponent(gt.value);
            $scope.data.goodPlayers.push(gt);
          }
          else if (member.err) {
            if (member.err.message && member.name) {
              toaster.pop({
                type: 'error',
                title: member.name,
                body: member.err.message,
                timeout: 5000
              });
            }
            else {
              toaster.pop({
                type: 'error',
                title: "Unexpected error",
                body: "Weird...",
                timeout: 5000
              });
            }
            gt.validclass = "has-error";
          }
        });
      }
      var allChars = [];
      //get complete list of chars
      $scope.data.goodPlayers.forEach(function(player) {
        player.chars.forEach(function (char) {
          char.parent = player;
          allChars.push(char);
        });
      });
      
      $scope.data.allChars = allChars;
      $scope.data.quests = cookQuests(allChars);
      if (saveHiddenHashes && $scope.data.quests){
        var someHidden = false;
        $scope.data.quests.forEach(function(q){
          if (saveHiddenHashes[q.questHash]==true){
            q.hidden = true;
            someHidden = true;
          }
        });
        if (someHidden==true){
          $scope.data.questsHidden = true;
        }
      }
      $scope.data.bounties = cookBounties(allChars);
      $scope.data.weekly = cookWeekly(allChars);
    }, handleError);
  }

  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return ($q.reject("An unknown error occurred."));
    }
    return ($q.reject(response.data));
  }

}]);