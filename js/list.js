var listApp = angular.module('listApp', ['toaster', 'ngSanitize', 'ngAnimate'])
  .controller('mainController', ['$rootScope', '$scope', '$q', 'toaster', 'ReadService', function ($rootScope, $scope, $q, toaster, ReadService) {
    
    $scope.vm = {
      players: [],
      loading: 0
    };

    LOAD_ME.forEach(function (name) {
      $scope.vm.players.push({
        platform: PLATFORM,
        name: name,
        encodedName: encodeURI(name),
        status: "Waiting to load..."
      })
    });
    
    $scope.vm.loadPlayer = function(player, noCache){
      ReadService.listChars(PLATFORM, player, $scope, noCache);
    }


    var tasks = [];
    $scope.vm.players.forEach(function (player) {
      tasks.push(function () {
        return ReadService.listChars(PLATFORM, player, $scope);
      });
    });
    $q.serial(tasks).then(function () {
      console.log("Done");
    });
  }]);


listApp.service("ReadService", ["$http", "$q", "toaster", function ($http, $q, toaster) {
  return ({
    listChars: listChars
  });


  function processRaid(mode) {
    if (!mode) return;
    if (!mode.threeninety){
      mode.threeninety = {};
    }
    mode = mode.threeninety;
    if (!mode.steps) {
      mode.dispClass = "active";
      return;
    }
    var complete = mode.steps[mode.steps.length - 1].isComplete;
    if (complete) {
      mode.dispClass = "success";
      return;
    }
    for (var cntr = 0; cntr < mode.steps.length; cntr++) {
      if (mode.steps[cntr].isComplete) {
        mode.dispClass = "warning";
        return;
      }
    }
    mode.dispClass = "danger";
  }

  function listChars(platform, player, $scope, noCache) {
    var request = $http({
      method: "get",
      url: "/api/getCharsForList2/" + platform + "/" + player.encodedName,
      params: {nocache: NO_CACHE||noCache}
    });
    $scope.vm.loading++;
    player.loading = true;
    player.status = "Loading...";
    return $q.when(request.then(handleErrors).then(function (response) {
      if (response.data && response.data.player && response.data.chars) {
        player.data = response.data;
        player.status = null;
        if (player.data){
          if (player.data.chars){
            player.data.chars.forEach(function(jo){
              processRaid(jo.publicAdv.wrath);
              processRaid(jo.publicAdv.kf);
              processRaid(jo.publicAdv.vog);
              processRaid(jo.publicAdv.ce);
            });
          }
          if (player.data.player && player.data.player.lastPlayed){
            player.data.player.lastPlayed = "Last played "+moment(player.data.player.lastPlayed).fromNow();
          }
        }
        
      }
      else if (response.data && response.data.name=="OtherPlatform"){
        player.status = "Wrong platform";
      }
      else if (response.data && response.data.name=="NoChars"){
        player.status = "No characters";
      }
      else if (response.data && response.data.name=="NotFound"){
        player.status = "No Destiny player found";
      }
      else if (response.data.error){
        return $q.reject(new Error(response.data.error));
      }
      else {
        return $q.reject(new Error("Unexpected response."));
      }
    }).catch(function (err) {
      player.status = err.message;
    }).finally(function() {
        player.loading = null;
        $scope.vm.loading--;
    })
    );
  }


  function handleErrors(response) {
    if (response.status === 503) {
      return $q.reject(new Error("Server is down."));
    }
    if (response.status < 200 || response.status >= 400) {
      return $q.reject(new Error('Network error: ' + response.status));
    }
    return response;
  }


}]);


listApp.config(['$provide', function ($provide) {
  $provide.decorator("$q", ['$delegate', function ($delegate) {
    //Helper method copied from q.js.
    var isPromiseLike = function (obj) {
      return obj && angular.isFunction(obj.then);
    }

    /*
     * @description Execute a collection of tasks serially.  A task is a function that returns a promise
     *
     * @param {Array.<Function>|Object.<Function>} tasks An array or hash of tasks.  A tasks is a function
     *   that returns a promise.  You can also provide a collection of objects with a success tasks, failure task, and/or notify function
     * @returns {Promise} Returns a single promise that will be resolved or rejected when the last task
     *   has been resolved or rejected.
     */
    function serial(tasks) {
      //Fake a "previous task" for our initial iteration
      var prevPromise;
      var error = new Error();
      angular.forEach(tasks, function (task, key) {
        var success = task.success || task;
        var fail = task.fail;
        var notify = task.notify;
        var nextPromise;

        //First task
        if (!prevPromise) {
          nextPromise = success();
          if (!isPromiseLike(nextPromise)) {
            error.message = "Task " + key + " did not return a promise.";
            throw error;
          }
        } else {
          //Wait until the previous promise has resolved or rejected to execute the next task
          nextPromise = prevPromise.then(
            /*success*/function (data) {
              if (!success) {
                return data;
              }
              var ret = success(data);
              if (!isPromiseLike(ret)) {
                error.message = "Task " + key + " did not return a promise.";
                throw error;
              }
              return ret;
            },
            /*failure*/function (reason) {
              if (!fail) {
                return $delegate.reject(reason);
              }
              var ret = fail(reason);
              if (!isPromiseLike(ret)) {
                error.message = "Fail for task " + key + " did not return a promise.";
                throw error;
              }
              return ret;
            },
            notify);
        }
        prevPromise = nextPromise;
      });

      return prevPromise || $delegate.when();
    }

    $delegate.serial = serial;
    return $delegate;
  }]);
}]);
