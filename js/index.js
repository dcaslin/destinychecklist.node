var INDEX_JS = (function () {
    var exports = {};


    function removeFavClick(b) {
        var target = $(b.target);
        if (target.attr("platform") == null)
            target = target.parent();
        var platform = target.attr("platform");
        var name = target.attr("name");
        removeFav(platform, name);
        target.closest('tr').remove();
    }

    function bulkClick() {
        var items = $(".bulk");
        var tags = "";
        var platform = null;
        $.each(items, function (i, t) {
            if (t.checked) {
                var jt = $(t);
                if (!platform)
                    platform = jt.attr("data-platform");
                tags += jt.attr('data-tag')+",";
            }
        });
        if (tags.length>0){
            tags = tags.substr(0, tags.length-1);
        }
        window.location = "list/"+platform+"/"+tags;
    }

    function getSelectedPlatform(){
      var platformBool = $("#selPlatform").is(':checked');
      var platform = 1;
      if (platformBool) platform = 2;
      return platform;
    }
  
    function grimChooseClick(){

        var platform = getSelectedPlatform();
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "grim/" + platform + "/" + encodeURIComponent(name);
        return false;
    }

    function cfChooseClick(){
        var platform = getSelectedPlatform();
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "cf/" + platform + "/" + encodeURIComponent(name);
        return false;
    }
    function srlChooseClick(){
        var platform = getSelectedPlatform();
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "srl/" + platform + "/" + encodeURIComponent(name);
        return false;
    }
    
    function viewClick() {
        var platform = getSelectedPlatform();      
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "tabs/" + platform + "/" + encodeURIComponent(name);
        return false;
    }


    function clanView() {

        var platformBool = $("#selClanPlatform").is(':checked');
        var platform = 1;
        if (platformBool) platform = 2;
        var name = $("#txtClanName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "clan/" + platform + "/" + encodeURIComponent(name);
        return false;
    }

    function checkChange() {
        var items = $(".bulk");
        var checked = false;
        $.each(items, function (i, t) {
            checked = checked || t.checked;

        });
        $('#bulkBtn').prop('disabled', !checked);
    }


    function ibChoose(){
        var platform = getSelectedPlatform();      
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "ib/" + platform + "/" + encodeURIComponent(name);
        return false;
    }

    function addRow(platform, name) {

        var chk = $('<input class="bulk" type="checkbox" data-platform="'+platform+'" data-tag="'+encodeURI(name)+'" />');
        chk.bind("change", checkChange);


        //var srlBtn = $('<a href="/srl/'+platform+'/'+name+'" class="btn btn-default"><span class="hidden-xs">SRL</span><span class="visible-xs-block"><i class="fa fa-flag-checkered"></i></span></a>');
        var cfBtn = $('<a href="/cf/'+platform+'/'+name+'" class="btn btn-default"><span class="hidden-xs">Calcified </span><i class="fa fa-diamond"></i></a>');
        var grimBtn = $('<a href="/grim/'+platform+'/'+name+'" class="btn btn-default"><span class="hidden-xs">Grim </span><i class="fa fa-book"></i></a>');
        var ibBtn = $('<a href="/ib/'+platform+'/'+name+'" class="btn btn-default"><span class="hidden-xs">IB </span><i class="fa fa-flag" aria-hidden="true"></i></button>');
            
        var remove = $('<button type="button" class="btn btn-default" platform="' + platform + '" name="'
            + name + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>');
        remove.bind("click", removeFavClick);

        var table = $("#favTable");
        var tr = $('<tr></tr>');
        
        var td1 = $('<td class="col-xs-1"></td>');
        td1.append(chk);
        var td2 = $('<td class="col-xs-11"></td>');
        
        var row = $('<div class="row"><b class="col-lg-3 col-md-12"><a href="tabs/' + platform + '/' + name + '" role="button">' + name + '</a></b></div>');
        
        var tlbar = $('<div class="btn-toolbar col-lg-9 col-md-12 "/>');
        tlbar.append(ibBtn);
        tlbar.append(cfBtn);
        tlbar.append(grimBtn);
        tlbar.append(remove);
        row.append(tlbar);
        td2.append(row);
        tr.append(td1);
        tr.append(td2);
        table.append(tr);
        $("#favTableParent").removeClass("hidden");
    }


    function addFavFromForm() {
        var platform = getSelectedPlatform();
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return false;
        if (name == null || name.trim().length == 0)
            return false;
        var added = addFav(platform, name);
        if (!added)
            return false;
        addRow(platform, name);
        return false;
    }

    function platformChanged() {
        var platform = getSelectedPlatform();
        localStorage.setItem("platform", platform);
    }

  function nameChanged() {
    var name = $("#txtName").val();
    localStorage.setItem("name", name);
  }

    function loadSavedPlatformAndName() {
      var savedPlatform = localStorage.getItem("platform");
      if (savedPlatform==2){
        $("#selPlatform").prop('checked', true).change();
      }

      var name = localStorage.getItem("name");
      if (name!=null){
        $("#txtName").val(name);
      }
    }

    function removeFav(platform, name) {
        var cookieString = localStorage.getItem("favorites");
        var favs = parseFavs(cookieString);
        for (var cntr = 0; cntr < favs.length; cntr++) {
            var fav = favs[cntr];
            if (fav.name == name && fav.platform == platform) {
                favs.splice(cntr, 1);
                break;
            }
        }
        cookieString = serializeFavs(favs);
        localStorage.setItem("favorites", cookieString);

    }

    function initMe() {
        //init tooltips
        $("#saveBtn").bind("click", addFavFromForm);
        $("#bulkBtn").bind("click", bulkClick);
        $("#selPlatform").bind("change", platformChanged);
        $("#txtName").bind("change", nameChanged);
        loadSavedPlatformAndName();
        var cookieString = localStorage.getItem("favorites");
        var favs = parseFavs(cookieString);
        if (favs.length > 0) {
            $.each(favs, function (i, t) {
                addRow(t.platform, t.name);
            });
        }

        $('#txtClanName').pressEnter(clanView);
        $("#viewBtn").bind("click", viewClick);
        $("#cfChooseBtn").bind("click", cfChooseClick);
        $("#srlChooseBtn").bind("click", srlChooseClick);
        $("#grimChooseBtn").bind("click", grimChooseClick);
        $("#ibChooseBtn").bind("click", ibChoose);
        $("#clanBtn").bind("click", clanView);
        $("txtName").focus();
        $('[data-toggle="popover"]').popover();
    }

    exports.initMe = initMe;
    return exports;
});