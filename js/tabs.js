var tabsApp = angular.module('tabsApp', ['toaster', 'ngSanitize', 'ngAnimate', 'as.sortable'])
  .controller('mainController', ['$rootScope', '$scope', 'toaster', 'ReadService', function ($rootScope, $scope, toaster, ReadService) {

    var loadedHidden = false;


    function defaultRows() {
      return ['closeFaction',
        
        
        //'dailyHeroic',
        //'dailyPvp',
        'ce390',
        'vog390',
        'kf390',
        'wrath390',
        'weeklyStory',

        'nightfall',
        'weeklyHeroic',
        'weeklyPvp',
        'coe',

        'publicEvent',

        'gunsmith',

        'shiro',
        'shaxx',
        'crotaBty',
        'queen',
        'variks',

        'cehm',
        'cenm',
        'voghm',
        'vognm',
        'kfhm',
        'kfnm',

        'wrathhm',
        'wrath',
        'poe41',
        'poe35',
        'poe34',
        'poe32',
        ];
    }
    
    var ROW_SETTINGS_KEY = "rowSettingsV2";

    function navTabs() {
      var hash = document.location.hash;
      if (hash) {
        $('.globalNav a[href="' + hash + '"]').tab('show');
      }

      // Change hash for page-reload
      $('.globalNav a').on('show.bs.tab', function (e) {
        window.location.hash = e.target.hash;
      });
    }

    navTabs();


    var rowSettings = null, customRows = false;
    try {
      var sRowSettings = localStorage.getItem(ROW_SETTINGS_KEY);
      if (sRowSettings) {
        rowSettings = JSON.parse(sRowSettings);
        if (!rowSettings.length || rowSettings.length <= 0) rowSettings = null;
      }
    }
    catch (e) {
      //ignore
    }
    if (!rowSettings) {
      rowSettings = defaultRows();
    }
    else {
      customRows = true;
    }


    $scope.vm = {
      loading: {
        active: true,
        pct: "5%"
      },
      showHiddenFactions: false,
      warningMsg: null,
      checklistRows: rowSettings,
      customRows: customRows
    };

    $scope.ngSortSettings = {
      //containment: "#dragContainer",
      //containerPositioning: 'relative',
      orderChanged: function (event) {
        $scope.vm.saveRowSettings();
      }

      //scrollableContainer: "#sort_" +index+ "",
      //accept: function (sourceItemHandleScope, destSortableScope) {
      //  return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
      //}
    };


    console.log("loaded");

    $scope.vm.clearRowSettings = function () {
      $scope.vm.checklistRows = defaultRows();
      $scope.vm.customRows = false;
      localStorage.removeItem(ROW_SETTINGS_KEY);
    }

    $scope.vm.saveRowSettings = function () {
      var sRowSettings = JSON.stringify($scope.vm.checklistRows);
      $scope.vm.customRows = true;
      localStorage.setItem(ROW_SETTINGS_KEY, sRowSettings);
    }

    $scope.vm.hideRow = function ($index) {
      console.log($index);
      $scope.vm.checklistRows.splice($index, 1);
      $scope.vm.saveRowSettings();
    }


    $scope.vm.showQuest = function (q) {
      $scope.vm.selectedQuest = q;
      $('#questModal').modal();
    }


    function processRaidTier(mode) {
      if (!mode || !mode.steps) {
        mode.dispClass = "active";
        return;
      }
      var complete = mode.steps[mode.steps.length - 1].isComplete;
      if (complete) {
        mode.dispClass = "success";
        return;
      }
      for (var cntr = 0; cntr < mode.steps.length; cntr++) {
        if (mode.steps[cntr].isComplete) {
          mode.dispClass = "warning";
          return;
        }
      }
      mode.dispClass = "danger";
    }

    function processRaid(raid) {

      processRaidTier(raid.threeninety);
      processRaidTier(raid.hm);
      processRaidTier(raid.nm);

      if (raid.hm.dispClass === "success") {
        raid.nm.dispClass = "success";
      }
    }


    function getPct(progress) {
      return Math.floor(progress.done / progress.total * 95 + 5) + "%";
    }

    ReadService.listChars(PLAYER_PLATFORM, MEMBER_ID, PLAYER_NAME, function (err, jo) {
      if (err) {
        console.dir(err);
        $scope.vm.errorMsg = err;
        return;
      }

      addFav(PLAYER_PLATFORM, PLAYER_NAME);
      jo.player.minsPlayed = Math.floor(jo.player.minsPlayed / 60);
      jo.player.lastPlayed = moment(jo.player.lastPlayed).fromNow();

      var progress = {
        done: 0,
        total: jo.chars.length * 2
      }
      $scope.vm.loading.active = true;
      $scope.vm.loading.pct = getPct(progress);
      $scope.vm.player = jo.player;
      $scope.vm.chars = jo.chars;
      $scope.vm.player.grimoire = jo.grimoire;
      console.dir(jo);

      $scope.vm.repProgressPct = function (progress) {
        var nla = progress.nextLevelAt;
        if (!nla) nla = 1;
        return Math.floor(100 * (progress.progressToNextLevel / nla));
      }
      //https://my.destinytrialsreport.com/xbox/E5%20Dweezil
      //https://my.destinytrialsreport.com/ps/Gothalion
      $scope.vm.buildTrialsUrl = function () {

        var console = PLAYER_PLATFORM == 1 ? "xbox" : "ps";
        var url = "https://my.destinytrialsreport.com/" + console + "/" + encodeURIComponent(PLAYER_NAME);
        return url;
      }

      $scope.vm.buildRepURL = function (characterid, progressionHash) {
        if (progressionHash === 3186678724) {

          return "/cf/1/" + encodeURIComponent(PLAYER_NAME);
        }
        var console = PLAYER_PLATFORM == 1 ? "xb" : "ps";
        var url = "http://destiny.neonblack.moe/reputation/#" + console + "=" + encodeURIComponent(PLAYER_NAME) + "&" + characterid + "=" + progressionHash;
        return url;
      }
      
      $scope.vm.getTier = function(value){
        if (!value) return "";
        if (value<60) return 0;
        if (value<120) return 1;
        if (value<180) return 2;
        if (value<240) return 3;
        if (value<300) return 4;
        return 5;
      };

      $scope.vm.chars.forEach(function (char) {
        ReadService.getCharInfoPt1($scope, PLAYER_PLATFORM, MEMBER_ID, char.id, function (err, jo) {
          if (err) {

            toaster.pop({
              type: 'error',
              title: char.class,
              body: "Error getting advisors/progress: " + JSON.stringify(err),
              timeout: 5000
            });
          }
          else {
            char.progression = jo.progression;
            char.publicAdv = jo.publicAdv;
            processRaid(jo.publicAdv.wrath);
            processRaid(jo.publicAdv.kf);
            processRaid(jo.publicAdv.vog);
            processRaid(jo.publicAdv.ce);
          }

          progress.done++;
          $scope.vm.loading.pct = getPct(progress);
          if (progress.done >= progress.total) {
            $scope.vm.loading.active = false;
          }

          ReadService.getCharInfoPt2(PLAYER_PLATFORM, MEMBER_ID, char.id, function (err, jo) {
            if (err) {
              var msg = err;
              if (err.data) {
                msg = err.data;
              }
              else if (err.statusText) {
                msg = err.statusText;
              }

              toaster.pop({
                type: 'error',
                title: char.class,
                body: "Error getting weekly/lifetime/item: " + msg,
                timeout: 5000
              });
            }
            else {
              char.weekly = jo.weekly;
              char.weeklyPvP = jo.weeklyPvP;
              char.items = jo.items;
              char.lifetime = jo.lifetime;
            }
            progress.done++;
            $scope.vm.loading.pct = getPct(progress);
            if (progress.done >= progress.total) {
              $scope.vm.loading.active = false;
            }
          });

        });
      });

    });

  }]);

tabsApp.service("ReadService", ["$http", "$q", "toaster", function ($http, $q, toaster) {
  return ({
    listChars: listChars,
    getCharInfoPt1: getCharInfoPt1,
    getCharInfoPt2: getCharInfoPt2

  });
  function getCharInfoPt1($scope, platform, memberId, charId, callback) {
    var request = $http({
      method: "get",
      url: "/api/getCharInfoPt1/" + platform + "/" + memberId + "/" + charId,
      params: {nocache: NO_CACHE}
    });
    (request.then(function (response) {
      if (response.data && response.data.progression && response.data.publicAdv) {
        callback(null, response.data);
      }
      else {
        if (response.data.privacy==true){
          $scope.vm.errorMsg = " This user has selected to hide parts of their data on Bungie.net which prevents this page from being properly displayed." +
            " If you are the user in question, you can fix this by: Going to Bungie.net, clicking the settings gear icon, " +
            "clicking 'Privacy' and checking the 'Show My Advisors' box.";
          callback("Advisors are private");
          //callback("Unexpected response");
        }
        else
          callback("Unexpected response");
      }
    }, function (err) {
      callback(err);
    }));
  }

  function getCharInfoPt2(platform, memberId, charId, callback) {
    var request = $http({
      method: "get",
      url: "/api/getCharInfoPt2/" + platform + "/" + memberId + "/" + charId,
      params: {nocache: NO_CACHE}
    });
    (request.then(function (response) {
      if (response.data && response.data.weekly && response.data.weeklyPvP && response.data.items && response.data.lifetime) {
        callback(null, response.data);
      }
      else {
        callback("Unexpected response");
      }
    }, function (err) {
      callback(err);
    }));
  }


  function listChars(platform, memberId, name, callback) {
    var request = $http({
      method: "get",
      url: "/api/listChars/" + platform + "/" + memberId + "/" + encodeURI(name),
      params: {nocache: NO_CACHE}
    });
    (request.then(function (response) {
      if (response.data && response.data.player && response.data.chars) {
        callback(null, response.data);
      }
      else if (response.data.error) {
        callback(response.data.error);
      }
      else {
        callback("Unexpected response");
      }
    }, function (err) {
      callback(err);
    }));
  }

}]);