var gunsmithApp = angular.module('gunsmithApp', ['ngSanitize'])
  .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {
    isCache();
    $scope.data = {
      loading: true
    };
    $scope.getTestClass = function(tests){
      if (tests==0) return "danger";
      if (tests>=5) return "success";
      return "warning";
    };
    
    $scope.buildNeonLink = function(char){
      var console = PLAYER_PLATFORM == 1 ? "xb" : "ps";
      var url = "http://destiny.neonblack.moe/reputation/#" + console + "=" + encodeURIComponent(PLAYER_NAME) + 
        "&" + char.id + "=2335631936";
      return url;

    };

    $scope.getProgressClass = function(progress){
      var returnMe = {
        'progress-bar': true
      };
      if (progress<25){
        returnMe["progress-bar-danger"] = true;
      }
      else if (progress<75){
        returnMe["progress-bar-warning"] = true;
      }
      else
        returnMe["progress-bar-success"] = true;
      
      return returnMe;
    };
    function getRankProgressText(progress){
      return (progress).toFixed(0)+"%";
    };
    $scope.getRankProgressText = getRankProgressText;

    $scope.getRankProgressStyle = function(progress){
      return {
        'min-width': '2em',
        'width': getRankProgressText(progress)
      };
    };
    
    $scope.launchCopyModal = function(){
      if (!FOUNDRY_ROLLS) return;
      var text = "___\n\n";
      FOUNDRY_ROLLS.forEach(function(gun){
        text+="**"+gun.name+" - "+gun.itemTypeName+"**\n\n";
        gun.damageAndSteps.forEach(function(roll, index, arr){
          text+=(index+1)+". ";
          roll.steps.forEach(function(step, index, arr){
            step.forEach(function(node, index,arr){
              text+=node;
              if (index<(arr.length-1)){
                text+=" / "
              }
            });
            if (index<(arr.length-1)){
              text+=" - "
            }
          });
          text+="\n";
        });
        text+="\n";
        text+="___\n";
      });
      $scope.data.redditRolls = text;
      $('#copyModal').modal();
    }
    if (PLAYER_PLATFORM>0)
      ReadService.load(PLAYER_PLATFORM, MEMBER_ID, PLAYER_NAME, $scope);
    
    console.log("loaded");
  }]);

gunsmithApp.service("ReadService", ["$http", "$q", function ($http, $q) {
  return ({
    load: load
  });

  function load(platform, memberId, name, $scope) {
    $http({
      method: "get",
      url: "/api/gunsmith/" + platform + "/" + memberId + "/" + encodeURIComponent(name),
      params: {nocache: NO_CACHE}
    }).then(function (response) {
      $scope.data.loading = false;
      if (!response.data) {
        $scope.data.errormsg = "Problem loading data from Bungie";
        return;
      }
      if (response.data.error) {
        $scope.data.errormsg = response.data.error;
        return;
      }
      $scope.data.chars = response.data.chars;
      if ($scope.data.chars.length==0) return;
      if ($scope.data.chars[0].privacy == true) {
        $scope.data.errormsg = " This user has selected to hide parts of their data on Bungie.net which prevents this page from being properly displayed." +
          " If you are the user in question, you can fix this by: Going to Bungie.net, clicking the settings gear icon, " +
          "clicking 'Privacy' and checking the 'Show My Advisors' box.";
      }
      response.data.chars.forEach(function(char){
        var faction = char.progression.gunsmithProg.progress;
        char.progress = 100*(faction.progressToNextLevel / faction.nextLevelAt);
      });

    }, handleError);
  }

  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return ($q.reject("An unknown error occurred."));
    }
    return ($q.reject(response.data));
  }

}]);