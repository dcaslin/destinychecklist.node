var vendorApp = angular.module('vendorApp', ['toaster', 'ngSanitize', 'ngAnimate'])
  .controller('mainController', ['$rootScope', '$scope', 'toaster', 'ReadService', function ($rootScope, $scope, toaster, ReadService) {


    var armorSlotFilters = [
      //{label: "All Armor", filter: null},
      {label: "Helmet", filter: "Helmet", selected: true},
      {label: "Gauntlet", filter: "Gauntlets", selected: true},
      {label: "Chest Armor", filter: "Chest Armor", selected: true},
      {label: "Leg Armor", filter: "Leg Armor", selected: true},
      {label: "Class Armor", filter: "Class Armor", selected: true},
      {label: "Artifact", filter: "Artifacts", selected: true},
      {label: "Ghost", filter: "Ghost", selected: true}
    ];

    var weaponSlotFilters = [
      {label: "All Weapons", filter: null, slot: true},
      {label: "Primary", filter: "Primary Weapons", slot: true},
      {label: "Auto Rifle", filter: "Auto Rifle"},
      {label: "Hand Cannon", filter: "Hand Cannon"},
      {label: "Pulse Rifle", filter: "Pulse Rifle"},
      {label: "Scout Rifle", filter: "Scout Rifle"},
      {label: "Special", filter: "Special Weapons", slot: true},
      {label: "Shotgun", filter: "Shotgun"},
      {label: "Fusion Rifle", filter: "Fusion Rifle"},
      {label: "Sidearm", filter: "Sidearm"},
      {label: "Sniper Rifle", filter: "Sniper Rifle"},
      {label: "Heavy", filter: "Heavy Weapons", slot: true},
      {label: "Sword", filter: "Sword"},
      {label: "Machine Gun", filter: "Machine Gun"},
      {label: "Rocket Launcher", filter: "Rocket Launcher"}
    ];

    var armorEquippableFilters = [
      {label: "Usable By Any", filter: null},
      {label: "Titan", filter: "Titan"},
      {label: "Hunter", filter: "Hunter"},
      {label: "Warlock", filter: "Warlock"},
      {label: "All Classes", filter: "Any"}
    ];

    var rarityFilters = [
      {label: "Any Rarity", filter: null},
      {label: "Exotic", filter: "Exotic"},
      {label: "Legendary", filter: "Legendary"}
    ];


    var splitFilters = [
      {label: "Any Split", filter: null},
      {label: "Int/Dis", filter: "ID"},
      {label: "Int/Str", filter: "IS"},
      {label: "Dis/Str", filter: "DS"}
    ];

    $scope.vm = {
      loading: false,
      weapons: [],
      weaponFilters: {
        slots: weaponSlotFilters,
        selectedSlot: weaponSlotFilters[0],
        selectedRarity: rarityFilters[0],
        displayText: null,
        filterText: null
      },
      weaponSort: 'item.name',
      weaponAsc: true,
      armor: [],
      rarityFilters: rarityFilters,
      armorFilters: {
        selectedSplit: splitFilters[0],
        splits: splitFilters,
        slots: armorSlotFilters,
        selectedRarity: rarityFilters[0],
        equippable: armorEquippableFilters,
        selectedEquippable: armorEquippableFilters[0],
        displayText: null,
        filterText: null
      },
      armorSort: 'item.qualityPct',
      armorAsc: false
    };
    
    $scope.vm.selectAllArmorSlots = function(val){
      $scope.vm.armorFilters.slots.forEach(function(slot){
        slot.selected = val;
      });
    };

    $scope.vm.setArmorTextFilter = function () {
      if (!$scope.vm.armorFilters.displayText) $scope.vm.armorFilters.filterText = null;
      else $scope.vm.armorFilters.filterText = $scope.vm.armorFilters.displayText.toUpperCase();
    };

    $scope.vm.setWeaponTextFilter = function () {
      if (!$scope.vm.weaponFilters.displayText) $scope.vm.weaponFilters.filterText = null;
      else $scope.vm.weaponFilters.filterText = $scope.vm.weaponFilters.displayText.toUpperCase();
    };

    $scope.vm.stepTip = function ($event) {
      $($event.target).popover();
      $($event.target).popover("show");
    };

    $scope.vm.sortWeapon = function (attr) {
      if ($scope.vm.weaponSort == attr) {
        $scope.vm.weaponAsc = !$scope.vm.weaponAsc;
      }
      else {
        $scope.vm.weaponSort = attr;
        $scope.vm.weaponAsc = true;
      }
    };

    function filterWeaponItem(r) {
      var vm = $scope.vm;
      
      if (vm.weaponFilters.selectedSlot.filter != null) {
        if (vm.weaponFilters.selectedSlot.slot==true){
          if (vm.weaponFilters.selectedSlot.filter != r.item.slot)
            return true;  
        }
        else{
          if (vm.weaponFilters.selectedSlot.filter != r.item.type)
            return true;
        }
      }
      if (vm.weaponFilters.selectedRarity.filter) {
        if (vm.weaponFilters.selectedRarity.filter != r.item.tierTypeName) return true;
      }
      if ($scope.vm.weaponFilters.filterText) {
        if (r.item.searchText.indexOf($scope.vm.weaponFilters.filterText) == -1) return true;
      }
      return false;
    }

    $scope.vm.filterWeapons = function () {
      if (!$scope.vm.weapons) return;
      for (var cntr = 0; cntr < $scope.vm.weapons.length; cntr++) {
        var itm = $scope.vm.weapons[cntr]
        itm.hidden = filterWeaponItem(itm);
      }
    };

    $scope.vm.sortArmor = function (attr) {
      if ($scope.vm.armorSort == attr) {
        $scope.vm.armorAsc = !$scope.vm.armorAsc;
      }
      else {
        $scope.vm.armorSort = attr;
        $scope.vm.armorAsc = false;
      }
    };

    function filterArmorItem(r) {
      var vm = $scope.vm;

      for (var cntr=0; cntr<vm.armorFilters.slots.length; cntr++){
        var slot = vm.armorFilters.slots[cntr];
        if (slot.selected) continue;
        if (slot.filter!= r.item.slot) continue;
        return true;
      }
      if (vm.armorFilters.selectedSplit.filter!=null){
        if (!r.item.quality) return true;
        if (r.item.quality){
          if (r.item.quality.statSplit != vm.armorFilters.selectedSplit.filter){
            return true;
          }
        }
      }
      if (vm.armorFilters.selectedEquippable.filter) {
        if (vm.armorFilters.selectedEquippable.filter != r.item.classAllowed)
          return true;
      }
      if (vm.armorFilters.selectedRarity.filter) {
        if (vm.armorFilters.selectedRarity.filter != r.item.tierTypeName) return true;
      }
      if ($scope.vm.armorFilters.filterText) {
        if (r.item.searchText.indexOf($scope.vm.armorFilters.filterText) == -1) return true;
      }
      return false;
    }

    $scope.vm.filterArmor = function () {
      if (!$scope.vm.armor) return;
      for (var cntr = 0; cntr < $scope.vm.armor.length; cntr++) {
        var itm = $scope.vm.armor[cntr]
        itm.hidden = filterArmorItem(itm);
      }
    };

    ReadService.load($scope);

  }]);

vendorApp.service("ReadService", ["$http", "$q", "toaster", function ($http, $q, toaster) {
  return ({
    load: load
  });

  function load($scope) {
    $scope.vm.loading = true;
    $http({
      method: "get",
      url: "/api/vendor"
    }).then(function (response) {
      $scope.vm.loading = false;
      if (!response.data) {
        $scope.data.errormsg = "Problem loading data";
        return;
      }
      else {
        $scope.vm.weapons = response.data.weapons;
        $scope.vm.armor = response.data.armor;
        $scope.vm.emblems = response.data.emblems;
        $scope.vm.shaders = response.data.shaders;
        console.dir($scope.vm.shaders);
        $scope.vm.ships = response.data.ships;
      }
    }, function (response) {
      var errMsg = "";
      if (!angular.isObject(response.data) || !response.data.message) {
        errMsg = "Problem loading data"
      }
      else {
        errMsg = response.data.message;
      }
      $scope.vm.loading = false;
      $scope.vm.errormsg = errMsg;
    });
  }

}]);