

var srlApp = angular.module('srlApp', ['ui.bootstrap-slider'])

    .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {


//   RACE VAL 45, 65, 80, 100, 45 - 100
//   RACE PER DAY 0 - 100
//   BOUNTIES PER DAY 1-3
// QUEST, C, B, A, S  --- S = 600
        $scope.data = {
            assumptions: {
                raceVal: 65,
                races: 5,
                bounties: 2,
                classItem: false
            }
        };
        var localSettings = localStorage.getItem("srlSaved");
        if (localSettings) {
            $scope.data.assumptions = JSON.parse(localSettings);
            if (!$scope.data.assumptions.raceVal) $scope.data.assumptions.raceVal = 45;
            if (!$scope.data.assumptions.races) $scope.data.assumptions.races = 0;
            if (!$scope.data.assumptions.bounties) $scope.data.assumptions.bounties = 0;
            if (!$scope.data.assumptions.classItem) $scope.data.assumptions.classItem = false;
        }

        var CLASS_MOD = 1.15;
        var BTY_VAL = 100;
        var QUEST_VAL = 300;

        $scope.calcTable = function () {
            console.log("Calc table");
            $scope.data.calculated = {
            };

            var classItemMod = $scope.data.assumptions.classItem?1.15:1;
            var btyVal = BTY_VAL * classItemMod;
            var raceVal = $scope.data.assumptions.raceVal * classItemMod;
            var btyTotal = $scope.data.assumptions.bounties * btyVal;
            var raceTotal = $scope.data.assumptions.races * raceVal;
            
            var current = moment(6, "HH");
            var end = moment("2015-12-29 7", "YYYY-MM-DD H");
            $scope.data.calculated = {
                current:getRankInfo($scope.data.selectedChar.prog.currentProgress),
                constants: {
                    btyVal: btyVal,
                    raceVal: raceVal,
                    btyTotal: btyTotal,
                    raceTotal: raceTotal
                },
                tableRows: [],
                racesAbove2:  0,
                racesAbove3:  0,
                racesAbove4 :  0,
                dayReach2 :  null,
                dayReach3 :  null,
                dayReach4: null
            };
            var xp = $scope.data.selectedChar.prog.currentProgress;
            while (current.valueOf()<end.valueOf()){
                var rankInfo = getRankInfo(xp);

                console.log(current+": "+xp+" "+rankInfo.rank);
                if (rankInfo.rank>=2){
                    if (!$scope.data.calculated.dayReach2){
                        $scope.data.calculated.dayReach2 = moment(current);
                        $scope.data.calculated.durDay2 = moment.duration(moment().diff(current));
                    }
                    if (rankInfo.rank<3)
                        $scope.data.calculated.racesAbove2+=$scope.data.assumptions.races;
                }
                if (rankInfo.rank>=3){
                    if (!$scope.data.calculated.dayReach3){
                        $scope.data.calculated.dayReach3 = moment(current);
                        $scope.data.calculated.durDay3 = moment.duration(moment().diff(current));
                    }
                    $scope.data.calculated.racesAbove3+=$scope.data.assumptions.races;
                }
                if (rankInfo.rank>=4){
                    if (!$scope.data.calculated.dayReach4){
                        $scope.data.calculated.dayReach4 = moment(current);
                        $scope.data.calculated.durDay4 = moment.duration(moment().diff(current));
                    }
                }
                rankInfo.day = moment(current);
                $scope.data.calculated.tableRows.push(rankInfo);
                xp+=btyTotal+raceTotal;
                current = current.add(1, 'day');
            }
            localStorage.setItem("srlSaved", JSON.stringify($scope.data.assumptions));
        };

        function getRankInfo(xp) {
            var rank, lastRankXp, nextRankXp;
            if (xp > 17499) {
                rank = 4;
                lastRankXp = 17500;
                nextRankXp = 0;
            }
            else if (xp > 8999) {
                rank = 3;
                lastRankXp = 9000;
                nextRankXp = 17500;
            }
            else if (xp > 4499) {
                rank = 2;
                lastRankXp = 4500;
                nextRankXp = 9000;
            }
            else if (xp > 1749) {
                rank = 1;
                lastRankXp = 1750;
                nextRankXp = 4500;
            }
            else {
                rank = 0;
                lastRankXp = 0;
                nextRankXp = 1750;
            }
            var nextRank;
            if (nextRankXp > 0) {
                pctToNextRank = parseInt((100 * (xp - lastRankXp) / (nextRankXp - lastRankXp)).toFixed(0));
                nextRank = rank + 1;
            }
            else {
                //handle "you're done" case
                pctToNextRank = 100;
                lastRankXp = 9000;
                nextRankXp = 17500;
                nextRank = 4;
            }
            var pctToTotal = parseInt((100 * (xp / 17500)).toFixed(0));
            return {
                xp: xp,
                lastRankXp: lastRankXp,
                nextRankXp: nextRankXp,
                rank: rank,
                rankProgress: xp - lastRankXp,
                rankTotal: nextRankXp - lastRankXp,
                nextRank: nextRank,
                pctToNextRank: pctToNextRank,
                pctToTotal: pctToTotal,
                pctToTotalStyle: {
                    width: pctToTotal + '%'
                },
                pctToNextRankStyle: {
                    width: pctToNextRank + '%'
                }
            };

        };



        ReadService.load(PLAYER_PLATFORM, MEMBER_ID, $scope);
        console.log("loaded");
    }]);

srlApp.service("ReadService", ["$http", "$q", function ($http, $q) {
    return ({
        load: load
    });

    function load(platform, memberId, $scope) {
        var request = $http({
            method: "get",
            url: "/api/srlChars/" + platform + "/" + memberId,
            params: {nocache: NO_CACHE}
        });
        (request.then(function (response) {
            if (!response.data.chars) {
                $scope.data.errormsg = "Problem loading data from Bungie";
                return;
            }
            $scope.data.chars = response.data.chars;
            $scope.data.prs = response.data.prs;
            if ($scope.data.chars.length >= 1){
                $scope.data.selectedChar = $scope.data.chars[0];
                $scope.calcTable();
                
            } 
        }, handleError));
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
        }
        return ($q.reject(response.data));
    }

}]);