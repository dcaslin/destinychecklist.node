var cfApp = angular.module('cfApp', ['ngSanitize'])

  .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {

    $scope.data = {
      loading: true,
      sortType: 'info.number'
    };

    $scope.buildIgnUrl = function (card) {
      return "http://www.ign.com/wikis/destiny/Calcified_Fragments#" + card.numeral;
    };

    $scope.filterItem = function (item) {
      if ($scope.data.hideComplete) return !item.complete;
      return true;
    };
    ReadService.load(PLAYER_PLATFORM, MEMBER_ID, $scope);
    console.log("loaded");
  }]);

cfApp.service("ReadService", ["$http", "$q", function ($http, $q) {
  return ({
    load: load
  });

  function load(platform, memberId, $scope) {
    var request = $http({
      method: "get",
      url: "/api/cf/" + platform + "/" + memberId,
      params: {nocache: NO_CACHE}
    });
    (request.then(function (response) {
      if (!response.data.chars) {
        $scope.data.errormsg = "Problem loading data from Bungie";
        $scope.data.loading = false;
        return;
      }
      $scope.data.chars = response.data.chars;
      if ($scope.data.chars.length >= 1) $scope.data.selectedChar = $scope.data.chars[0];
      if ($scope.data.selectedChar.privacy == true) {
        $scope.data.errormsg = " This user has selected to hide parts of their data on Bungie.net which prevents this page from being properly displayed." +
          " If you are the user in question, you can fix this by: Going to Bungie.net, clicking the settings gear icon, " +
          "clicking 'Privacy' and checking the 'Show My Advisors' box.";
      }
      $scope.data.loading = false;

    }, handleError));
  }

  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return ($q.reject("An unknown error occurred."));
    }
    return ($q.reject(response.data));
  }

}]);