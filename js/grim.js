

var grimApp = angular.module('grimApp', ['ngSanitize'])

    .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {

        $scope.data = {
            loading: true,
            sortType: "pctToNextRank",
            sortReverse: true,
            showComplete: false
        };
        
        $scope.getStyle = function(card){
            if (card.stats.length>0) 
                return {
                "cursor": "zoom-in"
                };
            else
                return {};
        }
        $scope.toggleRow = function(card){
            if (card.stats.length==0) return;
            
            var showing = $('#'+card.id).is(":visible");
            var parRowId = "row" + card.id;

            if (!showing){
                $("#" + parRowId).show();
                $('#'+card.id).collapse('show');
            }
            else{
                $("#" + parRowId).hide();
                $('#'+card.id).collapse('hide');
            }
        }

        $scope.filterItem = function (item) {
            if (!$scope.data.showComplete) return !item.complete;
            return true;
        };


        ReadService.load(PLAYER_PLATFORM, MEMBER_ID, $scope);
        console.log("loaded");
    }]);

grimApp.service("ReadService", ["$http", "$q", function ($http, $q) {
    return ({
        load: load
    });

    function load(platform, memberId, $scope) {
        var request = $http({
            method: "get",
            url: "/api/grim/" + platform + "/" + memberId,
            params: {nocache: NO_CACHE}
        });
        (request.then(function (response) {
            if (!response.data.cards) {
                $scope.data.errormsg = "Problem loading data from Bungie";
                $scope.data.loading = false;
                return;
            }
            $scope.data.cards = response.data.cards;
            $scope.data.totals = response.data.totals;
            $scope.data.loading = false;
            initMe();
        }, handleError));
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
        }
        return ($q.reject(response.data));
    }

}]);