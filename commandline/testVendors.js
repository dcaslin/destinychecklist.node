/**
 * Created by Dave on 11/29/2016.
 */
var UTILS = require("../platform/utils");
var conf = require('../config.js')();

var ITEM_PARSE = require('./itemParseService.js')();
var fs = require("fs");
var CACHE = require(conf.dataDir + "/destiny.json");
var SECURE_REQUEST = require("../platform/secureRequest")();
var async = require("async");

//var BUCKETS_WE_CARE = [1585787867,1498876634,953998645,434908299,20886954,14239492,4274335291,4046403665,4023194814,3551918588,3448274439,2465295065];
//
//var TEST_ME = {"itemHash":2672107537,"bindStatus":0,"isEquipped":false,"itemInstanceId":"0","itemLevel":55,"stackSize":1,"qualityLevel":0,"stats":[{"statHash":144602215,"value":0,"maximumValue":0},{"statHash":1735777505,"value":65,"maximumValue":0},{"statHash":4244567218,"value":0,"maximumValue":0}],"primaryStat":{"statHash":3897883278,"value":350,"maximumValue":0},"canEquip":true,"equipRequiredLevel":40,"unlockFlagHashRequiredToEquip":2166136261,"cannotEquipReason":0,"damageType":0,"damageTypeHash":0,"damageTypeNodeIndex":-1,"damageTypeStepIndex":-1,"progression":{"dailyProgress":0,"weeklyProgress":0,"currentProgress":0,"level":1,"step":0,"progressToNextLevel":0,"nextLevelAt":7438,"progressionHash":2043912351},"talentGridHash":2564144596,"nodes":[{"isActivated":true,"stepIndex":0,"state":10,"hidden":false,"nodeHash":0},{"isActivated":true,"stepIndex":4,"state":10,"hidden":false,"nodeHash":1},{"isActivated":false,"stepIndex":4,"state":11,"hidden":false,"nodeHash":2},{"isActivated":false,"stepIndex":4,"state":11,"hidden":false,"nodeHash":3},{"isActivated":false,"stepIndex":1,"state":3,"hidden":false,"nodeHash":4},{"isActivated":false,"stepIndex":0,"state":3,"hidden":false,"nodeHash":5},{"isActivated":true,"stepIndex":0,"state":10,"hidden":false,"nodeHash":6},{"isActivated":true,"stepIndex":0,"state":10,"hidden":false,"nodeHash":7}],"useCustomDyes":true,"artRegions":{},"isEquipment":true,"isGridComplete":false,"perks":[{"iconPath":"/common/destiny_content/icons/cb074e9ea0037ee18600bbd66562b5a2.png","perkHash":944754331,"isActive":true},{"iconPath":"/common/destiny_content/icons/9879f903f6ad76c3dc9da6dfd0d8916b.png","perkHash":2871746970,"isActive":false},{"iconPath":"/common/destiny_content/icons/d2b90529f125a47837fc405990011cf6.png","perkHash":1299594097,"isActive":false}],"location":3,"transferStatus":2,"locked":false,"lockable":false,"objectives":[],"state":0};
//var parsed = ITEM_PARSE.parseItem(TEST_ME);
//console.log(JSON.stringify(parsed, null, '\t'));
//return;

function dumpItems(charId, vendorId, itemPile, callback) {
  var FOUND_ITEMS = [];
  var venDesc = CACHE.Vendor[vendorId];
  SECURE_REQUEST.sendRequest("https://www.bungie.net/Platform/Destiny/1/MyAccount/Character/" + charId + "/Vendor/" + vendorId, function (err, jo) {
    if (err) {
      callback(err);
      return;
    }
    var data = jo.Response.data;
    data.saleItemCategories.forEach(function (cat) {
      cat.saleItems.forEach(function (saleItem) {
        //saleItem.costs;
        var itm = saleItem.item;
        var itmHash = itm.itemHash;
        if (FOUND_ITEMS.indexOf(itmHash) > -1) return;
        var jDesc = CACHE.InventoryItem[itmHash];
        if (!jDesc) return;
        //only look at weapons and armor
        if (jDesc.itemType != 3 && jDesc.itemType != 2) return;

        FOUND_ITEMS.push(itmHash);
        var item = ITEM_PARSE.parseItem(itm);
        var costs = [];
        saleItem.costs.forEach(function(cost){
          costs.push({
            name: CACHE.InventoryItem[cost.itemHash].itemName,
            value: cost.value
          });
        });
        itemPile[itmHash] = {
          vendor: venDesc.summary.vendorName,
          item: item,
          costs: costs
        }
        var pct = item.quality!=null?item.quality.qualityPct:'-';
        console.log(venDesc.summary.vendorName+": "+jDesc.itemName + " "+ item.type+ " "+ item.slot+ " " +pct+"%");
        console.dir(costs);
      });
    });
    callback(null);
    return;
  });
}
// Hunter: 2305843009345941076
// Warlock: 2305843009219352385
// Titan: 2305843009219816265
//
// DO: 3611686524
// FWC: 1821699360
// NM: 1808244981
//
// Variks: 1998812735
// Speaker: 2680694281
// Shiro: 2190824860
// Eris: 174528503
//
// Tyra: 2190824863


// "bucketHash":1585787867,"bucketName":"Class Armor",
// "bucketHash":1498876634,"bucketName":"Primary Weapons"
// "bucketHash":953998645,"bucketName":"Heavy Weapons"
// "bucketHash":434908299,"bucketName":"Artifacts",
// "bucketHash":20886954,"bucketName":"Leg Armor"
// "bucketHash":14239492,"bucketName":"Chest Armor",
// "bucketHash":4274335291,"bucketName":"Emblems",
// "bucketHash":4046403665,"bucketName":"Weapons"
// "bucketHash":4023194814,"bucketName":"Ghost"
// "bucketHash":3551918588,"bucketName":"Gauntlets"
// "bucketHash":3448274439,"bucketName":"Helmet",
// "bucketHash":2465295065,"bucketName":"Special Weapons"


var CHAR_IDS = ['2305843009345941076', '2305843009219352385', '2305843009219816265'];
var CHAR_VENDORS = ['3611686524', '1821699360', '1808244981', '1998812735', '2680694281', '2190824860', '174528503', '2190824863'];


SECURE_REQUEST.init(function (err) {
  if (err) {
    console.dir(err);
    return;
  }

  var callMe = [];
  var item_pile = {};
  CHAR_IDS.forEach(function (charId) {
    CHAR_VENDORS.forEach(function (vendorId) {
      // dumpItems(charId,vendorId, charId+"_"+vendorId+".json");
      callMe.push(dumpItems.bind(null, charId, vendorId, item_pile));
    });
  });
  async.parallel(callMe, function (err, results) {
    if (err) console.dir(err);
    else console.log(results.length);
    //fs.writeFile(conf.dataDir + '/itemPile.json', JSON.stringify(item_pile, null, '\t'), function (err2) {
    //  if (err2) throw err2;
    //});
  });
  // dumpItems("2305843009345941076","3611686524", "HUNTER_DO.json");
  // dumpItems("2305843009219352385","3611686524", "WARLOCK_DO.json");
  // dumpItems("2305843009219816265","3611686524", "TITAN_DO.json");
  // SECURE_REQUEST.sendRequest("https://www.bungie.net/Platform/Destiny/1/MyAccount/Character/2305843009219816265/Vendor/134701236", function (err2, jo2) {
  //   if (err2) {
  //     console.dir(err2);
  //     return;
  //   }
  //   console.dir(jo2);
  // });
});

