/**
 * Created by Dave on 9/9/2015.
 */
"use strict";

var fs = require("fs");
var conf = require('../config.js')();
var async = require("async");

function saveRowsItem(db, obj, tableName, label, idName, callback, useSummary) {
  console.log(tableName)
  obj[label] = {};
  db.all("SELECT id, json FROM " + tableName, function (err, rows) {
    if (err) {
      console.log(err);
      callback(err);
    }
    else {
      for (var cntr = 0; cntr < rows.length; cntr++) {
        var jo = JSON.parse(rows[cntr].json);
        var id;
        if (useSummary) {
          id = jo.summary[idName]
        }
        else {
          id = jo[idName];
        }
        if (id == null) throw "No id found for " + rows[cntr].json;
        var val = JSON.parse(rows[cntr].json);
        obj[label][id] = val;
        //if it is an IB bounty
        if (val.values && val.values[3208461659]) {
          obj.ironBounties.push(val.itemHash);
        }
      }
      callback();
    }
  });
}

function saveRows2(db, obj, tableName, label, idName, callback, useSummary) {
  console.log(tableName)
  obj[label] = {};
  db.all("SELECT id, json FROM " + tableName, function (err, rows) {
    if (err) {
      console.log(err);
      callback(err);
    }
    else {
      for (var cntr = 0; cntr < rows.length; cntr++) {
        var jo = JSON.parse(rows[cntr].json);
        var id;
        if (useSummary) {
          id = jo.summary[idName]
        }
        else {
          id = jo[idName];
        }
        if (id == null) throw "No id found for " + rows[cntr].json;
        obj[label][id] = JSON.parse(rows[cntr].json);
      }
      callback();
    }
  });
}

function saveRowsGrimCards(db, obj, tableName, idName, callback, useSummary) {
  obj.GrimoireCard = [];
  obj.GrimoireCardById = {};
  var links = {};
  db.all("SELECT id, json FROM " + tableName, function (err, rows) {
    if (err) {
      console.log(err);
      callback(err);
    }
    else {
      for (var cntr = 0; cntr < rows.length; cntr++) {
        var jo = JSON.parse(rows[cntr].json);
        var id;
        if (useSummary) {
          id = jo.summary[idName]
        }
        else {
          id = jo[idName];
        }
        if (id == null) throw "No id found for " + rows[cntr].json;
        //console.log(tableName + ": " + id);
        var j = JSON.parse(rows[cntr].json);
        delete j.normalResolution;
        delete j.highResolution;
        j.totalPoints = j.points;
        if (j.statisticCollection) {
          for (var cntr2 = 0; cntr2 < j.statisticCollection.length; cntr2++) {
            var s = j.statisticCollection[cntr2];
            if (!s.rankCollection) continue;
            j.hasRanks = true;
            for (var cntr3 = 0; cntr3 < s.rankCollection.length; cntr3++) {
              var r = s.rankCollection[cntr3];
              j.totalPoints += r.points;
            }
          }
        }
        obj.GrimoireCardById[id] = j;
        obj.GrimoireCard.push(j);
      }
      //console.dir(obj.GrimoireCard);
      callback();
    }
  });
}

function saveRow(db, obj, tableName, label, callback) {
  obj[label] = {};
  db.all("SELECT id, json FROM " + tableName, function (err, rows) {
    if (err) {
      console.log(err);
      callback(err);
    }
    else { 
      if (rows.length != 1) throw "Expected 1 row";
      var jo = JSON.parse(rows[0].json);
      console.log(tableName + ": 1");
      obj[label] = JSON.parse(rows[0].json);
      callback();
    }
  });
}
//
//function saveRows(db, client, tableName, label, idName, useSummary) {
//    db.all("SELECT id, json FROM " + tableName, function (err, rows) {
//        if (err) {
//            console.log(err);
//        }
//        else {
//            for (var cntr = 0; cntr < rows.length; cntr++) {
//                var jo = JSON.parse(rows[cntr].json);
//                var id;
//                if (useSummary) {
//                    id = jo.summary[idName]
//                }
//                else {
//                    id = jo[idName];
//                }
//                if (id == null) throw "No id found for " + rows[cntr].json;
//                console.log(tableName + ": " + id);
//                client.set(label + ":" + id, rows[cntr].json);
//
//            }
//        }
//    });
//
//
//}


function saveData() {
  var sqlite3 = require('sqlite3').verbose();

  //var client = redis.createClient();
  //client.on("error", function (err) {
  //    console.log("Redis error " + err);
  //});

  console.log(conf.dataDir + "/destiny.db");
  var db = new sqlite3.Database(conf.dataDir + "/destiny.en.db", sqlite3.OPEN_READONLY, function (err) {

  });
  db.serialize(function () {

    var cache = {
      ironBounties: []
    };

    async.waterfall([

        function (callback) {
          saveRows2(db, cache, "DestinyVendorDefinition", "Vendor", "vendorHash", callback, true);
        },
        function (callback) {
          saveRows2(db, cache, "DestinyEnemyRaceDefinition", "EnemyRace", "raceHash", callback);
        },
        function (callback) {
          saveRows2(db, cache, "DestinyActivityBundleDefinition", "ActivityBundle", "bundleHash", callback);
        },
        function (callback) {
          saveRows2(db, cache, "DestinyActivityTypeDefinition", "ActivityType", "activityTypeHash", callback);
        },
        function (callback) {
          saveRowsItem(db, cache, "DestinyInventoryItemDefinition", "InventoryItem", "itemHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinySandboxPerkDefinition", "SandboxPerk", "perkHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyActivityDefinition", "Activity", "activityHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyClassDefinition", "Class", "classHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyRaceDefinition", "Race", "raceHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyGenderDefinition", "Gender", "genderHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyInventoryBucketDefinition", "InventoryBucket", "bucketHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyStatDefinition", "Stat", "statHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyProgressionDefinition", "Progression", "progressionHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyScriptedSkullDefinition", "ScriptedSkull", "skullHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyTalentGridDefinition", "TalentGrid", "gridHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyObjectiveDefinition", "Objective", "objectiveHash", callback);
        },
        function (callback) {

          saveRows2(db, cache, "DestinyFactionDefinition", "Faction", "progressionHash", callback);
        },
        function (callback) {
          saveRowsGrimCards(db, cache, "DestinyGrimoireCardDefinition", "cardId", callback);
        }, function (callback) {
          saveRow(db, cache, "DestinyGrimoireDefinition", "Grimoire", callback);
        }, function (callback) {
          saveRows2(db, cache, "DestinyCombatantDefinition", "Combatant", "combatantHash", callback);
        }
      ]
      ,
      function (err, returnMe) {
        var sDB = JSON.stringify(cache);
        fs.writeFileSync(conf.dataDir + "/destiny.json", sDB);

        saveSubset(cache, "destiny-cache", ["InventoryItem", "Progression", "InventoryBucket", "TalentGrid", "Stat", "Race", "Gender", "Class", "Objective"]);
        db.close(function (err) {
        });

      });

    var REMOVE_NODE_PROPS = ['prerequisiteNodeIndexes',
      'binaryPairNodeIndex',
      'randomActivationRequirement',
      'isRandomRepurchasable',
      'exlusiveWithNodes',
      'randomStartProgressionBarAtProgression',
      'originalNodeHash',
      'isRealStepSelectionRandom'];

    var REMOVE_STEP_PROPS = ['interactionDescription',
      'damageType',
      'damageTypeHash',
      //'activationRequirement',
      'canActivateNextStep',
      'nextStepIndex',
      'isNextStepRandom',
      'startProgressionBarAtProgress',
      'affectsQuality',
      'affectsLevel'];
    
    var REMOVE_PROPS = [
      'eqippingBlock',
      'itemLevels',
      //'itemDescription',
      'hasIcon',
      'hasAction',
      'deleteOnAction',
      'tierType',
      'specialItemType',
      'hasGeometry',
      'statGroupHash',
      'itemCategoryHashes',
      'tooltipStyle',
      'questlineItemHash',
      'objectiveHashes',
      'questTrackingUnlockValueHash',
      'bountyResetUnlockHash',
      'showActiveNodesInTooltip',
      'sources',
      'sourceHashes'
    ];

    function removeStepCrap(itm){
      Object.keys(itm).forEach(function(key) {
        if (REMOVE_STEP_PROPS.indexOf(key)>-1){
          delete  itm[key];
        }
      });
    }
    
    function removeNodeCrap(itm){
      Object.keys(itm).forEach(function(key) {
        if (REMOVE_NODE_PROPS.indexOf(key)>-1){
          delete  itm[key];
        }
        if (key==="steps") {
          var val = itm[key];
          for (var cntr=0; cntr<val.length; cntr++){
            removeStepCrap(val[cntr]);
          }
        }
      });
    }
    
    function removeCrap(itm){
      Object.keys(itm).forEach(function(key) {
        if (REMOVE_PROPS.indexOf(key)>-1){
          delete  itm[key];
        }
        if (key==="nodes") {
          var val = itm[key];
          for (var cntr=0; cntr<val.length; cntr++){
            removeNodeCrap(val[cntr]); 
          }
        }
      });
    }
    
    function saveSubset(cache, filename, props) {
      var zip = new require('node-zip')();
      var obj = {};
      props.forEach(function (prop) {
        var moveMe = cache[prop];
        if (prop==="InventoryItem"){
          Object.keys(moveMe).forEach(function(key) {
            var type = moveMe[key].itemType;
            var typeName = moveMe[key].itemTypeName;

            //2 = armor, 3 = weapon, 8 = engram
            //4 crucible bounty, 0 weekly elite bounty
            if (typeName==="Material" || typeName==="Currency" || typeName==="Weekly Elite Bounty" 
              || (type==4 && typeName && typeName==="Crucible Bounty") 
              || (typeName==="Consumable")
              || (key=="1826822442") //Iron lord token
            ) {
              moveMe[key].dontClean = true;
            }
            else if(type===2 || type === 3 || type === 8 || (type==0 && typeName && typeName.endsWith("Subclass"))){
            }
            else{
              delete moveMe[key];
            }
          });
        }
        console.log(prop+" "+Object.keys(moveMe).length);
        Object.keys(moveMe).forEach(function(key) {
          if (moveMe[key].dontClean) return;
          removeCrap(moveMe[key]);
        });
        obj[prop] = moveMe;
      });
      var sDB = JSON.stringify(obj);
      zip.file('destiny.json', sDB);
      var data = zip.generate({base64: false, compression: 'DEFLATE'
        , compressionOptions: {level:9}
      });
      fs.writeFileSync(conf.dataDir + "/" + filename + ".zip", data, "binary");
      console.log("Saved to "+conf.dataDir + "/" + filename + ".zip");
    }
  });

}

saveData();
