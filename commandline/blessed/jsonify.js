/**
 * Created by Dave on 1/25/2017.
 */

var conf = require('../../config.js')();
var CACHE = require(conf.dataDir + "/destiny.json");


var INV_ITEMS = CACHE.InventoryItem;
var GUNS_BY_NAME = {};
var PERKS_BY_NAME = {};

var keys = [];
for (var key in INV_ITEMS) {
  if (INV_ITEMS.hasOwnProperty(key)) {
    var jDesc = INV_ITEMS[key];
    if (jDesc.itemType!=3) continue;
    GUNS_BY_NAME[jDesc.itemName] = jDesc;
    
    var talents = CACHE.TalentGrid[jDesc.talentGridHash];
    if (talents==null) continue;

    console.log(jDesc.itemName);
    for (var cntr=0; cntr<talents.nodes.length; cntr++){
      var node = talents.nodes[cntr];
      var row = node.row;
      var col = node.col;
      for (var stepCntr=0; stepCntr<node.steps.length; stepCntr++){
        var step = node.steps[stepCntr];
        var name = step.nodeStepName;
        var hash = step.nodeStepHash;
        if (PERKS_BY_NAME[name]){
          if (PERKS_BY_NAME[name].hash!=hash){
            throw new Error("Dupe perk");
          }
        }
        PERKS_BY_NAME[name] = {
          name: name,
          hash: hash
        }
      }
    }
    
  }
}
console.dir(PERKS_BY_NAME);

var fs = require("fs");
var pveCsv = fs.readFileSync("pveWeapons.csv", "utf-8");
var pvpCsv = fs.readFileSync("pvpWeapons.csv", "utf-8");

function parseLines(sCsv) {

  var guns = {};

  var aLines = sCsv.split(/\r?\n/);
  aLines.forEach(
    
    function (line) {
      parseGun(line);
      var aEntries = line.split(",");
      var name = aEntries.shift();
      var cols = [];
      aEntries.forEach(
        function(slashPerks){
          var aPerk = slashPerks.split("/");
          cols.push(aPerk);
        }
      );
      var addMe ={
        name: name,
        perks: cols
      };
      console.dir(addMe);
      guns[name] = addMe;
    }
  );
  return guns;
}

function parseGun(line){
  var aEntries = line.split(",");
  var name = aEntries.shift();
  
}

