/**
 * Created by Dave on 9/9/2015.
 */
"use strict";

var fs = require("fs");
var conf = require('../config.js')();
var unzip = require('unzipper');
var request = require('request');


function saveDB(path, title){
  console.log("Downloading ["+title+"]: " + path);
  request({
      url: path, encoding: null, headers: {
        'x-api-key': '997251a328d246f5ac3331b818673d12'
      }
    }, function (error, response, body) {
      var filename = path.substring(path.lastIndexOf("/") + 1, path.length);

      console.log("Downloaded, unzipping: " + filename);
      console.log("Writing to "+conf.dataDir + "/destiny."+title+".zip");
      fs.writeFileSync(conf.dataDir + "/destiny."+title+".zip", body);
      fs.createReadStream(conf.dataDir + "/destiny."+title+".zip")
        .pipe(unzip.Parse())
        .on('entry', function (entry) {
          entry.pipe(fs.createWriteStream(conf.dataDir + "/destiny."+title+".db"));
        });
    }
  );
}

function downloadData() {

    request(
        {
            url: 'https://www.bungie.net/D1/Platform/Destiny/Manifest/',
            headers: {
                'x-api-key': '997251a328d246f5ac3331b818673d12'
            }
        },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
              for (var key in data.Response.mobileWorldContentPaths) {
                if (data.Response.mobileWorldContentPaths.hasOwnProperty(key)) {
                  var value = data.Response.mobileWorldContentPaths[key];
                  var path = 'http://www.bungie.net' +value;
                  saveDB(path, key);
                }
              }
            }
            else {
                throw "Unexpected error (response code = " + response.statusCode + "): " + error;
            }
        });
}

downloadData();