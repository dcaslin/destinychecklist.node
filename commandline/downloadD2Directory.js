#!/usr/bin/env node

var fs = require('fs');
var request = require('request');
var unzip = require('unzipper');
var async = require("async");

function onManifestRequest(error, response, body) {
  var parsedResponse = JSON.parse(body);
  var manifestFile = fs.createWriteStream("manifest.zip");

  request
    .get('https://www.bungie.net' + parsedResponse.Response.mobileWorldContentPaths.en)
    .pipe(manifestFile)
    .on('close', onManifestDownloaded);
}

function onManifestDownloaded() {

  
  fs.createReadStream('manifest.zip')
    .pipe(unzip.Parse())
    .on('entry', function(entry) {
      var ws = fs.createWriteStream('C:/projects/temp/' + entry.path);
      entry.pipe(ws);
    });
}

request({
  headers: {
    'x-api-key': '997251a328d246f5ac3331b818673d12'
  },


  uri: 'http://www.bungie.net/platform/Destiny2/Manifest/',
  method: 'GET'
}, onManifestRequest);