/*! =======================================================
                      VERSION  5.1.3              
========================================================= */
/*! =========================================================
 * bootstrap-slider.js
 *
 * Maintainers:
 *		Kyle Kemp
 *			- Twitter: @seiyria
 *			- Github:  seiyria
 *		Rohit Kalkur
 *			- Twitter: @Rovolutionary
 *			- Github:  rovolution
 *
 * =========================================================
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */
!function(a,b){if("function"==typeof define&&define.amd)define(["jquery"],b);else if("object"==typeof module&&module.exports){var c;try{c=require("jquery")}catch(d){c=null}module.exports=b(c)}else a.Slider=b(a.jQuery)}(this,function(a){var b;return function(a){"use strict";function b(){}function c(a){function c(b){b.prototype.option||(b.prototype.option=function(b){a.isPlainObject(b)&&(this.options=a.extend(!0,this.options,b))})}function e(b,c){a.fn[b]=function(e){if("string"==typeof e){for(var g=d.call(arguments,1),h=0,i=this.length;i>h;h++){var j=this[h],k=a.data(j,b);if(k)if(a.isFunction(k[e])&&"_"!==e.charAt(0)){var l=k[e].apply(k,g);if(void 0!==l&&l!==k)return l}else f("no such method '"+e+"' for "+b+" instance");else f("cannot call methods on "+b+" prior to initialization; attempted to call '"+e+"'")}return this}var m=this.map(function(){var d=a.data(this,b);return d?(d.option(e),d._init()):(d=new c(this,e),a.data(this,b,d)),a(this)});return!m||m.length>1?m:m[0]}}if(a){var f="undefined"==typeof console?b:function(a){console.error(a)};return a.bridget=function(a,b){c(b),e(a,b)},a.bridget}}var d=Array.prototype.slice;c(a)}(a),function(a){function c(b,c){function d(a,b){var c="data-slider-"+b.replace(/_/g,"-"),d=a.getAttribute(c);try{return JSON.parse(d)}catch(e){return d}}this._state={value:null,enabled:null,offset:null,size:null,percentage:null,inDrag:!1,over:!1},"string"==typeof b?this.element=document.querySelector(b):b instanceof HTMLElement&&(this.element=b),c=c?c:{};for(var f=Object.keys(this.defaultOptions),g=0;g<f.length;g++){var h=f[g],i=c[h];i="undefined"!=typeof i?i:d(this.element,h),i=null!==i?i:this.defaultOptions[h],this.options||(this.options={}),this.options[h]=i}"vertical"!==this.options.orientation||"top"!==this.options.tooltip_position&&"bottom"!==this.options.tooltip_position?"horizontal"!==this.options.orientation||"left"!==this.options.tooltip_position&&"right"!==this.options.tooltip_position||(this.options.tooltip_position="top"):this.options.tooltip_position="right";var j,k,l,m,n,o=this.element.style.width,p=!1,q=this.element.parentNode;if(this.sliderElem)p=!0;else{this.sliderElem=document.createElement("div"),this.sliderElem.className="slider";var r=document.createElement("div");if(r.className="slider-track",k=document.createElement("div"),k.className="slider-track-low",j=document.createElement("div"),j.className="slider-selection",l=document.createElement("div"),l.className="slider-track-high",m=document.createElement("div"),m.className="slider-handle min-slider-handle",n=document.createElement("div"),n.className="slider-handle max-slider-handle",r.appendChild(k),r.appendChild(j),r.appendChild(l),this.ticks=[],Array.isArray(this.options.ticks)&&this.options.ticks.length>0){for(g=0;g<this.options.ticks.length;g++){var s=document.createElement("div");s.className="slider-tick",this.ticks.push(s),r.appendChild(s)}j.className+=" tick-slider-selection"}if(r.appendChild(m),r.appendChild(n),this.tickLabels=[],Array.isArray(this.options.ticks_labels)&&this.options.ticks_labels.length>0)for(this.tickLabelContainer=document.createElement("div"),this.tickLabelContainer.className="slider-tick-label-container",g=0;g<this.options.ticks_labels.length;g++){var t=document.createElement("div");t.className="slider-tick-label",t.innerHTML=this.options.ticks_labels[g],this.tickLabels.push(t),this.tickLabelContainer.appendChild(t)}var u=function(a){var b=document.createElement("div");b.className="tooltip-arrow";var c=document.createElement("div");c.className="tooltip-inner",a.appendChild(b),a.appendChild(c)},v=document.createElement("div");v.className="tooltip tooltip-main",u(v);var w=document.createElement("div");w.className="tooltip tooltip-min",u(w);var x=document.createElement("div");x.className="tooltip tooltip-max",u(x),this.sliderElem.appendChild(r),this.sliderElem.appendChild(v),this.sliderElem.appendChild(w),this.sliderElem.appendChild(x),this.tickLabelContainer&&this.sliderElem.appendChild(this.tickLabelContainer),q.insertBefore(this.sliderElem,this.element),this.element.style.display="none"}if(a&&(this.$element=a(this.element),this.$sliderElem=a(this.sliderElem)),this.eventToCallbackMap={},this.sliderElem.id=this.options.id,this.touchCapable="ontouchstart"in window||window.DocumentTouch&&document instanceof window.DocumentTouch,this.tooltip=this.sliderElem.querySelector(".tooltip-main"),this.tooltipInner=this.tooltip.querySelector(".tooltip-inner"),this.tooltip_min=this.sliderElem.querySelector(".tooltip-min"),this.tooltipInner_min=this.tooltip_min.querySelector(".tooltip-inner"),this.tooltip_max=this.sliderElem.querySelector(".tooltip-max"),this.tooltipInner_max=this.tooltip_max.querySelector(".tooltip-inner"),e[this.options.scale]&&(this.options.scale=e[this.options.scale]),p===!0&&(this._removeClass(this.sliderElem,"slider-horizontal"),this._removeClass(this.sliderElem,"slider-vertical"),this._removeClass(this.tooltip,"hide"),this._removeClass(this.tooltip_min,"hide"),this._removeClass(this.tooltip_max,"hide"),["left","top","width","height"].forEach(function(a){this._removeProperty(this.trackLow,a),this._removeProperty(this.trackSelection,a),this._removeProperty(this.trackHigh,a)},this),[this.handle1,this.handle2].forEach(function(a){this._removeProperty(a,"left"),this._removeProperty(a,"top")},this),[this.tooltip,this.tooltip_min,this.tooltip_max].forEach(function(a){this._removeProperty(a,"left"),this._removeProperty(a,"top"),this._removeProperty(a,"margin-left"),this._removeProperty(a,"margin-top"),this._removeClass(a,"right"),this._removeClass(a,"top")},this)),"vertical"===this.options.orientation?(this._addClass(this.sliderElem,"slider-vertical"),this.stylePos="top",this.mousePos="pageY",this.sizePos="offsetHeight"):(this._addClass(this.sliderElem,"slider-horizontal"),this.sliderElem.style.width=o,this.options.orientation="horizontal",this.stylePos="left",this.mousePos="pageX",this.sizePos="offsetWidth"),this._setTooltipPosition(),Array.isArray(this.options.ticks)&&this.options.ticks.length>0&&(this.options.max=Math.max.apply(Math,this.options.ticks),this.options.min=Math.min.apply(Math,this.options.ticks)),Array.isArray(this.options.value)?(this.options.range=!0,this._state.value=this.options.value):this._state.value=this.options.range?[this.options.value,this.options.max]:this.options.value,this.trackLow=k||this.trackLow,this.trackSelection=j||this.trackSelection,this.trackHigh=l||this.trackHigh,"none"===this.options.selection&&(this._addClass(this.trackLow,"hide"),this._addClass(this.trackSelection,"hide"),this._addClass(this.trackHigh,"hide")),this.handle1=m||this.handle1,this.handle2=n||this.handle2,p===!0)for(this._removeClass(this.handle1,"round triangle"),this._removeClass(this.handle2,"round triangle hide"),g=0;g<this.ticks.length;g++)this._removeClass(this.ticks[g],"round triangle hide");var y=["round","triangle","custom"],z=-1!==y.indexOf(this.options.handle);if(z)for(this._addClass(this.handle1,this.options.handle),this._addClass(this.handle2,this.options.handle),g=0;g<this.ticks.length;g++)this._addClass(this.ticks[g],this.options.handle);this._state.offset=this._offset(this.sliderElem),this._state.size=this.sliderElem[this.sizePos],this.setValue(this._state.value),this.handle1Keydown=this._keydown.bind(this,0),this.handle1.addEventListener("keydown",this.handle1Keydown,!1),this.handle2Keydown=this._keydown.bind(this,1),this.handle2.addEventListener("keydown",this.handle2Keydown,!1),this.mousedown=this._mousedown.bind(this),this.touchCapable&&this.sliderElem.addEventListener("touchstart",this.mousedown,!1),this.sliderElem.addEventListener("mousedown",this.mousedown,!1),"hide"===this.options.tooltip?(this._addClass(this.tooltip,"hide"),this._addClass(this.tooltip_min,"hide"),this._addClass(this.tooltip_max,"hide")):"always"===this.options.tooltip?(this._showTooltip(),this._alwaysShowTooltip=!0):(this.showTooltip=this._showTooltip.bind(this),this.hideTooltip=this._hideTooltip.bind(this),this.sliderElem.addEventListener("mouseenter",this.showTooltip,!1),this.sliderElem.addEventListener("mouseleave",this.hideTooltip,!1),this.handle1.addEventListener("focus",this.showTooltip,!1),this.handle1.addEventListener("blur",this.hideTooltip,!1),this.handle2.addEventListener("focus",this.showTooltip,!1),this.handle2.addEventListener("blur",this.hideTooltip,!1)),this.options.enabled?this.enable():this.disable()}var d={formatInvalidInputErrorMsg:function(a){return"Invalid input value '"+a+"' passed in"},callingContextNotSliderInstance:"Calling context element does not have instance of Slider bound to it. Check your code to make sure the JQuery object returned from the call to the slider() initializer is calling the method"},e={linear:{toValue:function(a){var b=a/100*(this.options.max-this.options.min);if(this.options.ticks_positions.length>0){for(var c,d,e,f=0,g=0;g<this.options.ticks_positions.length;g++)if(a<=this.options.ticks_positions[g]){c=g>0?this.options.ticks[g-1]:0,e=g>0?this.options.ticks_positions[g-1]:0,d=this.options.ticks[g],f=this.options.ticks_positions[g];break}if(g>0){var h=(a-e)/(f-e);b=c+h*(d-c)}}var i=this.options.min+Math.round(b/this.options.step)*this.options.step;return i<this.options.min?this.options.min:i>this.options.max?this.options.max:i},toPercentage:function(a){if(this.options.max===this.options.min)return 0;if(this.options.ticks_positions.length>0){for(var b,c,d,e=0,f=0;f<this.options.ticks.length;f++)if(a<=this.options.ticks[f]){b=f>0?this.options.ticks[f-1]:0,d=f>0?this.options.ticks_positions[f-1]:0,c=this.options.ticks[f],e=this.options.ticks_positions[f];break}if(f>0){var g=(a-b)/(c-b);return d+g*(e-d)}}return 100*(a-this.options.min)/(this.options.max-this.options.min)}},logarithmic:{toValue:function(a){var b=0===this.options.min?0:Math.log(this.options.min),c=Math.log(this.options.max),d=Math.exp(b+(c-b)*a/100);return d=this.options.min+Math.round((d-this.options.min)/this.options.step)*this.options.step,d<this.options.min?this.options.min:d>this.options.max?this.options.max:d},toPercentage:function(a){if(this.options.max===this.options.min)return 0;var b=Math.log(this.options.max),c=0===this.options.min?0:Math.log(this.options.min),d=0===a?0:Math.log(a);return 100*(d-c)/(b-c)}}};if(b=function(a,b){return c.call(this,a,b),this},b.prototype={_init:function(){},constructor:b,defaultOptions:{id:"",min:0,max:10,step:1,precision:0,orientation:"horizontal",value:5,range:!1,selection:"before",tooltip:"show",tooltip_split:!1,handle:"round",reversed:!1,enabled:!0,formatter:function(a){return Array.isArray(a)?a[0]+" : "+a[1]:a},natural_arrow_keys:!1,ticks:[],ticks_positions:[],ticks_labels:[],ticks_snap_bounds:0,scale:"linear",focus:!1,tooltip_position:null},getElement:function(){return this.sliderElem},getValue:function(){return this.options.range?this._state.value:this._state.value[0]},setValue:function(a,b,c){a||(a=0);var d=this.getValue();this._state.value=this._validateInputValue(a);var e=this._applyPrecision.bind(this);this.options.range?(this._state.value[0]=e(this._state.value[0]),this._state.value[1]=e(this._state.value[1]),this._state.value[0]=Math.max(this.options.min,Math.min(this.options.max,this._state.value[0])),this._state.value[1]=Math.max(this.options.min,Math.min(this.options.max,this._state.value[1]))):(this._state.value=e(this._state.value),this._state.value=[Math.max(this.options.min,Math.min(this.options.max,this._state.value))],this._addClass(this.handle2,"hide"),this._state.value[1]="after"===this.options.selection?this.options.max:this.options.min),this._state.percentage=this.options.max>this.options.min?[this._toPercentage(this._state.value[0]),this._toPercentage(this._state.value[1]),100*this.options.step/(this.options.max-this.options.min)]:[0,0,100],this._layout();var f=this.options.range?this._state.value:this._state.value[0];return b===!0&&this._trigger("slide",f),d!==f&&c===!0&&this._trigger("change",{oldValue:d,newValue:f}),this._setDataVal(f),this},destroy:function(){this._removeSliderEventHandlers(),this.sliderElem.parentNode.removeChild(this.sliderElem),this.element.style.display="",this._cleanUpEventCallbacksMap(),this.element.removeAttribute("data"),a&&(this._unbindJQueryEventHandlers(),this.$element.removeData("slider"))},disable:function(){return this._state.enabled=!1,this.handle1.removeAttribute("tabindex"),this.handle2.removeAttribute("tabindex"),this._addClass(this.sliderElem,"slider-disabled"),this._trigger("slideDisabled"),this},enable:function(){return this._state.enabled=!0,this.handle1.setAttribute("tabindex",0),this.handle2.setAttribute("tabindex",0),this._removeClass(this.sliderElem,"slider-disabled"),this._trigger("slideEnabled"),this},toggle:function(){return this._state.enabled?this.disable():this.enable(),this},isEnabled:function(){return this._state.enabled},on:function(a,b){return this._bindNonQueryEventHandler(a,b),this},off:function(b,c){a?(this.$element.off(b,c),this.$sliderElem.off(b,c)):this._unbindNonQueryEventHandler(b,c)},getAttribute:function(a){return a?this.options[a]:this.options},setAttribute:function(a,b){return this.options[a]=b,this},refresh:function(){return this._removeSliderEventHandlers(),c.call(this,this.element,this.options),a&&a.data(this.element,"slider",this),this},relayout:function(){return this._layout(),this},_removeSliderEventHandlers:function(){this.handle1.removeEventListener("keydown",this.handle1Keydown,!1),this.handle1.removeEventListener("focus",this.showTooltip,!1),this.handle1.removeEventListener("blur",this.hideTooltip,!1),this.handle2.removeEventListener("keydown",this.handle2Keydown,!1),this.handle2.removeEventListener("focus",this.handle2Keydown,!1),this.handle2.removeEventListener("blur",this.handle2Keydown,!1),this.sliderElem.removeEventListener("mouseenter",this.showTooltip,!1),this.sliderElem.removeEventListener("mouseleave",this.hideTooltip,!1),this.sliderElem.removeEventListener("touchstart",this.mousedown,!1),this.sliderElem.removeEventListener("mousedown",this.mousedown,!1)},_bindNonQueryEventHandler:function(a,b){void 0===this.eventToCallbackMap[a]&&(this.eventToCallbackMap[a]=[]),this.eventToCallbackMap[a].push(b)},_unbindNonQueryEventHandler:function(a,b){var c=this.eventToCallbackMap[a];if(void 0!==c)for(var d=0;d<c.length;d++)if(c[d]===b){c.splice(d,1);break}},_cleanUpEventCallbacksMap:function(){for(var a=Object.keys(this.eventToCallbackMap),b=0;b<a.length;b++){var c=a[b];this.eventToCallbackMap[c]=null}},_showTooltip:function(){this.options.tooltip_split===!1?(this._addClass(this.tooltip,"in"),this.tooltip_min.style.display="none",this.tooltip_max.style.display="none"):(this._addClass(this.tooltip_min,"in"),this._addClass(this.tooltip_max,"in"),this.tooltip.style.display="none"),this._state.over=!0},_hideTooltip:function(){this._state.inDrag===!1&&this.alwaysShowTooltip!==!0&&(this._removeClass(this.tooltip,"in"),this._removeClass(this.tooltip_min,"in"),this._removeClass(this.tooltip_max,"in")),this._state.over=!1},_layout:function(){var a;if(a=this.options.reversed?[100-this._state.percentage[0],this.options.range?100-this._state.percentage[1]:this._state.percentage[1]]:[this._state.percentage[0],this._state.percentage[1]],this.handle1.style[this.stylePos]=a[0]+"%",this.handle2.style[this.stylePos]=a[1]+"%",Array.isArray(this.options.ticks)&&this.options.ticks.length>0){var b="vertical"===this.options.orientation?"height":"width",c="vertical"===this.options.orientation?"marginTop":"marginLeft",d=this._state.size/(this.options.ticks.length-1);if(this.tickLabelContainer){var e=0;if(0===this.options.ticks_positions.length)this.tickLabelContainer.style[c]=-d/2+"px",e=this.tickLabelContainer.offsetHeight;else for(f=0;f<this.tickLabelContainer.childNodes.length;f++)this.tickLabelContainer.childNodes[f].offsetHeight>e&&(e=this.tickLabelContainer.childNodes[f].offsetHeight);"horizontal"===this.options.orientation&&(this.sliderElem.style.marginBottom=e+"px")}for(var f=0;f<this.options.ticks.length;f++){var g=this.options.ticks_positions[f]||this._toPercentage(this.options.ticks[f]);this.ticks[f].style[this.stylePos]=g+"%",this._removeClass(this.ticks[f],"in-selection"),this.options.range?g>=a[0]&&g<=a[1]&&this._addClass(this.ticks[f],"in-selection"):"after"===this.options.selection&&g>=a[0]?this._addClass(this.ticks[f],"in-selection"):"before"===this.options.selection&&g<=a[0]&&this._addClass(this.ticks[f],"in-selection"),this.tickLabels[f]&&(this.tickLabels[f].style[b]=d+"px",void 0!==this.options.ticks_positions[f]&&(this.tickLabels[f].style.position="absolute",this.tickLabels[f].style[this.stylePos]=this.options.ticks_positions[f]+"%",this.tickLabels[f].style[c]=-d/2+"px"))}}var h;if(this.options.range){h=this.options.formatter(this._state.value),this._setText(this.tooltipInner,h),this.tooltip.style[this.stylePos]=(a[1]+a[0])/2+"%","vertical"===this.options.orientation?this._css(this.tooltip,"margin-top",-this.tooltip.offsetHeight/2+"px"):this._css(this.tooltip,"margin-left",-this.tooltip.offsetWidth/2+"px"),"vertical"===this.options.orientation?this._css(this.tooltip,"margin-top",-this.tooltip.offsetHeight/2+"px"):this._css(this.tooltip,"margin-left",-this.tooltip.offsetWidth/2+"px");var i=this.options.formatter(this._state.value[0]);this._setText(this.tooltipInner_min,i);var j=this.options.formatter(this._state.value[1]);this._setText(this.tooltipInner_max,j),this.tooltip_min.style[this.stylePos]=a[0]+"%","vertical"===this.options.orientation?this._css(this.tooltip_min,"margin-top",-this.tooltip_min.offsetHeight/2+"px"):this._css(this.tooltip_min,"margin-left",-this.tooltip_min.offsetWidth/2+"px"),this.tooltip_max.style[this.stylePos]=a[1]+"%","vertical"===this.options.orientation?this._css(this.tooltip_max,"margin-top",-this.tooltip_max.offsetHeight/2+"px"):this._css(this.tooltip_max,"margin-left",-this.tooltip_max.offsetWidth/2+"px")}else h=this.options.formatter(this._state.value[0]),this._setText(this.tooltipInner,h),this.tooltip.style[this.stylePos]=a[0]+"%","vertical"===this.options.orientation?this._css(this.tooltip,"margin-top",-this.tooltip.offsetHeight/2+"px"):this._css(this.tooltip,"margin-left",-this.tooltip.offsetWidth/2+"px");if("vertical"===this.options.orientation)this.trackLow.style.top="0",this.trackLow.style.height=Math.min(a[0],a[1])+"%",this.trackSelection.style.top=Math.min(a[0],a[1])+"%",this.trackSelection.style.height=Math.abs(a[0]-a[1])+"%",this.trackHigh.style.bottom="0",this.trackHigh.style.height=100-Math.min(a[0],a[1])-Math.abs(a[0]-a[1])+"%";else{this.trackLow.style.left="0",this.trackLow.style.width=Math.min(a[0],a[1])+"%",this.trackSelection.style.left=Math.min(a[0],a[1])+"%",this.trackSelection.style.width=Math.abs(a[0]-a[1])+"%",this.trackHigh.style.right="0",this.trackHigh.style.width=100-Math.min(a[0],a[1])-Math.abs(a[0]-a[1])+"%";var k=this.tooltip_min.getBoundingClientRect(),l=this.tooltip_max.getBoundingClientRect();k.right>l.left?(this._removeClass(this.tooltip_max,"top"),this._addClass(this.tooltip_max,"bottom"),this.tooltip_max.style.top="18px"):(this._removeClass(this.tooltip_max,"bottom"),this._addClass(this.tooltip_max,"top"),this.tooltip_max.style.top=this.tooltip_min.style.top)}},_removeProperty:function(a,b){a.style.removeProperty?a.style.removeProperty(b):a.style.removeAttribute(b)},_mousedown:function(a){if(!this._state.enabled)return!1;this._state.offset=this._offset(this.sliderElem),this._state.size=this.sliderElem[this.sizePos];var b=this._getPercentage(a);if(this.options.range){var c=Math.abs(this._state.percentage[0]-b),d=Math.abs(this._state.percentage[1]-b);this._state.dragged=d>c?0:1}else this._state.dragged=0;this._state.percentage[this._state.dragged]=b,this._layout(),this.touchCapable&&(document.removeEventListener("touchmove",this.mousemove,!1),document.removeEventListener("touchend",this.mouseup,!1)),this.mousemove&&document.removeEventListener("mousemove",this.mousemove,!1),this.mouseup&&document.removeEventListener("mouseup",this.mouseup,!1),this.mousemove=this._mousemove.bind(this),this.mouseup=this._mouseup.bind(this),this.touchCapable&&(document.addEventListener("touchmove",this.mousemove,!1),document.addEventListener("touchend",this.mouseup,!1)),document.addEventListener("mousemove",this.mousemove,!1),document.addEventListener("mouseup",this.mouseup,!1),this._state.inDrag=!0;var e=this._calculateValue();return this._trigger("slideStart",e),this._setDataVal(e),this.setValue(e,!1,!0),this._pauseEvent(a),this.options.focus&&this._triggerFocusOnHandle(this._state.dragged),!0},_triggerFocusOnHandle:function(a){0===a&&this.handle1.focus(),1===a&&this.handle2.focus()},_keydown:function(a,b){if(!this._state.enabled)return!1;var c;switch(b.keyCode){case 37:case 40:c=-1;break;case 39:case 38:c=1}if(c){if(this.options.natural_arrow_keys){var d="vertical"===this.options.orientation&&!this.options.reversed,e="horizontal"===this.options.orientation&&this.options.reversed;(d||e)&&(c=-c)}var f=this._state.value[a]+c*this.options.step;return this.options.range&&(f=[a?this._state.value[0]:f,a?f:this._state.value[1]]),this._trigger("slideStart",f),this._setDataVal(f),this.setValue(f,!0,!0),this._setDataVal(f),this._trigger("slideStop",f),this._layout(),this._pauseEvent(b),!1}},_pauseEvent:function(a){a.stopPropagation&&a.stopPropagation(),a.preventDefault&&a.preventDefault(),a.cancelBubble=!0,a.returnValue=!1},_mousemove:function(a){if(!this._state.enabled)return!1;var b=this._getPercentage(a);this._adjustPercentageForRangeSliders(b),this._state.percentage[this._state.dragged]=b,this._layout();var c=this._calculateValue(!0);return this.setValue(c,!0,!0),!1},_adjustPercentageForRangeSliders:function(a){if(this.options.range){var b=this._getNumDigitsAfterDecimalPlace(a);b=b?b-1:0;var c=this._applyToFixedAndParseFloat(a,b);0===this._state.dragged&&this._applyToFixedAndParseFloat(this._state.percentage[1],b)<c?(this._state.percentage[0]=this._state.percentage[1],this._state.dragged=1):1===this._state.dragged&&this._applyToFixedAndParseFloat(this._state.percentage[0],b)>c&&(this._state.percentage[1]=this._state.percentage[0],this._state.dragged=0)}},_mouseup:function(){if(!this._state.enabled)return!1;this.touchCapable&&(document.removeEventListener("touchmove",this.mousemove,!1),document.removeEventListener("touchend",this.mouseup,!1)),document.removeEventListener("mousemove",this.mousemove,!1),document.removeEventListener("mouseup",this.mouseup,!1),this._state.inDrag=!1,this._state.over===!1&&this._hideTooltip();var a=this._calculateValue(!0);return this._layout(),this._setDataVal(a),this._trigger("slideStop",a),!1},_calculateValue:function(a){var b;if(this.options.range?(b=[this.options.min,this.options.max],0!==this._state.percentage[0]&&(b[0]=this._toValue(this._state.percentage[0]),b[0]=this._applyPrecision(b[0])),100!==this._state.percentage[1]&&(b[1]=this._toValue(this._state.percentage[1]),b[1]=this._applyPrecision(b[1]))):(b=this._toValue(this._state.percentage[0]),b=parseFloat(b),b=this._applyPrecision(b)),a){for(var c=[b,1/0],d=0;d<this.options.ticks.length;d++){var e=Math.abs(this.options.ticks[d]-b);e<=c[1]&&(c=[this.options.ticks[d],e])}if(c[1]<=this.options.ticks_snap_bounds)return c[0]}return b},_applyPrecision:function(a){var b=this.options.precision||this._getNumDigitsAfterDecimalPlace(this.options.step);return this._applyToFixedAndParseFloat(a,b)},_getNumDigitsAfterDecimalPlace:function(a){var b=(""+a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);return b?Math.max(0,(b[1]?b[1].length:0)-(b[2]?+b[2]:0)):0},_applyToFixedAndParseFloat:function(a,b){var c=a.toFixed(b);return parseFloat(c)},_getPercentage:function(a){!this.touchCapable||"touchstart"!==a.type&&"touchmove"!==a.type||(a=a.touches[0]);var b=a[this.mousePos],c=this._state.offset[this.stylePos],d=b-c,e=d/this._state.size*100;return e=Math.round(e/this._state.percentage[2])*this._state.percentage[2],this.options.reversed&&(e=100-e),Math.max(0,Math.min(100,e))},_validateInputValue:function(a){if("number"==typeof a)return a;if(Array.isArray(a))return this._validateArray(a),a;throw new Error(d.formatInvalidInputErrorMsg(a))},_validateArray:function(a){for(var b=0;b<a.length;b++){var c=a[b];if("number"!=typeof c)throw new Error(d.formatInvalidInputErrorMsg(c))}},_setDataVal:function(a){this.element.setAttribute("data-value",a),this.element.setAttribute("value",a),this.element.value=a},_trigger:function(b,c){c=c||0===c?c:void 0;var d=this.eventToCallbackMap[b];if(d&&d.length)for(var e=0;e<d.length;e++){var f=d[e];f(c)}a&&this._triggerJQueryEvent(b,c)},_triggerJQueryEvent:function(a,b){var c={type:a,value:b};this.$element.trigger(c),this.$sliderElem.trigger(c)},_unbindJQueryEventHandlers:function(){this.$element.off(),this.$sliderElem.off()},_setText:function(a,b){"undefined"!=typeof a.innerText?a.innerText=b:"undefined"!=typeof a.textContent&&(a.textContent=b)},_removeClass:function(a,b){for(var c=b.split(" "),d=a.className,e=0;e<c.length;e++){var f=c[e],g=new RegExp("(?:\\s|^)"+f+"(?:\\s|$)");d=d.replace(g," ")}a.className=d.trim()},_addClass:function(a,b){for(var c=b.split(" "),d=a.className,e=0;e<c.length;e++){var f=c[e],g=new RegExp("(?:\\s|^)"+f+"(?:\\s|$)"),h=g.test(d);h||(d+=" "+f)}a.className=d.trim()},_offsetLeft:function(a){return a.getBoundingClientRect().left},_offsetTop:function(a){for(var b=a.offsetTop;(a=a.offsetParent)&&!isNaN(a.offsetTop);)b+=a.offsetTop;return b},_offset:function(a){return{left:this._offsetLeft(a),top:this._offsetTop(a)}},_css:function(b,c,d){if(a)a.style(b,c,d);else{var e=c.replace(/^-ms-/,"ms-").replace(/-([\da-z])/gi,function(a,b){return b.toUpperCase()});b.style[e]=d}},_toValue:function(a){return this.options.scale.toValue.apply(this,[a])},_toPercentage:function(a){return this.options.scale.toPercentage.apply(this,[a])},_setTooltipPosition:function(){var a=[this.tooltip,this.tooltip_min,this.tooltip_max];if("vertical"===this.options.orientation){var b=this.options.tooltip_position||"right",c="left"===b?"right":"left";a.forEach(function(a){this._addClass(a,b),a.style[c]="100%"}.bind(this))}else a.forEach("bottom"===this.options.tooltip_position?function(a){this._addClass(a,"bottom"),a.style.top="22px"}.bind(this):function(a){this._addClass(a,"top"),a.style.top=-this.tooltip.outerHeight-14+"px"}.bind(this))}},a){var f=a.fn.slider?"bootstrapSlider":"slider";a.bridget(f,b)}}(a),b});;/*! ========================================================================
 * Bootstrap Toggle: bootstrap-toggle.js v2.2.0
 * http://www.bootstraptoggle.com
 * ========================================================================
 * Copyright 2014 Min Hur, The New York Times Company
 * Licensed under MIT
 * 
 * dcc: Modified to addd "brightoff" option
 * ======================================================================== */

+function ($) {
  'use strict';

  // TOGGLE PUBLIC CLASS DEFINITION
  // ==============================

  var Toggle = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, this.defaults(), options)
    this.render()
  }

  Toggle.VERSION  = '2.2.0'

  Toggle.DEFAULTS = {
    on: 'On',
    off: 'Off',
    onstyle: 'primary',
    offstyle: 'default',
    size: 'normal',
    style: '',
    brightoff: false,
    width: null,
    height: null
  }

  Toggle.prototype.defaults = function() {
    return {
      on: this.$element.attr('data-on') || Toggle.DEFAULTS.on,
      off: this.$element.attr('data-off') || Toggle.DEFAULTS.off,
      onstyle: this.$element.attr('data-onstyle') || Toggle.DEFAULTS.onstyle,
      offstyle: this.$element.attr('data-offstyle') || Toggle.DEFAULTS.offstyle,
      size: this.$element.attr('data-size') || Toggle.DEFAULTS.size,
      style: this.$element.attr('data-style') || Toggle.DEFAULTS.style,
      brightoff: this.$element.attr('data-brightoff')==="true" || Toggle.DEFAULTS.brightoff,
      width: this.$element.attr('data-width') || Toggle.DEFAULTS.width,
      height: this.$element.attr('data-height') || Toggle.DEFAULTS.height
    }
  }

  Toggle.prototype.render = function () {
    this._onstyle = 'btn-' + this.options.onstyle
    this._offstyle = 'btn-' + this.options.offstyle
    var size = this.options.size === 'large' ? 'btn-lg'
      : this.options.size === 'small' ? 'btn-sm'
      : this.options.size === 'mini' ? 'btn-xs'
      : ''
    var $toggleOn = $('<label class="btn">').html(this.options.on)
      .addClass(this._onstyle + ' ' + size)
    var $toggleOff = $('<label class="btn">').html(this.options.off);
    if (this.options.brightoff==true){
      $toggleOff.addClass(this._offstyle + ' ' + size)
    }
    else{
      $toggleOff.addClass(this._offstyle + ' ' + size + ' active')
    }

    var $toggleHandle = $('<span class="toggle-handle btn btn-default">')
      .addClass(size)
    var $toggleGroup = $('<div class="toggle-group">')
      .append($toggleOn, $toggleOff, $toggleHandle)
    var $toggle = $('<div class="toggle btn" data-toggle="toggle">')
      .addClass( this.$element.prop('checked') ? this._onstyle : this._offstyle+' off' )
      .addClass(size).addClass(this.options.style)

    this.$element.wrap($toggle)
    $.extend(this, {
      $toggle: this.$element.parent(),
      $toggleOn: $toggleOn,
      $toggleOff: $toggleOff,
      $toggleGroup: $toggleGroup
    })
    this.$toggle.append($toggleGroup)

    var width = this.options.width || Math.max($toggleOn.outerWidth(), $toggleOff.outerWidth())+($toggleHandle.outerWidth()/2)
    var height = this.options.height || Math.max($toggleOn.outerHeight(), $toggleOff.outerHeight())
    $toggleOn.addClass('toggle-on')
    $toggleOff.addClass('toggle-off')
    this.$toggle.css({ width: width, height: height })
    if (this.options.height) {
      $toggleOn.css('line-height', $toggleOn.height() + 'px')
      $toggleOff.css('line-height', $toggleOff.height() + 'px')
    }
    this.update(true)
    this.trigger(true)
  }

  Toggle.prototype.toggle = function () {
    if (this.$element.prop('checked')) this.off()
    else this.on()
  }

  Toggle.prototype.on = function (silent) {
    if (this.$element.prop('disabled')) return false
    this.$toggle.removeClass(this._offstyle + ' off').addClass(this._onstyle)
    this.$element.prop('checked', true)
    if (!silent) this.trigger()
  }

  Toggle.prototype.off = function (silent) {
    if (this.$element.prop('disabled')) return false
    this.$toggle.removeClass(this._onstyle).addClass(this._offstyle + ' off')
    this.$element.prop('checked', false)
    if (!silent) this.trigger()
  }

  Toggle.prototype.enable = function () {
    this.$toggle.removeAttr('disabled')
    this.$element.prop('disabled', false)
  }

  Toggle.prototype.disable = function () {
    this.$toggle.attr('disabled', 'disabled')
    this.$element.prop('disabled', true)
  }

  Toggle.prototype.update = function (silent) {
    if (this.$element.prop('disabled')) this.disable()
    else this.enable()
    if (this.$element.prop('checked')) this.on(silent)
    else this.off(silent)
  }

  Toggle.prototype.trigger = function (silent) {
    this.$element.off('change.bs.toggle')
    if (!silent) this.$element.change()
    this.$element.on('change.bs.toggle', $.proxy(function() {
      this.update()
    }, this))
  }

  Toggle.prototype.destroy = function() {
    this.$element.off('change.bs.toggle')
    this.$toggleGroup.remove()
    this.$element.removeData('bs.toggle')
    this.$element.unwrap()
  }

  // TOGGLE PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.toggle')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.toggle', (data = new Toggle(this, options)))
      if (typeof option == 'string' && data[option]) data[option]()
    })
  }

  var old = $.fn.bootstrapToggle

  $.fn.bootstrapToggle             = Plugin
  $.fn.bootstrapToggle.Constructor = Toggle

  // TOGGLE NO CONFLICT
  // ==================

  $.fn.toggle.noConflict = function () {
    $.fn.bootstrapToggle = old
    return this
  }

  // TOGGLE DATA-API
  // ===============

  $(function() {
    $('input[type=checkbox][data-toggle^=toggle]').bootstrapToggle()
  })

  $(document).on('click.bs.toggle', 'div[data-toggle^=toggle]', function(e) {
    var $checkbox = $(this).find('input[type=checkbox]')
    $checkbox.bootstrapToggle('toggle')
    e.preventDefault()
  })

}(jQuery);;/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(a){"use strict";var b=a.fn.jquery.split(" ")[0].split(".");if(b[0]<2&&b[1]<9||1==b[0]&&9==b[1]&&b[2]<1||b[0]>2)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3")}(jQuery),+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]};return!1}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one("bsTransitionEnd",function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b(),a.support.transition&&(a.event.special.bsTransitionEnd={bindType:a.support.transition.end,delegateType:a.support.transition.end,handle:function(b){return a(b.target).is(this)?b.handleObj.handler.apply(this,arguments):void 0}})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var c=a(this),e=c.data("bs.alert");e||c.data("bs.alert",e=new d(this)),"string"==typeof b&&e[b].call(c)})}var c='[data-dismiss="alert"]',d=function(b){a(b).on("click",c,this.close)};d.VERSION="3.3.6",d.TRANSITION_DURATION=150,d.prototype.close=function(b){function c(){g.detach().trigger("closed.bs.alert").remove()}var e=a(this),f=e.attr("data-target");f||(f=e.attr("href"),f=f&&f.replace(/.*(?=#[^\s]*$)/,""));var g=a(f);b&&b.preventDefault(),g.length||(g=e.closest(".alert")),g.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(g.removeClass("in"),a.support.transition&&g.hasClass("fade")?g.one("bsTransitionEnd",c).emulateTransitionEnd(d.TRANSITION_DURATION):c())};var e=a.fn.alert;a.fn.alert=b,a.fn.alert.Constructor=d,a.fn.alert.noConflict=function(){return a.fn.alert=e,this},a(document).on("click.bs.alert.data-api",c,d.prototype.close)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof b&&b;e||d.data("bs.button",e=new c(this,f)),"toggle"==b?e.toggle():b&&e.setState(b)})}var c=function(b,d){this.$element=a(b),this.options=a.extend({},c.DEFAULTS,d),this.isLoading=!1};c.VERSION="3.3.6",c.DEFAULTS={loadingText:"loading..."},c.prototype.setState=function(b){var c="disabled",d=this.$element,e=d.is("input")?"val":"html",f=d.data();b+="Text",null==f.resetText&&d.data("resetText",d[e]()),setTimeout(a.proxy(function(){d[e](null==f[b]?this.options[b]:f[b]),"loadingText"==b?(this.isLoading=!0,d.addClass(c).attr(c,c)):this.isLoading&&(this.isLoading=!1,d.removeClass(c).removeAttr(c))},this),0)},c.prototype.toggle=function(){var a=!0,b=this.$element.closest('[data-toggle="buttons"]');if(b.length){var c=this.$element.find("input");"radio"==c.prop("type")?(c.prop("checked")&&(a=!1),b.find(".active").removeClass("active"),this.$element.addClass("active")):"checkbox"==c.prop("type")&&(c.prop("checked")!==this.$element.hasClass("active")&&(a=!1),this.$element.toggleClass("active")),c.prop("checked",this.$element.hasClass("active")),a&&c.trigger("change")}else this.$element.attr("aria-pressed",!this.$element.hasClass("active")),this.$element.toggleClass("active")};var d=a.fn.button;a.fn.button=b,a.fn.button.Constructor=c,a.fn.button.noConflict=function(){return a.fn.button=d,this},a(document).on("click.bs.button.data-api",'[data-toggle^="button"]',function(c){var d=a(c.target);d.hasClass("btn")||(d=d.closest(".btn")),b.call(d,"toggle"),a(c.target).is('input[type="radio"]')||a(c.target).is('input[type="checkbox"]')||c.preventDefault()}).on("focus.bs.button.data-api blur.bs.button.data-api",'[data-toggle^="button"]',function(b){a(b.target).closest(".btn").toggleClass("focus",/^focus(in)?$/.test(b.type))})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},c.DEFAULTS,d.data(),"object"==typeof b&&b),g="string"==typeof b?b:f.slide;e||d.data("bs.carousel",e=new c(this,f)),"number"==typeof b?e.to(b):g?e[g]():f.interval&&e.pause().cycle()})}var c=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=null,this.sliding=null,this.interval=null,this.$active=null,this.$items=null,this.options.keyboard&&this.$element.on("keydown.bs.carousel",a.proxy(this.keydown,this)),"hover"==this.options.pause&&!("ontouchstart"in document.documentElement)&&this.$element.on("mouseenter.bs.carousel",a.proxy(this.pause,this)).on("mouseleave.bs.carousel",a.proxy(this.cycle,this))};c.VERSION="3.3.6",c.TRANSITION_DURATION=600,c.DEFAULTS={interval:5e3,pause:"hover",wrap:!0,keyboard:!0},c.prototype.keydown=function(a){if(!/input|textarea/i.test(a.target.tagName)){switch(a.which){case 37:this.prev();break;case 39:this.next();break;default:return}a.preventDefault()}},c.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},c.prototype.getItemIndex=function(a){return this.$items=a.parent().children(".item"),this.$items.index(a||this.$active)},c.prototype.getItemForDirection=function(a,b){var c=this.getItemIndex(b),d="prev"==a&&0===c||"next"==a&&c==this.$items.length-1;if(d&&!this.options.wrap)return b;var e="prev"==a?-1:1,f=(c+e)%this.$items.length;return this.$items.eq(f)},c.prototype.to=function(a){var b=this,c=this.getItemIndex(this.$active=this.$element.find(".item.active"));return a>this.$items.length-1||0>a?void 0:this.sliding?this.$element.one("slid.bs.carousel",function(){b.to(a)}):c==a?this.pause().cycle():this.slide(a>c?"next":"prev",this.$items.eq(a))},c.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},c.prototype.next=function(){return this.sliding?void 0:this.slide("next")},c.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},c.prototype.slide=function(b,d){var e=this.$element.find(".item.active"),f=d||this.getItemForDirection(b,e),g=this.interval,h="next"==b?"left":"right",i=this;if(f.hasClass("active"))return this.sliding=!1;var j=f[0],k=a.Event("slide.bs.carousel",{relatedTarget:j,direction:h});if(this.$element.trigger(k),!k.isDefaultPrevented()){if(this.sliding=!0,g&&this.pause(),this.$indicators.length){this.$indicators.find(".active").removeClass("active");var l=a(this.$indicators.children()[this.getItemIndex(f)]);l&&l.addClass("active")}var m=a.Event("slid.bs.carousel",{relatedTarget:j,direction:h});return a.support.transition&&this.$element.hasClass("slide")?(f.addClass(b),f[0].offsetWidth,e.addClass(h),f.addClass(h),e.one("bsTransitionEnd",function(){f.removeClass([b,h].join(" ")).addClass("active"),e.removeClass(["active",h].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger(m)},0)}).emulateTransitionEnd(c.TRANSITION_DURATION)):(e.removeClass("active"),f.addClass("active"),this.sliding=!1,this.$element.trigger(m)),g&&this.cycle(),this}};var d=a.fn.carousel;a.fn.carousel=b,a.fn.carousel.Constructor=c,a.fn.carousel.noConflict=function(){return a.fn.carousel=d,this};var e=function(c){var d,e=a(this),f=a(e.attr("data-target")||(d=e.attr("href"))&&d.replace(/.*(?=#[^\s]+$)/,""));if(f.hasClass("carousel")){var g=a.extend({},f.data(),e.data()),h=e.attr("data-slide-to");h&&(g.interval=!1),b.call(f,g),h&&f.data("bs.carousel").to(h),c.preventDefault()}};a(document).on("click.bs.carousel.data-api","[data-slide]",e).on("click.bs.carousel.data-api","[data-slide-to]",e),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var c=a(this);b.call(c,c.data())})})}(jQuery),+function(a){"use strict";function b(b){var c,d=b.attr("data-target")||(c=b.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"");return a(d)}function c(b){return this.each(function(){var c=a(this),e=c.data("bs.collapse"),f=a.extend({},d.DEFAULTS,c.data(),"object"==typeof b&&b);!e&&f.toggle&&/show|hide/.test(b)&&(f.toggle=!1),e||c.data("bs.collapse",e=new d(this,f)),"string"==typeof b&&e[b]()})}var d=function(b,c){this.$element=a(b),this.options=a.extend({},d.DEFAULTS,c),this.$trigger=a('[data-toggle="collapse"][href="#'+b.id+'"],[data-toggle="collapse"][data-target="#'+b.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()};d.VERSION="3.3.6",d.TRANSITION_DURATION=350,d.DEFAULTS={toggle:!0},d.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},d.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b,e=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");if(!(e&&e.length&&(b=e.data("bs.collapse"),b&&b.transitioning))){var f=a.Event("show.bs.collapse");if(this.$element.trigger(f),!f.isDefaultPrevented()){e&&e.length&&(c.call(e,"hide"),b||e.data("bs.collapse",null));var g=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;var h=function(){this.$element.removeClass("collapsing").addClass("collapse in")[g](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return h.call(this);var i=a.camelCase(["scroll",g].join("-"));this.$element.one("bsTransitionEnd",a.proxy(h,this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])}}}},d.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;var e=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")};return a.support.transition?void this.$element[c](0).one("bsTransitionEnd",a.proxy(e,this)).emulateTransitionEnd(d.TRANSITION_DURATION):e.call(this)}}},d.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()},d.prototype.getParent=function(){return a(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(a.proxy(function(c,d){var e=a(d);this.addAriaAndCollapsedClass(b(e),e)},this)).end()},d.prototype.addAriaAndCollapsedClass=function(a,b){var c=a.hasClass("in");a.attr("aria-expanded",c),b.toggleClass("collapsed",!c).attr("aria-expanded",c)};var e=a.fn.collapse;a.fn.collapse=c,a.fn.collapse.Constructor=d,a.fn.collapse.noConflict=function(){return a.fn.collapse=e,this},a(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(d){var e=a(this);e.attr("data-target")||d.preventDefault();var f=b(e),g=f.data("bs.collapse"),h=g?"toggle":e.data();c.call(f,h)})}(jQuery),+function(a){"use strict";function b(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#[A-Za-z]/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}function c(c){c&&3===c.which||(a(e).remove(),a(f).each(function(){var d=a(this),e=b(d),f={relatedTarget:this};e.hasClass("open")&&(c&&"click"==c.type&&/input|textarea/i.test(c.target.tagName)&&a.contains(e[0],c.target)||(e.trigger(c=a.Event("hide.bs.dropdown",f)),c.isDefaultPrevented()||(d.attr("aria-expanded","false"),e.removeClass("open").trigger(a.Event("hidden.bs.dropdown",f)))))}))}function d(b){return this.each(function(){var c=a(this),d=c.data("bs.dropdown");d||c.data("bs.dropdown",d=new g(this)),"string"==typeof b&&d[b].call(c)})}var e=".dropdown-backdrop",f='[data-toggle="dropdown"]',g=function(b){a(b).on("click.bs.dropdown",this.toggle)};g.VERSION="3.3.6",g.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=b(e),g=f.hasClass("open");if(c(),!g){"ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click",c);var h={relatedTarget:this};if(f.trigger(d=a.Event("show.bs.dropdown",h)),d.isDefaultPrevented())return;e.trigger("focus").attr("aria-expanded","true"),f.toggleClass("open").trigger(a.Event("shown.bs.dropdown",h))}return!1}},g.prototype.keydown=function(c){if(/(38|40|27|32)/.test(c.which)&&!/input|textarea/i.test(c.target.tagName)){var d=a(this);if(c.preventDefault(),c.stopPropagation(),!d.is(".disabled, :disabled")){var e=b(d),g=e.hasClass("open");if(!g&&27!=c.which||g&&27==c.which)return 27==c.which&&e.find(f).trigger("focus"),d.trigger("click");var h=" li:not(.disabled):visible a",i=e.find(".dropdown-menu"+h);if(i.length){var j=i.index(c.target);38==c.which&&j>0&&j--,40==c.which&&j<i.length-1&&j++,~j||(j=0),i.eq(j).trigger("focus")}}}};var h=a.fn.dropdown;a.fn.dropdown=d,a.fn.dropdown.Constructor=g,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=h,this},a(document).on("click.bs.dropdown.data-api",c).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",f,g.prototype.toggle).on("keydown.bs.dropdown.data-api",f,g.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",g.prototype.keydown)}(jQuery),+function(a){"use strict";function b(b,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},c.DEFAULTS,e.data(),"object"==typeof b&&b);f||e.data("bs.modal",f=new c(this,g)),"string"==typeof b?f[b](d):g.show&&f.show(d)})}var c=function(b,c){this.options=c,this.$body=a(document.body),this.$element=a(b),this.$dialog=this.$element.find(".modal-dialog"),this.$backdrop=null,this.isShown=null,this.originalBodyPad=null,this.scrollbarWidth=0,this.ignoreBackdropClick=!1,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,a.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))};c.VERSION="3.3.6",c.TRANSITION_DURATION=300,c.BACKDROP_TRANSITION_DURATION=150,c.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},c.prototype.toggle=function(a){return this.isShown?this.hide():this.show(a)},c.prototype.show=function(b){var d=this,e=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(e),this.isShown||e.isDefaultPrevented()||(this.isShown=!0,this.checkScrollbar(),this.setScrollbar(),this.$body.addClass("modal-open"),this.escape(),this.resize(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.$dialog.on("mousedown.dismiss.bs.modal",function(){d.$element.one("mouseup.dismiss.bs.modal",function(b){a(b.target).is(d.$element)&&(d.ignoreBackdropClick=!0)})}),this.backdrop(function(){var e=a.support.transition&&d.$element.hasClass("fade");d.$element.parent().length||d.$element.appendTo(d.$body),d.$element.show().scrollTop(0),d.adjustDialog(),e&&d.$element[0].offsetWidth,d.$element.addClass("in"),d.enforceFocus();var f=a.Event("shown.bs.modal",{relatedTarget:b});e?d.$dialog.one("bsTransitionEnd",function(){d.$element.trigger("focus").trigger(f)}).emulateTransitionEnd(c.TRANSITION_DURATION):d.$element.trigger("focus").trigger(f)}))},c.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),this.resize(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),this.$dialog.off("mousedown.dismiss.bs.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",a.proxy(this.hideModal,this)).emulateTransitionEnd(c.TRANSITION_DURATION):this.hideModal())},c.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.trigger("focus")},this))},c.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keydown.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keydown.dismiss.bs.modal")},c.prototype.resize=function(){this.isShown?a(window).on("resize.bs.modal",a.proxy(this.handleUpdate,this)):a(window).off("resize.bs.modal")},c.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.$body.removeClass("modal-open"),a.resetAdjustments(),a.resetScrollbar(),a.$element.trigger("hidden.bs.modal")})},c.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},c.prototype.backdrop=function(b){var d=this,e=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var f=a.support.transition&&e;if(this.$backdrop=a(document.createElement("div")).addClass("modal-backdrop "+e).appendTo(this.$body),this.$element.on("click.dismiss.bs.modal",a.proxy(function(a){return this.ignoreBackdropClick?void(this.ignoreBackdropClick=!1):void(a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus():this.hide()))},this)),f&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;f?this.$backdrop.one("bsTransitionEnd",b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):b()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");var g=function(){d.removeBackdrop(),b&&b()};a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):g()}else b&&b()},c.prototype.handleUpdate=function(){this.adjustDialog()},c.prototype.adjustDialog=function(){var a=this.$element[0].scrollHeight>document.documentElement.clientHeight;this.$element.css({paddingLeft:!this.bodyIsOverflowing&&a?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!a?this.scrollbarWidth:""})},c.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})},c.prototype.checkScrollbar=function(){var a=window.innerWidth;if(!a){var b=document.documentElement.getBoundingClientRect();a=b.right-Math.abs(b.left)}this.bodyIsOverflowing=document.body.clientWidth<a,this.scrollbarWidth=this.measureScrollbar()},c.prototype.setScrollbar=function(){var a=parseInt(this.$body.css("padding-right")||0,10);this.originalBodyPad=document.body.style.paddingRight||"",this.bodyIsOverflowing&&this.$body.css("padding-right",a+this.scrollbarWidth)},c.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)},c.prototype.measureScrollbar=function(){var a=document.createElement("div");a.className="modal-scrollbar-measure",this.$body.append(a);var b=a.offsetWidth-a.clientWidth;return this.$body[0].removeChild(a),b};var d=a.fn.modal;a.fn.modal=b,a.fn.modal.Constructor=c,a.fn.modal.noConflict=function(){return a.fn.modal=d,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(c){var d=a(this),e=d.attr("href"),f=a(d.attr("data-target")||e&&e.replace(/.*(?=#[^\s]+$)/,"")),g=f.data("bs.modal")?"toggle":a.extend({remote:!/#/.test(e)&&e},f.data(),d.data());d.is("a")&&c.preventDefault(),f.one("show.bs.modal",function(a){a.isDefaultPrevented()||f.one("hidden.bs.modal",function(){d.is(":visible")&&d.trigger("focus")})}),b.call(f,g,this)})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof b&&b;(e||!/destroy|hide/.test(b))&&(e||d.data("bs.tooltip",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.type=null,this.options=null,this.enabled=null,this.timeout=null,this.hoverState=null,this.$element=null,this.inState=null,this.init("tooltip",a,b)};c.VERSION="3.3.6",c.TRANSITION_DURATION=150,c.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}},c.prototype.init=function(b,c,d){if(this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d),this.$viewport=this.options.viewport&&a(a.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):this.options.viewport.selector||this.options.viewport),this.inState={click:!1,hover:!1,focus:!1},this.$element[0]instanceof document.constructor&&!this.options.selector)throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!");for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focusin",i="hover"==g?"mouseleave":"focusout";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},c.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},c.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),b instanceof a.Event&&(c.inState["focusin"==b.type?"focus":"hover"]=!0),c.tip().hasClass("in")||"in"==c.hoverState?void(c.hoverState="in"):(clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?void(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show)):c.show())},c.prototype.isInStateTrue=function(){for(var a in this.inState)if(this.inState[a])return!0;return!1},c.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),b instanceof a.Event&&(c.inState["focusout"==b.type?"focus":"hover"]=!1),c.isInStateTrue()?void 0:(clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?void(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide)):c.hide())},c.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){this.$element.trigger(b);var d=a.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]);if(b.isDefaultPrevented()||!d)return;var e=this,f=this.tip(),g=this.getUID(this.type);this.setContent(),f.attr("id",g),this.$element.attr("aria-describedby",g),this.options.animation&&f.addClass("fade");var h="function"==typeof this.options.placement?this.options.placement.call(this,f[0],this.$element[0]):this.options.placement,i=/\s?auto?\s?/i,j=i.test(h);j&&(h=h.replace(i,"")||"top"),f.detach().css({top:0,left:0,display:"block"}).addClass(h).data("bs."+this.type,this),this.options.container?f.appendTo(this.options.container):f.insertAfter(this.$element),this.$element.trigger("inserted.bs."+this.type);var k=this.getPosition(),l=f[0].offsetWidth,m=f[0].offsetHeight;if(j){var n=h,o=this.getPosition(this.$viewport);h="bottom"==h&&k.bottom+m>o.bottom?"top":"top"==h&&k.top-m<o.top?"bottom":"right"==h&&k.right+l>o.width?"left":"left"==h&&k.left-l<o.left?"right":h,f.removeClass(n).addClass(h)}var p=this.getCalculatedOffset(h,k,l,m);this.applyPlacement(p,h);var q=function(){var a=e.hoverState;e.$element.trigger("shown.bs."+e.type),e.hoverState=null,"out"==a&&e.leave(e)};a.support.transition&&this.$tip.hasClass("fade")?f.one("bsTransitionEnd",q).emulateTransitionEnd(c.TRANSITION_DURATION):q()}},c.prototype.applyPlacement=function(b,c){var d=this.tip(),e=d[0].offsetWidth,f=d[0].offsetHeight,g=parseInt(d.css("margin-top"),10),h=parseInt(d.css("margin-left"),10);isNaN(g)&&(g=0),isNaN(h)&&(h=0),b.top+=g,b.left+=h,a.offset.setOffset(d[0],a.extend({using:function(a){d.css({top:Math.round(a.top),left:Math.round(a.left)})}},b),0),d.addClass("in");var i=d[0].offsetWidth,j=d[0].offsetHeight;"top"==c&&j!=f&&(b.top=b.top+f-j);var k=this.getViewportAdjustedDelta(c,b,i,j);k.left?b.left+=k.left:b.top+=k.top;var l=/top|bottom/.test(c),m=l?2*k.left-e+i:2*k.top-f+j,n=l?"offsetWidth":"offsetHeight";d.offset(b),this.replaceArrow(m,d[0][n],l)},c.prototype.replaceArrow=function(a,b,c){this.arrow().css(c?"left":"top",50*(1-a/b)+"%").css(c?"top":"left","")},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},c.prototype.hide=function(b){function d(){"in"!=e.hoverState&&f.detach(),e.$element.removeAttr("aria-describedby").trigger("hidden.bs."+e.type),b&&b()}var e=this,f=a(this.$tip),g=a.Event("hide.bs."+this.type);return this.$element.trigger(g),g.isDefaultPrevented()?void 0:(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one("bsTransitionEnd",d).emulateTransitionEnd(c.TRANSITION_DURATION):d(),this.hoverState=null,this)},c.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},c.prototype.hasContent=function(){return this.getTitle()},c.prototype.getPosition=function(b){b=b||this.$element;var c=b[0],d="BODY"==c.tagName,e=c.getBoundingClientRect();null==e.width&&(e=a.extend({},e,{width:e.right-e.left,height:e.bottom-e.top}));var f=d?{top:0,left:0}:b.offset(),g={scroll:d?document.documentElement.scrollTop||document.body.scrollTop:b.scrollTop()},h=d?{width:a(window).width(),height:a(window).height()}:null;return a.extend({},e,g,h,f)},c.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},c.prototype.getViewportAdjustedDelta=function(a,b,c,d){var e={top:0,left:0};if(!this.$viewport)return e;var f=this.options.viewport&&this.options.viewport.padding||0,g=this.getPosition(this.$viewport);if(/right|left/.test(a)){var h=b.top-f-g.scroll,i=b.top+f-g.scroll+d;h<g.top?e.top=g.top-h:i>g.top+g.height&&(e.top=g.top+g.height-i)}else{var j=b.left-f,k=b.left+f+c;j<g.left?e.left=g.left-j:k>g.right&&(e.left=g.left+g.width-k)}return e},c.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},c.prototype.getUID=function(a){do a+=~~(1e6*Math.random());while(document.getElementById(a));return a},c.prototype.tip=function(){if(!this.$tip&&(this.$tip=a(this.options.template),1!=this.$tip.length))throw new Error(this.type+" `template` option must consist of exactly 1 top-level element!");return this.$tip},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},c.prototype.enable=function(){this.enabled=!0},c.prototype.disable=function(){this.enabled=!1},c.prototype.toggleEnabled=function(){this.enabled=!this.enabled},c.prototype.toggle=function(b){var c=this;b&&(c=a(b.currentTarget).data("bs."+this.type),c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c))),b?(c.inState.click=!c.inState.click,c.isInStateTrue()?c.enter(c):c.leave(c)):c.tip().hasClass("in")?c.leave(c):c.enter(c)},c.prototype.destroy=function(){var a=this;clearTimeout(this.timeout),this.hide(function(){a.$element.off("."+a.type).removeData("bs."+a.type),a.$tip&&a.$tip.detach(),a.$tip=null,a.$arrow=null,a.$viewport=null})};var d=a.fn.tooltip;a.fn.tooltip=b,a.fn.tooltip.Constructor=c,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=d,this}}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof b&&b;(e||!/destroy|hide/.test(b))&&(e||d.data("bs.popover",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");c.VERSION="3.3.6",c.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),c.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),c.prototype.constructor=c,c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content").children().detach().end()[this.options.html?"string"==typeof c?"html":"append":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},c.prototype.hasContent=function(){return this.getTitle()||this.getContent()},c.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")};var d=a.fn.popover;a.fn.popover=b,a.fn.popover.Constructor=c,a.fn.popover.noConflict=function(){return a.fn.popover=d,this}}(jQuery),+function(a){"use strict";function b(c,d){this.$body=a(document.body),this.$scrollElement=a(a(c).is(document.body)?window:c),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||"")+" .nav li > a",this.offsets=[],this.targets=[],this.activeTarget=null,this.scrollHeight=0,this.$scrollElement.on("scroll.bs.scrollspy",a.proxy(this.process,this)),this.refresh(),this.process()}function c(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})}b.VERSION="3.3.6",b.DEFAULTS={offset:10},b.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)},b.prototype.refresh=function(){var b=this,c="offset",d=0;this.offsets=[],this.targets=[],this.scrollHeight=this.getScrollHeight(),a.isWindow(this.$scrollElement[0])||(c="position",d=this.$scrollElement.scrollTop()),this.$body.find(this.selector).map(function(){var b=a(this),e=b.data("target")||b.attr("href"),f=/^#./.test(e)&&a(e);return f&&f.length&&f.is(":visible")&&[[f[c]().top+d,e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){b.offsets.push(this[0]),b.targets.push(this[1])})},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.getScrollHeight(),d=this.options.offset+c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(this.scrollHeight!=c&&this.refresh(),b>=d)return g!=(a=f[f.length-1])&&this.activate(a);if(g&&b<e[0])return this.activeTarget=null,this.clear();for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(void 0===e[a+1]||b<e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,this.clear();var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");
d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate.bs.scrollspy")},b.prototype.clear=function(){a(this.selector).parentsUntil(this.options.target,".active").removeClass("active")};var d=a.fn.scrollspy;a.fn.scrollspy=c,a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=d,this},a(window).on("load.bs.scrollspy.data-api",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);c.call(b,b.data())})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new c(this)),"string"==typeof b&&e[b]()})}var c=function(b){this.element=a(b)};c.VERSION="3.3.6",c.TRANSITION_DURATION=150,c.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.data("target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a"),f=a.Event("hide.bs.tab",{relatedTarget:b[0]}),g=a.Event("show.bs.tab",{relatedTarget:e[0]});if(e.trigger(f),b.trigger(g),!g.isDefaultPrevented()&&!f.isDefaultPrevented()){var h=a(d);this.activate(b.closest("li"),c),this.activate(h,h.parent(),function(){e.trigger({type:"hidden.bs.tab",relatedTarget:b[0]}),b.trigger({type:"shown.bs.tab",relatedTarget:e[0]})})}}},c.prototype.activate=function(b,d,e){function f(){g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),h?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu").length&&b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),e&&e()}var g=d.find("> .active"),h=e&&a.support.transition&&(g.length&&g.hasClass("fade")||!!d.find("> .fade").length);g.length&&h?g.one("bsTransitionEnd",f).emulateTransitionEnd(c.TRANSITION_DURATION):f(),g.removeClass("in")};var d=a.fn.tab;a.fn.tab=b,a.fn.tab.Constructor=c,a.fn.tab.noConflict=function(){return a.fn.tab=d,this};var e=function(c){c.preventDefault(),b.call(a(this),"show")};a(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',e).on("click.bs.tab.data-api",'[data-toggle="pill"]',e)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof b&&b;e||d.data("bs.affix",e=new c(this,f)),"string"==typeof b&&e[b]()})}var c=function(b,d){this.options=a.extend({},c.DEFAULTS,d),this.$target=a(this.options.target).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(b),this.affixed=null,this.unpin=null,this.pinnedOffset=null,this.checkPosition()};c.VERSION="3.3.6",c.RESET="affix affix-top affix-bottom",c.DEFAULTS={offset:0,target:window},c.prototype.getState=function(a,b,c,d){var e=this.$target.scrollTop(),f=this.$element.offset(),g=this.$target.height();if(null!=c&&"top"==this.affixed)return c>e?"top":!1;if("bottom"==this.affixed)return null!=c?e+this.unpin<=f.top?!1:"bottom":a-d>=e+g?!1:"bottom";var h=null==this.affixed,i=h?e:f.top,j=h?g:b;return null!=c&&c>=e?"top":null!=d&&i+j>=a-d?"bottom":!1},c.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(c.RESET).addClass("affix");var a=this.$target.scrollTop(),b=this.$element.offset();return this.pinnedOffset=b.top-a},c.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},c.prototype.checkPosition=function(){if(this.$element.is(":visible")){var b=this.$element.height(),d=this.options.offset,e=d.top,f=d.bottom,g=Math.max(a(document).height(),a(document.body).height());"object"!=typeof d&&(f=e=d),"function"==typeof e&&(e=d.top(this.$element)),"function"==typeof f&&(f=d.bottom(this.$element));var h=this.getState(g,b,e,f);if(this.affixed!=h){null!=this.unpin&&this.$element.css("top","");var i="affix"+(h?"-"+h:""),j=a.Event(i+".bs.affix");if(this.$element.trigger(j),j.isDefaultPrevented())return;this.affixed=h,this.unpin="bottom"==h?this.getPinnedOffset():null,this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix","affixed")+".bs.affix")}"bottom"==h&&this.$element.offset({top:g-b-f})}};var d=a.fn.affix;a.fn.affix=b,a.fn.affix.Constructor=c,a.fn.affix.noConflict=function(){return a.fn.affix=d,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var c=a(this),d=c.data();d.offset=d.offset||{},null!=d.offsetBottom&&(d.offset.bottom=d.offsetBottom),null!=d.offsetTop&&(d.offset.top=d.offsetTop),b.call(c,d)})})}(jQuery);;/*! jquery.cookie v1.4.1 | MIT */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?a(require("jquery")):a(jQuery)}(function(a){function b(a){return h.raw?a:encodeURIComponent(a)}function c(a){return h.raw?a:decodeURIComponent(a)}function d(a){return b(h.json?JSON.stringify(a):String(a))}function e(a){0===a.indexOf('"')&&(a=a.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return a=decodeURIComponent(a.replace(g," ")),h.json?JSON.parse(a):a}catch(b){}}function f(b,c){var d=h.raw?b:e(b);return a.isFunction(c)?c(d):d}var g=/\+/g,h=a.cookie=function(e,g,i){if(void 0!==g&&!a.isFunction(g)){if(i=a.extend({},h.defaults,i),"number"==typeof i.expires){var j=i.expires,k=i.expires=new Date;k.setTime(+k+864e5*j)}return document.cookie=[b(e),"=",d(g),i.expires?"; expires="+i.expires.toUTCString():"",i.path?"; path="+i.path:"",i.domain?"; domain="+i.domain:"",i.secure?"; secure":""].join("")}for(var l=e?void 0:{},m=document.cookie?document.cookie.split("; "):[],n=0,o=m.length;o>n;n++){var p=m[n].split("="),q=c(p.shift()),r=p.join("=");if(e&&e===q){l=f(r,g);break}e||void 0===(r=f(r))||(l[q]=r)}return l};h.defaults={},a.removeCookie=function(b,c){return void 0===a.cookie(b)?!1:(a.cookie(b,"",a.extend({},c,{expires:-1})),!a.cookie(b))}});;//! moment.js
//! version : 2.10.6
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
!function(a,b){"object"==typeof exports&&"undefined"!=typeof module?module.exports=b():"function"==typeof define&&define.amd?define(b):a.moment=b()}(this,function(){"use strict";function a(){return Hc.apply(null,arguments)}function b(a){Hc=a}function c(a){return"[object Array]"===Object.prototype.toString.call(a)}function d(a){return a instanceof Date||"[object Date]"===Object.prototype.toString.call(a)}function e(a,b){var c,d=[];for(c=0;c<a.length;++c)d.push(b(a[c],c));return d}function f(a,b){return Object.prototype.hasOwnProperty.call(a,b)}function g(a,b){for(var c in b)f(b,c)&&(a[c]=b[c]);return f(b,"toString")&&(a.toString=b.toString),f(b,"valueOf")&&(a.valueOf=b.valueOf),a}function h(a,b,c,d){return Ca(a,b,c,d,!0).utc()}function i(){return{empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1}}function j(a){return null==a._pf&&(a._pf=i()),a._pf}function k(a){if(null==a._isValid){var b=j(a);a._isValid=!(isNaN(a._d.getTime())||!(b.overflow<0)||b.empty||b.invalidMonth||b.invalidWeekday||b.nullInput||b.invalidFormat||b.userInvalidated),a._strict&&(a._isValid=a._isValid&&0===b.charsLeftOver&&0===b.unusedTokens.length&&void 0===b.bigHour)}return a._isValid}function l(a){var b=h(NaN);return null!=a?g(j(b),a):j(b).userInvalidated=!0,b}function m(a,b){var c,d,e;if("undefined"!=typeof b._isAMomentObject&&(a._isAMomentObject=b._isAMomentObject),"undefined"!=typeof b._i&&(a._i=b._i),"undefined"!=typeof b._f&&(a._f=b._f),"undefined"!=typeof b._l&&(a._l=b._l),"undefined"!=typeof b._strict&&(a._strict=b._strict),"undefined"!=typeof b._tzm&&(a._tzm=b._tzm),"undefined"!=typeof b._isUTC&&(a._isUTC=b._isUTC),"undefined"!=typeof b._offset&&(a._offset=b._offset),"undefined"!=typeof b._pf&&(a._pf=j(b)),"undefined"!=typeof b._locale&&(a._locale=b._locale),Jc.length>0)for(c in Jc)d=Jc[c],e=b[d],"undefined"!=typeof e&&(a[d]=e);return a}function n(b){m(this,b),this._d=new Date(null!=b._d?b._d.getTime():NaN),Kc===!1&&(Kc=!0,a.updateOffset(this),Kc=!1)}function o(a){return a instanceof n||null!=a&&null!=a._isAMomentObject}function p(a){return 0>a?Math.ceil(a):Math.floor(a)}function q(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=p(b)),c}function r(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++)(c&&a[d]!==b[d]||!c&&q(a[d])!==q(b[d]))&&g++;return g+f}function s(){}function t(a){return a?a.toLowerCase().replace("_","-"):a}function u(a){for(var b,c,d,e,f=0;f<a.length;){for(e=t(a[f]).split("-"),b=e.length,c=t(a[f+1]),c=c?c.split("-"):null;b>0;){if(d=v(e.slice(0,b).join("-")))return d;if(c&&c.length>=b&&r(e,c,!0)>=b-1)break;b--}f++}return null}function v(a){var b=null;if(!Lc[a]&&"undefined"!=typeof module&&module&&module.exports)try{b=Ic._abbr,require("./locale/"+a),w(b)}catch(c){}return Lc[a]}function w(a,b){var c;return a&&(c="undefined"==typeof b?y(a):x(a,b),c&&(Ic=c)),Ic._abbr}function x(a,b){return null!==b?(b.abbr=a,Lc[a]=Lc[a]||new s,Lc[a].set(b),w(a),Lc[a]):(delete Lc[a],null)}function y(a){var b;if(a&&a._locale&&a._locale._abbr&&(a=a._locale._abbr),!a)return Ic;if(!c(a)){if(b=v(a))return b;a=[a]}return u(a)}function z(a,b){var c=a.toLowerCase();Mc[c]=Mc[c+"s"]=Mc[b]=a}function A(a){return"string"==typeof a?Mc[a]||Mc[a.toLowerCase()]:void 0}function B(a){var b,c,d={};for(c in a)f(a,c)&&(b=A(c),b&&(d[b]=a[c]));return d}function C(b,c){return function(d){return null!=d?(E(this,b,d),a.updateOffset(this,c),this):D(this,b)}}function D(a,b){return a._d["get"+(a._isUTC?"UTC":"")+b]()}function E(a,b,c){return a._d["set"+(a._isUTC?"UTC":"")+b](c)}function F(a,b){var c;if("object"==typeof a)for(c in a)this.set(c,a[c]);else if(a=A(a),"function"==typeof this[a])return this[a](b);return this}function G(a,b,c){var d=""+Math.abs(a),e=b-d.length,f=a>=0;return(f?c?"+":"":"-")+Math.pow(10,Math.max(0,e)).toString().substr(1)+d}function H(a,b,c,d){var e=d;"string"==typeof d&&(e=function(){return this[d]()}),a&&(Qc[a]=e),b&&(Qc[b[0]]=function(){return G(e.apply(this,arguments),b[1],b[2])}),c&&(Qc[c]=function(){return this.localeData().ordinal(e.apply(this,arguments),a)})}function I(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"")}function J(a){var b,c,d=a.match(Nc);for(b=0,c=d.length;c>b;b++)Qc[d[b]]?d[b]=Qc[d[b]]:d[b]=I(d[b]);return function(e){var f="";for(b=0;c>b;b++)f+=d[b]instanceof Function?d[b].call(e,a):d[b];return f}}function K(a,b){return a.isValid()?(b=L(b,a.localeData()),Pc[b]=Pc[b]||J(b),Pc[b](a)):a.localeData().invalidDate()}function L(a,b){function c(a){return b.longDateFormat(a)||a}var d=5;for(Oc.lastIndex=0;d>=0&&Oc.test(a);)a=a.replace(Oc,c),Oc.lastIndex=0,d-=1;return a}function M(a){return"function"==typeof a&&"[object Function]"===Object.prototype.toString.call(a)}function N(a,b,c){dd[a]=M(b)?b:function(a){return a&&c?c:b}}function O(a,b){return f(dd,a)?dd[a](b._strict,b._locale):new RegExp(P(a))}function P(a){return a.replace("\\","").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e}).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}function Q(a,b){var c,d=b;for("string"==typeof a&&(a=[a]),"number"==typeof b&&(d=function(a,c){c[b]=q(a)}),c=0;c<a.length;c++)ed[a[c]]=d}function R(a,b){Q(a,function(a,c,d,e){d._w=d._w||{},b(a,d._w,d,e)})}function S(a,b,c){null!=b&&f(ed,a)&&ed[a](b,c._a,c,a)}function T(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate()}function U(a){return this._months[a.month()]}function V(a){return this._monthsShort[a.month()]}function W(a,b,c){var d,e,f;for(this._monthsParse||(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[]),d=0;12>d;d++){if(e=h([2e3,d]),c&&!this._longMonthsParse[d]&&(this._longMonthsParse[d]=new RegExp("^"+this.months(e,"").replace(".","")+"$","i"),this._shortMonthsParse[d]=new RegExp("^"+this.monthsShort(e,"").replace(".","")+"$","i")),c||this._monthsParse[d]||(f="^"+this.months(e,"")+"|^"+this.monthsShort(e,""),this._monthsParse[d]=new RegExp(f.replace(".",""),"i")),c&&"MMMM"===b&&this._longMonthsParse[d].test(a))return d;if(c&&"MMM"===b&&this._shortMonthsParse[d].test(a))return d;if(!c&&this._monthsParse[d].test(a))return d}}function X(a,b){var c;return"string"==typeof b&&(b=a.localeData().monthsParse(b),"number"!=typeof b)?a:(c=Math.min(a.date(),T(a.year(),b)),a._d["set"+(a._isUTC?"UTC":"")+"Month"](b,c),a)}function Y(b){return null!=b?(X(this,b),a.updateOffset(this,!0),this):D(this,"Month")}function Z(){return T(this.year(),this.month())}function $(a){var b,c=a._a;return c&&-2===j(a).overflow&&(b=c[gd]<0||c[gd]>11?gd:c[hd]<1||c[hd]>T(c[fd],c[gd])?hd:c[id]<0||c[id]>24||24===c[id]&&(0!==c[jd]||0!==c[kd]||0!==c[ld])?id:c[jd]<0||c[jd]>59?jd:c[kd]<0||c[kd]>59?kd:c[ld]<0||c[ld]>999?ld:-1,j(a)._overflowDayOfYear&&(fd>b||b>hd)&&(b=hd),j(a).overflow=b),a}function _(b){a.suppressDeprecationWarnings===!1&&"undefined"!=typeof console&&console.warn&&console.warn("Deprecation warning: "+b)}function aa(a,b){var c=!0;return g(function(){return c&&(_(a+"\n"+(new Error).stack),c=!1),b.apply(this,arguments)},b)}function ba(a,b){od[a]||(_(b),od[a]=!0)}function ca(a){var b,c,d=a._i,e=pd.exec(d);if(e){for(j(a).iso=!0,b=0,c=qd.length;c>b;b++)if(qd[b][1].exec(d)){a._f=qd[b][0];break}for(b=0,c=rd.length;c>b;b++)if(rd[b][1].exec(d)){a._f+=(e[6]||" ")+rd[b][0];break}d.match(ad)&&(a._f+="Z"),va(a)}else a._isValid=!1}function da(b){var c=sd.exec(b._i);return null!==c?void(b._d=new Date(+c[1])):(ca(b),void(b._isValid===!1&&(delete b._isValid,a.createFromInputFallback(b))))}function ea(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 1970>a&&h.setFullYear(a),h}function fa(a){var b=new Date(Date.UTC.apply(null,arguments));return 1970>a&&b.setUTCFullYear(a),b}function ga(a){return ha(a)?366:365}function ha(a){return a%4===0&&a%100!==0||a%400===0}function ia(){return ha(this.year())}function ja(a,b,c){var d,e=c-b,f=c-a.day();return f>e&&(f-=7),e-7>f&&(f+=7),d=Da(a).add(f,"d"),{week:Math.ceil(d.dayOfYear()/7),year:d.year()}}function ka(a){return ja(a,this._week.dow,this._week.doy).week}function la(){return this._week.dow}function ma(){return this._week.doy}function na(a){var b=this.localeData().week(this);return null==a?b:this.add(7*(a-b),"d")}function oa(a){var b=ja(this,1,4).week;return null==a?b:this.add(7*(a-b),"d")}function pa(a,b,c,d,e){var f,g=6+e-d,h=fa(a,0,1+g),i=h.getUTCDay();return e>i&&(i+=7),c=null!=c?1*c:e,f=1+g+7*(b-1)-i+c,{year:f>0?a:a-1,dayOfYear:f>0?f:ga(a-1)+f}}function qa(a){var b=Math.round((this.clone().startOf("day")-this.clone().startOf("year"))/864e5)+1;return null==a?b:this.add(a-b,"d")}function ra(a,b,c){return null!=a?a:null!=b?b:c}function sa(a){var b=new Date;return a._useUTC?[b.getUTCFullYear(),b.getUTCMonth(),b.getUTCDate()]:[b.getFullYear(),b.getMonth(),b.getDate()]}function ta(a){var b,c,d,e,f=[];if(!a._d){for(d=sa(a),a._w&&null==a._a[hd]&&null==a._a[gd]&&ua(a),a._dayOfYear&&(e=ra(a._a[fd],d[fd]),a._dayOfYear>ga(e)&&(j(a)._overflowDayOfYear=!0),c=fa(e,0,a._dayOfYear),a._a[gd]=c.getUTCMonth(),a._a[hd]=c.getUTCDate()),b=0;3>b&&null==a._a[b];++b)a._a[b]=f[b]=d[b];for(;7>b;b++)a._a[b]=f[b]=null==a._a[b]?2===b?1:0:a._a[b];24===a._a[id]&&0===a._a[jd]&&0===a._a[kd]&&0===a._a[ld]&&(a._nextDay=!0,a._a[id]=0),a._d=(a._useUTC?fa:ea).apply(null,f),null!=a._tzm&&a._d.setUTCMinutes(a._d.getUTCMinutes()-a._tzm),a._nextDay&&(a._a[id]=24)}}function ua(a){var b,c,d,e,f,g,h;b=a._w,null!=b.GG||null!=b.W||null!=b.E?(f=1,g=4,c=ra(b.GG,a._a[fd],ja(Da(),1,4).year),d=ra(b.W,1),e=ra(b.E,1)):(f=a._locale._week.dow,g=a._locale._week.doy,c=ra(b.gg,a._a[fd],ja(Da(),f,g).year),d=ra(b.w,1),null!=b.d?(e=b.d,f>e&&++d):e=null!=b.e?b.e+f:f),h=pa(c,d,e,g,f),a._a[fd]=h.year,a._dayOfYear=h.dayOfYear}function va(b){if(b._f===a.ISO_8601)return void ca(b);b._a=[],j(b).empty=!0;var c,d,e,f,g,h=""+b._i,i=h.length,k=0;for(e=L(b._f,b._locale).match(Nc)||[],c=0;c<e.length;c++)f=e[c],d=(h.match(O(f,b))||[])[0],d&&(g=h.substr(0,h.indexOf(d)),g.length>0&&j(b).unusedInput.push(g),h=h.slice(h.indexOf(d)+d.length),k+=d.length),Qc[f]?(d?j(b).empty=!1:j(b).unusedTokens.push(f),S(f,d,b)):b._strict&&!d&&j(b).unusedTokens.push(f);j(b).charsLeftOver=i-k,h.length>0&&j(b).unusedInput.push(h),j(b).bigHour===!0&&b._a[id]<=12&&b._a[id]>0&&(j(b).bigHour=void 0),b._a[id]=wa(b._locale,b._a[id],b._meridiem),ta(b),$(b)}function wa(a,b,c){var d;return null==c?b:null!=a.meridiemHour?a.meridiemHour(b,c):null!=a.isPM?(d=a.isPM(c),d&&12>b&&(b+=12),d||12!==b||(b=0),b):b}function xa(a){var b,c,d,e,f;if(0===a._f.length)return j(a).invalidFormat=!0,void(a._d=new Date(NaN));for(e=0;e<a._f.length;e++)f=0,b=m({},a),null!=a._useUTC&&(b._useUTC=a._useUTC),b._f=a._f[e],va(b),k(b)&&(f+=j(b).charsLeftOver,f+=10*j(b).unusedTokens.length,j(b).score=f,(null==d||d>f)&&(d=f,c=b));g(a,c||b)}function ya(a){if(!a._d){var b=B(a._i);a._a=[b.year,b.month,b.day||b.date,b.hour,b.minute,b.second,b.millisecond],ta(a)}}function za(a){var b=new n($(Aa(a)));return b._nextDay&&(b.add(1,"d"),b._nextDay=void 0),b}function Aa(a){var b=a._i,e=a._f;return a._locale=a._locale||y(a._l),null===b||void 0===e&&""===b?l({nullInput:!0}):("string"==typeof b&&(a._i=b=a._locale.preparse(b)),o(b)?new n($(b)):(c(e)?xa(a):e?va(a):d(b)?a._d=b:Ba(a),a))}function Ba(b){var f=b._i;void 0===f?b._d=new Date:d(f)?b._d=new Date(+f):"string"==typeof f?da(b):c(f)?(b._a=e(f.slice(0),function(a){return parseInt(a,10)}),ta(b)):"object"==typeof f?ya(b):"number"==typeof f?b._d=new Date(f):a.createFromInputFallback(b)}function Ca(a,b,c,d,e){var f={};return"boolean"==typeof c&&(d=c,c=void 0),f._isAMomentObject=!0,f._useUTC=f._isUTC=e,f._l=c,f._i=a,f._f=b,f._strict=d,za(f)}function Da(a,b,c,d){return Ca(a,b,c,d,!1)}function Ea(a,b){var d,e;if(1===b.length&&c(b[0])&&(b=b[0]),!b.length)return Da();for(d=b[0],e=1;e<b.length;++e)(!b[e].isValid()||b[e][a](d))&&(d=b[e]);return d}function Fa(){var a=[].slice.call(arguments,0);return Ea("isBefore",a)}function Ga(){var a=[].slice.call(arguments,0);return Ea("isAfter",a)}function Ha(a){var b=B(a),c=b.year||0,d=b.quarter||0,e=b.month||0,f=b.week||0,g=b.day||0,h=b.hour||0,i=b.minute||0,j=b.second||0,k=b.millisecond||0;this._milliseconds=+k+1e3*j+6e4*i+36e5*h,this._days=+g+7*f,this._months=+e+3*d+12*c,this._data={},this._locale=y(),this._bubble()}function Ia(a){return a instanceof Ha}function Ja(a,b){H(a,0,0,function(){var a=this.utcOffset(),c="+";return 0>a&&(a=-a,c="-"),c+G(~~(a/60),2)+b+G(~~a%60,2)})}function Ka(a){var b=(a||"").match(ad)||[],c=b[b.length-1]||[],d=(c+"").match(xd)||["-",0,0],e=+(60*d[1])+q(d[2]);return"+"===d[0]?e:-e}function La(b,c){var e,f;return c._isUTC?(e=c.clone(),f=(o(b)||d(b)?+b:+Da(b))-+e,e._d.setTime(+e._d+f),a.updateOffset(e,!1),e):Da(b).local()}function Ma(a){return 15*-Math.round(a._d.getTimezoneOffset()/15)}function Na(b,c){var d,e=this._offset||0;return null!=b?("string"==typeof b&&(b=Ka(b)),Math.abs(b)<16&&(b=60*b),!this._isUTC&&c&&(d=Ma(this)),this._offset=b,this._isUTC=!0,null!=d&&this.add(d,"m"),e!==b&&(!c||this._changeInProgress?bb(this,Ya(b-e,"m"),1,!1):this._changeInProgress||(this._changeInProgress=!0,a.updateOffset(this,!0),this._changeInProgress=null)),this):this._isUTC?e:Ma(this)}function Oa(a,b){return null!=a?("string"!=typeof a&&(a=-a),this.utcOffset(a,b),this):-this.utcOffset()}function Pa(a){return this.utcOffset(0,a)}function Qa(a){return this._isUTC&&(this.utcOffset(0,a),this._isUTC=!1,a&&this.subtract(Ma(this),"m")),this}function Ra(){return this._tzm?this.utcOffset(this._tzm):"string"==typeof this._i&&this.utcOffset(Ka(this._i)),this}function Sa(a){return a=a?Da(a).utcOffset():0,(this.utcOffset()-a)%60===0}function Ta(){return this.utcOffset()>this.clone().month(0).utcOffset()||this.utcOffset()>this.clone().month(5).utcOffset()}function Ua(){if("undefined"!=typeof this._isDSTShifted)return this._isDSTShifted;var a={};if(m(a,this),a=Aa(a),a._a){var b=a._isUTC?h(a._a):Da(a._a);this._isDSTShifted=this.isValid()&&r(a._a,b.toArray())>0}else this._isDSTShifted=!1;return this._isDSTShifted}function Va(){return!this._isUTC}function Wa(){return this._isUTC}function Xa(){return this._isUTC&&0===this._offset}function Ya(a,b){var c,d,e,g=a,h=null;return Ia(a)?g={ms:a._milliseconds,d:a._days,M:a._months}:"number"==typeof a?(g={},b?g[b]=a:g.milliseconds=a):(h=yd.exec(a))?(c="-"===h[1]?-1:1,g={y:0,d:q(h[hd])*c,h:q(h[id])*c,m:q(h[jd])*c,s:q(h[kd])*c,ms:q(h[ld])*c}):(h=zd.exec(a))?(c="-"===h[1]?-1:1,g={y:Za(h[2],c),M:Za(h[3],c),d:Za(h[4],c),h:Za(h[5],c),m:Za(h[6],c),s:Za(h[7],c),w:Za(h[8],c)}):null==g?g={}:"object"==typeof g&&("from"in g||"to"in g)&&(e=_a(Da(g.from),Da(g.to)),g={},g.ms=e.milliseconds,g.M=e.months),d=new Ha(g),Ia(a)&&f(a,"_locale")&&(d._locale=a._locale),d}function Za(a,b){var c=a&&parseFloat(a.replace(",","."));return(isNaN(c)?0:c)*b}function $a(a,b){var c={milliseconds:0,months:0};return c.months=b.month()-a.month()+12*(b.year()-a.year()),a.clone().add(c.months,"M").isAfter(b)&&--c.months,c.milliseconds=+b-+a.clone().add(c.months,"M"),c}function _a(a,b){var c;return b=La(b,a),a.isBefore(b)?c=$a(a,b):(c=$a(b,a),c.milliseconds=-c.milliseconds,c.months=-c.months),c}function ab(a,b){return function(c,d){var e,f;return null===d||isNaN(+d)||(ba(b,"moment()."+b+"(period, number) is deprecated. Please use moment()."+b+"(number, period)."),f=c,c=d,d=f),c="string"==typeof c?+c:c,e=Ya(c,d),bb(this,e,a),this}}function bb(b,c,d,e){var f=c._milliseconds,g=c._days,h=c._months;e=null==e?!0:e,f&&b._d.setTime(+b._d+f*d),g&&E(b,"Date",D(b,"Date")+g*d),h&&X(b,D(b,"Month")+h*d),e&&a.updateOffset(b,g||h)}function cb(a,b){var c=a||Da(),d=La(c,this).startOf("day"),e=this.diff(d,"days",!0),f=-6>e?"sameElse":-1>e?"lastWeek":0>e?"lastDay":1>e?"sameDay":2>e?"nextDay":7>e?"nextWeek":"sameElse";return this.format(b&&b[f]||this.localeData().calendar(f,this,Da(c)))}function db(){return new n(this)}function eb(a,b){var c;return b=A("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+this>+a):(c=o(a)?+a:+Da(a),c<+this.clone().startOf(b))}function fb(a,b){var c;return b=A("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+a>+this):(c=o(a)?+a:+Da(a),+this.clone().endOf(b)<c)}function gb(a,b,c){return this.isAfter(a,c)&&this.isBefore(b,c)}function hb(a,b){var c;return b=A(b||"millisecond"),"millisecond"===b?(a=o(a)?a:Da(a),+this===+a):(c=+Da(a),+this.clone().startOf(b)<=c&&c<=+this.clone().endOf(b))}function ib(a,b,c){var d,e,f=La(a,this),g=6e4*(f.utcOffset()-this.utcOffset());return b=A(b),"year"===b||"month"===b||"quarter"===b?(e=jb(this,f),"quarter"===b?e/=3:"year"===b&&(e/=12)):(d=this-f,e="second"===b?d/1e3:"minute"===b?d/6e4:"hour"===b?d/36e5:"day"===b?(d-g)/864e5:"week"===b?(d-g)/6048e5:d),c?e:p(e)}function jb(a,b){var c,d,e=12*(b.year()-a.year())+(b.month()-a.month()),f=a.clone().add(e,"months");return 0>b-f?(c=a.clone().add(e-1,"months"),d=(b-f)/(f-c)):(c=a.clone().add(e+1,"months"),d=(b-f)/(c-f)),-(e+d)}function kb(){return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")}function lb(){var a=this.clone().utc();return 0<a.year()&&a.year()<=9999?"function"==typeof Date.prototype.toISOString?this.toDate().toISOString():K(a,"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]"):K(a,"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")}function mb(b){var c=K(this,b||a.defaultFormat);return this.localeData().postformat(c)}function nb(a,b){return this.isValid()?Ya({to:this,from:a}).locale(this.locale()).humanize(!b):this.localeData().invalidDate()}function ob(a){return this.from(Da(),a)}function pb(a,b){return this.isValid()?Ya({from:this,to:a}).locale(this.locale()).humanize(!b):this.localeData().invalidDate()}function qb(a){return this.to(Da(),a)}function rb(a){var b;return void 0===a?this._locale._abbr:(b=y(a),null!=b&&(this._locale=b),this)}function sb(){return this._locale}function tb(a){switch(a=A(a)){case"year":this.month(0);case"quarter":case"month":this.date(1);case"week":case"isoWeek":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===a&&this.weekday(0),"isoWeek"===a&&this.isoWeekday(1),"quarter"===a&&this.month(3*Math.floor(this.month()/3)),this}function ub(a){return a=A(a),void 0===a||"millisecond"===a?this:this.startOf(a).add(1,"isoWeek"===a?"week":a).subtract(1,"ms")}function vb(){return+this._d-6e4*(this._offset||0)}function wb(){return Math.floor(+this/1e3)}function xb(){return this._offset?new Date(+this):this._d}function yb(){var a=this;return[a.year(),a.month(),a.date(),a.hour(),a.minute(),a.second(),a.millisecond()]}function zb(){var a=this;return{years:a.year(),months:a.month(),date:a.date(),hours:a.hours(),minutes:a.minutes(),seconds:a.seconds(),milliseconds:a.milliseconds()}}function Ab(){return k(this)}function Bb(){return g({},j(this))}function Cb(){return j(this).overflow}function Db(a,b){H(0,[a,a.length],0,b)}function Eb(a,b,c){return ja(Da([a,11,31+b-c]),b,c).week}function Fb(a){var b=ja(this,this.localeData()._week.dow,this.localeData()._week.doy).year;return null==a?b:this.add(a-b,"y")}function Gb(a){var b=ja(this,1,4).year;return null==a?b:this.add(a-b,"y")}function Hb(){return Eb(this.year(),1,4)}function Ib(){var a=this.localeData()._week;return Eb(this.year(),a.dow,a.doy)}function Jb(a){return null==a?Math.ceil((this.month()+1)/3):this.month(3*(a-1)+this.month()%3)}function Kb(a,b){return"string"!=typeof a?a:isNaN(a)?(a=b.weekdaysParse(a),"number"==typeof a?a:null):parseInt(a,10)}function Lb(a){return this._weekdays[a.day()]}function Mb(a){return this._weekdaysShort[a.day()]}function Nb(a){return this._weekdaysMin[a.day()]}function Ob(a){var b,c,d;for(this._weekdaysParse=this._weekdaysParse||[],b=0;7>b;b++)if(this._weekdaysParse[b]||(c=Da([2e3,1]).day(b),d="^"+this.weekdays(c,"")+"|^"+this.weekdaysShort(c,"")+"|^"+this.weekdaysMin(c,""),this._weekdaysParse[b]=new RegExp(d.replace(".",""),"i")),this._weekdaysParse[b].test(a))return b}function Pb(a){var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=Kb(a,this.localeData()),this.add(a-b,"d")):b}function Qb(a){var b=(this.day()+7-this.localeData()._week.dow)%7;return null==a?b:this.add(a-b,"d")}function Rb(a){return null==a?this.day()||7:this.day(this.day()%7?a:a-7)}function Sb(a,b){H(a,0,0,function(){return this.localeData().meridiem(this.hours(),this.minutes(),b)})}function Tb(a,b){return b._meridiemParse}function Ub(a){return"p"===(a+"").toLowerCase().charAt(0)}function Vb(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM"}function Wb(a,b){b[ld]=q(1e3*("0."+a))}function Xb(){return this._isUTC?"UTC":""}function Yb(){return this._isUTC?"Coordinated Universal Time":""}function Zb(a){return Da(1e3*a)}function $b(){return Da.apply(null,arguments).parseZone()}function _b(a,b,c){var d=this._calendar[a];return"function"==typeof d?d.call(b,c):d}function ac(a){var b=this._longDateFormat[a],c=this._longDateFormat[a.toUpperCase()];return b||!c?b:(this._longDateFormat[a]=c.replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1)}),this._longDateFormat[a])}function bc(){return this._invalidDate}function cc(a){return this._ordinal.replace("%d",a)}function dc(a){return a}function ec(a,b,c,d){var e=this._relativeTime[c];return"function"==typeof e?e(a,b,c,d):e.replace(/%d/i,a)}function fc(a,b){var c=this._relativeTime[a>0?"future":"past"];return"function"==typeof c?c(b):c.replace(/%s/i,b)}function gc(a){var b,c;for(c in a)b=a[c],"function"==typeof b?this[c]=b:this["_"+c]=b;this._ordinalParseLenient=new RegExp(this._ordinalParse.source+"|"+/\d{1,2}/.source)}function hc(a,b,c,d){var e=y(),f=h().set(d,b);return e[c](f,a)}function ic(a,b,c,d,e){if("number"==typeof a&&(b=a,a=void 0),a=a||"",null!=b)return hc(a,b,c,e);var f,g=[];for(f=0;d>f;f++)g[f]=hc(a,f,c,e);return g}function jc(a,b){return ic(a,b,"months",12,"month")}function kc(a,b){return ic(a,b,"monthsShort",12,"month")}function lc(a,b){return ic(a,b,"weekdays",7,"day")}function mc(a,b){return ic(a,b,"weekdaysShort",7,"day")}function nc(a,b){return ic(a,b,"weekdaysMin",7,"day")}function oc(){var a=this._data;return this._milliseconds=Wd(this._milliseconds),this._days=Wd(this._days),this._months=Wd(this._months),a.milliseconds=Wd(a.milliseconds),a.seconds=Wd(a.seconds),a.minutes=Wd(a.minutes),a.hours=Wd(a.hours),a.months=Wd(a.months),a.years=Wd(a.years),this}function pc(a,b,c,d){var e=Ya(b,c);return a._milliseconds+=d*e._milliseconds,a._days+=d*e._days,a._months+=d*e._months,a._bubble()}function qc(a,b){return pc(this,a,b,1)}function rc(a,b){return pc(this,a,b,-1)}function sc(a){return 0>a?Math.floor(a):Math.ceil(a)}function tc(){var a,b,c,d,e,f=this._milliseconds,g=this._days,h=this._months,i=this._data;return f>=0&&g>=0&&h>=0||0>=f&&0>=g&&0>=h||(f+=864e5*sc(vc(h)+g),g=0,h=0),i.milliseconds=f%1e3,a=p(f/1e3),i.seconds=a%60,b=p(a/60),i.minutes=b%60,c=p(b/60),i.hours=c%24,g+=p(c/24),e=p(uc(g)),h+=e,g-=sc(vc(e)),d=p(h/12),h%=12,i.days=g,i.months=h,i.years=d,this}function uc(a){return 4800*a/146097}function vc(a){return 146097*a/4800}function wc(a){var b,c,d=this._milliseconds;if(a=A(a),"month"===a||"year"===a)return b=this._days+d/864e5,c=this._months+uc(b),"month"===a?c:c/12;switch(b=this._days+Math.round(vc(this._months)),a){case"week":return b/7+d/6048e5;case"day":return b+d/864e5;case"hour":return 24*b+d/36e5;case"minute":return 1440*b+d/6e4;case"second":return 86400*b+d/1e3;case"millisecond":return Math.floor(864e5*b)+d;default:throw new Error("Unknown unit "+a)}}function xc(){return this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*q(this._months/12)}function yc(a){return function(){return this.as(a)}}function zc(a){return a=A(a),this[a+"s"]()}function Ac(a){return function(){return this._data[a]}}function Bc(){return p(this.days()/7)}function Cc(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d)}function Dc(a,b,c){var d=Ya(a).abs(),e=ke(d.as("s")),f=ke(d.as("m")),g=ke(d.as("h")),h=ke(d.as("d")),i=ke(d.as("M")),j=ke(d.as("y")),k=e<le.s&&["s",e]||1===f&&["m"]||f<le.m&&["mm",f]||1===g&&["h"]||g<le.h&&["hh",g]||1===h&&["d"]||h<le.d&&["dd",h]||1===i&&["M"]||i<le.M&&["MM",i]||1===j&&["y"]||["yy",j];return k[2]=b,k[3]=+a>0,k[4]=c,Cc.apply(null,k)}function Ec(a,b){return void 0===le[a]?!1:void 0===b?le[a]:(le[a]=b,!0)}function Fc(a){var b=this.localeData(),c=Dc(this,!a,b);return a&&(c=b.pastFuture(+this,c)),b.postformat(c)}function Gc(){var a,b,c,d=me(this._milliseconds)/1e3,e=me(this._days),f=me(this._months);a=p(d/60),b=p(a/60),d%=60,a%=60,c=p(f/12),f%=12;var g=c,h=f,i=e,j=b,k=a,l=d,m=this.asSeconds();return m?(0>m?"-":"")+"P"+(g?g+"Y":"")+(h?h+"M":"")+(i?i+"D":"")+(j||k||l?"T":"")+(j?j+"H":"")+(k?k+"M":"")+(l?l+"S":""):"P0D"}var Hc,Ic,Jc=a.momentProperties=[],Kc=!1,Lc={},Mc={},Nc=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,Oc=/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,Pc={},Qc={},Rc=/\d/,Sc=/\d\d/,Tc=/\d{3}/,Uc=/\d{4}/,Vc=/[+-]?\d{6}/,Wc=/\d\d?/,Xc=/\d{1,3}/,Yc=/\d{1,4}/,Zc=/[+-]?\d{1,6}/,$c=/\d+/,_c=/[+-]?\d+/,ad=/Z|[+-]\d\d:?\d\d/gi,bd=/[+-]?\d+(\.\d{1,3})?/,cd=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,dd={},ed={},fd=0,gd=1,hd=2,id=3,jd=4,kd=5,ld=6;H("M",["MM",2],"Mo",function(){return this.month()+1}),H("MMM",0,0,function(a){return this.localeData().monthsShort(this,a)}),H("MMMM",0,0,function(a){return this.localeData().months(this,a)}),z("month","M"),N("M",Wc),N("MM",Wc,Sc),N("MMM",cd),N("MMMM",cd),Q(["M","MM"],function(a,b){b[gd]=q(a)-1}),Q(["MMM","MMMM"],function(a,b,c,d){var e=c._locale.monthsParse(a,d,c._strict);null!=e?b[gd]=e:j(c).invalidMonth=a});var md="January_February_March_April_May_June_July_August_September_October_November_December".split("_"),nd="Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),od={};a.suppressDeprecationWarnings=!1;var pd=/^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,qd=[["YYYYYY-MM-DD",/[+-]\d{6}-\d{2}-\d{2}/],["YYYY-MM-DD",/\d{4}-\d{2}-\d{2}/],["GGGG-[W]WW-E",/\d{4}-W\d{2}-\d/],["GGGG-[W]WW",/\d{4}-W\d{2}/],["YYYY-DDD",/\d{4}-\d{3}/]],rd=[["HH:mm:ss.SSSS",/(T| )\d\d:\d\d:\d\d\.\d+/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],sd=/^\/?Date\((\-?\d+)/i;a.createFromInputFallback=aa("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.",function(a){a._d=new Date(a._i+(a._useUTC?" UTC":""))}),H(0,["YY",2],0,function(){return this.year()%100}),H(0,["YYYY",4],0,"year"),H(0,["YYYYY",5],0,"year"),H(0,["YYYYYY",6,!0],0,"year"),z("year","y"),N("Y",_c),N("YY",Wc,Sc),N("YYYY",Yc,Uc),N("YYYYY",Zc,Vc),N("YYYYYY",Zc,Vc),Q(["YYYYY","YYYYYY"],fd),Q("YYYY",function(b,c){c[fd]=2===b.length?a.parseTwoDigitYear(b):q(b)}),Q("YY",function(b,c){c[fd]=a.parseTwoDigitYear(b)}),a.parseTwoDigitYear=function(a){return q(a)+(q(a)>68?1900:2e3)};var td=C("FullYear",!1);H("w",["ww",2],"wo","week"),H("W",["WW",2],"Wo","isoWeek"),z("week","w"),z("isoWeek","W"),N("w",Wc),N("ww",Wc,Sc),N("W",Wc),N("WW",Wc,Sc),R(["w","ww","W","WW"],function(a,b,c,d){b[d.substr(0,1)]=q(a)});var ud={dow:0,doy:6};H("DDD",["DDDD",3],"DDDo","dayOfYear"),z("dayOfYear","DDD"),N("DDD",Xc),N("DDDD",Tc),Q(["DDD","DDDD"],function(a,b,c){c._dayOfYear=q(a)}),a.ISO_8601=function(){};var vd=aa("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548",function(){var a=Da.apply(null,arguments);return this>a?this:a}),wd=aa("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548",function(){var a=Da.apply(null,arguments);return a>this?this:a});Ja("Z",":"),Ja("ZZ",""),N("Z",ad),N("ZZ",ad),Q(["Z","ZZ"],function(a,b,c){c._useUTC=!0,c._tzm=Ka(a)});var xd=/([\+\-]|\d\d)/gi;a.updateOffset=function(){};var yd=/(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,zd=/^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/;Ya.fn=Ha.prototype;var Ad=ab(1,"add"),Bd=ab(-1,"subtract");a.defaultFormat="YYYY-MM-DDTHH:mm:ssZ";var Cd=aa("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",function(a){return void 0===a?this.localeData():this.locale(a)});H(0,["gg",2],0,function(){return this.weekYear()%100}),H(0,["GG",2],0,function(){return this.isoWeekYear()%100}),Db("gggg","weekYear"),Db("ggggg","weekYear"),Db("GGGG","isoWeekYear"),Db("GGGGG","isoWeekYear"),z("weekYear","gg"),z("isoWeekYear","GG"),N("G",_c),N("g",_c),N("GG",Wc,Sc),N("gg",Wc,Sc),N("GGGG",Yc,Uc),N("gggg",Yc,Uc),N("GGGGG",Zc,Vc),N("ggggg",Zc,Vc),R(["gggg","ggggg","GGGG","GGGGG"],function(a,b,c,d){b[d.substr(0,2)]=q(a)}),R(["gg","GG"],function(b,c,d,e){c[e]=a.parseTwoDigitYear(b)}),H("Q",0,0,"quarter"),z("quarter","Q"),N("Q",Rc),Q("Q",function(a,b){b[gd]=3*(q(a)-1)}),H("D",["DD",2],"Do","date"),z("date","D"),N("D",Wc),N("DD",Wc,Sc),N("Do",function(a,b){return a?b._ordinalParse:b._ordinalParseLenient}),Q(["D","DD"],hd),Q("Do",function(a,b){b[hd]=q(a.match(Wc)[0],10)});var Dd=C("Date",!0);H("d",0,"do","day"),H("dd",0,0,function(a){return this.localeData().weekdaysMin(this,a)}),H("ddd",0,0,function(a){return this.localeData().weekdaysShort(this,a)}),H("dddd",0,0,function(a){return this.localeData().weekdays(this,a)}),H("e",0,0,"weekday"),H("E",0,0,"isoWeekday"),z("day","d"),z("weekday","e"),z("isoWeekday","E"),N("d",Wc),N("e",Wc),N("E",Wc),N("dd",cd),N("ddd",cd),N("dddd",cd),R(["dd","ddd","dddd"],function(a,b,c){var d=c._locale.weekdaysParse(a);null!=d?b.d=d:j(c).invalidWeekday=a}),R(["d","e","E"],function(a,b,c,d){b[d]=q(a)});var Ed="Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),Fd="Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),Gd="Su_Mo_Tu_We_Th_Fr_Sa".split("_");H("H",["HH",2],0,"hour"),H("h",["hh",2],0,function(){return this.hours()%12||12}),Sb("a",!0),Sb("A",!1),z("hour","h"),N("a",Tb),N("A",Tb),N("H",Wc),N("h",Wc),N("HH",Wc,Sc),N("hh",Wc,Sc),Q(["H","HH"],id),Q(["a","A"],function(a,b,c){c._isPm=c._locale.isPM(a),c._meridiem=a}),Q(["h","hh"],function(a,b,c){b[id]=q(a),j(c).bigHour=!0});var Hd=/[ap]\.?m?\.?/i,Id=C("Hours",!0);H("m",["mm",2],0,"minute"),z("minute","m"),N("m",Wc),N("mm",Wc,Sc),Q(["m","mm"],jd);var Jd=C("Minutes",!1);H("s",["ss",2],0,"second"),z("second","s"),N("s",Wc),N("ss",Wc,Sc),Q(["s","ss"],kd);var Kd=C("Seconds",!1);H("S",0,0,function(){return~~(this.millisecond()/100)}),H(0,["SS",2],0,function(){return~~(this.millisecond()/10)}),H(0,["SSS",3],0,"millisecond"),H(0,["SSSS",4],0,function(){return 10*this.millisecond()}),H(0,["SSSSS",5],0,function(){return 100*this.millisecond()}),H(0,["SSSSSS",6],0,function(){return 1e3*this.millisecond()}),H(0,["SSSSSSS",7],0,function(){return 1e4*this.millisecond()}),H(0,["SSSSSSSS",8],0,function(){return 1e5*this.millisecond()}),H(0,["SSSSSSSSS",9],0,function(){return 1e6*this.millisecond()}),z("millisecond","ms"),N("S",Xc,Rc),N("SS",Xc,Sc),N("SSS",Xc,Tc);var Ld;for(Ld="SSSS";Ld.length<=9;Ld+="S")N(Ld,$c);for(Ld="S";Ld.length<=9;Ld+="S")Q(Ld,Wb);var Md=C("Milliseconds",!1);H("z",0,0,"zoneAbbr"),H("zz",0,0,"zoneName");var Nd=n.prototype;Nd.add=Ad,Nd.calendar=cb,Nd.clone=db,Nd.diff=ib,Nd.endOf=ub,Nd.format=mb,Nd.from=nb,Nd.fromNow=ob,Nd.to=pb,Nd.toNow=qb,Nd.get=F,Nd.invalidAt=Cb,Nd.isAfter=eb,Nd.isBefore=fb,Nd.isBetween=gb,Nd.isSame=hb,Nd.isValid=Ab,Nd.lang=Cd,Nd.locale=rb,Nd.localeData=sb,Nd.max=wd,Nd.min=vd,Nd.parsingFlags=Bb,Nd.set=F,Nd.startOf=tb,Nd.subtract=Bd,Nd.toArray=yb,Nd.toObject=zb,Nd.toDate=xb,Nd.toISOString=lb,Nd.toJSON=lb,Nd.toString=kb,Nd.unix=wb,Nd.valueOf=vb,Nd.year=td,Nd.isLeapYear=ia,Nd.weekYear=Fb,Nd.isoWeekYear=Gb,Nd.quarter=Nd.quarters=Jb,Nd.month=Y,Nd.daysInMonth=Z,Nd.week=Nd.weeks=na,Nd.isoWeek=Nd.isoWeeks=oa,Nd.weeksInYear=Ib,Nd.isoWeeksInYear=Hb,Nd.date=Dd,Nd.day=Nd.days=Pb,Nd.weekday=Qb,Nd.isoWeekday=Rb,Nd.dayOfYear=qa,Nd.hour=Nd.hours=Id,Nd.minute=Nd.minutes=Jd,Nd.second=Nd.seconds=Kd,
Nd.millisecond=Nd.milliseconds=Md,Nd.utcOffset=Na,Nd.utc=Pa,Nd.local=Qa,Nd.parseZone=Ra,Nd.hasAlignedHourOffset=Sa,Nd.isDST=Ta,Nd.isDSTShifted=Ua,Nd.isLocal=Va,Nd.isUtcOffset=Wa,Nd.isUtc=Xa,Nd.isUTC=Xa,Nd.zoneAbbr=Xb,Nd.zoneName=Yb,Nd.dates=aa("dates accessor is deprecated. Use date instead.",Dd),Nd.months=aa("months accessor is deprecated. Use month instead",Y),Nd.years=aa("years accessor is deprecated. Use year instead",td),Nd.zone=aa("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779",Oa);var Od=Nd,Pd={sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},Qd={LTS:"h:mm:ss A",LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY h:mm A",LLLL:"dddd, MMMM D, YYYY h:mm A"},Rd="Invalid date",Sd="%d",Td=/\d{1,2}/,Ud={future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},Vd=s.prototype;Vd._calendar=Pd,Vd.calendar=_b,Vd._longDateFormat=Qd,Vd.longDateFormat=ac,Vd._invalidDate=Rd,Vd.invalidDate=bc,Vd._ordinal=Sd,Vd.ordinal=cc,Vd._ordinalParse=Td,Vd.preparse=dc,Vd.postformat=dc,Vd._relativeTime=Ud,Vd.relativeTime=ec,Vd.pastFuture=fc,Vd.set=gc,Vd.months=U,Vd._months=md,Vd.monthsShort=V,Vd._monthsShort=nd,Vd.monthsParse=W,Vd.week=ka,Vd._week=ud,Vd.firstDayOfYear=ma,Vd.firstDayOfWeek=la,Vd.weekdays=Lb,Vd._weekdays=Ed,Vd.weekdaysMin=Nb,Vd._weekdaysMin=Gd,Vd.weekdaysShort=Mb,Vd._weekdaysShort=Fd,Vd.weekdaysParse=Ob,Vd.isPM=Ub,Vd._meridiemParse=Hd,Vd.meridiem=Vb,w("en",{ordinalParse:/\d{1,2}(th|st|nd|rd)/,ordinal:function(a){var b=a%10,c=1===q(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";return a+c}}),a.lang=aa("moment.lang is deprecated. Use moment.locale instead.",w),a.langData=aa("moment.langData is deprecated. Use moment.localeData instead.",y);var Wd=Math.abs,Xd=yc("ms"),Yd=yc("s"),Zd=yc("m"),$d=yc("h"),_d=yc("d"),ae=yc("w"),be=yc("M"),ce=yc("y"),de=Ac("milliseconds"),ee=Ac("seconds"),fe=Ac("minutes"),ge=Ac("hours"),he=Ac("days"),ie=Ac("months"),je=Ac("years"),ke=Math.round,le={s:45,m:45,h:22,d:26,M:11},me=Math.abs,ne=Ha.prototype;ne.abs=oc,ne.add=qc,ne.subtract=rc,ne.as=wc,ne.asMilliseconds=Xd,ne.asSeconds=Yd,ne.asMinutes=Zd,ne.asHours=$d,ne.asDays=_d,ne.asWeeks=ae,ne.asMonths=be,ne.asYears=ce,ne.valueOf=xc,ne._bubble=tc,ne.get=zc,ne.milliseconds=de,ne.seconds=ee,ne.minutes=fe,ne.hours=ge,ne.days=he,ne.weeks=Bc,ne.months=ie,ne.years=je,ne.humanize=Fc,ne.toISOString=Gc,ne.toString=Gc,ne.toJSON=Gc,ne.locale=rb,ne.localeData=sb,ne.toIsoString=aa("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",Gc),ne.lang=Cd,H("X",0,0,"unix"),H("x",0,0,"valueOf"),N("x",_c),N("X",bd),Q("X",function(a,b,c){c._d=new Date(1e3*parseFloat(a,10))}),Q("x",function(a,b,c){c._d=new Date(q(a))}),a.version="2.10.6",b(Da),a.fn=Od,a.min=Fa,a.max=Ga,a.utc=h,a.unix=Zb,a.months=jc,a.isDate=d,a.locale=w,a.invalid=l,a.duration=Ya,a.isMoment=o,a.weekdays=lc,a.parseZone=$b,a.localeData=y,a.isDuration=Ia,a.monthsShort=kc,a.weekdaysMin=nc,a.defineLocale=x,a.weekdaysShort=mc,a.normalizeUnits=A,a.relativeTimeThreshold=Ec;var oe=a;return oe});;!function(){"use strict";angular.module("as.sortable",[]).constant("sortableConfig",{itemClass:"as-sortable-item",handleClass:"as-sortable-item-handle",placeHolderClass:"as-sortable-placeholder",dragClass:"as-sortable-drag",hiddenClass:"as-sortable-hidden",dragging:"as-sortable-dragging"})}(),function(){"use strict";var mainModule=angular.module("as.sortable");mainModule.factory("$helper",["$document","$window",function($document,$window){return{height:function(element){return element[0].getBoundingClientRect().height},width:function(element){return element[0].getBoundingClientRect().width},offset:function(element,scrollableContainer){var boundingClientRect=element[0].getBoundingClientRect();return scrollableContainer||(scrollableContainer=$document[0].documentElement),{width:boundingClientRect.width||element.prop("offsetWidth"),height:boundingClientRect.height||element.prop("offsetHeight"),top:boundingClientRect.top+($window.pageYOffset||scrollableContainer.scrollTop-scrollableContainer.offsetTop),left:boundingClientRect.left+($window.pageXOffset||scrollableContainer.scrollLeft-scrollableContainer.offsetLeft)}},eventObj:function(event){var obj=event;return void 0!==event.targetTouches?obj=event.targetTouches.item(0):void 0!==event.originalEvent&&void 0!==event.originalEvent.targetTouches&&(obj=event.originalEvent.targetTouches.item(0)),obj},isTouchInvalid:function(event){var touchInvalid=!1;return void 0!==event.touches&&event.touches.length>1?touchInvalid=!0:void 0!==event.originalEvent&&void 0!==event.originalEvent.touches&&event.originalEvent.touches.length>1&&(touchInvalid=!0),touchInvalid},positionStarted:function(event,target,scrollableContainer){var pos={};return pos.offsetX=event.pageX-this.offset(target,scrollableContainer).left,pos.offsetY=event.pageY-this.offset(target,scrollableContainer).top,pos.startX=pos.lastX=event.pageX,pos.startY=pos.lastY=event.pageY,pos.nowX=pos.nowY=pos.distX=pos.distY=pos.dirAx=0,pos.dirX=pos.dirY=pos.lastDirX=pos.lastDirY=pos.distAxX=pos.distAxY=0,pos},calculatePosition:function(pos,event){pos.lastX=pos.nowX,pos.lastY=pos.nowY,pos.nowX=event.pageX,pos.nowY=event.pageY,pos.distX=pos.nowX-pos.lastX,pos.distY=pos.nowY-pos.lastY,pos.lastDirX=pos.dirX,pos.lastDirY=pos.dirY,pos.dirX=0===pos.distX?0:pos.distX>0?1:-1,pos.dirY=0===pos.distY?0:pos.distY>0?1:-1;var newAx=Math.abs(pos.distX)>Math.abs(pos.distY)?1:0;pos.dirAx!==newAx?(pos.distAxX=0,pos.distAxY=0):(pos.distAxX+=Math.abs(pos.distX),0!==pos.dirX&&pos.dirX!==pos.lastDirX&&(pos.distAxX=0),pos.distAxY+=Math.abs(pos.distY),0!==pos.dirY&&pos.dirY!==pos.lastDirY&&(pos.distAxY=0)),pos.dirAx=newAx},movePosition:function(event,element,pos,container,containerPositioning,scrollableContainer){var bounds,useRelative="relative"===containerPositioning;element.x=event.pageX-pos.offsetX,element.y=event.pageY-pos.offsetY,container&&(bounds=this.offset(container,scrollableContainer),useRelative&&(element.x-=bounds.left,element.y-=bounds.top,bounds.left=0,bounds.top=0),element.x<bounds.left?element.x=bounds.left:element.x>=bounds.width+bounds.left-this.offset(element).width&&(element.x=bounds.width+bounds.left-this.offset(element).width),element.y<bounds.top?element.y=bounds.top:element.y>=bounds.height+bounds.top-this.offset(element).height&&(element.y=bounds.height+bounds.top-this.offset(element).height)),element.css({left:element.x+"px",top:element.y+"px"}),this.calculatePosition(pos,event)},dragItem:function(item){return{index:item.index(),parent:item.sortableScope,source:item,targetElement:null,targetElementOffset:null,sourceInfo:{index:item.index(),itemScope:item.itemScope,sortableScope:item.sortableScope},canMove:function(itemPosition,targetElement,targetElementOffset){return this.targetElement!==targetElement?(this.targetElement=targetElement,this.targetElementOffset=targetElementOffset,!0):itemPosition.dirX*(targetElementOffset.left-this.targetElementOffset.left)>0||itemPosition.dirY*(targetElementOffset.top-this.targetElementOffset.top)>0?(this.targetElementOffset=targetElementOffset,!0):!1},moveTo:function(parent,index){this.parent=parent,this.isSameParent()&&this.source.index()<index&&!this.sourceInfo.sortableScope.cloning&&(index-=1),this.index=index},isSameParent:function(){return this.parent.element===this.sourceInfo.sortableScope.element},isOrderChanged:function(){return this.index!==this.sourceInfo.index},eventArgs:function(){return{source:this.sourceInfo,dest:{index:this.index,sortableScope:this.parent}}},apply:function(){this.sourceInfo.sortableScope.cloning?this.parent.options.clone||this.parent.insertItem(this.index,angular.copy(this.source.modelValue)):(this.sourceInfo.sortableScope.removeItem(this.sourceInfo.index),(this.parent.options.allowDuplicates||this.parent.modelValue.indexOf(this.source.modelValue)<0)&&this.parent.insertItem(this.index,this.source.modelValue))}}},noDrag:function(element){return void 0!==element.attr("no-drag")||void 0!==element.attr("data-no-drag")},findAncestor:function(el,selector){el=el[0];for(var matches=Element.matches||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector||Element.prototype.webkitMatchesSelector;(el=el.parentElement)&&!matches.call(el,selector););return el?angular.element(el):angular.element(document.body)}}}])}(),function(){"use strict";var mainModule=angular.module("as.sortable");mainModule.controller("as.sortable.sortableController",["$scope",function($scope){this.scope=$scope,$scope.modelValue=null,$scope.callbacks=null,$scope.type="sortable",$scope.options={longTouch:!1},$scope.isDisabled=!1,$scope.insertItem=function(index,itemData){$scope.options.allowDuplicates?$scope.modelValue.splice(index,0,angular.copy(itemData)):$scope.modelValue.splice(index,0,itemData)},$scope.removeItem=function(index){var removedItem=null;return index>-1&&(removedItem=$scope.modelValue.splice(index,1)[0]),removedItem},$scope.isEmpty=function(){return $scope.modelValue&&0===$scope.modelValue.length},$scope.accept=function(sourceItemHandleScope,destScope,destItemScope){return $scope.callbacks.accept(sourceItemHandleScope,destScope,destItemScope)}}]),mainModule.directive("asSortable",function(){return{require:"ngModel",restrict:"A",scope:!0,controller:"as.sortable.sortableController",link:function(scope,element,attrs,ngModelController){var ngModel,callbacks;ngModel=ngModelController,ngModel&&(ngModel.$render=function(){scope.modelValue=ngModel.$modelValue},scope.element=element,element.data("_scope",scope),callbacks={accept:null,orderChanged:null,itemMoved:null,dragStart:null,dragMove:null,dragCancel:null,dragEnd:null},callbacks.accept=function(sourceItemHandleScope,destSortableScope,destItemScope){return!0},callbacks.orderChanged=function(event){},callbacks.itemMoved=function(event){},callbacks.dragStart=function(event){},callbacks.dragMove=angular.noop,callbacks.dragCancel=function(event){},callbacks.dragEnd=function(event){},scope.$watch(attrs.asSortable,function(newVal,oldVal){angular.forEach(newVal,function(value,key){callbacks[key]?"function"==typeof value&&(callbacks[key]=value):scope.options[key]=value}),scope.callbacks=callbacks},!0),angular.isDefined(attrs.isDisabled)&&scope.$watch(attrs.isDisabled,function(newVal,oldVal){angular.isUndefined(newVal)||(scope.isDisabled=newVal)},!0))}}})}(),function(){"use strict";function isParent(possibleParent,elem){return elem&&"HTML"!==elem.nodeName?elem.parentNode===possibleParent?!0:isParent(possibleParent,elem.parentNode):!1}var mainModule=angular.module("as.sortable");mainModule.controller("as.sortable.sortableItemHandleController",["$scope",function($scope){this.scope=$scope,$scope.itemScope=null,$scope.type="handle"}]),mainModule.directive("asSortableItemHandle",["sortableConfig","$helper","$window","$document","$timeout",function(sortableConfig,$helper,$window,$document,$timeout){return{require:"^asSortableItem",scope:!0,restrict:"A",controller:"as.sortable.sortableItemHandleController",link:function(scope,element,attrs,itemController){function insertBefore(targetElement,targetScope){"table-row"!==placeHolder.css("display")&&placeHolder.css("display","block"),targetScope.sortableScope.options.clone||(targetElement[0].parentNode.insertBefore(placeHolder[0],targetElement[0]),dragItemInfo.moveTo(targetScope.sortableScope,targetScope.index()))}function insertAfter(targetElement,targetScope){"table-row"!==placeHolder.css("display")&&placeHolder.css("display","block"),targetScope.sortableScope.options.clone||(targetElement.after(placeHolder),dragItemInfo.moveTo(targetScope.sortableScope,targetScope.index()+1))}function fetchScope(element){for(var scope;!scope&&element.length;)scope=element.data("_scope"),scope||(element=element.parent());return scope}function rollbackDragChanges(){scope.itemScope.sortableScope.cloning||placeElement.replaceWith(scope.itemScope.element),placeHolder.remove(),dragElement.remove(),dragElement=null,dragHandled=!1,containment.css("cursor",""),containment.removeClass("as-sortable-un-selectable")}var dragElement,placeHolder,placeElement,itemPosition,dragItemInfo,containment,containerPositioning,dragListen,scrollableContainer,dragStart,dragMove,dragEnd,dragCancel,isDraggable,placeHolderIndex,bindDrag,unbindDrag,bindEvents,unBindEvents,hasTouch,isIOS,longTouchStart,longTouchCancel,longTouchTimer,dragHandled,createPlaceholder,isPlaceHolderPresent,escapeListen,isDisabled=!1,isLongTouch=!1;hasTouch="ontouchstart"in $window,isIOS=/iPad|iPhone|iPod/.test($window.navigator.userAgent)&&!$window.MSStream,sortableConfig.handleClass&&element.addClass(sortableConfig.handleClass),scope.itemScope=itemController.scope,element.data("_scope",scope),scope.$watchGroup(["sortableScope.isDisabled","sortableScope.options.longTouch"],function(newValues){isDisabled!==newValues[0]?(isDisabled=newValues[0],isDisabled?unbindDrag():bindDrag()):isLongTouch!==newValues[1]?(isLongTouch=newValues[1],unbindDrag(),bindDrag()):bindDrag()}),scope.$on("$destroy",function(){angular.element($document[0].body).unbind("keydown",escapeListen)}),createPlaceholder=function(itemScope){return"function"==typeof scope.sortableScope.options.placeholder?angular.element(scope.sortableScope.options.placeholder(itemScope)):"string"==typeof scope.sortableScope.options.placeholder?angular.element(scope.sortableScope.options.placeholder):angular.element($document[0].createElement(itemScope.element.prop("tagName")))},dragListen=function(event){var startPosition,unbindMoveListen=function(){angular.element($document).unbind("mousemove",moveListen),angular.element($document).unbind("touchmove",moveListen),element.unbind("mouseup",unbindMoveListen),element.unbind("touchend",unbindMoveListen),element.unbind("touchcancel",unbindMoveListen)},moveListen=function(e){e.preventDefault();var eventObj=$helper.eventObj(e);startPosition||(startPosition={clientX:eventObj.clientX,clientY:eventObj.clientY}),Math.abs(eventObj.clientX-startPosition.clientX)+Math.abs(eventObj.clientY-startPosition.clientY)>10&&(unbindMoveListen(),dragStart(event))};angular.element($document).bind("mousemove",moveListen),angular.element($document).bind("touchmove",moveListen),element.bind("mouseup",unbindMoveListen),element.bind("touchend",unbindMoveListen),element.bind("touchcancel",unbindMoveListen),event.stopPropagation()},dragStart=function(event){var eventObj,tagName;(hasTouch||2!==event.button&&3!==event.which)&&(hasTouch&&$helper.isTouchInvalid(event)||!dragHandled&&isDraggable(event)&&(dragHandled=!0,event.preventDefault(),eventObj=$helper.eventObj(event),scope.sortableScope=scope.sortableScope||scope.itemScope.sortableScope,scope.callbacks=scope.callbacks||scope.itemScope.callbacks,scope.itemScope.sortableScope.options.clone||scope.itemScope.sortableScope.options.ctrlClone&&event.ctrlKey?scope.itemScope.sortableScope.cloning=!0:scope.itemScope.sortableScope.cloning=!1,scrollableContainer=angular.element($document[0].querySelector(scope.sortableScope.options.scrollableContainer)).length>0?$document[0].querySelector(scope.sortableScope.options.scrollableContainer):$document[0].documentElement,containment=scope.sortableScope.options.containment?$helper.findAncestor(element,scope.sortableScope.options.containment):angular.element($document[0].body),containment.css("cursor","move"),containment.css("cursor","-webkit-grabbing"),containment.css("cursor","-moz-grabbing"),containment.addClass("as-sortable-un-selectable"),containerPositioning=scope.sortableScope.options.containerPositioning||"absolute",dragItemInfo=$helper.dragItem(scope),tagName=scope.itemScope.element.prop("tagName"),dragElement=angular.element($document[0].createElement(scope.sortableScope.element.prop("tagName"))).addClass(scope.sortableScope.element.attr("class")).addClass(sortableConfig.dragClass),dragElement.css("width",$helper.width(scope.itemScope.element)+"px"),dragElement.css("height",$helper.height(scope.itemScope.element)+"px"),placeHolder=createPlaceholder(scope.itemScope).addClass(sortableConfig.placeHolderClass).addClass(scope.sortableScope.options.additionalPlaceholderClass),placeHolder.css("width",$helper.width(scope.itemScope.element)+"px"),placeHolder.css("height",$helper.height(scope.itemScope.element)+"px"),placeElement=angular.element($document[0].createElement(tagName)),sortableConfig.hiddenClass&&placeElement.addClass(sortableConfig.hiddenClass),itemPosition=$helper.positionStarted(eventObj,scope.itemScope.element,scrollableContainer),scope.itemScope.sortableScope.options.clone||scope.itemScope.element.after(placeHolder),scope.itemScope.sortableScope.cloning?dragElement.append(scope.itemScope.element.clone()):(scope.itemScope.element.after(placeElement),dragElement.append(scope.itemScope.element)),containment.append(dragElement),$helper.movePosition(eventObj,dragElement,itemPosition,containment,containerPositioning,scrollableContainer),scope.sortableScope.$apply(function(){scope.callbacks.dragStart(dragItemInfo.eventArgs())}),bindEvents()))},isDraggable=function(event){var elementClicked,sourceScope,isDraggable;for(elementClicked=angular.element(event.target),sourceScope=fetchScope(elementClicked),isDraggable=sourceScope&&"handle"===sourceScope.type;isDraggable&&elementClicked[0]!==element[0];)$helper.noDrag(elementClicked)&&(isDraggable=!1),elementClicked=elementClicked.parent();return isDraggable},dragMove=function(event){var eventObj,targetX,targetY,targetScope,targetElement;if((!hasTouch||!$helper.isTouchInvalid(event))&&dragHandled&&dragElement){if(event.preventDefault(),eventObj=$helper.eventObj(event),scope.callbacks.dragMove!==angular.noop&&scope.sortableScope.$apply(function(){scope.callbacks.dragMove(itemPosition,containment,eventObj)}),targetX=eventObj.pageX-$document[0].documentElement.scrollLeft,targetY=eventObj.pageY-($window.pageYOffset||$document[0].documentElement.scrollTop),dragElement.addClass(sortableConfig.hiddenClass),targetElement=angular.element($document[0].elementFromPoint(targetX,targetY)),dragElement.removeClass(sortableConfig.hiddenClass),$helper.movePosition(eventObj,dragElement,itemPosition,containment,containerPositioning,scrollableContainer),dragElement.addClass(sortableConfig.dragging),targetScope=fetchScope(targetElement),!targetScope||!targetScope.type)return;if("handle"===targetScope.type&&(targetScope=targetScope.itemScope),"item"!==targetScope.type&&"sortable"!==targetScope.type)return;if("item"===targetScope.type&&targetScope.accept(scope,targetScope.sortableScope,targetScope)){targetElement=targetScope.element;var targetElementOffset=$helper.offset(targetElement,scrollableContainer);if(!dragItemInfo.canMove(itemPosition,targetElement,targetElementOffset))return;var placeholderIndex=placeHolderIndex(targetScope.sortableScope.element);0>placeholderIndex?insertBefore(targetElement,targetScope):placeholderIndex<=targetScope.index()?insertAfter(targetElement,targetScope):insertBefore(targetElement,targetScope)}"sortable"===targetScope.type&&targetScope.accept(scope,targetScope)&&!isParent(targetScope.element[0],targetElement[0])&&(isPlaceHolderPresent(targetElement)||targetScope.options.clone||(targetElement[0].appendChild(placeHolder[0]),dragItemInfo.moveTo(targetScope,targetScope.modelValue.length)))}},placeHolderIndex=function(targetElement){var itemElements,i;if(targetElement.hasClass(sortableConfig.placeHolderClass))return 0;for(itemElements=targetElement.children(),i=0;i<itemElements.length;i+=1)if(angular.element(itemElements[i]).hasClass(sortableConfig.placeHolderClass))return i;return-1},isPlaceHolderPresent=function(targetElement){return placeHolderIndex(targetElement)>=0},dragEnd=function(event){dragHandled&&(event.preventDefault(),dragElement&&(rollbackDragChanges(),dragItemInfo.apply(),scope.sortableScope.$apply(function(){dragItemInfo.isSameParent()?dragItemInfo.isOrderChanged()&&scope.callbacks.orderChanged(dragItemInfo.eventArgs()):scope.callbacks.itemMoved(dragItemInfo.eventArgs())}),scope.sortableScope.$apply(function(){scope.callbacks.dragEnd(dragItemInfo.eventArgs())}),dragItemInfo=null),unBindEvents())},dragCancel=function(event){dragHandled&&(event.preventDefault(),dragElement&&(rollbackDragChanges(),scope.sortableScope.$apply(function(){scope.callbacks.dragCancel(dragItemInfo.eventArgs())}),dragItemInfo=null),unBindEvents())},bindDrag=function(){hasTouch&&(isLongTouch?isIOS?(element.bind("touchstart",longTouchStart),element.bind("touchend",longTouchCancel),element.bind("touchmove",longTouchCancel)):element.bind("contextmenu",dragListen):element.bind("touchstart",dragListen)),element.bind("mousedown",dragListen)},unbindDrag=function(){element.unbind("touchstart",longTouchStart),element.unbind("touchend",longTouchCancel),element.unbind("touchmove",longTouchCancel),element.unbind("contextmenu",dragListen),element.unbind("touchstart",dragListen),element.unbind("mousedown",dragListen)},longTouchStart=function(event){longTouchTimer=$timeout(function(){dragListen(event)},500)},longTouchCancel=function(){$timeout.cancel(longTouchTimer)},escapeListen=function(event){27===event.keyCode&&dragCancel(event)},angular.element($document[0].body).bind("keydown",escapeListen),bindEvents=function(){angular.element($document).bind("touchmove",dragMove),angular.element($document).bind("touchend",dragEnd),angular.element($document).bind("touchcancel",dragCancel),angular.element($document).bind("mousemove",dragMove),angular.element($document).bind("mouseup",dragEnd)},unBindEvents=function(){angular.element($document).unbind("touchend",dragEnd),angular.element($document).unbind("touchcancel",dragCancel),angular.element($document).unbind("touchmove",dragMove),angular.element($document).unbind("mouseup",dragEnd),angular.element($document).unbind("mousemove",dragMove)}}}}])}(),function(){"use strict";var mainModule=angular.module("as.sortable");mainModule.controller("as.sortable.sortableItemController",["$scope",function($scope){this.scope=$scope,$scope.sortableScope=null,$scope.modelValue=null,$scope.type="item",$scope.index=function(){return $scope.$index},$scope.itemData=function(){return $scope.sortableScope.modelValue[$scope.$index]}}]),mainModule.directive("asSortableItem",["sortableConfig",function(sortableConfig){return{require:["^asSortable","?ngModel"],restrict:"A",controller:"as.sortable.sortableItemController",link:function(scope,element,attrs,ctrl){var sortableController=ctrl[0],ngModelController=ctrl[1];sortableConfig.itemClass&&element.addClass(sortableConfig.itemClass),scope.sortableScope=sortableController.scope,ngModelController?ngModelController.$render=function(){scope.modelValue=ngModelController.$modelValue}:scope.modelValue=sortableController.scope.modelValue[scope.$index],scope.element=element,element.data("_scope",scope)}}}])}();;/*
 * AngularJS Toaster
 * Version: 2.0.0
 *
 * Copyright 2013-2016 Jiri Kavulak.
 * All Rights Reserved.
 * Use, reproduction, distribution, and modification of this code is subject to the terms and
 * conditions of the MIT license, available at http://www.opensource.org/licenses/mit-license.php
 *
 * Author: Jiri Kavulak
 * Related to project of John Papa, Hans Fjällemark and Nguyễn Thiện Hùng (thienhung1989)
 */
!function(t,e){"use strict";angular.module("toaster",[]).constant("toasterConfig",{limit:0,"tap-to-dismiss":!0,"close-button":!1,"close-html":'<button class="toast-close-button" type="button">&times;</button>',"newest-on-top":!0,"time-out":5e3,"icon-classes":{error:"toast-error",info:"toast-info",wait:"toast-wait",success:"toast-success",warning:"toast-warning"},"body-output-type":"","body-template":"toasterBodyTmpl.html","icon-class":"toast-info","position-class":"toast-top-right","title-class":"toast-title","message-class":"toast-message","prevent-duplicates":!1,"mouseover-timer-stop":!0}).service("toaster",["$rootScope","toasterConfig",function(t,e){function o(t){return function(e,o,s,i,a,n,r,c,l){return angular.isString(e)?this.pop(t,e,o,s,i,a,n,r,c,l):this.pop(angular.extend(e,{type:t}))}}var s=function(){var t={};return t.newGuid=function(){return"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(t){var e=16*Math.random()|0,o="x"==t?e:3&e|8;return o.toString(16)})},t}();this.pop=function(e,o,i,a,n,r,c,l,u,d){if(angular.isObject(e)){var m=e;this.toast={type:m.type,title:m.title,body:m.body,timeout:m.timeout,bodyOutputType:m.bodyOutputType,clickHandler:m.clickHandler,showCloseButton:m.showCloseButton,closeHtml:m.closeHtml,toastId:m.toastId,onShowCallback:m.onShowCallback,onHideCallback:m.onHideCallback,directiveData:m.directiveData},c=m.toasterId}else this.toast={type:e,title:o,body:i,timeout:a,bodyOutputType:n,clickHandler:r,showCloseButton:l,toastId:u,onHideCallback:d};return this.toast.toastId&&this.toast.toastId.length||(this.toast.toastId=s.newGuid()),t.$emit("toaster-newToast",c,this.toast.toastId),{toasterId:c,toastId:this.toast.toastId}},this.clear=function(e,o){angular.isObject(e)?t.$emit("toaster-clearToasts",e.toasterId,e.toastId):t.$emit("toaster-clearToasts",e,o)};for(var i in e["icon-classes"])this[i]=o(i)}]).factory("toasterEventRegistry",["$rootScope",function(t){var e,o=null,s=null,i=[],a=[];return e={setup:function(){o||(o=t.$on("toaster-newToast",function(t,e,o){for(var s=0,a=i.length;a>s;s++)i[s](t,e,o)})),s||(s=t.$on("toaster-clearToasts",function(t,e,o){for(var s=0,i=a.length;i>s;s++)a[s](t,e,o)}))},subscribeToNewToastEvent:function(t){i.push(t)},subscribeToClearToastsEvent:function(t){a.push(t)},unsubscribeToNewToastEvent:function(t){var e=i.indexOf(t);e>=0&&i.splice(e,1),0===i.length&&(o(),o=null)},unsubscribeToClearToastsEvent:function(t){var e=a.indexOf(t);e>=0&&a.splice(e,1),0===a.length&&(s(),s=null)}},{setup:e.setup,subscribeToNewToastEvent:e.subscribeToNewToastEvent,subscribeToClearToastsEvent:e.subscribeToClearToastsEvent,unsubscribeToNewToastEvent:e.unsubscribeToNewToastEvent,unsubscribeToClearToastsEvent:e.unsubscribeToClearToastsEvent}}]).directive("directiveTemplate",["$compile","$injector",function(t,e){return{restrict:"A",scope:{directiveName:"@directiveName",directiveData:"@directiveData"},replace:!0,link:function(o,s,i){o.$watch("directiveName",function(a){if(angular.isUndefined(a)||a.length<=0)throw new Error("A valid directive name must be provided via the toast body argument when using bodyOutputType: directive");var n;try{n=e.get(i.$normalize(a)+"Directive")}catch(r){throw new Error(a+" could not be found. The name should appear as it exists in the markup, not camelCased as it would appear in the directive declaration, e.g. directive-name not directiveName.")}var c=n[0];if(c.scope!==!0&&c.scope)throw new Error("Cannot use a directive with an isolated scope. The scope must be either true or falsy (e.g. false/null/undefined). Occurred for directive "+a+".");if(c.restrict.indexOf("A")<0)throw new Error('Directives must be usable as attributes. Add "A" to the restrict option (or remove the option entirely). Occurred for directive '+a+".");o.directiveData&&(o.directiveData=angular.fromJson(o.directiveData));var l=t("<div "+a+"></div>")(o);s.append(l)})}}}]).directive("toasterContainer",["$parse","$rootScope","$interval","$sce","toasterConfig","toaster","toasterEventRegistry",function(t,e,o,s,i,a,n){return{replace:!0,restrict:"EA",scope:!0,link:function(e,r,c){function l(t,s){t.timeoutPromise=o(function(){e.removeToast(t.toastId)},s,1)}function u(o,i){if(o.type=v["icon-classes"][o.type],o.type||(o.type=v["icon-class"]),v["prevent-duplicates"]===!0&&e.toasters.length){if(e.toasters[e.toasters.length-1].body===o.body)return;var a,n,r=!1;for(a=0,n=e.toasters.length;n>a;a++)if(e.toasters[a].toastId===i){r=!0;break}if(r)return}var c=v["close-button"];if("boolean"==typeof o.showCloseButton);else if("boolean"==typeof c)o.showCloseButton=c;else if("object"==typeof c){var l=c[o.type];"undefined"!=typeof l&&null!==l&&(o.showCloseButton=l)}else o.showCloseButton=!1;switch(o.showCloseButton&&(o.closeHtml=s.trustAsHtml(o.closeHtml||e.config.closeHtml)),o.bodyOutputType=o.bodyOutputType||v["body-output-type"],o.bodyOutputType){case"trustedHtml":o.html=s.trustAsHtml(o.body);break;case"template":o.bodyTemplate=o.body||v["body-template"];break;case"templateWithData":var u=t(o.body||v["body-template"]),d=u(e);o.bodyTemplate=d.template,o.data=d.data;break;case"directive":o.html=o.body}e.configureTimer(o),v["newest-on-top"]===!0?(e.toasters.unshift(o),v.limit>0&&e.toasters.length>v.limit&&e.toasters.pop()):(e.toasters.push(o),v.limit>0&&e.toasters.length>v.limit&&e.toasters.shift()),angular.isFunction(o.onShowCallback)&&o.onShowCallback()}function d(t){var s=e.toasters[t];s.timeoutPromise&&o.cancel(s.timeoutPromise),e.toasters.splice(t,1),angular.isFunction(s.onHideCallback)&&s.onHideCallback()}function m(t){for(var o=e.toasters.length-1;o>=0;o--)p(t)?d(o):e.toasters[o].toastId==t&&d(o)}function p(t){return angular.isUndefined(t)||null===t}var v;v=angular.extend({},i,e.$eval(c.toasterOptions)),e.config={toasterId:v["toaster-id"],position:v["position-class"],title:v["title-class"],message:v["message-class"],tap:v["tap-to-dismiss"],closeButton:v["close-button"],closeHtml:v["close-html"],animation:v["animation-class"],mouseoverTimer:v["mouseover-timer-stop"]},e.$on("$destroy",function(){n.unsubscribeToNewToastEvent(e._onNewToast),n.unsubscribeToClearToastsEvent(e._onClearToasts)}),e.configureTimer=function(t){var e=angular.isNumber(t.timeout)?t.timeout:v["time-out"];"object"==typeof e&&(e=e[t.type]),e>0&&l(t,e)},e.removeToast=function(t){var o,s;for(o=0,s=e.toasters.length;s>o;o++)if(e.toasters[o].toastId===t){d(o);break}},e.toasters=[],e._onNewToast=function(t,o,s){(p(e.config.toasterId)&&p(o)||!p(e.config.toasterId)&&!p(o)&&e.config.toasterId==o)&&u(a.toast,s)},e._onClearToasts=function(t,o,s){("*"==o||p(e.config.toasterId)&&p(o)||!p(e.config.toasterId)&&!p(o)&&e.config.toasterId==o)&&m(s)},n.setup(),n.subscribeToNewToastEvent(e._onNewToast),n.subscribeToClearToastsEvent(e._onClearToasts)},controller:["$scope","$element","$attrs",function(t,e,s){t.stopTimer=function(e){t.config.mouseoverTimer===!0&&e.timeoutPromise&&(o.cancel(e.timeoutPromise),e.timeoutPromise=null)},t.restartTimer=function(e){t.config.mouseoverTimer===!0?e.timeoutPromise||t.configureTimer(e):null===e.timeoutPromise&&t.removeToast(e.toastId)},t.click=function(e,o){if(t.config.tap===!0||e.showCloseButton===!0&&o===!0){var s=!0;e.clickHandler&&(angular.isFunction(e.clickHandler)?s=e.clickHandler(e,o):angular.isFunction(t.$parent.$eval(e.clickHandler))?s=t.$parent.$eval(e.clickHandler)(e,o):console.log("TOAST-NOTE: Your click handler is not inside a parent scope of toaster-container.")),s&&t.removeToast(e.toastId)}}}],template:'<div id="toast-container" ng-class="[config.position, config.animation]"><div ng-repeat="toaster in toasters" class="toast" ng-class="toaster.type" ng-click="click(toaster)" ng-mouseover="stopTimer(toaster)" ng-mouseout="restartTimer(toaster)"><div ng-if="toaster.showCloseButton" ng-click="click(toaster, true)" ng-bind-html="toaster.closeHtml"></div><div ng-class="config.title">{{toaster.title}}</div><div ng-class="config.message" ng-switch on="toaster.bodyOutputType"><div ng-switch-when="trustedHtml" ng-bind-html="toaster.html"></div><div ng-switch-when="template"><div ng-include="toaster.bodyTemplate"></div></div><div ng-switch-when="templateWithData"><div ng-include="toaster.bodyTemplate"></div></div><div ng-switch-when="directive"><div directive-template directive-name="{{toaster.html}}" directive-data="{{toaster.directiveData}}"></div></div><div ng-switch-default >{{toaster.body}}</div></div></div></div>'}}])}(window,document);;$.fn.pressEnter = function(fn) {

    return this.each(function() {
        $(this).bind('enterPress', fn);
        $(this).keyup(function(e){
            if(e.keyCode == 13)
            {
                $(this).trigger("enterPress");
            }
        })
    });
};


function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

(function($) {
	$.QueryString = (function(a) {
		if (a == "") return {};
		var b = {};
		for (var i = 0; i < a.length; ++i)
		{
			var p=a[i].split('=');
			if (p.length != 2) continue;
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	})(window.location.search.substr(1).split('&'))
})(jQuery);

var NO_CACHE = false;
function isCache(){
    NO_CACHE = $.QueryString["nocache"];
}

//isCache();
$(document).ready(isCache);

function tagSearchClick(tag){
  var platformBool = $("#"+tag+"Platform").is(':checked');
  var platform = 1;
  if (platformBool) platform = 2;

  var name = $("#"+tag+"Name").val();
  localStorage.setItem(tag+"platform", platform);
  localStorage.setItem(tag+"Tag", name);

  if (platform < 1 || platform > 2)
    return false;
  if (name == null || name.trim().length == 0)
    return false;
  
  var dest = tag;
  if (dest=="layout") dest = "tabs";
  window.location.href = "/"+dest+"/" + platform + "/" + encodeURIComponent(name);
  return false; 
}

function tagSetupSearch(tag){
  var savedPlatform = localStorage.getItem(tag+"platform");
  var savedGt = localStorage.getItem(tag+"Tag");
  if (savedPlatform==2){
    $("#"+tag+"Platform").prop('checked', true).change();
  }
  if (savedGt){
    $("#"+tag+"Name").val(savedGt);
  }
  var searchClick = tagSearchClick.bind(undefined, tag);
  $("#"+tag+"ViewBtn").bind("click", searchClick);
  $('#'+tag+'Name').pressEnter(searchClick);
}



function searchNavBarClick() {
    var platformBool = $("#layoutPlatform").is(':checked');
    var platform = 1;
    if (platformBool) platform = 2;

    var name = $("#layoutName").val();
    localStorage.setItem("navplatform", platform);
    localStorage.setItem("navTag", name);

    console.log(platform+" "+name);
    if (platform < 1 || platform > 2)
        return false;
    if (name == null || name.trim().length == 0)
        return false;
    window.location.href = "/tabs/" + platform + "/" + encodeURIComponent(name);
    return false;
}


function setupSpecificSearch(){
  if (PATH){
    tagSetupSearch(PATH);
  }
}

function setupSearch(){
    var savedPlatform = localStorage.getItem("navplatform");
    var savedGt = localStorage.getItem("navTag");
    if (savedPlatform==2){
        $("#layoutPlatform").prop('checked', true).change();
    }
    if (savedGt){
        $("#layoutName").val(savedGt);
    }
    $("#layoutViewBtn").bind("click", searchNavBarClick);
    $('#layoutName').pressEnter(searchNavBarClick);
}

function setupCss(){
	$(".cssSelector").click ( function() {
	    var name = $(this).attr ( "data-name" );
	    var url = $(this).attr ( "data-url" );
	    changeTheme(name, url);
	});
}

//used for Leaderboard only since it's static
function initTheme(){
	var storedTheme = $.cookie('csstheme');

	if (storedTheme){
		var curTheme = $('#csstheme').attr('href');
		if (storedTheme != curTheme) {
			var storedName = $.cookie('cssname');
			changeTheme(storedName, storedTheme);
		}	
	}
	
}

function changeTheme(name, newCss) {
	var curTheme = $('#csstheme').attr('href');
	if (curTheme != newCss) {
		console.log(curTheme + "->" + newCss);
		$('#csstheme').attr('href', newCss);
        $.cookie('csstheme', newCss, { expires: 365*10, path: '/' });
        $.cookie('cssname', name, { expires: 365*10, path: '/' });
	}
	
	$(".cssSelector").each ( function(i, e) {
		e = $(e);
	    var cssUrl = e.attr ( "data-url" );
	    if (cssUrl==newCss){
	    	console.log("Selected "+newCss);
	    	e.parent().addClass("active");
	    }
	    else{
	    	e.parent().removeClass("active");
	    }
	});

	
};

(function(i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function() {
		(i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o), m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', GOOG_ANAL, 'auto');
ga('send', 'pageview');

function trunc(num){
	if (num==null) return 0;
	if (isNaN(num)) return 0;
	return Number(Number(num).toFixed(0));
}


$(document).ready(setupSearch);
$(document).ready(setupSpecificSearch);
$(document).ready(initMe);
$(document).ready(setupCss);;var cfApp = angular.module('cfApp', ['ngSanitize'])

  .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {

    $scope.data = {
      loading: true,
      sortType: 'info.number'
    };

    $scope.buildIgnUrl = function (card) {
      return "http://www.ign.com/wikis/destiny/Calcified_Fragments#" + card.numeral;
    };

    $scope.filterItem = function (item) {
      if ($scope.data.hideComplete) return !item.complete;
      return true;
    };
    ReadService.load(PLAYER_PLATFORM, MEMBER_ID, $scope);
    console.log("loaded");
  }]);

cfApp.service("ReadService", ["$http", "$q", function ($http, $q) {
  return ({
    load: load
  });

  function load(platform, memberId, $scope) {
    var request = $http({
      method: "get",
      url: "/api/cf/" + platform + "/" + memberId,
      params: {nocache: NO_CACHE}
    });
    (request.then(function (response) {
      if (!response.data.chars) {
        $scope.data.errormsg = "Problem loading data from Bungie";
        $scope.data.loading = false;
        return;
      }
      $scope.data.chars = response.data.chars;
      if ($scope.data.chars.length >= 1) $scope.data.selectedChar = $scope.data.chars[0];
      if ($scope.data.selectedChar.privacy == true) {
        $scope.data.errormsg = " This user has selected to hide parts of their data on Bungie.net which prevents this page from being properly displayed." +
          " If you are the user in question, you can fix this by: Going to Bungie.net, clicking the settings gear icon, " +
          "clicking 'Privacy' and checking the 'Show My Advisors' box.";
      }
      $scope.data.loading = false;

    }, handleError));
  }

  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return ($q.reject("An unknown error occurred."));
    }
    return ($q.reject(response.data));
  }

}]);;var checkRaidLeaderApp = angular.module('checkRaidLeaderApp', ['ngSanitize'])

  .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {

    $scope.data = {
      loading: false,
      name: null,
      errormsg: null
    };

    $scope.searchPlayer = function () {

      var platformBool = $("#checkPlatform").is(':checked');
      var platform = 1;
      if (platformBool) platform = 2;
      if (!$scope.data.name || $scope.data.name.trim().length==0) return;
      console.log(platform+" "+$scope.data.name);
      ReadService.load(platform, $scope.data.name, $scope);
    }

    console.log("loaded");
  }]);

checkRaidLeaderApp.service("ReadService", ["$http", "$q", function ($http, $q) {
  return ({
    load: load
  });

  function load(platform, name, $scope) {
    $scope.data.loading = true;
    $scope.data.errormsg = "";
    var request = $http({
      method: "get",
      url: "/api/checkRaidLeader/" + platform + "/" + encodeURIComponent(name),
      params: {nocache: true}
    });
    (request.then(function (response) {
      if (!response.data.chars) {
        if (response.data.message){

          $scope.data.errormsg = response.data.message;
        }
        else{
          $scope.data.errormsg = "Problem loading data from Bungie";
        }
        $scope.data.loading = false;
        return;
      }
      $scope.data.chars = response.data.chars;
      $scope.data.loading = false;
      console.log(JSON.stringify(response.data));
    }, handleError));
  }

  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return ($q.reject("An unknown error occurred."));
    }
    return ($q.reject(response.data));
  }

}]);;function addFav(platform, name) {
    var cookieString = localStorage.getItem("favorites");
    var favs = parseFavs(cookieString);
    for (var cntr = 0; cntr < favs.length; cntr++) {
        if (favs[cntr].name == name) {
            return false;
        }
    }
    favs.push({
        "platform": platform,
        "name": name
    });
    cookieString = serializeFavs(favs);
    localStorage.setItem("favorites", cookieString);
    return true;
}

function serializeFavs(favs) {
    var returnMe = "";
    for (var cntr = 0; cntr < favs.length; cntr++) {
        returnMe += favs[cntr].platform + "^#^" + favs[cntr].name + "|^|";
    }
    return returnMe;
}

function parseFavs(favs) {
    if (favs == null)
        return [];
    if (favs.trim().length == 0)
        return [];
    var listFavs = favs.split("|^|");
    var returnMe = [];
    for (var cntr = 0; cntr < listFavs.length; cntr++) {
        var fav = listFavs[cntr];
        if (fav.trim().length == 0)
            continue;
        var aFav = fav.split("^#^");
        returnMe.push({
            "platform": aFav[0],
            "name": aFav[1]
        });
    }
    return returnMe;
}
;

var grimApp = angular.module('grimApp', ['ngSanitize'])

    .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {

        $scope.data = {
            loading: true,
            sortType: "pctToNextRank",
            sortReverse: true,
            showComplete: false
        };
        
        $scope.getStyle = function(card){
            if (card.stats.length>0) 
                return {
                "cursor": "zoom-in"
                };
            else
                return {};
        }
        $scope.toggleRow = function(card){
            if (card.stats.length==0) return;
            
            var showing = $('#'+card.id).is(":visible");
            var parRowId = "row" + card.id;

            if (!showing){
                $("#" + parRowId).show();
                $('#'+card.id).collapse('show');
            }
            else{
                $("#" + parRowId).hide();
                $('#'+card.id).collapse('hide');
            }
        }

        $scope.filterItem = function (item) {
            if (!$scope.data.showComplete) return !item.complete;
            return true;
        };


        ReadService.load(PLAYER_PLATFORM, MEMBER_ID, $scope);
        console.log("loaded");
    }]);

grimApp.service("ReadService", ["$http", "$q", function ($http, $q) {
    return ({
        load: load
    });

    function load(platform, memberId, $scope) {
        var request = $http({
            method: "get",
            url: "/api/grim/" + platform + "/" + memberId,
            params: {nocache: NO_CACHE}
        });
        (request.then(function (response) {
            if (!response.data.cards) {
                $scope.data.errormsg = "Problem loading data from Bungie";
                $scope.data.loading = false;
                return;
            }
            $scope.data.cards = response.data.cards;
            $scope.data.totals = response.data.totals;
            $scope.data.loading = false;
            initMe();
        }, handleError));
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
        }
        return ($q.reject(response.data));
    }

}]);;var gunsmithApp = angular.module('gunsmithApp', ['ngSanitize'])
  .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {
    isCache();
    $scope.data = {
      loading: true
    };
    $scope.getTestClass = function(tests){
      if (tests==0) return "danger";
      if (tests>=5) return "success";
      return "warning";
    };
    
    $scope.buildNeonLink = function(char){
      var console = PLAYER_PLATFORM == 1 ? "xb" : "ps";
      var url = "http://destiny.neonblack.moe/reputation/#" + console + "=" + encodeURIComponent(PLAYER_NAME) + 
        "&" + char.id + "=2335631936";
      return url;

    };

    $scope.getProgressClass = function(progress){
      var returnMe = {
        'progress-bar': true
      };
      if (progress<25){
        returnMe["progress-bar-danger"] = true;
      }
      else if (progress<75){
        returnMe["progress-bar-warning"] = true;
      }
      else
        returnMe["progress-bar-success"] = true;
      
      return returnMe;
    };
    function getRankProgressText(progress){
      return (progress).toFixed(0)+"%";
    };
    $scope.getRankProgressText = getRankProgressText;

    $scope.getRankProgressStyle = function(progress){
      return {
        'min-width': '2em',
        'width': getRankProgressText(progress)
      };
    };
    
    $scope.launchCopyModal = function(){
      if (!FOUNDRY_ROLLS) return;
      var text = "___\n\n";
      FOUNDRY_ROLLS.forEach(function(gun){
        text+="**"+gun.name+" - "+gun.itemTypeName+"**\n\n";
        gun.damageAndSteps.forEach(function(roll, index, arr){
          text+=(index+1)+". ";
          roll.steps.forEach(function(step, index, arr){
            step.forEach(function(node, index,arr){
              text+=node;
              if (index<(arr.length-1)){
                text+=" / "
              }
            });
            if (index<(arr.length-1)){
              text+=" - "
            }
          });
          text+="\n";
        });
        text+="\n";
        text+="___\n";
      });
      $scope.data.redditRolls = text;
      $('#copyModal').modal();
    }
    if (PLAYER_PLATFORM>0)
      ReadService.load(PLAYER_PLATFORM, MEMBER_ID, PLAYER_NAME, $scope);
    
    console.log("loaded");
  }]);

gunsmithApp.service("ReadService", ["$http", "$q", function ($http, $q) {
  return ({
    load: load
  });

  function load(platform, memberId, name, $scope) {
    $http({
      method: "get",
      url: "/api/gunsmith/" + platform + "/" + memberId + "/" + encodeURIComponent(name),
      params: {nocache: NO_CACHE}
    }).then(function (response) {
      $scope.data.loading = false;
      if (!response.data) {
        $scope.data.errormsg = "Problem loading data from Bungie";
        return;
      }
      if (response.data.error) {
        $scope.data.errormsg = response.data.error;
        return;
      }
      $scope.data.chars = response.data.chars;
      if ($scope.data.chars.length==0) return;
      if ($scope.data.chars[0].privacy == true) {
        $scope.data.errormsg = " This user has selected to hide parts of their data on Bungie.net which prevents this page from being properly displayed." +
          " If you are the user in question, you can fix this by: Going to Bungie.net, clicking the settings gear icon, " +
          "clicking 'Privacy' and checking the 'Show My Advisors' box.";
      }
      response.data.chars.forEach(function(char){
        var faction = char.progression.gunsmithProg.progress;
        char.progress = 100*(faction.progressToNextLevel / faction.nextLevelAt);
      });

    }, handleError);
  }

  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return ($q.reject("An unknown error occurred."));
    }
    return ($q.reject(response.data));
  }

}]);;var ibApp = angular.module('ibApp', ['ui.bootstrap-slider'])
    
    .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {
      tagSetupSearch()
        var lastRankXp, nextRankXp;
        var weekday = new Array(7);
        weekday[0] = "Tue";
        weekday[1] = "Wed";
        weekday[2] = "Thu";
        weekday[3] = "Fri";
        weekday[4] = "Sat";
        weekday[5] = "Sun";
        weekday[6] = "Mon";

        var btyWeekday = new Array(7);
        btyWeekday[0] = "Skip";
        btyWeekday[1] = "Tue";
        btyWeekday[2] = "Wed";
        btyWeekday[3] = "Thu";
        btyWeekday[4] = "Fri";
        btyWeekday[5] = "Sat";
        btyWeekday[6] = "Sun";
        btyWeekday[7] = "Mon";

        function getRankInfo(xp) {
            var nextRank, lastRank, rank, pctToNextRank;
            if (xp > 8499) {
                rank = 5;
                lastRankXp = 8500;
                nextRankXp = 0;
            }
            else if (xp > 6099) {
                rank = 4;
                lastRankXp = 6100;
                nextRankXp = 8500;
            }
            else if (xp > 3699) {
                rank = 3;
                lastRankXp = 3700;
                nextRankXp = 6100;
            }
            else if (xp > 1299) {
                rank = 2;
                lastRankXp = 1300;
                nextRankXp = 3700;
            }
            else if (xp > 99) {
                rank = 1;
                lastRankXp = 100;
                nextRankXp = 1300;
            }
            else {
                rank = 0;
                lastRankXp = 0;
                nextRankXp = 100;
            }
            var nextRank;
            if (nextRankXp > 0) {
                pctToNextRank = parseInt((100 * (xp - lastRankXp) / (nextRankXp - lastRankXp)).toFixed(0));
                nextRank = rank + 1;
            }
            else {
                //handle "you're done" case
                pctToNextRank = 100;
                lastRankXp = 6100;
                nextRankXp = 8500;
                nextRank = 5;
            }

            var pctToTotal = parseInt((100 * (xp / 8500)).toFixed(0));

            return {
                xp: xp,
                lastRankXp: lastRankXp,
                nextRankXp: nextRankXp,
                rank: rank,
                rankProgress: xp - lastRankXp,
                rankTotal: nextRankXp - lastRankXp,
                nextRank: nextRank,
                pctToNextRank: pctToNextRank,
                pctToTotal: pctToTotal,
                pctToTotalStyle: {
                    width: pctToTotal + '%'
                },
                pctToNextRankStyle: {
                    width: pctToNextRank + '%'
                }
            };

        };
        
        var WIN_VAL = 250;
        var MED_VAL = 150;

        function buildRow(iDay, startXp, data) {
            var startRank = $scope.getRank(startXp);
            
            var temperBonus = 0; //$scope.getTemper(iDay);
            var nfBonus = 0;
            var altBonus = 0;
            var gearBonus = data.calculated.gearBonus;
            if (startRank < data.calculated.maxAlt && data.calculated.maxAlt > 1)
                altBonus = 1;
            if ($scope.hasNightfallBonus()){
              nfBonus = 0.25;
            }
            
            var totalBonus = (1 + gearBonus) * (1 + temperBonus) * (1 + altBonus) * (1+nfBonus);
            var winValue = parseInt((WIN_VAL * totalBonus).toFixed(0));
            //var bountyValue = parseInt((125 * totalBonus).toFixed(0));
            var medallionValue = parseInt((MED_VAL * totalBonus).toFixed(0));
            var wkBtyValue = parseInt((750 * totalBonus).toFixed(0));
            var matchCnt = data.assumptions.matches;
            var btyCnt = data.assumptions.bounty;
            if (data.assumptions.advanced.overrideDays[iDay].match != null) matchCnt = data.assumptions.advanced.overrideDays[iDay].match;
            if (data.assumptions.advanced.overrideDays[iDay].bounty != null) btyCnt = data.assumptions.advanced.overrideDays[iDay].bounty;
            var wkBtyCnt = 0;
            if ((data.assumptions.wkBty1-1) == iDay) wkBtyCnt++;
            if ((data.assumptions.wkBty2-1) == iDay) wkBtyCnt++;
            if ((data.assumptions.wkBty3-1) == iDay) wkBtyCnt++;
            if ((data.assumptions.wkBty4-1) == iDay) wkBtyCnt++;
            
            var winPct = data.assumptions.winPct / 100;

            var winCnt = matchCnt * winPct;
            var medCnt = matchCnt * (1 - winPct);
            var winXp = winCnt * winValue;
            var medXp = medCnt * medallionValue;
            //var btyXp = btyCnt * bountyValue;
            var btyXp = 0;
            var wkBtyXp = wkBtyCnt * wkBtyValue;
            data.calculated.totalWeeklyBountyXp+=wkBtyXp;
            
            var finalXp = startXp + winXp + medXp + btyXp + wkBtyXp;
            //if (finalXp > 8500) finalXp = 8500;
            var final = getRankInfo(finalXp);

            var rankMsg = null;
            if (final.rank == 5 && startRank!=5) {
                data.calculated.lastDay = iDay;
                rankMsg = "Complete as of "+weekday[iDay];
                data.calculated.finalMsg = rankMsg;
            }
            else if (final.rank==5){
                rankMsg = data.calculated.finalMsg;
            }
            else {
                rankMsg = final.pctToNextRank + "% of the way to Rank " + (final.rank + 1);
            }
            data.calculated.totalMatches += matchCnt;
            data.calculated.totalBounties += btyCnt;
            var row = {
                day: weekday[iDay],
                rank: {
                    rank: final.rank,
                    msg: rankMsg
                },
                bonus: {
                    gear: gearBonus,
                    temper: temperBonus,
                    alt: altBonus,
                    total: totalBonus-1
                },
                value: {
                    win: winValue,
                    //bounty: bountyValue,
                    medallion: medallionValue,
                    wkBty: wkBtyValue
                },
                earned: {
                    win: {
                        cnt: winCnt.toFixed(1),
                        val: winXp
                    },
                    bounty: {
                        cnt: btyCnt.toFixed(1),
                        val: btyXp
                    },
                    wkBounty: {
                        cnt: wkBtyCnt,
                        val: wkBtyXp
                    },
                    med: {
                        cnt: medCnt.toFixed(1),
                        val: medXp
                    }
                }
            };
            
            data.calculated.table.push(row);
            return finalXp;

        };

        isCache();

        var dayOfWeek = new Date().getDay() - 2;
        if (dayOfWeek < -1)
            dayOfWeek = 5;

        if (dayOfWeek < 0)
            dayOfWeek = 6;

        $scope.showBountyTab = function(){
            $('.nav-tabs a[href="#bounty"]').tab('show');
        }

        $scope.calcBounties = function(data){
            if (!data.selectedChar || !data.selectedChar.publicAdv) return;
            var btys = data.selectedChar.publicAdv.ib.bounties;
            var totalWkVal =  btys.length * data.calculated.table[0].value.wkBty;
            var cmpWkly=0;
            
            for (var cntr=0; cntr<btys.length; cntr++){
                if (btys[cntr].isFullyComplete){
                    cmpWkly++;
                }
            }
            var cmpXpWkly = cmpWkly * data.calculated.table[0].value.wkBty;

            var curXP =  data.calculated.current.xp;
            var isDone=false;
            if (curXP==8500){
                isDone=true;
            }
            
            var hasCompleted = cmpWkly>0;
            var hasHeld = (data.selectedChar.publicAdv.ib.bounties.length)>0;
            var moreForCompleted = (8500-(curXP+cmpXpWkly));
            var moreForHeld = (8500-(curXP+totalWkVal));
            
            console.log(hasCompleted + " "+ hasHeld +" " + moreForCompleted+ " " +moreForHeld);

            data.calculated.btyInfo = {
                completed: {
                    weekly: cmpWkly,
                    weeklyXp: cmpXpWkly
                },
                rank5: {
                    done: isDone,
                    hasCompleted: hasCompleted,
                    hasHeld: hasHeld,
                    moreForCompleted: moreForCompleted,
                    moreForHeld: moreForHeld
                }
            };

        }

        $scope.neededForR5 = function(data){
            var curXP =  data.calculated.current.xp;
            if (curXP>=8500) return 0;
            var btys = data.selectedChar.publicAdv.ib.bounties;
            
            for (var cntr=0; cntr<btys.length; cntr++){
                if (btys[cntr].isFullyComplete){

                }
            }

            var wkVal =  data.selectedChar.publicAdv.ib.bounties.length * data.calculated.table[0].value.wkBty;
            var total = curXP + wkVal;
            return 8500-total;
        }

        $scope.getRank = function (xp) {
            if (xp > 8499)
                return 5;
            else if (xp > 6099)
                return 4;
            else if (xp > 3699)
                return 3;
            else if (xp > 1299)
                return 2;
            else if (xp > 99)
                return 1;
            else
                return 0;

        }

        //$scope.getTemper = function (iDay) {
        //    if (iDay == 0)
        //        return .1;
        //    else if (iDay == 1)
        //        return .15;
        //    else if (iDay == 2)
        //        return .25;
        //    else if (iDay == 3)
        //        return .4;
        //    else if (iDay == 4)
        //        return .6;
        //    else if (iDay == 5)
        //        return 1.0;
        //    else if (iDay == 6)
        //        return 1.5;
        //    else
        //        throw "Day " + iDay + " makes no sense";
        //
        //};

        $scope.calcTable = function () {
            console.log("Calc table");
            $scope.data.calculated = {
                totalMatches: 0,
                totalBounties: 0,
                totalWeeklyBountyXp: 0,
                pctToNextRankStyle: {},
                pctToTotalStyle: {}
            };

            var xp;
            if ($scope.data.assumptions.advanced.overrideXP != null)
                xp = $scope.data.assumptions.advanced.overrideXP;
            else
                xp = $scope.data.selectedChar.ibXp;

            $scope.data.calculated.current = getRankInfo(xp);
            
            var bonus = 1;
            if ($scope.data.selectedChar.ibEmblem)
                bonus = bonus * 1.1;
            if ($scope.data.selectedChar.ibClass)
                bonus = bonus * 1.1;
            if ($scope.data.selectedChar.ibShader)
                bonus = bonus * 1.1;
            $scope.data.calculated.gearBonus = bonus - 1;
            $scope.data.calculated.maxAlt = $scope.data.maxAltRank;
            if ($scope.data.assumptions.advanced.overrideAlt != null) {
                $scope.data.calculated.maxAlt = $scope.data.assumptions.advanced.overrideAlt;
            }
            $scope.data.calculated.altBonusMsg = "";
            $scope.data.calculated.displayAltBonus = (100 * $scope.data.calculated.altBonus).toFixed(0);
            $scope.data.calculated.totalBonus = (1 + $scope.data.calculated.gearBonus) * (1 + $scope.data.calculated.temperBonus) * (1 + $scope.data.calculated.altBonus) - 1;
            $scope.data.calculated.displayTotalBonus = (100 * $scope.data.calculated.totalBonus).toFixed(0);

            $scope.data.calculated.table = [];
            var cntr;
            //repeat for remaning days in week
            var xp = $scope.data.calculated.current.xp;
            for (cntr = $scope.data.dayOfWeek; cntr < 7; cntr++) {
                xp = buildRow(cntr, xp, $scope.data);
            }
            $scope.data.calculated.final = getRankInfo(xp);
            $scope.calcBounties($scope.data);
            localStorage.setItem("ibSaved", JSON.stringify($scope.data.assumptions));
            console.log("Done build");


        };

        var dummy = {
            label: "Loading...",
            ibEmblem: false,
            ibClass: false,
            ibShader: false,
            ibXp: 0,
            medallions: 0,
            history: []
        };
        
        $scope.hasNightfallBonus = function(){
          return $scope.data.assumptions.advanced.overrideNf || ($scope.data.selectedChar.publicAdv && $scope.data.selectedChar.publicAdv.nightfall);
        };

        $scope.buildDefaultOverrides = function () {
            return {
                overrideXp: null,
                overrideNf: false,
                overrideDays: [{bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null},
                    {bounty: null, match: null}]
            }
        };

        var assumptions = {
            winPct: 50,
            matches: 5,
            bounty: 2,
            wkBty1: 7,
            wkBty2: 7,
            wkBty3: 0,
            wkBty4: 0,
            advanced: $scope.buildDefaultOverrides()
        };

        var localSettings = localStorage.getItem("ibSaved");
        if (localSettings) {
            assumptions = JSON.parse(localSettings);
            if (!assumptions.wkBty1) assumptions.wkBty1 = 7;
            if (!assumptions.wkBty2) assumptions.wkBty2 = 7;
            if (!assumptions.wkBty3) assumptions.wkBty3 = 0;
            if (!assumptions.wkBty4) assumptions.wkBty4 = 0;
        }

        $scope.data = {
            loading: true,
            selectedChar: dummy,
            weekdays: weekday,
            btyWeekdays: btyWeekday,
            dayOfWeek: dayOfWeek,
            maxAltRank: 0,
            chars: [dummy],
            calculated: {
                final: getRankInfo(0),
                current: getRankInfo(0),
                gearBonus: 0,
                maxAlt: 0
            },
            assumptions: assumptions
        };
        $scope.calcTable();
        if (PLAYER_PLATFORM!=0) {
          ReadService.loadIb(PLAYER_PLATFORM, MEMBER_ID, $scope);
        }
        else{
          $scope.data.loading = false;
        }
        console.log("loaded");
    }]);

ibApp.service("ReadService", ["$http", "$q", function ($http, $q) {
    return ({
        loadIb: loadIb
    });

    function loadIb(platform, memberId, $scope) {
        var request = $http({
            method: "get",
            url: "/api/ibChars/" + platform + "/" + memberId,
            params: {nocache: NO_CACHE}
        });
        (request.then(function (response) {
            if (!response.data.chars) {
                $scope.data.errormsg = "Problem loading data from Bungie. You'll have to enter your own data below for XP and IB gear, but the rest of the calculator should work!";
                $scope.data.loading = false;
                return;
            }
            $scope.data.chars = response.data.chars;
            if ($scope.data.chars.length >= 1) $scope.data.selectedChar = $scope.data.chars[0];
            if ($scope.data.selectedChar.privacy==true){
                $scope.data.errormsg = " This user has selected to hide parts of their data on Bungie.net which prevents this page from being properly displayed." +
                    " If you are the user in question, you can fix this by: Going to Bungie.net, clicking the settings gear icon, " +
                    "clicking 'Privacy' and checking the 'Show My Advisors' box.";
            }
            $scope.data.loading = false;
            var maxAltRank = 0;
            var cntr;
            for (cntr = 0; cntr < $scope.data.chars.length; cntr++) {
                var char = $scope.data.chars[cntr];
                if (char.stale) char.ibXp = 0;

            }
            
            for (cntr = 0; cntr < $scope.data.chars.length; cntr++) {
                var char = $scope.data.chars[cntr];
                var altRank = $scope.getRank(char.ibXp);
                if (altRank > maxAltRank) maxAltRank = altRank;
            }
            $scope.data.maxAltRank = maxAltRank;

            $scope.calcTable();
        }, handleError));
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
        }
        return ($q.reject(response.data));
    }

}]);;var INDEX_JS = (function () {
    var exports = {};


    function removeFavClick(b) {
        var target = $(b.target);
        if (target.attr("platform") == null)
            target = target.parent();
        var platform = target.attr("platform");
        var name = target.attr("name");
        removeFav(platform, name);
        target.closest('tr').remove();
    }

    function bulkClick() {
        var items = $(".bulk");
        var tags = "";
        var platform = null;
        $.each(items, function (i, t) {
            if (t.checked) {
                var jt = $(t);
                if (!platform)
                    platform = jt.attr("data-platform");
                tags += jt.attr('data-tag')+",";
            }
        });
        if (tags.length>0){
            tags = tags.substr(0, tags.length-1);
        }
        window.location = "list/"+platform+"/"+tags;
    }

    function getSelectedPlatform(){
      var platformBool = $("#selPlatform").is(':checked');
      var platform = 1;
      if (platformBool) platform = 2;
      return platform;
    }
  
    function grimChooseClick(){

        var platform = getSelectedPlatform();
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "grim/" + platform + "/" + encodeURIComponent(name);
        return false;
    }

    function cfChooseClick(){
        var platform = getSelectedPlatform();
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "cf/" + platform + "/" + encodeURIComponent(name);
        return false;
    }
    function srlChooseClick(){
        var platform = getSelectedPlatform();
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "srl/" + platform + "/" + encodeURIComponent(name);
        return false;
    }
    
    function viewClick() {
        var platform = getSelectedPlatform();      
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "tabs/" + platform + "/" + encodeURIComponent(name);
        return false;
    }


    function clanView() {

        var platformBool = $("#selClanPlatform").is(':checked');
        var platform = 1;
        if (platformBool) platform = 2;
        var name = $("#txtClanName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "clan/" + platform + "/" + encodeURIComponent(name);
        return false;
    }

    function checkChange() {
        var items = $(".bulk");
        var checked = false;
        $.each(items, function (i, t) {
            checked = checked || t.checked;

        });
        $('#bulkBtn').prop('disabled', !checked);
    }


    function ibChoose(){
        var platform = getSelectedPlatform();      
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return;
        if (name == null || name.trim().length == 0)
            return;
        window.location.href = "ib/" + platform + "/" + encodeURIComponent(name);
        return false;
    }

    function addRow(platform, name) {

        var chk = $('<input class="bulk" type="checkbox" data-platform="'+platform+'" data-tag="'+encodeURI(name)+'" />');
        chk.bind("change", checkChange);


        //var srlBtn = $('<a href="/srl/'+platform+'/'+name+'" class="btn btn-default"><span class="hidden-xs">SRL</span><span class="visible-xs-block"><i class="fa fa-flag-checkered"></i></span></a>');
        var cfBtn = $('<a href="/cf/'+platform+'/'+name+'" class="btn btn-default"><span class="hidden-xs">Calcified </span><i class="fa fa-diamond"></i></a>');
        var grimBtn = $('<a href="/grim/'+platform+'/'+name+'" class="btn btn-default"><span class="hidden-xs">Grim </span><i class="fa fa-book"></i></a>');
        var ibBtn = $('<a href="/ib/'+platform+'/'+name+'" class="btn btn-default"><span class="hidden-xs">IB </span><i class="fa fa-flag" aria-hidden="true"></i></button>');
            
        var remove = $('<button type="button" class="btn btn-default" platform="' + platform + '" name="'
            + name + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>');
        remove.bind("click", removeFavClick);

        var table = $("#favTable");
        var tr = $('<tr></tr>');
        
        var td1 = $('<td class="col-xs-1"></td>');
        td1.append(chk);
        var td2 = $('<td class="col-xs-11"></td>');
        
        var row = $('<div class="row"><b class="col-lg-3 col-md-12"><a href="tabs/' + platform + '/' + name + '" role="button">' + name + '</a></b></div>');
        
        var tlbar = $('<div class="btn-toolbar col-lg-9 col-md-12 "/>');
        tlbar.append(ibBtn);
        tlbar.append(cfBtn);
        tlbar.append(grimBtn);
        tlbar.append(remove);
        row.append(tlbar);
        td2.append(row);
        tr.append(td1);
        tr.append(td2);
        table.append(tr);
        $("#favTableParent").removeClass("hidden");
    }


    function addFavFromForm() {
        var platform = getSelectedPlatform();
        var name = $("#txtName").val();
        if (platform < 1 || platform > 2)
            return false;
        if (name == null || name.trim().length == 0)
            return false;
        var added = addFav(platform, name);
        if (!added)
            return false;
        addRow(platform, name);
        return false;
    }

    function platformChanged() {
        var platform = getSelectedPlatform();
        localStorage.setItem("platform", platform);
    }

  function nameChanged() {
    var name = $("#txtName").val();
    localStorage.setItem("name", name);
  }

    function loadSavedPlatformAndName() {
      var savedPlatform = localStorage.getItem("platform");
      if (savedPlatform==2){
        $("#selPlatform").prop('checked', true).change();
      }

      var name = localStorage.getItem("name");
      if (name!=null){
        $("#txtName").val(name);
      }
    }

    function removeFav(platform, name) {
        var cookieString = localStorage.getItem("favorites");
        var favs = parseFavs(cookieString);
        for (var cntr = 0; cntr < favs.length; cntr++) {
            var fav = favs[cntr];
            if (fav.name == name && fav.platform == platform) {
                favs.splice(cntr, 1);
                break;
            }
        }
        cookieString = serializeFavs(favs);
        localStorage.setItem("favorites", cookieString);

    }

    function initMe() {
        //init tooltips
        $("#saveBtn").bind("click", addFavFromForm);
        $("#bulkBtn").bind("click", bulkClick);
        $("#selPlatform").bind("change", platformChanged);
        $("#txtName").bind("change", nameChanged);
        loadSavedPlatformAndName();
        var cookieString = localStorage.getItem("favorites");
        var favs = parseFavs(cookieString);
        if (favs.length > 0) {
            $.each(favs, function (i, t) {
                addRow(t.platform, t.name);
            });
        }

        $('#txtClanName').pressEnter(clanView);
        $("#viewBtn").bind("click", viewClick);
        $("#cfChooseBtn").bind("click", cfChooseClick);
        $("#srlChooseBtn").bind("click", srlChooseClick);
        $("#grimChooseBtn").bind("click", grimChooseClick);
        $("#ibChooseBtn").bind("click", ibChoose);
        $("#clanBtn").bind("click", clanView);
        $("txtName").focus();
        $('[data-toggle="popover"]').popover();
    }

    exports.initMe = initMe;
    return exports;
});;var listApp = angular.module('listApp', ['toaster', 'ngSanitize', 'ngAnimate'])
  .controller('mainController', ['$rootScope', '$scope', '$q', 'toaster', 'ReadService', function ($rootScope, $scope, $q, toaster, ReadService) {
    
    $scope.vm = {
      players: [],
      loading: 0
    };

    LOAD_ME.forEach(function (name) {
      $scope.vm.players.push({
        platform: PLATFORM,
        name: name,
        encodedName: encodeURI(name),
        status: "Waiting to load..."
      })
    });
    
    $scope.vm.loadPlayer = function(player, noCache){
      ReadService.listChars(PLATFORM, player, $scope, noCache);
    }


    var tasks = [];
    $scope.vm.players.forEach(function (player) {
      tasks.push(function () {
        return ReadService.listChars(PLATFORM, player, $scope);
      });
    });
    $q.serial(tasks).then(function () {
      console.log("Done");
    });
  }]);


listApp.service("ReadService", ["$http", "$q", "toaster", function ($http, $q, toaster) {
  return ({
    listChars: listChars
  });


  function processRaid(mode) {
    if (!mode) return;
    if (!mode.threeninety){
      mode.threeninety = {};
    }
    mode = mode.threeninety;
    if (!mode.steps) {
      mode.dispClass = "active";
      return;
    }
    var complete = mode.steps[mode.steps.length - 1].isComplete;
    if (complete) {
      mode.dispClass = "success";
      return;
    }
    for (var cntr = 0; cntr < mode.steps.length; cntr++) {
      if (mode.steps[cntr].isComplete) {
        mode.dispClass = "warning";
        return;
      }
    }
    mode.dispClass = "danger";
  }

  function listChars(platform, player, $scope, noCache) {
    var request = $http({
      method: "get",
      url: "/api/getCharsForList2/" + platform + "/" + player.encodedName,
      params: {nocache: NO_CACHE||noCache}
    });
    $scope.vm.loading++;
    player.loading = true;
    player.status = "Loading...";
    return $q.when(request.then(handleErrors).then(function (response) {
      if (response.data && response.data.player && response.data.chars) {
        player.data = response.data;
        player.status = null;
        if (player.data){
          if (player.data.chars){
            player.data.chars.forEach(function(jo){
              processRaid(jo.publicAdv.wrath);
              processRaid(jo.publicAdv.kf);
              processRaid(jo.publicAdv.vog);
              processRaid(jo.publicAdv.ce);
            });
          }
          if (player.data.player && player.data.player.lastPlayed){
            player.data.player.lastPlayed = "Last played "+moment(player.data.player.lastPlayed).fromNow();
          }
        }
        
      }
      else if (response.data && response.data.name=="OtherPlatform"){
        player.status = "Wrong platform";
      }
      else if (response.data && response.data.name=="NoChars"){
        player.status = "No characters";
      }
      else if (response.data && response.data.name=="NotFound"){
        player.status = "No Destiny player found";
      }
      else if (response.data.error){
        return $q.reject(new Error(response.data.error));
      }
      else {
        return $q.reject(new Error("Unexpected response."));
      }
    }).catch(function (err) {
      player.status = err.message;
    }).finally(function() {
        player.loading = null;
        $scope.vm.loading--;
    })
    );
  }


  function handleErrors(response) {
    if (response.status === 503) {
      return $q.reject(new Error("Server is down."));
    }
    if (response.status < 200 || response.status >= 400) {
      return $q.reject(new Error('Network error: ' + response.status));
    }
    return response;
  }


}]);


listApp.config(['$provide', function ($provide) {
  $provide.decorator("$q", ['$delegate', function ($delegate) {
    //Helper method copied from q.js.
    var isPromiseLike = function (obj) {
      return obj && angular.isFunction(obj.then);
    }

    /*
     * @description Execute a collection of tasks serially.  A task is a function that returns a promise
     *
     * @param {Array.<Function>|Object.<Function>} tasks An array or hash of tasks.  A tasks is a function
     *   that returns a promise.  You can also provide a collection of objects with a success tasks, failure task, and/or notify function
     * @returns {Promise} Returns a single promise that will be resolved or rejected when the last task
     *   has been resolved or rejected.
     */
    function serial(tasks) {
      //Fake a "previous task" for our initial iteration
      var prevPromise;
      var error = new Error();
      angular.forEach(tasks, function (task, key) {
        var success = task.success || task;
        var fail = task.fail;
        var notify = task.notify;
        var nextPromise;

        //First task
        if (!prevPromise) {
          nextPromise = success();
          if (!isPromiseLike(nextPromise)) {
            error.message = "Task " + key + " did not return a promise.";
            throw error;
          }
        } else {
          //Wait until the previous promise has resolved or rejected to execute the next task
          nextPromise = prevPromise.then(
            /*success*/function (data) {
              if (!success) {
                return data;
              }
              var ret = success(data);
              if (!isPromiseLike(ret)) {
                error.message = "Task " + key + " did not return a promise.";
                throw error;
              }
              return ret;
            },
            /*failure*/function (reason) {
              if (!fail) {
                return $delegate.reject(reason);
              }
              var ret = fail(reason);
              if (!isPromiseLike(ret)) {
                error.message = "Fail for task " + key + " did not return a promise.";
                throw error;
              }
              return ret;
            },
            notify);
        }
        prevPromise = nextPromise;
      });

      return prevPromise || $delegate.when();
    }

    $delegate.serial = serial;
    return $delegate;
  }]);
}]);
;var ORACLE_JS = (function () {
    var exports = {};

    var ROUND = 1;

    function update() {

        if (ROUND == 1) {
            $("#prevBtn").attr("disabled", true);
        }
        else {
            $("#prevBtn").removeAttr("disabled");
        }

        if (ROUND == 7) {
            $("#nextBtn").attr("disabled", true);
        }
        else {
            $("#nextBtn").removeAttr("disabled");
        }

        var tbody = $("#rounds");

        var cntr;
        var children = tbody.children();
        for (cntr = 0; cntr < 7; cntr++) {
            var row = $(children[cntr]);
            if (ROUND == (cntr + 1)) {
                row.addClass("highlighted");
            }
            else {
                row.removeClass("highlighted");
            }
        }
    }

    function prev() {
        if (ROUND > 1) {
            ROUND--;
        }
        update();
    }

    function next() {
        if (ROUND < 7) {
            ROUND++;
        }
        update();

    }
    
    function playSound(oracleNumber, event){
      var player = $("#oraclePlayer"+oracleNumber);
      player[0].play();
      
    }

    function initMe() {
      $(".asdf").bind("click", function(){
        player = document.getElementById('asdf');
        player.play();
      });
        $(".oracleMid").bind("click", playSound.bind(null, 1));
        $(".oracleL1").bind("click", playSound.bind(null, 2));
        $(".oracleR1").bind("click", playSound.bind(null, 3));
        $(".oracleL2").bind("click", playSound.bind(null, 4));
        $(".oracleR2").bind("click", playSound.bind(null, 5));
        $(".oracleL3").bind("click", playSound.bind(null, 6));
        $(".oracleR3").bind("click", playSound.bind(null, 7));
      
        $("#prevBtn").bind("click", prev);
        $("#nextBtn").bind("click", next);
        update();
        $(document).on('keydown', function (e) {

            //right down space enter
            if (e.which == 39 || e.which == 40 || e.which == 32 || e.which == 13) {
                next();
                e.preventDefault();
            }
            //left up backspace
            else if (e.which == 37 || e.which == 38) {
                prev();
                e.preventDefault();
            }
        });
    }

    exports.initMe = initMe;
    return exports;
});;/* 
Dcc: Angular modifier for slider
 */
angular.module('ui.bootstrap-slider', [])
    .directive('slider', ['$parse', '$timeout', '$rootScope', function ($parse, $timeout, $rootScope) {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div><input class="slider-input" type="text" style="width:100%" /></div>',
            require: 'ngModel',
            scope: {
                max: "=",
                min: "=",
                step: "=",
                value: "=",
                ngModel: '=',
                ngDisabled: '=',
                range: '=',
                sliderid: '=',
                ticks: '=',
                ticksLabels: '=',
                ticksSnapBounds: '=',
                ticksPositions: '=',
                scale: '=',
                formatter: '&',
                onStartSlide: '&',
                onStopSlide: '&',
                onSlide: '&'
            },
            link: function ($scope, element, attrs, ngModelCtrl, $compile) {
                var ngModelDeregisterFn, ngDisabledDeregisterFn;

                initSlider();

                function initSlider() {
                    var options = {};

                    function setOption(key, value, defaultValue) {
                        options[key] = value || defaultValue;
                    }

                    function setFloatOption(key, value, defaultValue) {
                        options[key] = value || value === 0 ? parseFloat(value) : defaultValue;
                    }

                    function setBooleanOption(key, value, defaultValue) {
                        options[key] = value ? value + '' === 'true' : defaultValue;
                    }

                    function getArrayOrValue(value) {
                        return (angular.isString(value) && value.indexOf("[") === 0) ? angular.fromJson(value) : value;
                    }

                    setOption('id', $scope.sliderid);
                    setOption('orientation', attrs.orientation, 'horizontal');
                    setOption('selection', attrs.selection, 'before');
                    setOption('handle', attrs.handle, 'round');
                    setOption('tooltip', attrs.sliderTooltip || attrs.tooltip, 'show');
                    setOption('tooltip_position', attrs.sliderTooltipPosition, 'top');
                    setOption('tooltipseparator', attrs.tooltipseparator, ':');
                    setOption('ticks', $scope.ticks);
                    setOption('ticks_labels', $scope.ticksLabels);
                    setOption('ticks_snap_bounds', $scope.ticksSnapBounds);
                    setOption('ticks_positions', $scope.ticksPositions);
                    setOption('scale', $scope.scale, 'linear');

                    setFloatOption('min', $scope.min, 0);
                    setFloatOption('max', $scope.max, 10);
                    setFloatOption('step', $scope.step, 1);
                    var strNbr = options.step + '';
                    var decimals = strNbr.substring(strNbr.lastIndexOf('.') + 1);
                    setFloatOption('precision', attrs.precision, decimals);

                    setBooleanOption('tooltip_split', attrs.tooltipsplit, false);
                    setBooleanOption('enabled', attrs.enabled, true);
                    setBooleanOption('naturalarrowkeys', attrs.naturalarrowkeys, false);
                    setBooleanOption('reversed', attrs.reversed, false);

                    setBooleanOption('range', $scope.range, false);
                    if (options.range) {
                        if (angular.isArray($scope.value)) {
                            options.value = $scope.value;
                        }
                        else if (angular.isString($scope.value)) {
                            options.value = getArrayOrValue($scope.value);
                            if (!angular.isArray(options.value)) {
                                var value = parseFloat($scope.value);
                                if (isNaN(value)) value = 5;

                                if (value < $scope.min) {
                                    value = $scope.min;
                                    options.value = [value, options.max];
                                }
                                else if (value > $scope.max) {
                                    value = $scope.max;
                                    options.value = [options.min, value];
                                }
                                else {
                                    options.value = [options.min, options.max];
                                }
                            }
                        }
                        else {
                            options.value = [options.min, options.max]; // This is needed, because of value defined at $.fn.slider.defaults - default value 5 prevents creating range slider
                        }
                        $scope.ngModel = options.value; // needed, otherwise turns value into [null, ##]
                    }
                    else {
                        setFloatOption('value', $scope.value, 5);
                    }

                    if ($scope.formatter) options.formatter = $scope.$eval($scope.formatter);


                    // check if slider jQuery plugin exists
                    if ('$' in window && $.fn.slider) {
                        // adding methods to jQuery slider plugin prototype
                        $.fn.slider.constructor.prototype.disable = function () {
                            this.picker.off();
                        };
                        $.fn.slider.constructor.prototype.enable = function () {
                            this.picker.on();
                        };
                    }

                    // destroy previous slider to reset all options
                    if (element[0].__slider)
                        element[0].__slider.destroy();

                    var slider = new Slider(element[0].getElementsByClassName('slider-input')[0], options);
                    element[0].__slider = slider;

                    // everything that needs slider element
                    var updateEvent = getArrayOrValue(attrs.updateevent);
                    if (angular.isString(updateEvent)) {
                        // if only single event name in string
                        updateEvent = [updateEvent];
                    }
                    else {
                        // default to slide event
                        updateEvent = ['slide'];
                    }
                    angular.forEach(updateEvent, function (sliderEvent) {
                        slider.on(sliderEvent, function (ev) {
                            ngModelCtrl.$setViewValue(ev);
                            $timeout(function () {
                                $scope.$apply();
                            });
                        });
                    });
                    slider.on('change', function (ev) {
                        ngModelCtrl.$setViewValue(ev.newValue);
                        $timeout(function () {
                            $scope.$apply();
                        });
                    });

                    // Event listeners
                    var sliderEvents = {
                        slideStart: 'onStartSlide',
                        slide: 'onSlide',
                        slideStop: 'onStopSlide'
                    };
                    angular.forEach(sliderEvents, function (sliderEventAttr, sliderEvent) {
                        var fn = $parse(attrs[sliderEventAttr]);
                        slider.on(sliderEvent, function (ev) {
                            if ($scope[sliderEventAttr]) {

                                var callback = function () {
                                    fn($scope.$parent, { $event: ev, value: ev });
                                }

                                if ($rootScope.$$phase) {
                                    $scope.$evalAsync(callback);
                                } else {
                                    $scope.$apply(callback);
                                }
                            }
                        });
                    });

                    // deregister ngDisabled watcher to prevent memory leaks
                    if (angular.isFunction(ngDisabledDeregisterFn)) {
                        ngDisabledDeregisterFn();
                        ngDisabledDeregisterFn = null;
                    }

                    ngDisabledDeregisterFn = $scope.$watch('ngDisabled', function (value) {
                        if (value) {
                            slider.disable();
                        }
                        else {
                            slider.enable();
                        }
                    });

                    // deregister ngModel watcher to prevent memory leaks
                    if (angular.isFunction(ngModelDeregisterFn)) ngModelDeregisterFn();
                    ngModelDeregisterFn = $scope.$watch('ngModel', function (value) {
                        if($scope.range){
                            slider.setValue(value);
                        }else{
                            slider.setValue(parseFloat(value));
                        }
                    }, true);
                }


                var watchers = ['min', 'max', 'step', 'range', 'scale'];
                angular.forEach(watchers, function (prop) {
                    $scope.$watch(prop, function () {
                        initSlider();
                    });
                });
            }
        };
    }])
;;

var srlApp = angular.module('srlApp', ['ui.bootstrap-slider'])

    .controller('mainController', ['$rootScope', '$scope', 'ReadService', function ($rootScope, $scope, ReadService) {


//   RACE VAL 45, 65, 80, 100, 45 - 100
//   RACE PER DAY 0 - 100
//   BOUNTIES PER DAY 1-3
// QUEST, C, B, A, S  --- S = 600
        $scope.data = {
            assumptions: {
                raceVal: 65,
                races: 5,
                bounties: 2,
                classItem: false
            }
        };
        var localSettings = localStorage.getItem("srlSaved");
        if (localSettings) {
            $scope.data.assumptions = JSON.parse(localSettings);
            if (!$scope.data.assumptions.raceVal) $scope.data.assumptions.raceVal = 45;
            if (!$scope.data.assumptions.races) $scope.data.assumptions.races = 0;
            if (!$scope.data.assumptions.bounties) $scope.data.assumptions.bounties = 0;
            if (!$scope.data.assumptions.classItem) $scope.data.assumptions.classItem = false;
        }

        var CLASS_MOD = 1.15;
        var BTY_VAL = 100;
        var QUEST_VAL = 300;

        $scope.calcTable = function () {
            console.log("Calc table");
            $scope.data.calculated = {
            };

            var classItemMod = $scope.data.assumptions.classItem?1.15:1;
            var btyVal = BTY_VAL * classItemMod;
            var raceVal = $scope.data.assumptions.raceVal * classItemMod;
            var btyTotal = $scope.data.assumptions.bounties * btyVal;
            var raceTotal = $scope.data.assumptions.races * raceVal;
            
            var current = moment(6, "HH");
            var end = moment("2015-12-29 7", "YYYY-MM-DD H");
            $scope.data.calculated = {
                current:getRankInfo($scope.data.selectedChar.prog.currentProgress),
                constants: {
                    btyVal: btyVal,
                    raceVal: raceVal,
                    btyTotal: btyTotal,
                    raceTotal: raceTotal
                },
                tableRows: [],
                racesAbove2:  0,
                racesAbove3:  0,
                racesAbove4 :  0,
                dayReach2 :  null,
                dayReach3 :  null,
                dayReach4: null
            };
            var xp = $scope.data.selectedChar.prog.currentProgress;
            while (current.valueOf()<end.valueOf()){
                var rankInfo = getRankInfo(xp);

                console.log(current+": "+xp+" "+rankInfo.rank);
                if (rankInfo.rank>=2){
                    if (!$scope.data.calculated.dayReach2){
                        $scope.data.calculated.dayReach2 = moment(current);
                        $scope.data.calculated.durDay2 = moment.duration(moment().diff(current));
                    }
                    if (rankInfo.rank<3)
                        $scope.data.calculated.racesAbove2+=$scope.data.assumptions.races;
                }
                if (rankInfo.rank>=3){
                    if (!$scope.data.calculated.dayReach3){
                        $scope.data.calculated.dayReach3 = moment(current);
                        $scope.data.calculated.durDay3 = moment.duration(moment().diff(current));
                    }
                    $scope.data.calculated.racesAbove3+=$scope.data.assumptions.races;
                }
                if (rankInfo.rank>=4){
                    if (!$scope.data.calculated.dayReach4){
                        $scope.data.calculated.dayReach4 = moment(current);
                        $scope.data.calculated.durDay4 = moment.duration(moment().diff(current));
                    }
                }
                rankInfo.day = moment(current);
                $scope.data.calculated.tableRows.push(rankInfo);
                xp+=btyTotal+raceTotal;
                current = current.add(1, 'day');
            }
            localStorage.setItem("srlSaved", JSON.stringify($scope.data.assumptions));
        };

        function getRankInfo(xp) {
            var rank, lastRankXp, nextRankXp;
            if (xp > 17499) {
                rank = 4;
                lastRankXp = 17500;
                nextRankXp = 0;
            }
            else if (xp > 8999) {
                rank = 3;
                lastRankXp = 9000;
                nextRankXp = 17500;
            }
            else if (xp > 4499) {
                rank = 2;
                lastRankXp = 4500;
                nextRankXp = 9000;
            }
            else if (xp > 1749) {
                rank = 1;
                lastRankXp = 1750;
                nextRankXp = 4500;
            }
            else {
                rank = 0;
                lastRankXp = 0;
                nextRankXp = 1750;
            }
            var nextRank;
            if (nextRankXp > 0) {
                pctToNextRank = parseInt((100 * (xp - lastRankXp) / (nextRankXp - lastRankXp)).toFixed(0));
                nextRank = rank + 1;
            }
            else {
                //handle "you're done" case
                pctToNextRank = 100;
                lastRankXp = 9000;
                nextRankXp = 17500;
                nextRank = 4;
            }
            var pctToTotal = parseInt((100 * (xp / 17500)).toFixed(0));
            return {
                xp: xp,
                lastRankXp: lastRankXp,
                nextRankXp: nextRankXp,
                rank: rank,
                rankProgress: xp - lastRankXp,
                rankTotal: nextRankXp - lastRankXp,
                nextRank: nextRank,
                pctToNextRank: pctToNextRank,
                pctToTotal: pctToTotal,
                pctToTotalStyle: {
                    width: pctToTotal + '%'
                },
                pctToNextRankStyle: {
                    width: pctToNextRank + '%'
                }
            };

        };



        ReadService.load(PLAYER_PLATFORM, MEMBER_ID, $scope);
        console.log("loaded");
    }]);

srlApp.service("ReadService", ["$http", "$q", function ($http, $q) {
    return ({
        load: load
    });

    function load(platform, memberId, $scope) {
        var request = $http({
            method: "get",
            url: "/api/srlChars/" + platform + "/" + memberId,
            params: {nocache: NO_CACHE}
        });
        (request.then(function (response) {
            if (!response.data.chars) {
                $scope.data.errormsg = "Problem loading data from Bungie";
                return;
            }
            $scope.data.chars = response.data.chars;
            $scope.data.prs = response.data.prs;
            if ($scope.data.chars.length >= 1){
                $scope.data.selectedChar = $scope.data.chars[0];
                $scope.calcTable();
                
            } 
        }, handleError));
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
        }
        return ($q.reject(response.data));
    }

}]);;var tabsApp = angular.module('tabsApp', ['toaster', 'ngSanitize', 'ngAnimate', 'as.sortable'])
  .controller('mainController', ['$rootScope', '$scope', 'toaster', 'ReadService', function ($rootScope, $scope, toaster, ReadService) {

    var loadedHidden = false;


    function defaultRows() {
      return ['closeFaction',
        
        
        //'dailyHeroic',
        //'dailyPvp',
        'ce390',
        'vog390',
        'kf390',
        'wrath390',
        'weeklyStory',

        'nightfall',
        'weeklyHeroic',
        'weeklyPvp',
        'coe',

        'publicEvent',

        'gunsmith',

        'shiro',
        'shaxx',
        'crotaBty',
        'queen',
        'variks',

        'cehm',
        'cenm',
        'voghm',
        'vognm',
        'kfhm',
        'kfnm',

        'wrathhm',
        'wrath',
        'poe41',
        'poe35',
        'poe34',
        'poe32',
        ];
    }
    
    var ROW_SETTINGS_KEY = "rowSettingsV2";

    function navTabs() {
      var hash = document.location.hash;
      if (hash) {
        $('.globalNav a[href="' + hash + '"]').tab('show');
      }

      // Change hash for page-reload
      $('.globalNav a').on('show.bs.tab', function (e) {
        window.location.hash = e.target.hash;
      });
    }

    navTabs();


    var rowSettings = null, customRows = false;
    try {
      var sRowSettings = localStorage.getItem(ROW_SETTINGS_KEY);
      if (sRowSettings) {
        rowSettings = JSON.parse(sRowSettings);
        if (!rowSettings.length || rowSettings.length <= 0) rowSettings = null;
      }
    }
    catch (e) {
      //ignore
    }
    if (!rowSettings) {
      rowSettings = defaultRows();
    }
    else {
      customRows = true;
    }


    $scope.vm = {
      loading: {
        active: true,
        pct: "5%"
      },
      showHiddenFactions: false,
      warningMsg: null,
      checklistRows: rowSettings,
      customRows: customRows
    };

    $scope.ngSortSettings = {
      //containment: "#dragContainer",
      //containerPositioning: 'relative',
      orderChanged: function (event) {
        $scope.vm.saveRowSettings();
      }

      //scrollableContainer: "#sort_" +index+ "",
      //accept: function (sourceItemHandleScope, destSortableScope) {
      //  return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
      //}
    };


    console.log("loaded");

    $scope.vm.clearRowSettings = function () {
      $scope.vm.checklistRows = defaultRows();
      $scope.vm.customRows = false;
      localStorage.removeItem(ROW_SETTINGS_KEY);
    }

    $scope.vm.saveRowSettings = function () {
      var sRowSettings = JSON.stringify($scope.vm.checklistRows);
      $scope.vm.customRows = true;
      localStorage.setItem(ROW_SETTINGS_KEY, sRowSettings);
    }

    $scope.vm.hideRow = function ($index) {
      console.log($index);
      $scope.vm.checklistRows.splice($index, 1);
      $scope.vm.saveRowSettings();
    }


    $scope.vm.showQuest = function (q) {
      $scope.vm.selectedQuest = q;
      $('#questModal').modal();
    }


    function processRaidTier(mode) {
      if (!mode || !mode.steps) {
        mode.dispClass = "active";
        return;
      }
      var complete = mode.steps[mode.steps.length - 1].isComplete;
      if (complete) {
        mode.dispClass = "success";
        return;
      }
      for (var cntr = 0; cntr < mode.steps.length; cntr++) {
        if (mode.steps[cntr].isComplete) {
          mode.dispClass = "warning";
          return;
        }
      }
      mode.dispClass = "danger";
    }

    function processRaid(raid) {

      processRaidTier(raid.threeninety);
      processRaidTier(raid.hm);
      processRaidTier(raid.nm);

      if (raid.hm.dispClass === "success") {
        raid.nm.dispClass = "success";
      }
    }


    function getPct(progress) {
      return Math.floor(progress.done / progress.total * 95 + 5) + "%";
    }

    ReadService.listChars(PLAYER_PLATFORM, MEMBER_ID, PLAYER_NAME, function (err, jo) {
      if (err) {
        console.dir(err);
        $scope.vm.errorMsg = err;
        return;
      }

      addFav(PLAYER_PLATFORM, PLAYER_NAME);
      jo.player.minsPlayed = Math.floor(jo.player.minsPlayed / 60);
      jo.player.lastPlayed = moment(jo.player.lastPlayed).fromNow();

      var progress = {
        done: 0,
        total: jo.chars.length * 2
      }
      $scope.vm.loading.active = true;
      $scope.vm.loading.pct = getPct(progress);
      $scope.vm.player = jo.player;
      $scope.vm.chars = jo.chars;
      $scope.vm.player.grimoire = jo.grimoire;
      console.dir(jo);

      $scope.vm.repProgressPct = function (progress) {
        var nla = progress.nextLevelAt;
        if (!nla) nla = 1;
        return Math.floor(100 * (progress.progressToNextLevel / nla));
      }
      //https://my.destinytrialsreport.com/xbox/E5%20Dweezil
      //https://my.destinytrialsreport.com/ps/Gothalion
      $scope.vm.buildTrialsUrl = function () {

        var console = PLAYER_PLATFORM == 1 ? "xbox" : "ps";
        var url = "https://my.destinytrialsreport.com/" + console + "/" + encodeURIComponent(PLAYER_NAME);
        return url;
      }

      $scope.vm.buildRepURL = function (characterid, progressionHash) {
        if (progressionHash === 3186678724) {

          return "/cf/1/" + encodeURIComponent(PLAYER_NAME);
        }
        var console = PLAYER_PLATFORM == 1 ? "xb" : "ps";
        var url = "http://destiny.neonblack.moe/reputation/#" + console + "=" + encodeURIComponent(PLAYER_NAME) + "&" + characterid + "=" + progressionHash;
        return url;
      }
      
      $scope.vm.getTier = function(value){
        if (!value) return "";
        if (value<60) return 0;
        if (value<120) return 1;
        if (value<180) return 2;
        if (value<240) return 3;
        if (value<300) return 4;
        return 5;
      };

      $scope.vm.chars.forEach(function (char) {
        ReadService.getCharInfoPt1($scope, PLAYER_PLATFORM, MEMBER_ID, char.id, function (err, jo) {
          if (err) {

            toaster.pop({
              type: 'error',
              title: char.class,
              body: "Error getting advisors/progress: " + JSON.stringify(err),
              timeout: 5000
            });
          }
          else {
            char.progression = jo.progression;
            char.publicAdv = jo.publicAdv;
            processRaid(jo.publicAdv.wrath);
            processRaid(jo.publicAdv.kf);
            processRaid(jo.publicAdv.vog);
            processRaid(jo.publicAdv.ce);
          }

          progress.done++;
          $scope.vm.loading.pct = getPct(progress);
          if (progress.done >= progress.total) {
            $scope.vm.loading.active = false;
          }

          ReadService.getCharInfoPt2(PLAYER_PLATFORM, MEMBER_ID, char.id, function (err, jo) {
            if (err) {
              var msg = err;
              if (err.data) {
                msg = err.data;
              }
              else if (err.statusText) {
                msg = err.statusText;
              }

              toaster.pop({
                type: 'error',
                title: char.class,
                body: "Error getting weekly/lifetime/item: " + msg,
                timeout: 5000
              });
            }
            else {
              char.weekly = jo.weekly;
              char.weeklyPvP = jo.weeklyPvP;
              char.items = jo.items;
              char.lifetime = jo.lifetime;
            }
            progress.done++;
            $scope.vm.loading.pct = getPct(progress);
            if (progress.done >= progress.total) {
              $scope.vm.loading.active = false;
            }
          });

        });
      });

    });

  }]);

tabsApp.service("ReadService", ["$http", "$q", "toaster", function ($http, $q, toaster) {
  return ({
    listChars: listChars,
    getCharInfoPt1: getCharInfoPt1,
    getCharInfoPt2: getCharInfoPt2

  });
  function getCharInfoPt1($scope, platform, memberId, charId, callback) {
    var request = $http({
      method: "get",
      url: "/api/getCharInfoPt1/" + platform + "/" + memberId + "/" + charId,
      params: {nocache: NO_CACHE}
    });
    (request.then(function (response) {
      if (response.data && response.data.progression && response.data.publicAdv) {
        callback(null, response.data);
      }
      else {
        if (response.data.privacy==true){
          $scope.vm.errorMsg = " This user has selected to hide parts of their data on Bungie.net which prevents this page from being properly displayed." +
            " If you are the user in question, you can fix this by: Going to Bungie.net, clicking the settings gear icon, " +
            "clicking 'Privacy' and checking the 'Show My Advisors' box.";
          callback("Advisors are private");
          //callback("Unexpected response");
        }
        else
          callback("Unexpected response");
      }
    }, function (err) {
      callback(err);
    }));
  }

  function getCharInfoPt2(platform, memberId, charId, callback) {
    var request = $http({
      method: "get",
      url: "/api/getCharInfoPt2/" + platform + "/" + memberId + "/" + charId,
      params: {nocache: NO_CACHE}
    });
    (request.then(function (response) {
      if (response.data && response.data.weekly && response.data.weeklyPvP && response.data.items && response.data.lifetime) {
        callback(null, response.data);
      }
      else {
        callback("Unexpected response");
      }
    }, function (err) {
      callback(err);
    }));
  }


  function listChars(platform, memberId, name, callback) {
    var request = $http({
      method: "get",
      url: "/api/listChars/" + platform + "/" + memberId + "/" + encodeURI(name),
      params: {nocache: NO_CACHE}
    });
    (request.then(function (response) {
      if (response.data && response.data.player && response.data.chars) {
        callback(null, response.data);
      }
      else if (response.data.error) {
        callback(response.data.error);
      }
      else {
        callback("Unexpected response");
      }
    }, function (err) {
      callback(err);
    }));
  }

}]);;;var vendorApp = angular.module('vendorApp', ['toaster', 'ngSanitize', 'ngAnimate'])
  .controller('mainController', ['$rootScope', '$scope', 'toaster', 'ReadService', function ($rootScope, $scope, toaster, ReadService) {


    var armorSlotFilters = [
      //{label: "All Armor", filter: null},
      {label: "Helmet", filter: "Helmet", selected: true},
      {label: "Gauntlet", filter: "Gauntlets", selected: true},
      {label: "Chest Armor", filter: "Chest Armor", selected: true},
      {label: "Leg Armor", filter: "Leg Armor", selected: true},
      {label: "Class Armor", filter: "Class Armor", selected: true},
      {label: "Artifact", filter: "Artifacts", selected: true},
      {label: "Ghost", filter: "Ghost", selected: true}
    ];

    var weaponSlotFilters = [
      {label: "All Weapons", filter: null, slot: true},
      {label: "Primary", filter: "Primary Weapons", slot: true},
      {label: "Auto Rifle", filter: "Auto Rifle"},
      {label: "Hand Cannon", filter: "Hand Cannon"},
      {label: "Pulse Rifle", filter: "Pulse Rifle"},
      {label: "Scout Rifle", filter: "Scout Rifle"},
      {label: "Special", filter: "Special Weapons", slot: true},
      {label: "Shotgun", filter: "Shotgun"},
      {label: "Fusion Rifle", filter: "Fusion Rifle"},
      {label: "Sidearm", filter: "Sidearm"},
      {label: "Sniper Rifle", filter: "Sniper Rifle"},
      {label: "Heavy", filter: "Heavy Weapons", slot: true},
      {label: "Sword", filter: "Sword"},
      {label: "Machine Gun", filter: "Machine Gun"},
      {label: "Rocket Launcher", filter: "Rocket Launcher"}
    ];

    var armorEquippableFilters = [
      {label: "Usable By Any", filter: null},
      {label: "Titan", filter: "Titan"},
      {label: "Hunter", filter: "Hunter"},
      {label: "Warlock", filter: "Warlock"},
      {label: "All Classes", filter: "Any"}
    ];

    var rarityFilters = [
      {label: "Any Rarity", filter: null},
      {label: "Exotic", filter: "Exotic"},
      {label: "Legendary", filter: "Legendary"}
    ];


    var splitFilters = [
      {label: "Any Split", filter: null},
      {label: "Int/Dis", filter: "ID"},
      {label: "Int/Str", filter: "IS"},
      {label: "Dis/Str", filter: "DS"}
    ];

    $scope.vm = {
      loading: false,
      weapons: [],
      weaponFilters: {
        slots: weaponSlotFilters,
        selectedSlot: weaponSlotFilters[0],
        selectedRarity: rarityFilters[0],
        displayText: null,
        filterText: null
      },
      weaponSort: 'item.name',
      weaponAsc: true,
      armor: [],
      rarityFilters: rarityFilters,
      armorFilters: {
        selectedSplit: splitFilters[0],
        splits: splitFilters,
        slots: armorSlotFilters,
        selectedRarity: rarityFilters[0],
        equippable: armorEquippableFilters,
        selectedEquippable: armorEquippableFilters[0],
        displayText: null,
        filterText: null
      },
      armorSort: 'item.qualityPct',
      armorAsc: false
    };
    
    $scope.vm.selectAllArmorSlots = function(val){
      $scope.vm.armorFilters.slots.forEach(function(slot){
        slot.selected = val;
      });
    };

    $scope.vm.setArmorTextFilter = function () {
      if (!$scope.vm.armorFilters.displayText) $scope.vm.armorFilters.filterText = null;
      else $scope.vm.armorFilters.filterText = $scope.vm.armorFilters.displayText.toUpperCase();
    };

    $scope.vm.setWeaponTextFilter = function () {
      if (!$scope.vm.weaponFilters.displayText) $scope.vm.weaponFilters.filterText = null;
      else $scope.vm.weaponFilters.filterText = $scope.vm.weaponFilters.displayText.toUpperCase();
    };

    $scope.vm.stepTip = function ($event) {
      $($event.target).popover();
      $($event.target).popover("show");
    };

    $scope.vm.sortWeapon = function (attr) {
      if ($scope.vm.weaponSort == attr) {
        $scope.vm.weaponAsc = !$scope.vm.weaponAsc;
      }
      else {
        $scope.vm.weaponSort = attr;
        $scope.vm.weaponAsc = true;
      }
    };

    function filterWeaponItem(r) {
      var vm = $scope.vm;
      
      if (vm.weaponFilters.selectedSlot.filter != null) {
        if (vm.weaponFilters.selectedSlot.slot==true){
          if (vm.weaponFilters.selectedSlot.filter != r.item.slot)
            return true;  
        }
        else{
          if (vm.weaponFilters.selectedSlot.filter != r.item.type)
            return true;
        }
      }
      if (vm.weaponFilters.selectedRarity.filter) {
        if (vm.weaponFilters.selectedRarity.filter != r.item.tierTypeName) return true;
      }
      if ($scope.vm.weaponFilters.filterText) {
        if (r.item.searchText.indexOf($scope.vm.weaponFilters.filterText) == -1) return true;
      }
      return false;
    }

    $scope.vm.filterWeapons = function () {
      if (!$scope.vm.weapons) return;
      for (var cntr = 0; cntr < $scope.vm.weapons.length; cntr++) {
        var itm = $scope.vm.weapons[cntr]
        itm.hidden = filterWeaponItem(itm);
      }
    };

    $scope.vm.sortArmor = function (attr) {
      if ($scope.vm.armorSort == attr) {
        $scope.vm.armorAsc = !$scope.vm.armorAsc;
      }
      else {
        $scope.vm.armorSort = attr;
        $scope.vm.armorAsc = false;
      }
    };

    function filterArmorItem(r) {
      var vm = $scope.vm;

      for (var cntr=0; cntr<vm.armorFilters.slots.length; cntr++){
        var slot = vm.armorFilters.slots[cntr];
        if (slot.selected) continue;
        if (slot.filter!= r.item.slot) continue;
        return true;
      }
      if (vm.armorFilters.selectedSplit.filter!=null){
        if (!r.item.quality) return true;
        if (r.item.quality){
          if (r.item.quality.statSplit != vm.armorFilters.selectedSplit.filter){
            return true;
          }
        }
      }
      if (vm.armorFilters.selectedEquippable.filter) {
        if (vm.armorFilters.selectedEquippable.filter != r.item.classAllowed)
          return true;
      }
      if (vm.armorFilters.selectedRarity.filter) {
        if (vm.armorFilters.selectedRarity.filter != r.item.tierTypeName) return true;
      }
      if ($scope.vm.armorFilters.filterText) {
        if (r.item.searchText.indexOf($scope.vm.armorFilters.filterText) == -1) return true;
      }
      return false;
    }

    $scope.vm.filterArmor = function () {
      if (!$scope.vm.armor) return;
      for (var cntr = 0; cntr < $scope.vm.armor.length; cntr++) {
        var itm = $scope.vm.armor[cntr]
        itm.hidden = filterArmorItem(itm);
      }
    };

    ReadService.load($scope);

  }]);

vendorApp.service("ReadService", ["$http", "$q", "toaster", function ($http, $q, toaster) {
  return ({
    load: load
  });

  function load($scope) {
    $scope.vm.loading = true;
    $http({
      method: "get",
      url: "/api/vendor"
    }).then(function (response) {
      $scope.vm.loading = false;
      if (!response.data) {
        $scope.data.errormsg = "Problem loading data";
        return;
      }
      else {
        $scope.vm.weapons = response.data.weapons;
        $scope.vm.armor = response.data.armor;
        $scope.vm.emblems = response.data.emblems;
        $scope.vm.shaders = response.data.shaders;
        console.dir($scope.vm.shaders);
        $scope.vm.ships = response.data.ships;
      }
    }, function (response) {
      var errMsg = "";
      if (!angular.isObject(response.data) || !response.data.message) {
        errMsg = "Problem loading data"
      }
      else {
        errMsg = response.data.message;
      }
      $scope.vm.loading = false;
      $scope.vm.errormsg = errMsg;
    });
  }

}]);;var vennApp = angular.module('vennApp', ['toaster','ngSanitize','ngAnimate'])
  .controller('mainController', ['$rootScope', '$scope', 'toaster', 'ReadService', function ($rootScope, $scope, toaster, ReadService) {
    $scope.data = {
      loading: false,
      query: {
        platform: 1,
        gt1: {
          value: null
        },
        gt2: {
          value: null
        },
        gt3: {
          value: null
        },
        filter: "someQuests",
        btyFilter: "someBounties"
      }
    };


    var loadedPlatform = VENN_PLATFORM;
    if (loadedPlatform) {
      $scope.data.query.platform = loadedPlatform;
      $scope.data.query.gt1.value = VENN_NAME != '' ? VENN_NAME : null;
      $scope.data.query.gt2.value = VENN_NAME2 != '' ? VENN_NAME2 : null;
      $scope.data.query.gt3.value = VENN_NAME3 != '' ? VENN_NAME3 : null;
    }
    else {
      var savedPlatform = localStorage.getItem("vennPlatform");
      if (savedPlatform != 2) savedPlatform = 1;
      $scope.data.query.platform = savedPlatform;
      $scope.data.query.gt1.value = localStorage.getItem("vennGt1");
      $scope.data.query.gt2.value = localStorage.getItem("vennGt2");
      $scope.data.query.gt3.value = localStorage.getItem("vennGt3");
    }

    if ($scope.data.query.platform == 2) $("#vennPlatform").prop('checked', true).change();

    $('#vennPlatform').change(function () {
      console.log("Change");
      var val = $(this).prop('checked');
      var platform = val ? 2 : 1;
      $scope.data.query.platform = platform;
      $scope.$digest();
    });

    function submitQuery(query) {
      localStorage.setItem("vennPlatform", query.platform);
      localStorage.setItem("vennGt1", query.gt1.value);
      localStorage.setItem("vennGt2", query.gt2.value);
      if (query.gt3.value == null)
        localStorage.setItem("vennGt3", null);
      else
        localStorage.setItem("vennGt3", query.gt3.value);
      ReadService.load(query.platform, query.gt1, query.gt2, query.gt3, query.refresh, $scope, loadHidden());
    }

    $scope.submitVenn = function (query) {
      submitQuery(query);
    }
    
    $scope.hideQuest = function(quest){
      quest.hidden = true;
      $scope.data.questsHidden = true;
      saveHidden();
    }
    
    function saveHidden(){
      if (!$scope.data.quests) return;
      var oHash = {};
      $scope.data.quests.forEach(function(q){
        if (q.hidden) 
          oHash[q.questHash] = true;
      });
      localStorage.setItem("hiddenQuests", JSON.stringify(oHash));
    }
    
    function loadHidden(quests){
      var oHash = {};
      try{
        var sQuests = localStorage.getItem("hiddenQuests");
        oHash = JSON.parse(sQuests);
      }
      catch (e)
      {
        //ignore
      }
      return oHash;
    }

    $scope.showAllQuests = function(){
      $scope.data.quests.forEach(function(quest){
        delete quest.hidden;
      });
      delete $scope.data.questsHidden;
      saveHidden();
    }

    $scope.clickToOpen = function (q) {
      $scope.data.selectedQuest = q;
      $('#questModal').modal();
    };

    $scope.clickToOpenBty = function (q) {
      $scope.data.selectedBty = q;
      $('#btyModal').modal();
    };

    $scope.getDirectLink = function(query){
      var val = "/venn/"+query.platform+"/"+query.gt1.value+"/"+query.gt2.value;
      if (query.gt3.value) {
        val += "/" + query.gt3.value;
      }
      return val;
    };

    $scope.filterBounties= function(bty) {
      switch($scope.data.query.btyFilter) {
        case 'someBounties':
          return bty.members && bty.members.length>1;
          break;
        case 'allBounties':
          return bty.members && bty.members>=$scope.data.goodPlayers;
          break;
        default:
          return true;
      }
    }
    
    $scope.filterQuests = function(quest) {
      switch($scope.data.query.filter) {
        case 'someQuests':
          return quest.members && quest.members.length>1;
          break;
        case 'allQuests':
          return quest.members && quest.members>=$scope.data.goodPlayers;
          break;
        case 'someSteps':
          return quest.membersOnSameStep && quest.membersOnSameStep>1;
          break;
        case 'allSteps':
          return quest.membersOnSameStep && quest.membersOnSameStep>=$scope.data.goodPlayers.length;
          break;
        default:
          return true;
      }
    }

      console.log("loaded");

    //auto search if we're full
    if ($scope.data.query.platform && $scope.data.query.gt1.value && $scope.data.query.gt2.value){
      submitQuery($scope.data.query);
    }
  }]);

vennApp.service("ReadService", ["$http", "$q", "toaster", function ($http, $q, toaster) {
  return ({
    load: load
  });
  
  function cookRaid(raid){
    if (!raid) return;
    if (raid.hm && raid.hm.steps) {
      if (raid.hm.steps[raid.hm.steps.length - 1].isComplete == true) {
        raid.hmComplete = true;
      }
    }
    else if (raid.nm.steps[raid.nm.steps.length-1].isComplete==true){
      raid.nmComplete = true;
    }
    return raid;
  }

  function cookWeekly(allChars){
   var weeklyChars = [];

    allChars.forEach(function (char){
      var wkly = {};
      if (!char.publicAdv) return;
      wkly.coe = char.publicAdv.coe;
      if (wkly.coe){
        if (wkly.coe.high.complete && wkly.coe.cum.complete)
          wkly.coe.done = true;
        else wkly.coe.done = false;
      }
      wkly.kf = cookRaid(char.publicAdv.kf);
      wkly.ce = cookRaid(char.publicAdv.ce);
      wkly.vog = cookRaid(char.publicAdv.vog);
      wkly.wrath = cookRaid(char.publicAdv.wrath);
      

      wkly.gunsmith = char.publicAdv.gunsmith;
      wkly.nightfall = char.publicAdv.nightfall;
      wkly.dailyHeroic = char.publicAdv.dailyHeroic;
      wkly.dailyPvp = char.publicAdv.dailyPvp;
      wkly.weeklyHeroic = char.publicAdv.weeklyHeroic;
      wkly.weeklyCrucible = char.publicAdv.weeklyCrucible;
      wkly.nightfall = char.publicAdv.nightfall;
      weeklyChars.push(wkly);
    });
    return weeklyChars;
  }

  function cookBounties(allChars){
    var bounties = {};

    //load up all the quests
    allChars.forEach(function (char){
      if (char.publicAdv){
        //if the char has any quests, iterate them
        char.publicAdv.bounties.forEach(function(bounty){
          //if it's complete we don't care about it
          if (bounty.completed) return;
          //if all the objectives are met, it's also done
          
          if (!bounty.objectives) bounty.objectives = bounty.steps;
          if (!bounty.objectives) bounty.objectives = [];
          
          var done = true;
          if (bounty.objectives) {
            if (bounty.objectives.length==0){
              done = false;
            }
            else {
              bounty.objectives.forEach(function (obj) {
                if (obj.progress < obj.value) done = false;
              });
            }
          }
          if (done) return;
          //try to grab the globally cached quest, if it's not there copy this one
          if (!bounties[bounty.questHash]){
            bounties[bounty.questHash] =  angular.copy(bounty);

            //delete started, completed, objects[].progress
            delete bounties[bounty.questHash].started;
            delete bounties[bounty.questHash].completed;
            if (bounties[bounty.questHash].objectives){
              bounties[bounty.questHash].objectives.forEach(function(obj){
                delete obj.progress;
                obj.progress = [];
              });
            }
             //associate array of all chars holding the quest, we'll delete this
            bounties[bounty.questHash].holdingChars = {};
            //and we'll push the chars to this array instead
            bounties[bounty.questHash].chars = [];
            bounties[bounty.questHash].holdingMembers = {};
            //same plan here, holdingMembers goes away and gets pushed to this array
            bounties[bounty.questHash].members = [];
          }
          var target = bounties[bounty.questHash];
          bounty.objectives.forEach(function(obj, index){
            bounties[bounty.questHash].objectives[index].progress.push({
              char: char, 
              progress: obj.progress
            });
          });
          target.holdingChars[char.id] = true;
          target.holdingMembers[char.parent.id] = char.parent;
        });
      }
    });
    //now that we have a superset of quests, iterate through each char to mark them
    var aBounties = [];
    angular.forEach(bounties, function(bounty){
      allChars.forEach(function (char){
        //this char has the bounty
        if (bounty.holdingChars[char.id]){
          bounties[bounty.questHash].chars.push(bounty.holdingChars[char.id]);
        }
        else{
          bounties[bounty.questHash].chars.push(null);
        }
      });
      angular.forEach(bounty.holdingMembers, function(member){
        bounty.members.push(member);
      });
      delete bounty.holdingMembers;
      delete bounty.holdingChars;
      aBounties.push(bounty);
    });

    return aBounties;
  }



  function cookQuests(allChars){
    var quests = {};
   
    //load up all the quests
    allChars.forEach(function (char){
      if (char.publicAdv){
        //if the char has any quests, iterate them
        char.publicAdv.quests.forEach(function(quest){
          //try to grab the globally cached quest, if it's not there copy this one
          if (!quests[quest.questHash]){
            quests[quest.questHash] =  angular.copy(quest);
            //there is no shared current step, delete it
            delete quests[quest.questHash].currentStep;
            //associate array of all chars holding the quest, we'll delete this
            quests[quest.questHash].holdingChars = {};
            //and we'll push the chars to this array instead
            quests[quest.questHash].chars = [];
            quests[quest.questHash].holdingMembers = {};
            //same plan here, holdingMembers goes away and gets pushed to this array
            quests[quest.questHash].members = [];
          }
          var target = quests[quest.questHash];
          target.holdingChars[char.id] = quest.currentStep;
          if (target.steps[quest.currentStep-1].chars == null){
            target.steps[quest.currentStep-1].chars = [];

            target.steps[quest.currentStep-1].holdingMembers = {};
          }
          target.steps[quest.currentStep-1].chars.push(char);
          target.steps[quest.currentStep-1].holdingMembers[char.parent.id] = true;
          target.holdingMembers[char.parent.id] = char.parent;
          
        });
      }
    });
    //now that we have a superset of quests, iterate through each char to mark them
    var aQuests = [];
    angular.forEach(quests, function(quest){
      allChars.forEach(function (char){
        //this char has the quest
        if (quest.holdingChars[char.id]>0){
          quests[quest.questHash].chars.push(quest.holdingChars[char.id]);
        }
        else{
          quests[quest.questHash].chars.push(-1);
        }
      });
      angular.forEach(quest.holdingMembers, function(member){
        quest.members.push(member);
      });
      
      var maxMaxMembers = 0;
      quest.steps.forEach(function(step, stepIndex){
        if (!step.chars || step.chars.length==0) return;
        step.maxMembers = 0;
        angular.forEach(step.holdingMembers, function(member){
          step.maxMembers++;
        });
        if (step.maxMembers>maxMaxMembers) maxMaxMembers = step.maxMembers;
        delete step.holdingMembers;
      });
      
      quest.membersOnSameStep = maxMaxMembers;
      
      delete quest.holdingMembers;
      delete quest.holdingChars;
      aQuests.push(quest);
    });
    
    return aQuests;
  }

  function load(platform, gt1, gt2, gt3, refresh, $scope, saveHiddenHashes) {
    var url = "/api/venn/" + platform + "/" + encodeURIComponent(gt1.value) + "/" + encodeURIComponent(gt2.value);
    if (gt3.value) url += "/" + encodeURIComponent(gt3.value);
    gt1.validclass = null;
    gt2.validclass = null;
    gt3.validclass = null;
    var gts = [gt1, gt2, gt3];
    $scope.data.loading = true;
    $http({
      method: "get",
      url: url,
      params: {nocache: refresh}
    }).then(function (response) {
      $scope.data.loading = false;
      if (!response.data) {
        $scope.data.errormsg = "Problem loading data from Bungie";
        return;
      }
      
      $scope.data.goodPlayers = [];
      //should have at least 2 items in the array
      if (response.data.length > 1) {
        response.data.forEach(function (member, index) {
          var gt = gts[index];
          if (member.memberId) {
            gt.chars = member.chars
            gt.id = member.memberId;
            gt.validclass = "has-success";
            gt.encodeName = encodeURIComponent(gt.value);
            $scope.data.goodPlayers.push(gt);
          }
          else if (member.err) {
            if (member.err.message && member.name) {
              toaster.pop({
                type: 'error',
                title: member.name,
                body: member.err.message,
                timeout: 5000
              });
            }
            else {
              toaster.pop({
                type: 'error',
                title: "Unexpected error",
                body: "Weird...",
                timeout: 5000
              });
            }
            gt.validclass = "has-error";
          }
        });
      }
      var allChars = [];
      //get complete list of chars
      $scope.data.goodPlayers.forEach(function(player) {
        player.chars.forEach(function (char) {
          char.parent = player;
          allChars.push(char);
        });
      });
      
      $scope.data.allChars = allChars;
      $scope.data.quests = cookQuests(allChars);
      if (saveHiddenHashes && $scope.data.quests){
        var someHidden = false;
        $scope.data.quests.forEach(function(q){
          if (saveHiddenHashes[q.questHash]==true){
            q.hidden = true;
            someHidden = true;
          }
        });
        if (someHidden==true){
          $scope.data.questsHidden = true;
        }
      }
      $scope.data.bounties = cookBounties(allChars);
      $scope.data.weekly = cookWeekly(allChars);
    }, handleError);
  }

  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return ($q.reject("An unknown error occurred."));
    }
    return ($q.reject(response.data));
  }

}]);