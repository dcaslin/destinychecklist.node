/**
 * Created by Dave on 6/30/2016.
 */
var UTILS = require("./utils");
var cs = require("./cacheService")();
var moment = require('moment-timezone');
var async = require("async");

var rClient;
var COMMON;
var fns = {
  listIbChars: listIbChars
};

module.exports = function (redis, common) {
  
  rClient = redis;
  COMMON = common;
  return fns;
};


function applyIbProgression(platform, memberId, char, callback) {
  char.medallions = 0;

  UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/' + platform + "/Account/" + memberId + "/Character/" + char.id + "/Progression",
    function (error, jo) {
      if (error) {
        callback(error);
        return;
      }
      var ja = jo.Response.data.progressions;
      for (var cntr = 0; cntr < ja.length; cntr++) {
        if (ja[cntr].progressionHash == 2161005788) {
          char.ibXp = ja[cntr].currentProgress;
        }
        if (ja[cntr].progressionHash == 594203991) {
          char.medallions = ja[cntr].level;
        }

      }
      callback();
    });
}


function listIbChars(platform, memberId, callback, req) {
  var key = "ibChars:" + platform + ":" + memberId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      cached = JSON.parse(cached);
      callback(null, cached);

    }
    else {
      UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/' + platform + "/Account/" + memberId + "/Summary/",
        function (error, jo) {
          if (error) {
            callback(error);
            return;
          }
          var rm = {
            player: {
              memberId: memberId,
              platform: platform
            }, chars: []
          };
          var j = jo.Response.data;
          for (var cntr = 0; cntr < j.characters.length; cntr++) {
            var char = j.characters[cntr];
            char = char.characterBase;
            var raceName = cs.cache(req).Race[char.raceHash].raceName;
            var genderName = cs.cache(req).Gender[char.genderHash].genderName;
            var className = cs.cache(req).Class[char.classHash].className;

            var jchar = {
              id: char.characterId,
              label: className + " (" + char.stats.STAT_LIGHT.value + ") " + raceName.substr(0, 2) + " " + genderName.substr(0, 1),
              race: raceName,
              gender: genderName,
              class: className,
              power: char.stats.STAT_LIGHT.value
            };
            rm.chars.push(jchar);
          }
          async.each(rm.chars, function (char, subCb) {
            applyIbProgression(platform, memberId, char, subCb);
          }, function (err) {
            if (err) {
              callback(error);
              return;
            }
            async.each(rm.chars, function (char3, subCb3) {
              COMMON.applyPublicAdvisors(platform, memberId, char3.id, char3, subCb3, req);
            }, function (err) {
              if (err) {
                callback(error);
                return;
              }
              rClient.set(key, JSON.stringify(rm));
              rClient.expire(key, 3600);
              callback(null, rm);
            });
          });
        }
      );
    }
  });
}
