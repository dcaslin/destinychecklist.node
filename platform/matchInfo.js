var UTILS = require("./utils");

var cs = require("./cacheService")();
var moment = require('moment-timezone');
var LEADERBOARDS;
var rClient;

var fns = {
  matchInfo: matchInfo,
  pvpMatchInfo: pvpMatchInfo,
  srlMatchInfo: srlMatchInfo
};

module.exports = function (redis, leaderboards) {
  rClient = redis;
  LEADERBOARDS = leaderboards;
  return fns;
};

function pad(num, size){
  var s = num+"";
  while (s.length < size) s = "0" + s;
  return s;
}

function formatDuration(duration){
//  return "Asdf";
  return pad(duration.hours(), 2)+":"+pad(duration.minutes(), 2)+":"+pad(duration.seconds(), 2);
}

function pvpMatchInfo(instanceId, callback, req) {
  var key = "pvpMI:" + instanceId;
  
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      cached = JSON.parse(cached);
      callback(null, cached);

    }
    else {
      UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/Stats/PostGameCarnageReport/" + instanceId,
        function (error, jo) {
          if (error) {
            callback(error);
            return;
          }
          var hash = jo.Response.data.activityDetails.referenceId;
          var aDesc = cs.cache(req).Activity[hash];
          var level = aDesc.activityLevel;
          var name = aDesc.activityName;
          
          var pvpType = jo.Response.data.activityDetails.activityTypeHashOverride>0?cs.cache(req).ActivityType[jo.Response.data.activityDetails.activityTypeHashOverride].activityTypeName: 'Custom';
          var complete = false;
          var duration = 0;

          //teams
          var teams = [];
          var dTeam = {};
          var added = false;
          if (pvpType == "Rumble" || pvpType == "Mayhem Rumble") {

          }
          else {
            for (var cntr = 0; cntr < jo.Response.data.teams.length; cntr++) {
              var team = jo.Response.data.teams[cntr];
              var oTeam = {
                id: team.teamId,
                name: team.teamName,
                score: team.score.basic.value,
                result: team.standing.basic.displayValue,
                players: []
              };
              teams.push(oTeam);
              dTeam[team.teamId] = oTeam;
              added = true;
            }
          }
          if (!added) {
            dTeam[0] = {
              id: 0,
              name: "No team",
              score: 0,
              result: "N/A",
              players: []
            };
            teams.push(dTeam[0]);
          }

          var entries = jo.Response.data.entries;
          var fireTeamMap = {};
          var validFireTeam = {};
          var fireTeamCntr = 0;
          var compReason = 0;

          for (var cntr = 0; cntr < entries.length; cntr++) {
            var entry = entries[cntr];
            var teamId = 0;
            if (entry.values.team)
              teamId = entry.values.team.basic.value;

            if (entry.values.completed.basic.value >= 1) complete = true;

            var pd = entry.player.destinyUserInfo;

            var weaponKills = [];

            var fireTeamId = null;
            compReason = entry.values.completionReason.basic.value;

            if (entry.extended) {
              if (entry.extended.values) {
                if (entry.extended.values.fireTeamId) {
                  var tId = entry.extended.values.fireTeamId.basic.value;
                  if (!fireTeamMap[tId]) {
                    fireTeamCntr++;
                    fireTeamMap[tId] = fireTeamCntr;
                  }
                  else {
                    validFireTeam[fireTeamMap[tId]] = true;
                  }
                  fireTeamId = fireTeamMap[tId];
                }
              }


            if (entry.extended.weapons) {
              for (var cntr2 = 0; cntr2 < entry.extended.weapons.length; cntr2++) {
                var k = entry.extended.weapons[cntr2];

                var cd = cs.cache(req).InventoryItem[k.referenceId];
                var weaponName, weaponType;
                if (cd) {
                  weaponName = cd.itemName;
                  weaponType = cd.itemTypeName;
                }
                else {
                  weaponName = "Secret";
                  weaponType = "Not in DB";
                }
                weaponKills.push({
                  name: weaponName,
                  type: weaponType,
                  killCount: k.values.uniqueWeaponKills.basic.value,
                  prec: k.values.uniqueWeaponKillsPrecisionKills.basic.value
                });
              }
            }

            if (entry.extended.values.weaponKillsGrenade)
              weaponKills.push({
                name: "Grenades",
                type: "",
                killCount: entry.extended.values.weaponKillsGrenade.basic.value,
                prec: 0
              });
            if (entry.extended.values.weaponKillsSuper)
              weaponKills.push({
                name: "Super",
                type: "",
                killCount: entry.extended.values.weaponKillsSuper.basic.value,
                prec: 0
              });
            if (entry.extended.values.weaponKillsMelee)
              weaponKills.push({
                name: "Melee",
                type: "",
                killCount: entry.extended.values.weaponKillsMelee.basic.value,
                prec: 0
              });
            }
            weaponKills.sort(function (a, b) {
              if (a.killCount < b.killCount) return 1;
              if (a.killCount > b.killCount) return -1;
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            });

            if (entry.values.activityDurationSeconds.basic.value > duration)
              duration = entry.values.activityDurationSeconds.basic.value;


            var kdr = "100.0";
            if (entry.values.deaths.basic.value) {
              kdr = (entry.values.kills.basic.value / entry.values.deaths.basic.value).toFixed(1);
            }
            var addToTeam;
            if (dTeam[teamId]) {
              addToTeam = dTeam[teamId];
            }
            else if (!added) {
              addToTeam = dTeam[0];
            }

            if (addToTeam) {
              addToTeam.players.push({
                fireTeam: fireTeamId,
                platform: pd.membershipType,
                memberId: pd.membershipId,
                class: entry.player.characterClass,
                name: pd.displayName,
                completeMsg: entry.values.completed.basic.displayValue,
                duration: formatDuration(moment.duration(entry.values.activityDurationSeconds.basic.value, "seconds")),
                kills: entry.values.kills.basic.value,
                deaths: entry.values.deaths.basic.value,
                assists: entry.values.assists.basic.value,

                revives: entry.extended && entry.extended.values.resurrectionsPerformed ? entry.extended.values.resurrectionsPerformed.basic.value : 0,
                revived: entry.extended && entry.extended.values.resurrectionsReceived ? entry.extended.values.resurrectionsReceived.basic.value : 0,
                weapons: weaponKills,
                kdr: kdr,
                score: entry.values.score.basic.value
              });
            }
          }

          teams.sort(UTILS.sortByName);

          for (var cntr = 0; cntr < teams.length; cntr++) {

            for (var cntr2 = 0; cntr2 < teams[cntr].players.length; cntr2++) {
              var fireTeamId = teams[cntr].players[cntr2].fireTeam;
              if (!validFireTeam[fireTeamId])
                teams[cntr].players[cntr2].fireTeam = null;


            }

            teams[cntr].players.sort(function (a, b) {
              if (a.score < b.score) return 1;
              if (a.score > b.score) return -1;
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            });
          }
          var rm = {
            name: name,
            type: pvpType,
            level: level,
            compReason: compReason,
            end: moment(jo.Response.data.period).add(duration, "seconds").format('MM/DD/YYYY, h:mm:ss A z'),
            duration: formatDuration(moment.duration(duration, "seconds")),
            complete: complete,
            teams: teams
          };
          rClient.set(key, JSON.stringify(rm));
          rClient.expire(key, 3600);
          callback(null, rm);
        });
    }
  });

}

function matchInfo(instanceId, callback, req, checkLeader, check390) {
  var key = "pveMI:" + instanceId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      cached = JSON.parse(cached);
      callback(null, cached);

    }
    else {
      UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/Stats/PostGameCarnageReport/" + instanceId,
        function (error, jo) {
          if (error) {
            callback(error);
            return;
          }
          var hash = jo.Response.data.activityDetails.referenceId;
          var aDesc = cs.cache(req).Activity[hash];
          var level = aDesc.activityLevel;
          var name = aDesc.activityName;
          var complete = false;
          var duration = 0;
          var entries = jo.Response.data.entries;
          var hmRaid = false;
          if (aDesc.activityTypeHash) {
            //raid
            if (aDesc.activityTypeHash == 2043403989) {
              if (aDesc.skulls) {
                for (var cntr = 0; cntr < aDesc.skulls.length; cntr++) {
                  if (aDesc.skulls[cntr].displayName == "Heroic") {
                    hmRaid = true;
                    break;
                  }
                }
              }

            }
          }
          var players = [];
          var hasScore = false;
          var teamScore = 0;
          var totalScore = 0;
          for (var cntr = 0; cntr < entries.length; cntr++) {
            var entry = entries[cntr];
            if (entry.values.completed.basic.value >= 1) complete = true;

            var pd = entry.player.destinyUserInfo;

            var weaponKills = [];
            var score = null, displayScore = null;
            if (entry.score && entry.score.basic) {
              score = entry.score.basic.value;
              displayScore = entry.score.basic.displayValue;
              totalScore += score;
              if (score > 0)
                hasScore = true;
              if (entry.values.teamScore) {
                var curTeamScore = entry.values.teamScore.basic.value;
                if (curTeamScore > teamScore) teamScore = curTeamScore;
              }
            }
            if (!entry.extended) entry.extended = { values: {}};
            if (entry.extended.weapons) {
              for (var cntr2 = 0; cntr2 < entry.extended.weapons.length; cntr2++) {
                var k = entry.extended.weapons[cntr2];

                var cd = cs.cache(req).InventoryItem[k.referenceId];
                var weaponName, weaponType;
                if (cd) {
                  weaponName = cd.itemName;
                  weaponType = cd.itemTypeName;
                }
                else {
                  weaponName = "Secret";
                  weaponType = "Not in DB";
                }
                weaponKills.push({
                  name: weaponName,
                  type: weaponType,
                  killCount: k.values.uniqueWeaponKills.basic.value,
                  prec: k.values.uniqueWeaponKillsPrecisionKills.basic.value
                });
              }
            }

            if (entry.extended.values.weaponKillsGrenade)
              weaponKills.push({
                name: "Grenades",
                type: "",
                killCount: entry.extended.values.weaponKillsGrenade.basic.value,
                prec: 0
              });
            if (entry.extended.values.weaponKillsSuper)
              weaponKills.push({
                name: "Super",
                type: "",
                killCount: entry.extended.values.weaponKillsSuper.basic.value,
                prec: 0
              });
            if (entry.extended.values.weaponKillsMelee)
              weaponKills.push({
                name: "Melee",
                type: "",
                killCount: entry.extended.values.weaponKillsMelee.basic.value,
                prec: 0
              });

            weaponKills.sort(function (a, b) {
              if (a.killCount < b.killCount) return 1;
              if (a.killCount > b.killCount) return -1;
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            });

            if (entry.values.activityDurationSeconds.basic.value > duration)
              duration = entry.values.activityDurationSeconds.basic.value;
            players.push({
              platform: pd.membershipType,
              memberId: pd.membershipId,
              class: entry.player.characterClass,
              name: pd.displayName,
              completeMsg: entry.values.completed.basic.displayValue,
              duration: formatDuration(moment.duration(entry.values.activityDurationSeconds.basic.value, "seconds")),
              kills: entry.values.kills.basic.value,
              deaths: entry.values.deaths.basic.value,
              assists: entry.values.assists.basic.value,
              revives: entry.extended.values.resurrectionsPerformed ? entry.extended.values.resurrectionsPerformed.basic.value : 0,
              revived: entry.extended.values.resurrectionsReceived ? entry.extended.values.resurrectionsReceived.basic.value : 0,
              weapons: weaponKills,
              score: score,
              displayScore: displayScore
            });
          }

          players.sort(function (a, b) {
            if (a.kills < b.kills) return 1;
            if (a.kills > b.kills) return -1;
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
            return 0;
          });
          
          var oPeriod =  moment(jo.Response.data.period);
          var oEnd = oPeriod.add(duration, "seconds");
          
          var rm = {
            name: name,
            activityHash: hash,
            hmRaid: hmRaid,
            level: level,
            period: jo.Response.data.period,
            end: oEnd.format('MM/DD/YYYY, h:mm:ss A z'),
            duration: formatDuration(moment.duration(duration, "seconds")),
            score: oEnd.valueOf(),
            complete: complete,
            players: players,
            hasScore: hasScore,
            teamScore: teamScore,
            displayScore: teamScore?entry.values.teamScore.basic.displayValue:"",
            scorePenalty: totalScore - teamScore
          };
          rClient.set(key, JSON.stringify(rm));
          rClient.expire(key, 3600);
          callback(null, rm);
        });
    }
  });
}

function srlMatchInfo(instanceId, callback, req) {
  var key = "srlMI:" + instanceId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      cached = JSON.parse(cached);
      callback(null, cached);

    }
    else {
      UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/Stats/PostGameCarnageReport/" + instanceId,
        function (error, jo) {
          if (error) {
            callback(error);
            return;
          }
          var hash = jo.Response.data.activityDetails.referenceId;
          var aDesc = cs.cache(req).Activity[hash];
          var level = aDesc.activityLevel;
          var name = aDesc.activityName;
          var pvpType = jo.Response.data.activityDetails.activityTypeHashOverride>0?cs.cache(req).ActivityType[jo.Response.data.activityDetails.activityTypeHashOverride].activityTypeName: 'Custom';
          var complete = false;
          var compReason = 0;
          var duration = 0;
          var players = [];

          var entries = jo.Response.data.entries;
          for (var cntr = 0; cntr < entries.length; cntr++) {
            var entry = entries[cntr];
            if (entry.values.completed.basic.value >= 1) complete = true;
            var pd = entry.player.destinyUserInfo;
            compReason = entry.values.completionReason.basic.value;
            var gatesHit = 0;
            if (entry.extended.values)
              if (entry.extended.values.gatesHit)
                gatesHit = entry.extended.values.gatesHit.basic.value;
            if (entry.values.activityDurationSeconds.basic.value > duration)
              duration = entry.values.activityDurationSeconds.basic.value;

            players.push({
              platform: pd.membershipType,
              memberId: pd.membershipId,
              class: entry.player.characterClass,
              name: pd.displayName,
              completeMsg: entry.values.completed.basic.displayValue,
              duration: moment.duration(entry.values.activityDurationSeconds.basic.value, "seconds"),
              deaths: entry.values.deaths.basic.value,
              gatesHit: gatesHit,
              timeMs: entry.values.raceCompletionMilliseconds.basic.value,
              timePretty: entry.values.raceCompletionMilliseconds.basic.displayValue,
              place: entry.values.standing.basic.value

            });
          }
          players.sort(function (a, b) {
            if (a.place < b.place) return -1;
            if (a.place > b.place) return 1;
            return 0;
          });


          var rm = {
            name: name,
            type: pvpType,
            level: level,
            compReason: compReason,
            end: moment(jo.Response.data.period).add(duration, "seconds").format('MM/DD/YYYY, h:mm:ss A z'),
            duration: formatDuration(moment.duration(duration, "seconds")),
            complete: complete,
            players: players
          };
          rClient.set(key, JSON.stringify(rm));
          rClient.expire(key, 3600);
          callback(null, rm);
        });
    }
  });
}