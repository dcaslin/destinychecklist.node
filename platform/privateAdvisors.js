/**
 * Created by Dave on 6/30/2016.
 */

var UTILS = require("./utils");

var conf = require('../config.js')();
var CACHE = require(conf.dataDir + "/destiny.json");
var SECURE_REQUEST = require("./secureRequest")();

var fns = {
  refreshAdvisors: refreshAdvisors,
  getAdvisors: getAdvisors,
  refreshPublicAdvisors: refreshPublicAdvisors
};

module.exports = function (common) {
  COMMON = common;
  return fns;
};

var ADVISORS = null;

var MODEL_ACCOUNT = { //adeese titan
  memberId: '4611686018429587879',
  charId: '2305843009217525651'
};

var MODEL_PRIV_CHAR = "2305843009223785827";


function parseGeneric(item) {
  var cd = CACHE.InventoryItem[item.itemHash];
  if (cd == null) {
    return {
      hash: item.itemHash,
      name: "Secret",
      tier: "Not in",
      type: "public Bungie DB",
      armor: 0,
      perks: [],
      stats: []
    };
  }
  var name = cd.itemName;
  var returnMe = {
    hash: item.itemHash,
    name: name,
    tier: cd.tierTypeName,
    type: cd.itemTypeName,
    icon: cd.icon,
    perks: [],
    stats: []
  };

  if (item.damageType) {
    returnMe.dmgType = COMMON.decodeDamage(item.damageType);
    if (item.primaryStat) returnMe.dmg = item.primaryStat.value;
  }
  else if (item.primaryStat)
    returnMe.armor = item.primaryStat.value;

  if (item.perks) {
    for (var cntr = 0; cntr < item.perks.length; cntr++) {
      var perk = item.perks[cntr];
      var perkDef = CACHE.SandboxPerk[perk.perkHash];
      returnMe.perks.push({
        name: perkDef.displayName,
        desc: perkDef.displayDescription,
        active: perk.isActive
      });
    }
  }
  if (item.stats) {
    for (var cntr = 0; cntr < item.stats.length; cntr++) {
      var stat = item.stats[cntr];
      var statDef = CACHE.Stat[stat.statHash];
      returnMe.stats.push({
        name: statDef.statName,
        value: stat.value
      });
    }
  }
  return returnMe;
}


function parsePublicAdvisors(callback){

  UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/1/Account/"+MODEL_ACCOUNT.memberId+"/Character/"+MODEL_ACCOUNT.charId+"/Advisors/V2/",
    function (err, jo) {
      if (err) {
        callback(err);
        return;
      }
      var returnMe = {
        hashes: {}
      }
      var activities = jo.Response.data.activities;

      if (activities.weeklystory && activities.weeklystory.extended && activities.weeklystory.extended.skullCategories 
        && activities.weeklystory.extended.skullCategories.length>=1){
        returnMe.weeklyStory = {
          name: "Weekly Story",
          skulls: activities.weeklystory.extended.skullCategories[0].skulls
        };
      }

      if (activities.weeklycrucible && activities.weeklycrucible.display && activities.weeklycrucible.display.activityHash){
        returnMe.hashes.weeklyPvp = activities.weeklycrucible.display.activityHash;
        returnMe.weeklyPvp = CACHE.Activity[returnMe.hashes.weeklyPvp];
      }

      if (activities.heroicstrike && activities.heroicstrike.extended && activities.heroicstrike.extended.skullCategories && activities.heroicstrike.extended.skullCategories.length>=1){
        returnMe.weeklyHeroic = {
          name: "Weekly Heroic",
          skulls: activities.heroicstrike.extended.skullCategories[0].skulls 
        };
      }

      if (activities.nightfall && activities.nightfall.display && activities.nightfall.display.activityHash){
        var jDesc = CACHE.Activity[activities.nightfall.display.activityHash];
        if (jDesc){
          returnMe.nightfall = {
            name: jDesc.activityName
          };

      if (activities.nightfall.extended && activities.nightfall.extended.skullCategories && activities.nightfall.extended.skullCategories.length>=1){
            returnMe.nightfall.skulls = activities.nightfall.extended.skullCategories[0].skulls;
          }
        }
      }
      
      if (activities.elderchallenge && activities.elderchallenge.activityTiers && activities.elderchallenge.activityTiers.length>=1){
        
        returnMe.coe = {
          skulls: [],
          rounds: []
        }                        
                                          
        var rounds = activities.elderchallenge.activityTiers[0].extended.rounds;
        rounds.forEach(function(round){
          returnMe.coe.rounds.push({
            enemy: CACHE.EnemyRace[round.enemyRaceHash],
            boss: CACHE.Combatant[round.bossCombatantHash],
            bossLightLevel: round.bossLightLevel
          });
        });
        
        var skullCats =  activities.elderchallenge.extended.skullCategories;
        skullCats.forEach(function(skullCat){
          skullCat.skulls.forEach(function(skull){
            returnMe.coe.skulls.push(skull);
          });
        });
      }

      if (activities.weeklyfeaturedraid && activities.weeklyfeaturedraid.activityTiers && activities.kingsfall.activityTiers.length>=1){
        
        var challengeName = activities.weeklyfeaturedraid.display.recruitmentIds[0];
        challengeName = challengeName.slice(0, 'Heroic'.length*-1);
        returnMe.raidChallengeName = challengeName;
        returnMe.raidChallengeHash = activities.weeklyfeaturedraid.display.activityHash;
        var skullCats = activities.weeklyfeaturedraid.activityTiers[0].skullCategories;

        if (skullCats && skullCats.length>=1){
          var skulls = skullCats[0].skulls;
          returnMe.raidChallenges = [];
          skulls.forEach(function(skull){
            //var sChal = skull.displayName.slice(0, " Challenge".length*-1);
            returnMe.raidChallenges.push(skull);
          });          
        }
      }
      callback(null, returnMe);
    }
  );

}

function parsePrivateAdvisors(callback){
  SECURE_REQUEST.sendRequest("https://www.bungie.net/D1/Platform/Destiny/1/MyAccount/Character/"+MODEL_PRIV_CHAR+"/Advisors/", function (err, jo) {
    if (err) {
      callback(err);
      return;
    }
    
    try {
      //gunsmith
      var fieldTestWeapons = [];
      var foundryOrders = [];
      if (jo.Response.data.armsDay) {
        var aOrder = jo.Response.data.armsDay.orders;
        for (var cntr = 0; cntr < aOrder.length; cntr++) {

          var cd = CACHE.InventoryItem[aOrder[cntr].item.itemHash];
          var pItem = {
            name: cd.itemName,
            type: cd.itemTypeName,
            desc: cd.itemDescription
          };
          foundryOrders.push(pItem);
        }

        var aTest = jo.Response.data.armsDay.testWeapons;
        for (var cntr = 0; cntr < aTest.length; cntr++) {

          var cd = CACHE.InventoryItem[aTest[cntr].item.itemHash];
          var pItem = {
            name: cd.itemName,
            type: cd.itemTypeName,
            desc: cd.itemDescription
          };
          fieldTestWeapons.push(pItem);

        }
      }

      var advisors = {
        gunsmith: {
          fieldTestWeapons: fieldTestWeapons,
          foundryOrders: foundryOrders
        }
      };
      
      callback(null, advisors);
    }
    catch (exc) {
      callback(exc);

    }
  });
}


function getSaleItemCat(cats, targetName){
  if (cats==null) return null;
  for (var cntr=0; cntr<cats.length; cntr++){
    if (targetName == cats[cntr].categoryTitle){
      if (cats[cntr].saleItems) 
        return cats[cntr].saleItems;
    }
  }
  return null;
}

function parseOutfitter(callback){
  //load emblem vendor, fork, ignore errors, no callback
  SECURE_REQUEST.sendRequest("https://www.bungie.net/D1/Platform/Destiny/1/MyAccount/Character/"+MODEL_PRIV_CHAR+"/Vendor/134701236", function (err2, jo2) {
    if (err2) {
      callback(err2);
      return;
    }
    try {
      var jaEmblems = getSaleItemCat(jo2.Response.data.saleItemCategories, "Emblems");
      var aEmblems = [];
      var aShaders = [];
      if (jaEmblems){
        //var jaEmblems = jo2.Response.data.saleItemCategories[0].saleItems;
        for (var cntr = 0; cntr < jaEmblems.length; cntr++) {
          var hash = jaEmblems[cntr].item.itemHash;
          var desc = CACHE.InventoryItem[hash];
          var newItem = {
            hash: hash,
            name: desc.itemName,
            icon: desc.icon,
            emblem: desc.secondaryIcon
          };
          aEmblems.push(newItem);
        }
      }
     
      var jaShaders = getSaleItemCat(jo2.Response.data.saleItemCategories, "Shaders");
      if (jaShaders){
        for (var cntr = 0; cntr < jaShaders.length; cntr++) {
          var hash = jaShaders[cntr].item.itemHash;
          var desc = CACHE.InventoryItem[hash];
          var newItem = {
            hash: hash,
            name: desc.itemName,
            icon: desc.icon
          };
          aShaders.push(newItem);
        }
      }
      
      callback(null, {
        emblems: aEmblems,
        shaders: aShaders
      });
      return;
    }
    catch (exc2) {
      callback(exc2);
      return;
    }
  });
}

function refreshPublicAdvisors(){
  console.log("Getting public advisors.");
  if (ADVISORS == null){
    ADVISORS = {};
  }
  parsePublicAdvisors(function(err, publicAdvisors){
    if (!err){

      console.log("PublicAdvisor refreshed successfully");
      ADVISORS.public = publicAdvisors;
    }
    else{
      console.log("PublicAdvisor refresh error");
      console.dir(err);
    }
  });
}

function refreshAdvisors(callback) {
  ADVISORS = {};
  console.log("Getting private advisors.");
  parsePrivateAdvisors(function(err, privAdviors){
    if (!err){

      console.log("PrivateAdvisor refreshed successfully");
      ADVISORS.private = privAdviors;
    }
    else{
      console.log("PrivateAdvisor refresh error");
      console.dir(err);
    }
  });

  refreshPublicAdvisors();


  console.log("Getting outfitter.");
  parseOutfitter(function(err, outfitter){
    if (!err){

      console.log("Outfitter refresh success");
      ADVISORS.outfitter = outfitter;
    }
    else{
      console.log("Outfitter refresh error");
      console.dir(err);
    }
  });

  callback(null, "success");
}

function getAdvisors() {
  return ADVISORS;
}
