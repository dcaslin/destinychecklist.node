/**
 * Created by Dave on 6/30/2016.
 */
var UTILS = require("./utils");

var cs = require("./cacheService")();
var moment = require('moment-timezone');
var async = require("async");

var rClient;
var fns = {
    listCalcFrags: listCalcFrags
};

module.exports = function (redis) {
    rClient = redis;
    return fns;
};

var CALC_FRAG_LOCS = {
    'I': {number: 1, activity: 'Patrol', location: 'Mausoleum', note: 'Invis Platforms'},
    'II': {number: 2, activity: 'Patrol', location: 'Mausoleum', note: 'Pentagonal Doorway'},
    'III': {number: 3, activity: 'Patrol', location: 'Mausoleum', note: 'Small Passage'},
    'IV': {number: 4, activity: 'Patrol', location: 'Mausoleum', note: 'Cabal Ship'},
    'V': {number: 5, activity: 'Patrol', location: 'Hull Breach', note: 'Cabal Ship'},
    'VI': {number: 6, activity: 'Patrol', location: 'Hull Breach', note: 'Near Wormsinger Rune'},
    'VII': {number: 7, activity: 'Patrol', location: 'Hull Breach', note: 'Lower Ship'},
    'VIII': {number: 8, activity: 'Patrol', location: 'Hull Breach', note: 'Up Wall'},
    'IX': {number: 9, activity: 'Patrol', location: 'Hull Breach', note: 'near Cabal Ship'},
    'X': {number: 10, activity: 'Patrol', location: 'Hall Of Souls', note: 'Passages'},
    'XI': {number: 11, activity: 'Patrol', location: 'Court of Oryx', note: 'Platform'},
    'XII': {number: 12, activity: 'Patrol', location: 'Court of Oryx', note: 'Top of Pillar'},
    'XIII': {number: 13, activity: 'Patrol', location: 'Court of Oryx', note: 'Side of Pillar'},
    'XIV': {number: 14, activity: 'Patrol', location: 'Trenchway', note: 'Near scanned worm'},
    'XV': {number: 15, activity: 'Patrol', location: 'Trenchway', note: 'Fallen Pillars'},
    'XVI': {number: 16, activity: 'Mission', location: 'Dreadnaught', note: 'Near start'},
    'XVII': {number: 17, activity: 'Mission', location: 'Dreadnaught', note: 'Near XVI'},
    'XVIII': {number: 18, activity: 'Mission', location: 'Regicide', note: 'Trenchway'},
    'XIX': {number: 19, activity: 'Mission', location: 'Dreadnaught', note: 'Past invis platform'},
    'XX': {number: 20, activity: 'Mission', location: 'Dreadnaught', note: 'Hive Tunnel'},
    'XXI': {number: 21, activity: 'Patrol', location: 'Mausoleum', note: 'Founts'},
    'XXII': {number: 22, activity: 'Patrol', location: 'Founts', note: 'Past XXI'},
    'XXIII': {number: 23, activity: 'Patrol', location: 'Founts', note: 'Shoot pod'},
    'XXIV': {number: 24, activity: 'Mission', location: 'Regicide', note: 'The Asylum'},
    'XXV': {number: 25, activity: 'Mission', location: 'Regicide', note: 'After XXIV'},
    'XXVI': {number: 26, activity: 'Strike', location: 'The Sunless Cell', note: 'Hallway'},
    'XXVII': {number: 27, activity: 'Mission', location: 'Regicide', note: 'The Asylum'},
    'XXVIII': {number: 28, activity: 'Strike', location: 'Shield Brothers', note: 'Debris pile'},
    'XXIX': {number: 29, activity: 'Raid', location: 'King\'s Fall', note: 'After jumping'},
    'XXX': {number: 30, activity: 'Raid', location: 'King\'s Fall', note: 'Before Totems'},
    'XXXI': {number: 31, activity: 'Raid', location: 'King\'s Fall', note: 'Maze after WP'},
    'XXXII': {number: 32, activity: 'Raid', location: 'King\'s Fall', note: 'After Golgy'},
    'XXXIII': {number: 33, activity: 'Raid', location: 'King\'s Fall', note: 'Piston Puzzle'},
    'XXXIV': {number: 34, activity: 'Patrol', location: 'Mausoleum', note: 'Smell Puzzle'},
    'XXXV': {number: 35, activity: 'Patrol', location: 'Hall of Souls', note: 'Poison passage'},
    'XXXVI': {number: 36, activity: 'Patrol', location: 'Hull Breach', note: 'Agonarch Rune'},
    'XXXVII': {number: 37, activity: 'Patrol', location: 'Mausoleum', note: 'Skyburners Command Pass'},
    'XXXVIII': {number: 38, activity: 'Patrol', location: 'Hull Breach', note: 'Use Wormsinger Rune'},
    'XXXIX': {number: 39, activity: 'Patrol', location: 'Court of Oryx', note: 'T2: Lookar + Vorlog'},
    'XL': {
        number: 40,
        activity: 'Patrol',
        location: 'Court of Oryx',
        note: 'T2: Bracus Horu\'usk + Mengoor and Cra\'adug'
    },
    'XLI': {number: 41, activity: 'Patrol', location: 'Court of Oryx', note: 'T2: Krughor + Mengoor and Cra\'adug'},
    'XLII': {number: 42, activity: 'Patrol', location: 'Court of Oryx', note: 'T2: Krughor + Vorlog'},
    'XLIII': {number: 43, activity: 'Raid', location: 'King\'s Fall HM', note: 'WP Challenge'},
    'XLIV': {number: 44, activity: 'Raid', location: 'King\'s Fall HM', note: 'Golgy Challenge'},
    'XLV': {number: 45, activity: 'Raid', location: 'King\'s Fall HM', note: 'Oryx Challenge'},
    'XLVI': {number: 46, activity: 'Strike', location: 'Nightfall', note: 'Complete on Dreadnaught'},
    'XLVII': {number: 47, activity: 'Patrol', location: 'Court of Oryx', note: 'T3: Thalnok'},
    'XLVIII': {number: 48, activity: 'Patrol', location: 'Court of Oryx', note: 'T3: Balwûr'},
    'XLIX': {number: 49, activity: 'Patrol', location: 'Court of Oryx', note: 'T3: Kagoor'},
    'L': {number: 50, activity: 'Patrol', location: 'Court of Oryx', note: 'Bounty Chain'}
};


function listCalcFrags(platform, memberId, callback, req) {
    var key = "cf:" + platform + ":" + memberId;
    if (req.nocache) rClient.del(key);
    rClient.get(key, function (err, cached) {
        if (cached) {
            cached = JSON.parse(cached);
            callback(null, cached);

        }
        else {
            UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/' + platform + "/Account/" + memberId + "/Summary/",
                function (error, jo) {
                    if (error) {
                        callback(error);
                        return;
                    }
                    var rm = {
                        player: {
                            memberId: memberId,
                            platform: platform
                        }, chars: []
                    };
                    var j = jo.Response.data;
                    for (var cntr = 0; cntr < j.characters.length; cntr++) {
                        var char = j.characters[cntr];
                        char = char.characterBase;
                        var raceName = cs.cache(req).Race[char.raceHash].raceName;
                        var genderName = cs.cache(req).Gender[char.genderHash].genderName;
                        var className = cs.cache(req).Class[char.classHash].className;

                        var jchar = {
                            id: char.characterId,
                            label: className + " (" + char.stats.STAT_LIGHT.value + ") " + raceName.substr(0, 2) + " " + genderName.substr(0, 1),
                            race: raceName,
                            gender: genderName,
                            class: className,
                            power: char.stats.STAT_LIGHT.value,
                            cf: []
                        };
                        rm.chars.push(jchar);
                    }
                    async.each(rm.chars, function (char2, subCb2) {
                        applyCalcFrags(platform, memberId, char2, subCb2);
                    }, function (err) {
                        if (err) {
                            callback(error);
                            return;
                        }
                        rClient.set(key, JSON.stringify(rm));
                        rClient.expire(key, 3600);
                        callback(null, rm);
                    });


                }
            );
        }
    });
}


function applyCalcFrags(platform, memberId, char, callback, req) {
    UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/" + platform + "/Account/" + memberId + "/Character/" + char.id + "/Advisors/",
        function (error, jo) {
            if (error) {
                if (error.message == "DestinyPrivacyRestriction") {
                    char.privacy = true;
                    callback(null);
                    return;
                }
                callback(error);
                return;
            }
            var found = 0;
            if (jo.Response.data.checklists) {
                for (var cntr = 0; cntr < jo.Response.data.checklists.length; cntr++) {
                    var cl = jo.Response.data.checklists[cntr];
                    if ("CHECKLIST_CALCIFIED_FRAGMENTS" != cl.identifier) continue;
                    for (var cntr2 = 0; cntr2 < cl.entries.length; cntr2++) {
                        var ent = cl.entries[cntr2];
                        var entId = ent.entityId;
                        var grimCard = cs.cache(req).GrimoireCardById[entId];
                        var colon = grimCard.cardName.indexOf(':');
                        if (colon < 0) colon = 0;
                        var numeral = grimCard.cardName.substring(0, colon);
                        char.cf.push({
                            id: entId,
                            numeral: numeral,
                            info: CALC_FRAG_LOCS[numeral],
                            name: grimCard.cardName,
                            complete: ent.state
                        });
                        if (ent.state == true) found++;
                    }
                }
                char.label += "  -  " + found + "/50";
            }
            callback(null);
        }
    );
}
