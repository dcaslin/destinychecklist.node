/**
 * Created by Dave on 12/2/2016.
 */

var conf = require('../config.js')();
var CACHE = require(conf.dataDir + "/destiny.json");

var fns = {
  parseItem: parseItem
};

module.exports = function () {
  return fns;
};

function parseItem(rawItm) {
  var itmHash = rawItm.itemHash;
  var jDesc = CACHE.InventoryItem[itmHash];
  if (!jDesc) return null;
  var steps = processNodes(rawItm.talentGridHash, rawItm.nodes);
  var dmgType = decodeDamage(rawItm.damageType);
  var quality = null;
  if (rawItm.stats) {
    rawItm.stats.forEach(function (stat) {
      stat.name = CACHE.Stat[stat.statHash].statName;
    });
  }
  if (rawItm.primaryStat) {
    rawItm.light = rawItm.primaryStat.value;
  }

  var stats = {};
  var metaType = "";

  if (jDesc.itemType == 14) {
    metaType = "emblem";
  }
  else if (jDesc.itemType == 0 && "Ship" == jDesc.itemTypeName) {
    metaType = "ship";
  }
  else if (jDesc.itemType == 0 && "Armor Shader" == jDesc.itemTypeName) {
    metaType = "shader";
  }
  else if (jDesc.itemType == 2) {
    metaType = "armor";
    quality = setQualityRating(rawItm, jDesc, steps);

    for (var cntr6 = 0; cntr6 < rawItm.stats.length; cntr6++) {
      if (rawItm.stats[cntr6].statHash == 144602215)
        stats.int = rawItm.stats[cntr6].value;
      else if (rawItm.stats[cntr6].statHash == 1735777505)
        stats.disc = rawItm.stats[cntr6].value;
      else if (rawItm.stats[cntr6].statHash == 4244567218)
        stats.str = rawItm.stats[cntr6].value;
    }
  }
  else if (jDesc.itemType == 3) {

    metaType = "weapon";
    stats.aa = jDesc.stats[1345609583] ? jDesc.stats[1345609583].value : 0;
    stats.inventory = jDesc.stats[1931675084] ? jDesc.stats[1931675084].value : 0;
    for (var cntr6 = 0; cntr6 < rawItm.stats.length; cntr6++) {
      if (rawItm.stats[cntr6].statHash == 1240592695)
        stats.range = rawItm.stats[cntr6].value;
      else if (rawItm.stats[cntr6].statHash == 4043523819)
        stats.impact = rawItm.stats[cntr6].value;
      else if (rawItm.stats[cntr6].statHash == 155624089)
        stats.stability = rawItm.stats[cntr6].value;
      else if (rawItm.stats[cntr6].statHash == 4284893193 && jDesc.itemTypeName != "Fusion Rifle")
        stats.rof = rawItm.stats[cntr6].value;
      else if (rawItm.stats[cntr6].statHash == 2961396640 && jDesc.itemTypeName == "Fusion Rifle")
        stats.rof = rawItm.stats[cntr6].value;
      else if (rawItm.stats[cntr6].statHash == 4188031367)
        stats.reload = rawItm.stats[cntr6].value;
      else if (rawItm.stats[cntr6].statHash == 3871231066)
        stats.magazine = rawItm.stats[cntr6].value;
    }
  }
  var classAllowed = "Any";
  if (jDesc.classType == 0) {
    classAllowed = "Titan";
  }
  else if (jDesc.classType == 1) {
    classAllowed = "Hunter";
  }
  else if (jDesc.classType == 2) {
    classAllowed = "Warlock";
  }

  var searchText = jDesc.itemName + " " + jDesc.itemTypeName;
  var keepCols = [];
  if (steps) {
    steps.forEach(function (column) {
      var keepSteps = [];
      column.forEach(function (step) {
        if (step.name == "Infuse") return;
        if (step.name.startsWith("Reforge")) return;
        //if (step.name=="Increase Discipline") return;
        //if (step.name=="Increase Strength") return;
        //if (step.name=="Increase Intellect") return;
        if (step.name == "Twist Fate") return;
        if (step.name == "Kinetic Damage") return;
        //if (step.name=="Solar Damage") return;
        //if (step.name=="Void Damage") return;
        //if (step.name=="Arc Damage") return;
        keepSteps.push(step);
        searchText += " " + step.name;
      });
      if (keepSteps.length > 0) {
        keepCols.push(keepSteps);
      }
    });
  }

  return {
    metaType: metaType,
    name: jDesc.itemName,
    type: jDesc.itemTypeName,
    light: rawItm.light,
    qualityPct: quality ? quality.qualityPct : null,
    icon: jDesc.icon,
    banner: jDesc.secondaryIcon,
    slot: CACHE.InventoryBucket[jDesc.bucketTypeHash].bucketName,
    itemType: jDesc.itemType,
    itemSubType: jDesc.itemSubType,
    tierTypeName: jDesc.tierTypeName,
    classAllowed: classAllowed,
    steps: keepCols,
    dmgType: dmgType,
    quality: quality,
    stats: stats,
    searchText: searchText.toUpperCase()
  };
}


function decodeDamage(type) {
  if (type == 0)
    return "Kinetic";
  else if (type == 1)
    return "Kinetic";
  else if (type == 2)
    return "Arc";
  else if (type == 3)
    return "Solar";
  else if (type == 4)
    return "Void";
  else
    return "";
}

function processNodes(talentGridHash, nodes) {
  var steps = [];
  var tDesc = CACHE.TalentGrid[talentGridHash];
  if (!tDesc) {
    return;
  }
  var selectedNodes = {};
  for (var cntr7 = 0; cntr7 < nodes.length; cntr7++) {
    var node = nodes[cntr7];
    selectedNodes[node.nodeHash] = node;
  }

  for (var cntr7 = 0; cntr7 < tDesc.nodes.length; cntr7++) {
    var node = tDesc.nodes[cntr7];
    //in certain cases grab the first node of the first column? like "Perun's Prowl"
    //if (node.column < 1) continue;

    for (var cntr8 = 0; cntr8 < node.steps.length; cntr8++) {
      var step = node.steps[cntr8];
      step.col = node.column;
      var selNode = selectedNodes[node.nodeHash];
      if (selNode) {
        if (selNode.stepIndex == cntr8) {
          if (step.nodeStepName) {

            //steps.push(step); //step.nodeStepDescription, step.icon   selNode.isActivated
            steps.push({
              col: step.col,
              name: step.nodeStepName,
              desc: step.nodeStepDescription,
              activated: selNode.isActivated
            });
          }
        }
      }
    }
  }

  steps.sort(function (a, b) {
    return a.col - b.col;
  });

  var groups = {};
  steps.forEach(function (step) {
    if (!groups[step.col]) {
      groups[step.col] = [];
    }
    groups[step.col].push(step);
  });
  var aGroups = [];
  Object.keys(groups).forEach(function (key) {
    var group = groups[key];
    aGroups.push(group);
  });
  return aGroups;
}


function setQualityRating(rawItm, itemDesc, steps) {

  var stats = rawItm.stats;
  var light = rawItm.light;

  if (light < 280) {
    return null;
  }
  var type = CACHE.InventoryBucket[itemDesc.bucketTypeHash].bucketName;

  if (!stats || !stats.length) {
    return;
  }
  var split = getSplit(type);
  if (split == null) {
    return null;
  }

  var hasI = false, hasD = false, hasS = false;
  var bonus = getBonus(light, type);
  var strBonus = 0, discBonus = 0, intBonus = 0;

  //get the bonuses available and active
  for (var cntr = 0; cntr < steps.length; cntr++) {
    var childSteps = steps[cntr];
    //step is an array of node step names
    for (var cntr2 = 0; cntr2 < childSteps.length; cntr2++) {
      var step = childSteps[cntr2];
      if ('Increase Intellect' == step.name) {
        hasI = true;
        if (step.activated) intBonus = bonus;
      }
      else if ('Increase Discipline' == step.name) {
        hasD = true;
        if (step.activated) discBonus = bonus;
      }
      else if ('Increase Strength' == step.name) {
        hasS = true;
        if (step.activated) strBonus = bonus;
      }
    }
  }

  var returnMe = {
    qualityPct: -1,
    statSplit: '',
    intPct: 0,
    strPct: 0,
    disPct: 0
  };

  if (hasI && hasD) {
    returnMe.statSplit = "ID";
  }
  else if (hasD && hasS) {
    returnMe.statSplit = "DS";
  }
  else if (hasS && hasI) {
    returnMe.statSplit = "IS";
  }


  var working = {
    total: {
      min: 0,
      max: 0
    },
    max: split * 2
  };

  var pure = 0;

  for (var cntr = 0; cntr < stats.length; cntr++) {
    var stat = stats[cntr];
    stat.name = CACHE.Stat[stat.statHash].statName;

    var base = 0;
    if ("Intellect" == stat.name) {
      base = stat.value - intBonus;
    }
    else if ("Discipline" == stat.name) {
      base = stat.value - discBonus;
    }
    else if ("Strength" == stat.name) {
      base = stat.value - strBonus;
    }
    else {
      continue;
    }
    stat.base = base;

    if (stat.base) {
      stat.scaled = getScaledStat(stat.base, light);
      pure = stat.scaled.min;
    }
    else {
      stat.scaled = {
        min: 0,
        max: 0
      };
    }
    stat.split = split;
    stat.qualityPercentage = {
      min: Math.round(100 * stat.scaled.min / stat.split),
      max: Math.round(100 * stat.scaled.max / stat.split)
    };
    working.total.min += stat.scaled.min || 0;
    working.total.max += stat.scaled.max || 0;
  }

  if (pure === working.total.min) {
    stats.forEach(function (stat) {
      stat.scaled = {
        min: Math.floor(stat.scaled.min / 2),
        max: Math.floor(stat.scaled.max / 2)
      };
      stat.qualityPercentage = {
        min: Math.round(100 * stat.scaled.min / stat.split),
        max: Math.round(100 * stat.scaled.max / stat.split)
      };
    });
  }

  var quality = {
    min: Math.round(working.total.min / working.max * 100),
    max: Math.round(working.total.max / working.max * 100)
  };

  // if (type.toLowerCase() !== 'artifact') {
  //   stats.forEach(function (stat) {
  //     stat.qualityPercentage = {
  //       min: Math.min(100, stat.qualityPercentage.min),
  //       max: Math.min(100, stat.qualityPercentage.max)
  //     };
  //   });
  //   quality = {
  //     min: Math.min(100, quality.min),
  //     max: Math.min(100, quality.max)
  //   };
  // }

  stats.forEach(function (stat) {
    if (!stat.qualityPercentage) return;
    if ("Intellect" == stat.name) {
      returnMe.intPct = stat.qualityPercentage.min;
    }
    else if ("Discipline" == stat.name) {
      returnMe.disPct = stat.qualityPercentage.min;

    }
    else if ("Strength" == stat.name) {
      returnMe.strPct = stat.qualityPercentage.min;
    }
  });
  returnMe.qualityPct = quality.min;
  return returnMe;
}


function fitValue(light) {
  if (light > 300) {
    return (0.2546 * light) - 23.825;
  }
  if (light > 200) {
    return (0.1801 * light) - 1.4612;
  } else {
    return -1;
  }
}


function getScaledStat(base, light) {
  var max = 335;

  if (light > 335) {
    light = 335;
  }

  return {
    min: Math.floor((base) * (fitValue(max) / fitValue(light))),
    max: Math.floor((base + 1) * (fitValue(max) / fitValue(light)))
  };
}


function getBonus(light, type) {
  switch (type.toLowerCase()) {
    case 'helmet':
    case 'helmets':
      return light < 292 ? 15
        : light < 307 ? 16
          : light < 319 ? 17
            : light < 332 ? 18
              : 19;
    case 'gauntlets':
      return light < 287 ? 13
        : light < 305 ? 14
          : light < 319 ? 15
            : light < 333 ? 16
              : 17;
    case 'chest':
    case 'chest armor':
      return light < 287 ? 20
        : light < 300 ? 21
          : light < 310 ? 22
            : light < 319 ? 23
              : light < 328 ? 24
                : 25;
    case 'leg':
    case 'leg armor':
      return light < 284 ? 18
        : light < 298 ? 19
          : light < 309 ? 20
            : light < 319 ? 21
              : light < 329 ? 22
                : 23;
    case 'classitem':
    case 'class items':
    case 'class armor':
    case 'ghost':
    case 'ghosts':
      return light < 295 ? 8
        : light < 319 ? 9
          : 10;
    case 'artifact':
    case 'artifacts':
      return light < 287 ? 34
        : light < 295 ? 35
          : light < 302 ? 36
            : light < 308 ? 37
              : light < 314 ? 38
                : light < 319 ? 39
                  : light < 325 ? 40
                    : light < 330 ? 41
                      : 42;
  }
  console.warn('item bonus not found', type);
  return 0;
}

function getSplit(type) {
  var split = 0;
  switch (type.toLowerCase()) {
    case 'helmet':
      split = 46; // bungie reports 48, but i've only seen 46
      break;
    case 'gauntlets':
      split = 41; // bungie reports 43, but i've only seen 41
      break;
    case 'chest':
    case 'chest armor':
      split = 61;
      break;
    case 'leg':
    case 'leg armor':
      split = 56;
      break;
    case 'classitem':
    case 'class items':
    case 'class armor':
    case 'ghost':
    case 'ghosts':
      split = 25;
      break;
    case 'artifact':
    case 'artifacts':
      split = 38;
      break;
    default:
      return null;
  }
  return split;
}