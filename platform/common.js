/**
 * Created by Dave on 6/30/2016.
 */
var UTILS = require("./utils");
var cs = require("./cacheService")();
var moment = require('moment-timezone');
var async = require("async");

var rClient;

var fns = {
  applyPublicAdvisors: applyPublicAdvisors,
  applyProgression: applyProgression,
  cacheMemberId: cacheMemberId,
  getMemberName: getMemberName,
  getMemberId: getMemberId,
  listCharacters: listCharacters,
  listCharactersByMemberId: listCharactersByMemberId,
  getSupporters: getSupporters,
  decodeDamage: decodeDamage
};

module.exports = function (redis) {
  rClient = redis;
  return fns;
};

function ApiError(name, message) {
    this.name = name;
    this.message = message || name;
}
ApiError.prototype = Object.create(Error.prototype);
ApiError.prototype.constructor = ApiError;

function cacheMemberId(memberId, platform, name) {
  rClient.set("mem:" + platform + ":" + name, memberId);
  rClient.set("gt:" + memberId, name);
}


var SUPPORTERS = ["AAraKKe", "Itsnotunusual_rk", "Meggito", "Njspradlin7163", "Garrecht", "Nician", "EASY EAS", "cowgod77", "Yonnage", "Eaglewing5"];
var SUPPORTERS_LOWER = ["e5 dweezil", "meggito", "itsnotunusual_rk", "aarakke", "njspradlin7163", "garrecht", "nician", "easy eas", "cowgod77", "yonnage", "eaglewing5"];


function getSupporters() {
  return SUPPORTERS;
}

function isSupporter(name) {
  if (SUPPORTERS_LOWER.indexOf(name.toLowerCase()) >= 0) {
    return true;
  }
  return false;
}

function handleRaidActivity(storeMe, raid, only390){
  if (!raid) return;
  var raidO = raid.raidActivities;
  for (var cntr=0; cntr<raidO.tiers.length; cntr++){
    var tier = raidO.tiers[cntr];
    if ('DIFFICULTY_NORMAL'==tier.difficultyIdentifier){
      storeMe.nm.steps = tier.steps;
    }
    else if ('DIFFICULTY_HARD'==tier.difficultyIdentifier && cntr<2){
      storeMe.hm.steps = tier.steps;
      if (only390){
        storeMe.threeninety.steps = tier.steps;
      }
    }
    else if ('DIFFICULTY_HARD'==tier.difficultyIdentifier){
      storeMe.threeninety.steps = tier.steps;
    }
  }
}

function supplyQuestData(quest, req){
  var jQ = cs.cache(req).InventoryItem[quest.questHash];
  if (jQ) {
    quest.name = jQ.itemName;
    quest.desc = jQ.itemDescription?jQ.itemDescription:jQ.displaySource;
    quest.steps = [];
    if (jQ.setItemHashes) {
      for (var cntr2 = 0; cntr2 < jQ.setItemHashes.length; cntr2++) {
        var sHash = jQ.setItemHashes[cntr2];
        var jStep = cs.cache(req).InventoryItem[sHash];
        var current = quest.stepHash == sHash;
        var step = {
          hash: sHash,
          name: jStep.itemName,
          desc: jStep.itemDescription,
          current: current,
          objectives: []
        };
        if (!current) {
          if (jStep.objectiveHashes) {
            for (var cntr3 = 0; cntr3 < jStep.objectiveHashes.length; cntr3++) {
              var objHash = jStep.objectiveHashes[cntr3];
              var objDesc = cs.cache(req).Objective[objHash];
              if (!objDesc.displayDescription) continue;
              if (objDesc) {

                var obj = {
                  hash: objHash,
                  value: objDesc.completionValue,
                  item: objDesc.displayDescription
                };
                step.objectives.push(obj);
              }
            }
          }
        }
        else{
          quest.currentStep = cntr2+1;
          if (quest.stepObjectives){
            for (var cntr3 = 0; cntr3 < quest.stepObjectives.length; cntr3++) {
              var stepObj = quest.stepObjectives[cntr3];
              var objHash = stepObj.objectiveHash;
              var objDesc = cs.cache(req).Objective[objHash];
              if (!objDesc.displayDescription) continue;
              if (objDesc) {
                var obj = {
                  hash: objHash,
                  value: objDesc.completionValue,
                  item: objDesc.displayDescription,
                  progress: stepObj.progress
                };
                step.objectives.push(obj);
              }

            }

          }
        }
        if (step.objectives.length==0) delete step.objectives;
        quest.steps.push(step);
      }
    }
    if (quest.stepObjectives){
      var objs = [];
      for (var cntr3 = 0; cntr3 < quest.stepObjectives.length; cntr3++) {
        var stepObj = quest.stepObjectives[cntr3];
        var objHash = stepObj.objectiveHash;
        var objDesc = cs.cache(req).Objective[objHash];
        if (!objDesc.displayDescription) continue;
        if (objDesc) {
          var obj = {
            hash: objHash,
            value: objDesc.completionValue,
            item: objDesc.displayDescription,
            progress: stepObj.progress
          };
          objs.push(obj);
        }
      }
      if (objs.length>0) quest.objectives = objs;
    }
    if (jQ.sources && jQ.sourceHashes && jQ.sourceHashes.length>0){
      
      for (var cntr=0; cntr<jQ.sourceHashes.length; cntr++){
        var hash = jQ.sourceHashes[cntr];
        //Shiro
        if (hash==1536427767 ){
          if (jQ.values && jQ.values[3110575382]==500){
            quest.shiro = true;
          }
        }
        //Shaxx
        else if (hash==1257353826){
          if (jQ.values && jQ.values[2873479814]==500){
            quest.shaxx = true;  
          }
        }
      }
    }
    if (jQ.values){
      if (jQ.values[3639789914]) quest.crotaRep = jQ.values[3639789914];
      if (jQ.values[1719582120]) quest.queenRep = jQ.values[1719582120];
      if (jQ.values[3208461659]) quest.ibRep = jQ.values[3208461659];
      if (jQ.values[2759745988]) quest.variksRep = jQ.values[2759745988];
    }
      
  }
  delete quest.stepObjectives;
}

function applyPublicAdvisors(platform, memberId, characterId, returnMe, callback, req, ignoreError) {
  var redisKey = "publicAdv:" + platform + ":" + memberId + ":" + characterId;
  if (req.nocache) rClient.del(redisKey);
  rClient.get(redisKey, function (err, cached) {
    if (err) {
      callback(err);
      return;
    }
    if (cached) {
      returnMe.publicAdv = JSON.parse(cached);
      callback(null, returnMe);

    }
    else {
      UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/" + platform + "/Account/" + memberId + "/Character/" + characterId + "/Advisors/",
        function (error, jo) {
            if (error){
                if (error.message=="DestinyPrivacyRestriction"){
                    returnMe.privacy = true;
                    callback(null, returnMe);
                    return;
                }
                if (ignoreError) {
                  if (!returnMe.errors){
                    returnMe.errors = [];
                  }
                  returnMe.partialError = true;
                  returnMe.errors.push = {
                    action: "applyPublicAdvisors",
                    error: error
                  };
                  callback(null, returnMe);
                }
                else
                  callback(error);
                return;
            }
          returnMe.publicAdv = {
            vog: {
              nm: {},
              hm: {},
              threeninety: {}
            },
            ce: {
              nm: {},
              hm: {},
              threeninety: {}
            },
            wrath: {
              nm: {},
              hm: {},
              threeninety: {}
            },
            kf: {
              nm: {},
              hm: {},
              threeninety: {}
            },
            gunsmith: {
              tested: 0,
              total: 5
            },
            ib: {
              bounties: []
            },
            weeklyStory:
              {
                partial: false,
                full: false
              },
            bounties: [],
            quests: [],
            shaxx: 0,
            shiro: 0,
            queen: 0,
            crota: 0,
            variks: 0,
            calcFrags: []
          };
        
          var heldIronBounties = {};
          for (var cntr = 0; cntr < cs.cache(req).ironBounties.length; cntr++) {
            if (jo.Response.data.bounties[cs.cache(req).ironBounties[cntr]]) {
              var bty = jo.Response.data.bounties[cs.cache(req).ironBounties[cntr]];
              var qDesc = cs.cache(req).InventoryItem[bty.questHash];
              heldIronBounties[bty.questHash] = true;
              var ibBty = {
                name: qDesc.itemName,
                desc: qDesc.itemDescription,
                xp: qDesc.values[3208461659],
                objectives: []
              };

              var isFullyComplete = false;

              if (bty.stepObjectives) {
                isFullyComplete = true;

                for (var cntr2 = 0; cntr2 < bty.stepObjectives.length; cntr2++) {
                  var btyObj = bty.stepObjectives[cntr2];
                  var objDesc = cs.cache(req).Objective[btyObj.objectiveHash];

                  ibBty.objectives.push({
                    progress: btyObj.progress,
                    total: objDesc.completionValue,
                    isComplete: btyObj.isComplete,
                    desc: objDesc.displayDescription
                  });
                  isFullyComplete = isFullyComplete && btyObj.isComplete;
                }

              }
              ibBty.isFullyComplete = isFullyComplete;
              returnMe.publicAdv.ib.bounties.push(ibBty);

            }
          }
          
          if (jo.Response.data.weeklyCrucible){
            returnMe.publicAdv.weeklyCrucible = jo.Response.data.weeklyCrucible[0].isCompleted;
          }

          if (jo.Response.data.activityAdvisors){
            var actAdv = jo.Response.data.activityAdvisors;

            //WRATH
            handleRaidActivity(returnMe.publicAdv.wrath, actAdv[260765522], true);
            //KF
            handleRaidActivity(returnMe.publicAdv.kf, actAdv[1733556769]);
            //CE
            handleRaidActivity(returnMe.publicAdv.ce, actAdv[1836893116]);
            //VOG
            handleRaidActivity(returnMe.publicAdv.vog, actAdv[2659248071]);

            for (var aa in actAdv) {
              if (actAdv.hasOwnProperty(aa)) {
                var adv = actAdv[aa];
                if (adv.nightfall){
                  returnMe.publicAdv.nightfall = adv.nightfall.tiers[0].isCompleted;
                }
                else if (adv.heroicStrike){
                  returnMe.publicAdv.weeklyHeroic = adv.heroicStrike.tiers[0].isCompleted;
                }
                else if (adv.weeklyStory){
                  var rewardCnt = adv.weeklyStory.activeRewardIndexes.length;
                  if (rewardCnt<=3){
                    returnMe.publicAdv.weeklyStory.partial = true;
                  }
                  if (rewardCnt<=1){
                    returnMe.publicAdv.weeklyStory.full = true;
                  }
                }

              }
            }            
          }

          if (jo.Response.data.elderChallenge) {
            if (jo.Response.data.elderChallenge.objectives) {
              if (jo.Response.data.elderChallenge.objectives.length >= 2) {

                returnMe.publicAdv.coe = {};
                var n = jo.Response.data.elderChallenge.objectives[0];
                returnMe.publicAdv.coe.high = {
                  complete: n.isComplete,
                  score: n.progress
                };
                returnMe.publicAdv.coe.highScore = n.progress;
                n = jo.Response.data.elderChallenge.objectives[1];
                returnMe.publicAdv.coe.cum = {
                  complete: n.isComplete,
                  score: n.progress
                };
              }
            }
          }

          if (jo.Response.data.trials){
            returnMe.publicAdv.trials = {
              highestWinRank: jo.Response.data.trials.highestWinRank,
              active: jo.Response.data.trials.active,
              ticket: jo.Response.data.trials.ticket
            };
          }
          
          if (jo.Response.data.quests && jo.Response.data.quests.quests){
            for (var cntr=0; cntr<jo.Response.data.quests.quests.length; cntr++){
              var quest = jo.Response.data.quests.quests[cntr];
              supplyQuestData(quest, req);
              returnMe.publicAdv.quests.push(quest);
            }
          }
          if (jo.Response.data.bounties){
            for (var key in jo.Response.data.bounties) {
              if (jo.Response.data.bounties.hasOwnProperty(key)) {
                var bounty = jo.Response.data.bounties[key];
                supplyQuestData(bounty, req);
                if (bounty.shiro===true) returnMe.publicAdv.shiro++;
                if (bounty.shaxx===true) returnMe.publicAdv.shaxx++;
                if (bounty.queenRep>0) returnMe.publicAdv.queen++;
                if (bounty.variksRep>0) returnMe.publicAdv.variks++;
                if (bounty.crotaRep>0) returnMe.publicAdv.crota++;
                returnMe.publicAdv.bounties.push(bounty);
              }
            }
          }
          rClient.set(redisKey, JSON.stringify(returnMe.publicAdv));
          rClient.expire(redisKey, 3600);
          callback(null, returnMe);
        }
      );

    }
  });
}

function getMemberName(memberId, callback){
    rClient.get("gt:" + memberId, function (err, cached) {
        if (cached) {
            callback(null, cached);
        }
        else{
            callback(null, "?");
        }
    });
}

function decodeDamage(type) {
  if (type == 0)
    return "Kinetic";
  else if (type == 1)
    return "Kinetic";
  else if (type == 2)
    return "Arc";
  else if (type == 3)
    return "Solar";
  else if (type == 4)
    return "Void";
  else
    throw new Error("Unknown damage type: " + type);
}


function getMemberId(platform, name, callback, req) {
  var key = "mem:" + platform + ":" + name;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      callback(null, cached);
    }
    else {
      UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/SearchDestinyPlayer/' + platform + "/" + encodeURIComponent(name),
        function (error, jo) {
          if (error) {
            callback(error);
            return;
          }
          //player not found
          if (jo.Response.length == 0) {
            var otherPlatform = 1;
            if (platform == 1) otherPlatform = 2;
            UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/SearchDestinyPlayer/' + otherPlatform + "/" + encodeURIComponent(name),
              function (error2, jo2) {
                if ((error2)) {
                  callback(new ApiError("NotFound", "Not found, checking second platform got error."));
                  return;
                }
                if (jo2.Response.length == 0) {
                  callback(new ApiError("NotFound", "No player by tag '" + name + "' was found on any platform."));
                  return;
                }
                var memberId = jo2.Response[0].membershipId;
                cacheMemberId(memberId, otherPlatform, name);
                callback(new ApiError("OtherPlatform", "Not found on specified platform, but exists."));

              });
          }
          else {
            var memberId = jo.Response[jo.Response.length-1].membershipId;
            cacheMemberId(memberId, platform, name);
            callback(null, memberId);
          }
        }
      );
    }
  });
}

function listCharactersByMemberId(platform, memberId, name, callback, req){
  var key = "memChars:" + platform + ":" + memberId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      cached = JSON.parse(cached);
      cached.player.lastPlayed = moment(cached.player.lastPlayedStamp);
      callback(null, cached);

    }
    else {
      UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/' + platform + "/Account/" + memberId + "/Summary/",
        function (error, jo) {
          if (error) {
            callback(error);
            return;
          }

          var rm = {
            player: {
              memberId: memberId,
              platform: platform,
              supporter: isSupporter(name),
              name: name,
              lastPlayedStamp: null,
              minsPlayed: 0
            }, chars: []
          };
          var j = jo.Response.data;
          if (j.clanName) {
            rm.clanName = j.clanName;
          }
          for (var cntr = 0; cntr < j.characters.length; cntr++) {
            var char = j.characters[cntr];

            char = char.characterBase;
            var charLastPlayed = moment(char.dateLastPlayed);
            if (rm.player.lastPlayedStamp == null) rm.player.lastPlayedStamp = charLastPlayed.valueOf();
            else if (charLastPlayed.valueOf() > rm.player.lastPlayedStamp) rm.player.lastPlayedStamp = charLastPlayed.valueOf();
            rm.player.minsPlayed += parseInt(char.minutesPlayedTotal);
            var raceName = cs.cache(req).Race[char.raceHash].raceName;
            var genderName = cs.cache(req).Gender[char.genderHash].genderName;
            var className = cs.cache(req).Class[char.classHash].className;
            rm.grimoire = char.grimoireScore;

            var stats = {};
            for (var dictKey in char.stats) {
              if (char.stats.hasOwnProperty(dictKey)) {

                stats[dictKey.toLowerCase().substring(5)] = char.stats[dictKey].value;
              }
            }
            var jchar = {
              id: char.characterId,
              race: raceName,
              gender: genderName,
              class: className,
              level: j.characters[cntr].characterLevel,
              power: char.stats.STAT_LIGHT.value,
              stats: stats

            };
            rm.chars.push(jchar);
          }
          rClient.set(key, JSON.stringify(rm));
          rClient.expire(key, 3600);
          rm.player.lastPlayed = moment(rm.player.lastPlayedStamp);
          callback(null, rm);
        }
      );
    }
  });
}

function listCharacters(platform, name, callback, req) {
  getMemberId(platform, name, function (err, memberId) {
    if (err) {
      callback(err);
      return;
    }
    listCharactersByMemberId(platform, memberId, name, callback, req);
  }, req);
}


var IMPORTANT_FACTIONS = {
  174528503: true, //crota's bane
  2778795080: true, // dead orbit
  1424722124: true, // FWC
  2335631936: true, // gunsmith
  3641985238: true, // House of Judgment
  3871980777: true, // New Monarchy
  1357277120: true, // Crucible
  3233510749: true, // Vanguard
  807090922: true, // Queen's wrath
};


function applyProgression(platform, memberId, characterId, returnMe, callback, req, ignoreError) {
  var key = "progression:" + platform + ":" + memberId + ":" + characterId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (err) {
      callback(err);
      return;
    }
    if (cached) {
      returnMe.progression = JSON.parse(cached);
      callback(null, returnMe);

    }
    else {
      UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/" + platform + "/Account/" + memberId + "/Character/" + characterId + "/Progression",
        function (error, jo) {
          if (error && !ignoreError) {
            callback(error);
            return;
          }
          else if (error) {
            if (!returnMe.errors) {
              returnMe.errors = [];
            }
            returnMe.partialError = true;
            returnMe.errors.push = {
              action: "applyProgression",
              error: error
            };
            callback(null, returnMe);
            return;
          }
          if (!jo) callback("Missing data");
          var ja = jo.Response.data.progressions;

          returnMe.progression = {
            normal: [],
            hidden: []
          };
          var closestPct = 0;
          var closestFaction = null;
          var closeFactions = [];
          for (var cntr = 0; cntr < ja.length; cntr++) {
            var hash = ja[cntr].progressionHash;
            var jDesc = cs.cache(req).Progression[hash];
            var fDesc = cs.cache(req).Faction[hash];
            var prog = {
              progress: ja[cntr],
              repeats: jDesc.repeatLastStep,
              totalRanks: jDesc.steps.length
            };

            if (prog.progress.progressToNextLevel != null) {
              var nla = prog.progress.nextLevelAt;
              if (!nla) nla = 1;
              prog.progress.pct = Math.floor(100 * (prog.progress.progressToNextLevel / nla));
            }

            //calc fragments
            if (hash == 3186678724) {
              prog.name = "_Calcified Fragments";
              prog.desc = "Get 45 to qualify for Touch of Malice";
              prog.icon = "/NA.png";
              returnMe.progression.normal.push(prog);
              returnMe.progression.calcFrags = prog.progress.level;
            }
            else if (!fDesc) {
              prog.name = jDesc.name.split(".").join(" ").split("_").join(" ");
              returnMe.progression.hidden.push(prog);
            }
            else if (hash == 372013324 || hash == 452808717){
              prog.name = "_"+fDesc.factionName;
              prog.desc = fDesc.factionDescription;
              prog.icon = fDesc.factionIcon;
              returnMe.progression.hidden.push(prog);
            }
            else {
              prog.name = fDesc.factionName;
              prog.desc = fDesc.factionDescription;
              prog.icon = fDesc.factionIcon;
              returnMe.progression.normal.push(prog);
            }

            //we care about this one;
            if (IMPORTANT_FACTIONS[hash]==true){
              if (prog.progress.pct>closestPct){
                closestPct = prog.progress.pct;
                closestFaction = prog;
              }

              if (prog.progress.pct>=75){
                closeFactions.push(prog);
              }
              if (1357277120 === hash) {// Crucible
                returnMe.progression.shaxx = prog;
              }
              else if (3233510749 === hash) {// Vanguard
                returnMe.progression.vanguard = prog;
              }
              else if (174528503 === hash) {
                returnMe.progression.crota = prog;
              }
              else if (807090922 === hash) {
                returnMe.progression.queen = prog;
              }
              else if (3641985238 === hash) {
                returnMe.progression.variks = prog;
              }
              else if (2335631936 === hash) {
                //gunsmith
                returnMe.progression.gunsmith = prog.progress.weeklyProgress/250;
                returnMe.progression.gunsmithProg = prog;
              }
            }
          }
          if (closeFactions.length==0){
            closeFactions.push(closestFaction);
          }

          closeFactions.sort(function(a, b) {
            return b.progress.pct - a.progress.pct;
          });

          returnMe.progression.normal.sort(UTILS.sortByName);
          returnMe.progression.hidden.sort(UTILS.sortByName);
          returnMe.progression.close = closeFactions;

          rClient.set(key, JSON.stringify(returnMe.progression));
          rClient.expire(key, 86400);
          callback(null, returnMe);
        }
      );

    }
  });
}