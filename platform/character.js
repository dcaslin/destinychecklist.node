/**
 * Created by Dave on 6/30/2016.
 */
var UTILS = require("./utils");
var cs = require("./cacheService")();
var moment = require('moment-timezone');
var async = require("async");
var ITEM_PARSE = require('./itemParseService')();

var rClient;
var COMMON, PRIVATE_ADVISORS, LEADERBOARDS;
var fns = {
  getCharacterinfo: getCharacterinfo,
  getCharsForList: getCharsForList,
  getCharsForList2: getCharsForList2,
  getCharInfoPt1: getCharInfoPt1,
  getCharInfoPt2: getCharInfoPt2,
  checkLeaderHistory: checkLeaderHistory
};

module.exports = function (redis, common, privateAdvisors, leaderboards) {
  rClient = redis;
  COMMON = common;
  PRIVATE_ADVISORS = privateAdvisors;
  LEADERBOARDS = leaderboards;
  return fns;
};

// wrath 3356249023
// KF 3978884648
// CE 4000873610
// VoG 856898338


var ACTIVITY_VOG_390_FEAT = 856898338;
var ACTIVITY_VOG_390_NON = 4038697181;
var ACTIVITY_VOG_NM = 2659248071;
var ACTIVITY_VOG_HM = 2659248068;

var ACTIVITY_CE_390_FEAT = 4000873610;
var ACTIVITY_CE_390_NON = 2324706853;
var ACTIVITY_CE_NM = 1836893116;
var ACTIVITY_CE_HM = 1836893119;

var ACTIVITY_KING_390_FEAT = 3978884648;
var ACTIVITY_KING_390_NON = 1016659723;//3390383282
var ACTIVITY_KING_NM = 1733556769;
var ACTIVITY_KING_HM = 3534581229; 

var ACTIVITY_WRATH_390_FEAT = 3356249023;
var ACTIVITY_WRATH_NM = 260765522;
var ACTIVITY_WRATH_HM = 1387993552;//ACTIVITY_WRATH_390_NON

var POE_ACTIVITIES =
  [2326253031,
    3508129769,
    3508129770,
    4079642013,
    4079642014,
    1208087916,
    1208087919,
    1441430564,
    1441430567,
    2074618692,
    2074618695,
    604017503,
    1551387515];

var COE_ACTIVITIES =
  [2201622127];

function applyLifetimeActivity(platform, memberId, characterId, returnMe, callback, req, ignoreError) {
  var key = "lifetime:" + platform + ":" + memberId + ":" + characterId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (err) {
      callback(err);
      return;
    }
    if (cached) {
      returnMe.lifetime = JSON.parse(cached);
      callback(null, returnMe);

    }
    else {
      UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/Stats/AggregateActivityStats/" + platform + "/" + memberId + "/" + characterId,
        function (error, jo) {
          if (error && !ignoreError) {
            callback(error);
            return;
          }
          else if (error) {
            if (!returnMe.errors) {
              returnMe.errors = [];
            }
            returnMe.partialError = true;
            returnMe.errors.push = {
              action: "applyLifetimeActivity",
              error: error
            };
            callback(null, returnMe);
            return;
          }
          var ja = jo.Response.data.activities;

          returnMe.lifetime.poe32 = 0;
          returnMe.lifetime.poe34 = 0;
          returnMe.lifetime.poe35 = 0;
          returnMe.lifetime.poe41 = 0;
          returnMe.lifetime.coe42 = 0;
          returnMe.lifetime.speed = {};

          returnMe.lifetime.ce390 = 0;
          returnMe.lifetime.vog390 = 0;
          returnMe.lifetime.kf390 = 0;
          returnMe.lifetime.wrathHm = 0;
          returnMe.lifetime.wrath390 = 0;

          if (ja) {
            for (var cntr = 0; cntr < ja.length; cntr++) {
              var hash = ja[cntr].activityHash;


              if (ACTIVITY_CE_390_FEAT == hash || ACTIVITY_CE_390_NON == hash)
                returnMe.lifetime.ce390 += ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_VOG_390_FEAT == hash || ACTIVITY_VOG_390_NON == hash)
                returnMe.lifetime.vog390 += ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_KING_390_FEAT == hash || ACTIVITY_KING_390_NON == hash)
                returnMe.lifetime.kf390 += ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_WRATH_390_FEAT == hash || ACTIVITY_WRATH_HM == hash) {
                returnMe.lifetime.wrath390 += ja[cntr].values.activityCompletions.basic.value;
                returnMe.lifetime.wrathHm += ja[cntr].values.activityCompletions.basic.value;
              }
              else if (ACTIVITY_CE_HM == hash)
                returnMe.lifetime.ceHm = ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_CE_NM == hash)
                returnMe.lifetime.ceNm = ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_VOG_HM == hash)
                returnMe.lifetime.vogHm = ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_VOG_NM == hash)
                returnMe.lifetime.vogNm = ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_KING_HM == hash)
                returnMe.lifetime.kingHm = ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_KING_NM == hash)
                returnMe.lifetime.kingNm = ja[cntr].values.activityCompletions.basic.value;
              else if (ACTIVITY_WRATH_NM == hash)
                returnMe.lifetime.wrathNm = ja[cntr].values.activityCompletions.basic.value;
              // else if (ACTIVITY_WRATH_HM == hash)
              //   returnMe.lifetime.wrathHm = ja[cntr].values.activityCompletions.basic.value;
              else if (POE_ACTIVITIES.indexOf(hash) >= 0) {
                var lvl = cs.cache(req).Activity[hash].activityLevel;
                if (32 == lvl) {
                  returnMe.lifetime.poe32 += ja[cntr].values.activityCompletions.basic.value;
                }
                else if (34 == lvl) {
                  returnMe.lifetime.poe34 += ja[cntr].values.activityCompletions.basic.value;
                }
                else if (35 == lvl) {
                  returnMe.lifetime.poe35 += ja[cntr].values.activityCompletions.basic.value;
                }
                else if (41 == lvl) {
                  returnMe.lifetime.poe41 += ja[cntr].values.activityCompletions.basic.value;
                }
              }
              else if (COE_ACTIVITIES.indexOf(hash) >= 0) {
                returnMe.lifetime.coe42 += ja[cntr].values.activityCompletions.basic.value;
                returnMe.lifetime.speed.coe42 = ja[cntr].values.fastestCompletionSecondsForActivity.basic.displayValue;
              }
            }
          }
          rClient.set(key, JSON.stringify(returnMe.lifetime));
          rClient.expire(key, 86400);
          callback(null, returnMe);
        }
      );

    }
  });
}


function applyWeeklyActivity(platform, memberId, characterId, returnMe, callback, req, ignoreError) {
  var key = "weekly:" + platform + ":" + memberId + ":" + characterId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (err) {
      callback(err);
      return;
    }
    if (cached) {
      returnMe.weekly = JSON.parse(cached);
      callback(null, returnMe);

    }
    else {
      UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/Stats/ActivityHistory/" + platform + "/" + memberId + "/" + characterId + "/?count=100&mode=AllPvE",
        function (error, jo) {
          if (error && !ignoreError) {
            callback(error);
            return;
          }
          else if (error) {
            returnMe.weekly = {};

            if (!returnMe.errors) {
              returnMe.errors = [];
            }
            returnMe.partialError = true;
            returnMe.errors.push = {
              action: "applyWeeklyActivity",
              error: error
            };

            callback(null, returnMe);
            return;
          }
          //get last weekly reset
          var lastReset = UTILS.getLastWeeklyReset();
          var lastDailyReset = UTILS.getLastDailyReset();

          var activities = jo.Response.data.activities;

          returnMe.weekly.history = [];

          if (activities) {
            for (var cntr = 0; cntr < activities.length; cntr++) {

              var a = activities[cntr];

              var hash = a.activityDetails.referenceId;
              var aDesc = cs.cache(req).Activity[hash];
              var period = moment(a.period);
              var pushMe = {
                name: aDesc.activityName,
                level: aDesc.activityLevel,
                date: period.format('MM/DD/YYYY'),
                dateDesc: period.from(moment()),
                hash: a.activityDetails.instanceId,
                kills: a.values.kills.basic.value,
                assists: a.values.assists.basic.value,
                deaths: a.values.deaths.basic.value,
                result: a.values.completed.basic.displayValue
              };

              returnMe.weekly.history.push(pushMe);
              //if not completed skip it
              if (a.values.completed.basic.value < 1) continue;

              var compRsn = a.values.completionReason.basic.value;
              if (compRsn != 0) {
                if (compRsn == 3) {
                  //leave it, sketchy though
                }
                else
                //skip it, loss/quit/boot to orbit
                  continue;
              }


              if (ACTIVITY_CE_390_FEAT == hash) {
                LEADERBOARDS.check390(LEADERBOARDS.KEY_CE390, returnMe, period, a.activityDetails.instanceId, a.values.activityDurationSeconds.basic.value, req);
              }
              else if (ACTIVITY_VOG_390_FEAT == hash) {
                LEADERBOARDS.check390(LEADERBOARDS.KEY_VOG390, returnMe, period, a.activityDetails.instanceId, a.values.activityDurationSeconds.basic.value, req);
              }
              else if (ACTIVITY_KING_390_FEAT == hash) {
                LEADERBOARDS.check390(LEADERBOARDS.KEY_KF390, returnMe, period, a.activityDetails.instanceId, a.values.activityDurationSeconds.basic.value, req);
              }

              //these are in order, once we hit one we can stop i think?
              if (period.isBefore(lastReset)) {
                continue;
              }

              var level = aDesc.activityLevel;
              if (ACTIVITY_KING_HM == hash) {
                returnMe.weekly.kingHm = true;
              }
              else if (ACTIVITY_KING_NM == hash) {
                returnMe.weekly.kingNm = true;
              }
              else if (ACTIVITY_WRATH_HM == hash) {
                returnMe.weekly.wrathHm = true;
              }
              else if (ACTIVITY_WRATH_NM == hash) {
                returnMe.weekly.wrathNm = true;
              }
              else if (ACTIVITY_CE_HM == hash)
                returnMe.weekly.ceHm = true;
              else if (ACTIVITY_CE_NM == hash)
                returnMe.weekly.ceNm = true;
              else if (ACTIVITY_VOG_HM == hash)
                returnMe.weekly.vogHm = true;
              else if (ACTIVITY_VOG_NM == hash)
                returnMe.weekly.vogNm = true;
              else if (POE_ACTIVITIES.indexOf(hash) >= 0) {
                if (32 == level) {
                  returnMe.weekly.poe32 = true;
                }
                else if (34 == level) {
                  returnMe.weekly.poe34 = true;
                }
                else if (35 == level) {
                  returnMe.weekly.poe35 = true;
                }
                else if (41 == level) {
                  returnMe.weekly.poe41 = true;
                }
              }
              else if (COE_ACTIVITIES.indexOf(hash) >= 0) {

                returnMe.weekly.coe42 = true;
                if (returnMe.publicAdv && returnMe.publicAdv.coe) {
                  LEADERBOARDS.checkCoe(period, lastReset, returnMe.publicAdv.coe.high.score, pushMe.hash, req);
                }
              }
              else if ("/img/theme/destiny/icons/node_strike_nightfall.png" == aDesc.icon) {
                returnMe.weekly.nightfall = true;
              }
              //Will of crota breaks this
              //else if ("/img/theme/destiny/icons/node_strike_featured.png" == aDesc.icon && aDesc.activityLevel >= 41) {
              //2889152536 strike playlist, 4110605575 is strike for bugged Will of Crota
              else if (aDesc.activityLevel >= 41 && (aDesc.activityTypeHash == 2889152536 || aDesc.activityTypeHash == 4110605575)) {
                if (!returnMe.weekly.weekly) returnMe.weekly.weekly = 0;
                returnMe.weekly.weekly++;
              }
              else {
                if (level < 40) continue;
                if (!aDesc.rewards) continue;
                if (aDesc.rewards.length < 1) continue;
                for (var rewCntr = 0; rewCntr < aDesc.rewards.length; rewCntr++) {
                  if (period.isBefore(lastDailyReset)) {
                    continue;
                  }
                  var reward = aDesc.rewards[rewCntr];
                  if (reward.rewardItems) {
                    for (var rewCntr2 = 0; rewCntr2 < reward.rewardItems.length; rewCntr2++) {
                      if (reward.rewardItems[rewCntr2].itemHash == 3510203897) {
                        if (reward.rewardItems[rewCntr2].value == 15) {
                          returnMe.weekly.daily = true;
                        }
                      }

                    }
                  }
                }
              }
            }
          }
          rClient.set(key, JSON.stringify(returnMe.weekly));
          rClient.expire(key, 3600);
          callback(null, returnMe);
        }
      );
    }

  });
}


function applyWeeklyPvPActivity(platform, memberId, characterId, returnMe, callback, req, ignoreError) {
  var key = "pvpweekly:" + platform + ":" + memberId + ":" + characterId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (err) {
      callback(err);
      return;
    }
    if (cached) {
      returnMe.weeklyPvP = JSON.parse(cached);
      callback(null, returnMe);

    }
    else {
      UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/Stats/ActivityHistory/" + platform + "/" + memberId + "/" + characterId + "/?count=100&mode=AllPvP",
        function (error, jo) {

          if (!PRIVATE_ADVISORS.getAdvisors()) {
            callback(new Error("Server still initializing, try again soon"));
            return;
          }

          if (error && !ignoreError) {
            callback(error);
            return;
          }
          else if (error) {
            returnMe.weeklyPvP = {
              daily: false,
              weekly: 0,
              history: []
            };


            if (!returnMe.errors) {
              returnMe.errors = [];
            }
            returnMe.partialError = true;
            returnMe.errors.push = {
              action: "applyWeeklyPvPActivity",
              error: error
            };

            callback(null, returnMe);
            return;
          }
          returnMe.weeklyPvP = {
            daily: false,
            weekly: 0,
            history: []
          };
          //get last weekly reset
          var lastReset = UTILS.getLastWeeklyReset();
          var lastDailyReset = UTILS.getLastDailyReset();
          var activities = jo.Response.data.activities;
          if (activities) {


            for (var cntr = 0; cntr < activities.length; cntr++) {
              var a = activities[cntr];

              var hash = a.activityDetails.referenceId;
              var typeHash = a.activityDetails.activityTypeHashOverride;
              var aDesc = cs.cache(req).Activity[hash];

              var period = moment(a.period);

              var pvpType = typeHash > 0 ? cs.cache(req).ActivityType[typeHash].activityTypeName : 'Custom';
              var bRumble = false;
              if (pvpType == "Rumble" || pvpType == "Mayhem Rumble") {
                bRumble = true;
              }
              var srlMatch = false;
              if (pvpType == "SRL") {
                srlMatch = true;
              }

              var pushMe = {
                name: aDesc.activityName,
                private: a.activityDetails.isPrivate,
                type: pvpType,
                date: period.format('MM/DD/YYYY'),
                dateDesc: period.from(moment()),
                hash: a.activityDetails.instanceId,
                kills: a.values.kills.basic.value,
                assists: a.values.assists.basic.value,
                deaths: a.values.deaths.basic.value,
                result: a.values.standing.basic.displayValue,
                srl: srlMatch
              };
              if (a.values.score) {
                pushMe.score = a.values.score.basic.value;
                pushMe.displayScore = a.values.score.basic.displayValue;
              }
              returnMe.weeklyPvP.history.push(pushMe);

              //if not completed skip it, actually no, rumble reports weird so count it
              if (a.values.completed.basic.value < 1) continue;
              var hash = a.activityDetails.activityTypeHashOverride;
              var period = moment(a.period);
              if (returnMe.items.subclass)
                LEADERBOARDS.checkPVP(period, lastReset, a, platform, memberId, pushMe, returnMe.items.subclass.name);

              var advisors = PRIVATE_ADVISORS.getAdvisors().public;
              if (advisors) {
                if (advisors.hashes.dailyPvp == hash || advisors.hashes.dailyPvp == typeHash) {
                  var period = moment(a.period);
                  //was it in the last day?
                  if (period.isBefore(lastDailyReset)) {
                    continue;
                  }
                  //score it
                  returnMe.weeklyPvP.daily = true;
                } else if (advisors.hashes.weeklyPvp == hash || advisors.hashes.weeklyPvp == typeHash) {

                  var victory;
                  // Did they win?
                  if (bRumble) {
                    victory = a.values.standing.basic.value < 3;
                  }
                  else
                    victory = a.values.standing.basic.value == 0;

                  if (!victory) continue;

                  //was it in the last week?
                  if (period.isBefore(lastReset)) {
                    continue;
                  }
                  //score it
                  returnMe.weeklyPvP.weekly++;
                }
              }


            }
            rClient.set(key, JSON.stringify(returnMe.weeklyPvP));
            rClient.expire(key, 3600);
          }
          callback(null, returnMe);
        }
      );
    }
  });
}

function getNodeStep(nodes, nodeHash, stepIndex, filterAutoUnlocks, filterTopRow, showOnlySelectable) {
  for (var cntr = 0; cntr < nodes.length; cntr++) {
    if (nodes[cntr].nodeHash == nodeHash) {
      if (filterAutoUnlocks && nodes[cntr].autoUnlocks) return null;
      if (filterTopRow && nodes[cntr].row == 0) return null;
      if (showOnlySelectable && nodes[cntr].exlusiveWithNodes.length == 0) return null;
      return nodes[cntr].steps[stepIndex];
    }
  }
  return null;
}


function parseSubClass(item, req) {
  var name = cs.cache(req).InventoryItem[item.itemHash].itemName;
  var dmgType = COMMON.decodeDamage(item.damageType);

  var returnMe = {
    name: name,
    dmgType: dmgType,
    selections: []
  };

  var talentGrid = cs.cache(req).TalentGrid[item.talentGridHash];
  for (var cntr = 0; cntr < item.nodes.length; cntr++) {
    var node = item.nodes[cntr];
    if (!node.isActivated) continue;
    var step = getNodeStep(talentGrid.nodes, node.nodeHash, node.stepIndex, true, true, false);
    if (step != null)
      returnMe.selections.push({name: step.nodeStepName, desc: step.nodeStepDescription});
  }

  return returnMe;
}

function parseArmor(item, req) {

  var cd = cs.cache(req).InventoryItem[item.itemHash];
  if (cd == null) {
    return {
      name: "Secret",
      tier: "Not in",
      type: "public Bungie DB",
      armor: 0,
      perks: [],
      stats: []
    };

  }
  var name = cd.itemName;
  var armor = 0;
  if (item.primaryStat) armor = item.primaryStat.value;

  var quality = null;
  var stats = null;
  if (armor >= 280) {

    var parsed = ITEM_PARSE.parseItem(item);
    if (parsed) {
      quality = parsed.quality;
      stats = parsed.stats;
    }
  }

  var returnMe = {
    name: name,
    tier: cd.tierTypeName,
    type: cd.itemTypeName,
    armor: armor,
    perks: [],
    stats: stats,
    quality: quality
  };
  if (item.perks) {
    for (var cntr = 0; cntr < item.perks.length; cntr++) {
      var perk = item.perks[cntr];
      var perkDef = cs.cache(req).SandboxPerk[perk.perkHash];
      returnMe.perks.push({
        name: perkDef.displayName,
        desc: perkDef.displayDescription,
        active: perk.isActive
      });
    }
  }
  return returnMe;
}

function parseWeapon(item, req) {
  var cd = cs.cache(req).InventoryItem[item.itemHash];
  if (cd == null) {
    return {
      name: "Secret",
      tier: "Not in",
      type: "public Bungie DB",
      dmg: 0,
      dmgType: 0,
      selections: [],
      perks: [],
      stats: []
    };

  }
  var dmgType = COMMON.decodeDamage(item.damageType);
  var returnMe = {
    name: cd.itemName,
    tier: cd.tierTypeName,
    type: cd.itemTypeName,
    dmg: item.primaryStat.value,
    dmgType: dmgType,
    selections: [],
    perks: [],
    stats: []
  };
  if (item.talentGridHash) {
    var talentGrid = cs.cache(req).TalentGrid[item.talentGridHash];
    for (var cntr = 0; cntr < item.nodes.length; cntr++) {
      var node = item.nodes[cntr];
      if (!node.isActivated) continue;
      var step = getNodeStep(talentGrid.nodes, node.nodeHash, node.stepIndex, false, false, true);
      if (step != null)
        returnMe.selections.push({name: step.nodeStepName, desc: step.nodeStepDescription});
    }
  }
  if (item.perks) {
    for (var cntr = 0; cntr < item.perks.length; cntr++) {
      var perk = item.perks[cntr];
      var perkDef = cs.cache(req).SandboxPerk[perk.perkHash];
      returnMe.perks.push({
        name: perkDef.displayName,
        desc: perkDef.displayDescription,
        active: perk.isActive
      });
    }
  }
  if (item.stats) {
    for (var cntr = 0; cntr < item.stats.length; cntr++) {
      var stat = item.stats[cntr];
      var statDef = cs.cache(req).Stat[stat.statHash];
      returnMe.stats.push({
        name: statDef.statName,
        value: stat.value
      });
    }
  }
  return returnMe;
}


function applyItems(platform, memberId, characterId, returnMe, callback, req, ignoreError) {
  var key = "items:" + platform + ":" + memberId + ":" + characterId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (err) {
      callback(err);
      return;
    }
    if (cached) {
      returnMe.items = JSON.parse(cached);
      callback(null, returnMe);

    }
    else {
      UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/' + platform + '/Account/' + memberId + '/Character/' + characterId + '/Inventory/',
        function (error, jo) {
          if (error && !ignoreError) {
            callback(error);
            return;
          }
          else if (error) {
            if (!returnMe.errors) {
              returnMe.errors = [];
            }
            returnMe.partialError = true;
            returnMe.errors.push = {
              action: "applyItems",
              error: error
            };
            callback(null, returnMe);
            return;
          }
          var ja = jo.Response.data.buckets.Equippable;
          returnMe.items.guns = [];
          returnMe.items.armor = [];
          for (var cntr = 0; cntr < ja.length; cntr++) {
            var bucket = ja[cntr];
            var hash = bucket.bucketHash;
            if (bucket.items.length > 1) throw new Error("Bucket length = " + bucket.items.length);
            if (bucket.items.length == 0) continue;
            var item = bucket.items[0];
            //subclass
            if (3284755031 == hash) {
              returnMe.items.subclass = parseSubClass(item, req);
            }
            //primary weapon
            else if (1498876634 == hash) {
              returnMe.items.guns.push(parseWeapon(item, req));
            }
            //special weapon
            else if (2465295065 == hash) {
              returnMe.items.guns.push(parseWeapon(item, req));
            }
            //heavy weapon
            else if (953998645 == hash) {
              returnMe.items.guns.push(parseWeapon(item, req));
            }
            //helmet
            else if (3448274439 == hash) {
              returnMe.items.armor.push(parseArmor(item, req));
            }
            //gauntlets
            else if (3551918588 == hash) {
              returnMe.items.armor.push(parseArmor(item, req));
            }
            //leg armor
            else if (20886954 == hash) {
              returnMe.items.armor.push(parseArmor(item, req));
            }
            //chest armor
            else if (14239492 == hash) {
              returnMe.items.armor.push(parseArmor(item, req));
            }
            //class armor
            else if (1585787867 == hash) {
              returnMe.items.armor.push(parseArmor(item, req));
            }
            //artifacts
            else if (434908299 == hash) {
              returnMe.items.armor.push(parseArmor(item, req));
            }
            //ghosts
            else if (4023194814 == hash) {
              returnMe.items.armor.push(parseArmor(item, req));
            }
            //otherwise we don't care
            else {

            }
          }
          rClient.set(key, JSON.stringify(returnMe.items));
          rClient.expire(key, 3600);
          callback(null, returnMe);
        }
      );
    }
  });
}


function getCharInfoPt1(platform, memberId, characterId, finalCallBack, req) {
  var returnMe = {};
  async.parallel([
      function (callback) {
        COMMON.applyPublicAdvisors(platform, memberId, characterId, returnMe, callback, req, true);
      },
      function (callback) {
        COMMON.applyProgression(platform, memberId, characterId, returnMe, callback, req, true);
      }
    ],
    function (err) {
      finalCallBack(err, returnMe);
    });
}


function getCharInfoPt2(platform, memberId, characterId, finalCallBack, req) {
  var returnMe = {
    weekly: {},
    lifetime: {},
    items: {}
  };
  async.parallel([
      function (callback) {
        applyItems(platform, memberId, characterId, returnMe, callback, req, true);
      }, function (callback) {
        //weekly Activity
        applyWeeklyActivity(platform, memberId, characterId, returnMe, callback, req, true);
      }, function (callback) {
        //weekly Activity
        applyWeeklyPvPActivity(platform, memberId, characterId, returnMe, callback, req, true);
      },
      function (callback) {
        applyLifetimeActivity(platform, memberId, characterId, returnMe, callback, req, true);
      }
    ],
    function (err) {
      finalCallBack(err, returnMe);
    });
}


function getCharacterinfo(platform, memberId, characterId, finalCallBack, req) {
  var returnMe = {
    weekly: {},
    lifetime: {},
    items: {}
  };
  async.waterfall([

      function (callback) {
        applyItems(platform, memberId, characterId, returnMe, callback, req, true);

      }, function (returnMe, callback) {
        //callback(null, returnMe);
        COMMON.applyPublicAdvisors(platform, memberId, characterId, returnMe, callback, req, true);
      }, function (returnMe, callback) {
        //weekly Activity
        applyWeeklyActivity(platform, memberId, characterId, returnMe, callback, req, true);
      }, function (returnMe, callback) {
        //weekly Activity
        applyWeeklyPvPActivity(platform, memberId, characterId, returnMe, callback, req, true);
      },
      function (returnMe, callback) {
        applyLifetimeActivity(platform, memberId, characterId, returnMe, callback, req, true);
      },
      function (returnMe, callback) {
        COMMON.applyProgression(platform, memberId, characterId, returnMe, callback, req, true);
      }],
    function (err, returnMe) {
      finalCallBack(err, returnMe);
    });
}


function checkLeadersForChar(platform, memberId, characterId, returnMe, callback, req) {

  UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/Stats/ActivityHistory/" + platform + "/" + memberId + "/" + characterId + "/?count=100&mode=Raid",
    function (error, jo) {
      if (error) {
        callback(error);
        return;
      }
      var activities = jo.Response.data.activities;

      returnMe.candidates = [];
      if (activities) {
        for (var cntr = 0; cntr < activities.length; cntr++) {

          var a = activities[cntr];

          var hash = a.activityDetails.referenceId;
          var aDesc = cs.cache(req).Activity[hash];
          var period = moment(a.period);
          var pushMe = {
            name: aDesc.activityName,
            level: aDesc.activityLevel,
            date: period.format('MM/DD/YYYY'),
            dateDesc: period.from(moment()),
            hash: a.activityDetails.instanceId,
            kills: a.values.kills.basic.value,
            assists: a.values.assists.basic.value,
            deaths: a.values.deaths.basic.value,
            result: a.values.completed.basic.displayValue
          };

          if (a.values.completed.basic.value < 1) continue;

          var compRsn = a.values.completionReason.basic.value;
          if (compRsn != 0) {
            if (compRsn == 3) {
              //leave it, sketchy though
            }
            else
            //skip it, loss/quit/boot to orbit
              continue;
          }
          var eligible = false;

          if (ACTIVITY_CE_390_FEAT == hash) {
            eligible = LEADERBOARDS.check390(LEADERBOARDS.KEY_CE390, returnMe, period, a.activityDetails.instanceId, a.values.activityDurationSeconds.basic.value, req);
          }
          else if (ACTIVITY_VOG_390_FEAT == hash) {
            eligible = LEADERBOARDS.check390(LEADERBOARDS.KEY_VOG390, returnMe, period, a.activityDetails.instanceId, a.values.activityDurationSeconds.basic.value, req);
          }
          else if (ACTIVITY_KING_390_FEAT == hash) {
            eligible = LEADERBOARDS.check390(LEADERBOARDS.KEY_KF390, returnMe, period, a.activityDetails.instanceId, a.values.activityDurationSeconds.basic.value, req);
          }
          else {
            continue;
          }
          returnMe.candidates.push({
            match: pushMe,
            eligible: eligible
          });
        }
      }
      callback(null, returnMe);
    }
  );


}


function checkLeaderHistory(platform, name, finalCallback, req) {
  COMMON.listCharacters(platform, name,
    function (err, data) {
      if (err) {
        finalCallback(err);

      }
      else {
        var chars = [];
        async.eachSeries(data.chars, function iterator(char, callback) {
          checkLeadersForChar(platform, data.player.memberId, char.id, char, function (err, data) {
            if (err) {
              callback(err);
              return;
            }
            chars.push(data);
            callback();
          }, req);
        }, function done(err) {
          finalCallback(null, chars);

        });
      }
    }, req);
}

function getCharsForList(platform, name, finalCallback, req) {
  COMMON.listCharacters(platform, name,
    function (err, data) {
      if (err) {
        finalCallback(err);

      }
      else {
        var chars = [];
        async.eachSeries(data.chars, function iterator(item, callback) {
          item.weekly = {};
          applyWeeklyActivity(platform, data.player.memberId, item.id, item, function (err, data) {
            if (err) {
              callback(err);
              return;
            }
            chars.push(data);
            callback();
          }, req);
        }, function done(err) {
          finalCallback(null, chars);

        });
      }
    }, req);
}

function getCharsForList2(platform, name, finalCallback, req) {
  COMMON.listCharacters(platform, name,
    function (err, data) {
      if (err) {
        finalCallback(err);

      }
      else {
        async.eachSeries(data.chars, function iterator(item, callback) {
          COMMON.applyPublicAdvisors(platform, data.player.memberId, item.id, item, callback, req);
        }, function done(err) {
          finalCallback(err, data);
        });
      }
    }, req);
}