"use strict";

var redis = require("redis");
var later = require('later');

var moment = require('moment-timezone');
require('string.prototype.endswith');

var SECURE_REQUEST = require("./secureRequest")();
var MATCH_INFO, IRONBANNER, COMMON, CALC_FRAGS, CHARACTER, PRIVATE_ADVISORS, CLAN, SRL, 
  GRIM, LEADER_BOARD, GUNSMITH, VENN, VENDORS;

var rClient;

function hourly(){
  console.log("Performing hourly actions");
  LEADER_BOARD.saveLeaders(function(err, info){
    if (err){
      console.log("Error saving leaders: "+err);
      return;
    }
  });
  
  //private endpoint stuff, grab tokens from a single place first, in case we need to refresh. Otherwise we'll fight across operations
  SECURE_REQUEST.getTokens().then(
    function(token){
      PRIVATE_ADVISORS.refreshAdvisors(function (err) {
        if (err) console.log("Error refreshing advisors: " + err);
        else console.log("Advisors refreshed on hour");
      });
      VENDORS.refresh(function (err) {
        if (err) console.log("Error refreshing vendors: " + err);
        else console.log("Vendors refreshed on hour");
      });
    })
  .catch(
    function(err){
      console.error("Error checking tokens, skipping private advisors and vendors: "+err);
      console.dir(err);
      PRIVATE_ADVISORS.refreshPublicAdvisors();
    }
  );
  

}

function init() {

  rClient = redis.createClient();
  rClient.on("error", function (err) {
    throw new Error("Redis error " + err);
  });

  COMMON = require("./common")(rClient);
  PRIVATE_ADVISORS = require("./privateAdvisors")(COMMON);
  VENDORS = require("./vendors")(COMMON);

  MATCH_INFO = require("./matchInfo")(rClient, LEADER_BOARD);
  LEADER_BOARD = require("./leaderboards")(COMMON, MATCH_INFO);
  LEADER_BOARD.init();
  CHARACTER = require("./character")(rClient, COMMON, PRIVATE_ADVISORS, LEADER_BOARD, MATCH_INFO);
  IRONBANNER = require("./ib")(rClient, COMMON);
  GUNSMITH = require("./gunsmith")(rClient, COMMON);
  VENN = require("./venn")(rClient, COMMON);
  SRL = require("./srl")(rClient, COMMON);
  GRIM = require("./grim")(rClient, COMMON);
  CALC_FRAGS = require("./calcFrags")(rClient);
  CLAN = require("./clan")(rClient, COMMON);
  //start off checking the tokens, keep running either way

  try {
    hourly();
  }
  catch (err) {
    console.dir(err);
  }
  later.setInterval(hourly, later.parse.cron('1 * * * *'));

  console.log("Done init");
}


function quit() {
  rClient.quit();
  LEADER_BOARD.quit();
}

module.exports = function () {
  init();
  return {
    init: init,
    quit: quit,

    grim: GRIM.grim,
    listSrlChars: SRL.listSrlChars,
    getSupporters: COMMON.getSupporters,
    getMemberId: COMMON.getMemberId,
    listCharacters: COMMON.listCharacters,
    getCharsForList: CHARACTER.getCharsForList,
    getCharsForList2: CHARACTER.getCharsForList2,
    getCharacterInfo: CHARACTER.getCharacterinfo,
    getCharInfoPt1: CHARACTER.getCharInfoPt1,
    getCharInfoPt2: CHARACTER.getCharInfoPt2,
    checkLeaderHistory: CHARACTER.checkLeaderHistory,
    listCalcFrags: CALC_FRAGS.listCalcFrags,
    listIbChars: IRONBANNER.listIbChars,
    matchInfo: MATCH_INFO.matchInfo,
    pvpMatchInfo: MATCH_INFO.pvpMatchInfo,
    srlMatchInfo: MATCH_INFO.srlMatchInfo,
    getAdvisors: PRIVATE_ADVISORS.getAdvisors,
    refreshAdvisors: PRIVATE_ADVISORS.refreshAdvisors,

    findClanMembers: CLAN.findClanMembers,
    getPVPLeaders: LEADER_BOARD.getPVPLeaders,
    getElderLeaders: LEADER_BOARD.getElderLeaders,
    getWrathLeaders: LEADER_BOARD.getWrathLeaders,
    get390Leaders: LEADER_BOARD.get390Leaders,
    check390PGCR: LEADER_BOARD.check390PGCR,
    saveLeaders: LEADER_BOARD.saveLeaders,
    removeWrathDupes: LEADER_BOARD.removeWrathDupes,
    listGunsmithChars: GUNSMITH.listGunsmithChars,
    listVennChars: VENN.listVennChars,
    storeFoundryRolls: GUNSMITH.storeFoundryRolls,
    getFoundryRolls: GUNSMITH.getFoundryRolls,
    listCharactersByMemberId: COMMON.listCharactersByMemberId,
    getVendors: VENDORS.getVendors,
    refreshVendors: VENDORS.refresh
  };
};




