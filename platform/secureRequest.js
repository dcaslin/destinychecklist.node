module.exports = function () {
  return {
    getTokens: getTokens,
    sendRequest: sendRequest
  };
};

var conf = require('../config.js')();

var moment = require('moment-timezone');
var rp = require('request-promise');
var tough = require('tough-cookie');
var htmlparser = require('htmlparser2');
var querystring = require('querystring');

var fs = require('fs');

var cookiejar = rp.jar();
rp = rp.defaults({
  jar: cookiejar
});

function sendRequest(url, callback) {
  console.log(url + " | " + moment().format());
  getTokens()
    .then(function (tokens) {
      var key = tokens.accessToken.value;

      var options = {
        url: url,
        headers: {
          'x-api-key': '997251a328d246f5ac3331b818673d12',
          'Authorization': 'Bearer ' + key
        }
      };
      rp(options)
        .then(function (body) {
          var jo = JSON.parse(body);
          if (jo.ErrorStatus && jo.ErrorStatus != "Success") {
            callback(jo.ErrorStatus);
            return;
          }
          callback(null, jo);
          return;
        });
    })
    .catch(function (err) {
      callback(err);
      return;
    });
}

function msLogon() {
  var options = {
    uri: 'https://www.bungie.net/en/User/SignIn/Xuid'
  };
  console.log(options.uri);
  return rp(options)
    .then(function (body) {
      var re = /id\="i0327" value\="(.*?)"\//ig;
      var result = re.exec(body);
      if (result == null) {
        throw new Error("Unexpected error: re found nothing in body. Body text: " + body);
      }
      var ppft = result[1];
      var re2 = /urlPost:\'(.*?)\'/ig;
      result = re2.exec(body);
      var postUrl = result[1];

      if (postUrl == null) {
        fs.writeFileSync(conf.dataDir + "/SIGN_IN_ERROR.html", res.body);
        throw new Error("No post url found for MS signin page");
      }

      var form = {
        login: process.env.UID,
        passwd: process.env.MAGIC,
        KMSI: 1,
        PPFT: ppft
      };
      options = {
        uri: postUrl,
        form: form,
        resolveWithFullResponse: true,
        method: 'POST',
        followAllRedirects: true
      };

      console.log(options.uri);
      return rp(options).then(function (res) {
        var bungleatk;
        cookiejar.getCookies("https://www.bungie.net/").forEach(function (cookie) {
          if (cookie.key === "bungleatk") {
            bungleatk = cookie.value;
          }
        });
        if (!bungleatk) {
          fs.writeFileSync(conf.dataDir + "/MS_POST_ERROR.html", res.body);
          throw new Error("Bungie headers not set");
        }
        return true;
      });
    });
}

function bungieAuth() {
  var options = {
    uri: 'https://www.bungie.net/en/Application/Authorize/863',
    resolveWithFullResponse: false
  };

  console.log(options.uri);
  return rp(options)
    .then(function (body) {
      var inForm = false;
      var form = {};
      var url;
      var parser = new htmlparser.Parser({
        onopentag: function (name, attribs) {
          if (name === "form" && attribs.action.indexOf("AuthorizeInput" > -1)) {
            inForm = true;
            url = attribs.action;
          }
          if (inForm) {
            if (attribs.value && attribs.name && attribs.name != "deny") {
              form[attribs.name] = attribs.value;
            }
          }
        },
        onclosetag: function (tagname) {
          if (tagname === "form") {
            inForm = false;
          }
        }
      }, {decodeEntities: true});
      parser.write(body);
      parser.end();

      if (!url) {
        fs.writeFileSync(conf.dataDir + "/BUNGIE_GRANT_FORM_ERROR.html", body);
        throw new Error("Bungie grant form not found");
      }

      lastUrl = "https://www.bungie.net/" + url;
      options = {
        uri: lastUrl,
        form: form,
        simple: false,
        method: 'POST',
        resolveWithFullResponse: true
      };

      console.log(options.uri);
      return rp(options)
        .then(function (res) {
          if (res.statusCode === 302) {
            var loc = res.headers.location;
            var qs = loc.substr(loc.indexOf("?") + 1);
            var code = querystring.parse(qs).code;
            if (!code) {

              fs.writeFileSync(conf.dataDir + "/BUNGIE_GRANT_RESULT_ERROR.html", body);
              throw new Error("No code from: " + res.statusCode + " from https://www.bungie.net/" + url);
            }
            return code;
          } else {
            fs.writeFileSync(conf.dataDir + "/BUNGIE_GRANT_RESULT_ERROR.html", body);
            throw new Error("Unexpected status code: " + res.statusCode + " from https://www.bungie.net/" + url);
          }
        });


    });
}

function processNewToken(tokens) {
  var inception = new Date().valueOf();
  tokens.accessToken.inception = inception;
  tokens.refreshToken.inception = inception;
  TOKENS = tokens;
  fs.writeFileSync(conf.dataDir + "/tokens.json", JSON.stringify(tokens));
  return tokens;
}

function getTokensFromCode(code) {
  var options = {
    uri: 'https://www.bungie.net/Platform/App/GetAccessTokensFromCode/',
    json: {
      code: code
    },
    headers: {
      'x-api-key': '997251a328d246f5ac3331b818673d12'
    },
    method: 'POST'
  };
  console.log(options.uri);
  return rp(options)
    .then(function (body) {
      if (body.ErrorCode == 1) {
        var token = body.Response;
        return processNewToken(token);
      } else {
        throw new Error("Error getting access tokens from code: " + body.ErrorCode + ": " + body.ErrorStatus + ": " + body.Message)
      }

    });
}

function refreshToken(tokens) {
  console.log("++++++ Refreshing token: " + JSON.stringify(tokens));

  var options = {
    uri: 'https://www.bungie.net/Platform/App/GetAccessTokensFromRefreshToken/',
    json: {
      refreshToken: tokens.refreshToken.value
    },
    headers: {
      'x-api-key': '997251a328d246f5ac3331b818673d12'
    },
    method: 'POST'
  };
  return rp(options)
    .then(function (body) {
      if (body.ErrorCode == 1) {
        var token = body.Response;
        return processNewToken(token);
      } else {
        throw new Error("Error getting access tokens from code: " + body.ErrorCode + ": " + body.ErrorStatus + ": " + body.Message);
      }
    });
}

function isValid(token) {
  var practicalExp = token.inception + token.expires * 1000 - 1800000;
  var now = new Date().valueOf();
  //it's expired
  if (now > practicalExp) return false;

  if (token.readyin) {
    var ready = token.inception + token.readyin * 1000;
    //it's not ready
    if (ready > now) return false;
  }
  return true;
}

var TOKENS = null;

//returns Promise<Token> with both tokens
function getTokens(verbose) {
  var tokens;
  //check in memory
  if (TOKENS != null) {
    tokens = TOKENS;
    if (isValid(tokens.accessToken)) {
      if (verbose)
        console.log("In memory TOKENS good!");
      return Promise.resolve(tokens);
    } else if (isValid(tokens.refreshToken)) {
      console.log("Refreshing from in memory TOKENS!!");
      return refreshToken(tokens);
    }
  }
  //try to read from file
  try {
    var tokens = fs.readFileSync(conf.dataDir + "/tokens.json", {encoding: 'utf8'});
    if (tokens != null) {
      tokens = JSON.parse(tokens);
      if (isValid(tokens.accessToken)) {
        TOKENS = tokens;

        console.log("In file TOKENS good!");
        return Promise.resolve(tokens);
      } else if (isValid(tokens.refreshToken)) {

        console.log("Refreshing from in file TOKENS!!");
        return refreshToken(tokens);
      }
    }
  } catch (e) {
    console.log("Ignoring error reading tokens from file: " + e);
  }

  console.log("Getting new tokens!");
  //get new tokens
  return msLogon()
    .then(function (success) {
      if (success) {
        return bungieAuth()
          .then(function (code) {
            return getTokensFromCode(code)
              .then(function (tokens) {
                TOKENS = tokens;
                return tokens;
              });
          });
      } else {
        throw new Error("Could not logon to MS");
      }
    })
}
