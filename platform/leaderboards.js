"use strict";
"use strict";

const fs = require('fs');
var UTILS = require("./utils");
var conf = require('../config.js')();
var redis = require("redis");

var moment = require('moment-timezone');
var async = require("async");

var COMMON, MATCH_INFO;

var KEY_PVP_KILLS = "pvp";
var KEY_ELDER = "elder";
var KEY_WRATH = "wrath";
var KEY_WRATHHM = "wrathhm";
var KEY_CE390 = "ce390";
var KEY_VOG390 = "vog390";
var KEY_KF390 = "kf390";
var DISABLED = false;
var fns = {
  KEY_CE390: KEY_CE390,
  KEY_VOG390: KEY_VOG390,
  KEY_KF390: KEY_KF390,
//  checkWrath: checkWrath,
//  checkWrathPGCR: checkWrathPGCR,
  getWrathLeaders: getWrathLeaders,
//  removeWrathDupes: removeWrathDupes,

  check390: check390,
  check390PGCR: check390PGCR,
  get390Leaders: get390Leaders,
  remove390Dupes:remove390Dupes,

  getElderLeaders: getElderLeaders,
  getPVPThreshold: getPVPThreshold,
  checkPVP: checkPVP,
  getPVPLeaders: getPVPLeaders,
  saveLeaders: saveLeaders,
  init: init,
  quit: quit
};

module.exports = function (common, matchInfo) {
  COMMON = common;
  MATCH_INFO = matchInfo;
  return fns;
};

var rClientPersist;
var THRESHOLDS;
if (!DISABLED) {
  THRESHOLDS = {
    elder: -1,
    pvp: -1,
    wrath: moment('2019-01-01'),
    wrathhm: moment('2019-01-01'),

    ce390: moment('2019-01-01'),
    vog390: moment('2019-01-01'),
    kf390: moment('2019-01-01'),
    
    totals: {
      elder: -51,
      pvp: -51,
      wrath: -1001,
      wrathhm: -1001,
      ce390: -1001,
      vog390: -1001,
      kf390: -1001
    }
  };

  rClientPersist = redis.createClient(6380);
  rClientPersist.on("error", function (err) {
    throw new Error("Redis error " + err);
  });
}
else {
  THRESHOLDS = {
    elder: Number.MAX_SAFE_INTEGER,
    pvp: Number.MAX_SAFE_INTEGER,
    wrath: moment('2012-01-01'),
    wrathhm: moment('2012-01-01'),
    ce309: moment('2012-01-01'),
    vog309: moment('2012-01-01'),
    kf309: moment('2012-01-01'),
    totals: {
      elder: -51,
      pvp: -51,
      wrath: -1001,
      wrathhm: -1001,
      ce390: -1001,
      vog390: -1001,
      kf390: -1001
    }
  };
}


function init() {
  updateThreshold(KEY_PVP_KILLS, getRealKey(KEY_PVP_KILLS));
  updateThreshold(KEY_ELDER, getRealKey(KEY_ELDER));
  //updateWrathThreshold(false);
  //updateWrathThreshold(true);
  update390Threshold(KEY_CE390);
  update390Threshold(KEY_VOG390);
  update390Threshold(KEY_KF390);
}

function getKey(key, resetDate) {
  return key + ":" + resetDate.format('YYYYMMDD');
}

function getRealKey(key) {
  return getKey(key, UTILS.getLastWeeklyReset());
}

function updateThreshold(key, realKey) {
  if (DISABLED) return;
  //100 would go to -101 
  var spot = THRESHOLDS.totals[key];
  rClientPersist.zrange(realKey, spot + 1, spot + 1, 'withscores', function (err, result) {
    if (err) {
      console.log("Error getting threshold for key " + key + ": " + err);
      return;
    }
    if (result && result.length > 0) {
      THRESHOLDS[key] = result[1];
    }
    else {
      THRESHOLDS[key] = -1;
    }
    rClientPersist.zremrangebyrank(realKey, 0, spot, function (err, data) {
      if (err) {
        console.log("Error getting trimming range for key " + key + ": " + err);
        return;
      }
    });
  });
}

function storeScore(key, period, score, data) {
  if (DISABLED) return;
  var properReset = UTILS.getResetForPeriod(period);
  var realKey = getKey(key, properReset);
  rClientPersist.zadd(realKey, score, JSON.stringify(data), function (err) {
    //just log it
    if (err) {
      console.log("Error storing key for " + realkey + ": " + err);
      return;
    }
    //otherwise update thresholds
    updateThreshold(key, realKey);
  });
}


function getPVPThreshold() {
  return THRESHOLDS.pvp;
}

function update390Threshold(key){
  if (DISABLED) return;
  var spot = THRESHOLDS.totals[key];
  rClientPersist.zrange(key, spot + 1, spot + 1, 'withscores', function (err, result) {
    if (err) {
      console.log("Error getting threshold for key " + key + ": " + err);
      return;
    }
    if (result && result.length > 0) {
      THRESHOLDS[key] = moment(-1 * result[1]);
    }
    else {
      THRESHOLDS[key] = moment('2019-01-01');
    }
    rClientPersist.zremrangebyrank(key, 0, spot, function (err, data) {
      if (err) {
        console.log("Error getting trimming range for key " + key + ": " + err);
        return;
      }
    });
  });

}

// function updateWrathThreshold(isHM) {
//   if (DISABLED) return;
//   var keyWrath = isHM ? KEY_WRATHHM : KEY_WRATH;
//   //100 would go to -101 
//   var spot = THRESHOLDS.totals[keyWrath];
//   rClientPersist.zrange(keyWrath, spot + 1, spot + 1, 'withscores', function (err, result) {
//     if (err) {
//       console.log("Error getting threshold for key " + key + ": " + err);
//       return;
//     }
//     if (result && result.length > 0) {
//       THRESHOLDS[keyWrath] = moment(-1 * result[1]);
//     }
//     else {
//       THRESHOLDS[keyWrath] = moment('2019-01-01');
//     }
//     rClientPersist.zremrangebyrank(keyWrath, 0, spot, function (err, data) {
//       if (err) {
//         console.log("Error getting trimming range for key " + keyWrath + ": " + err);
//         return;
//       }
//     });
//   });
// }

function store390Score(key, instanceId, data) {

  //date desc shouldn't be there
  //and the period is wrong
  delete data.dateDesc;
  //want to match with older items
  delete data.activityHash;
  
  instanceId = parseInt(instanceId, 10);
  var storeMe = {
    instanceId: instanceId,
    score: data.score * -1,
    displayScore: data.score,
    players: []
  };
  for (var cntr = 0; cntr < data.players.length; cntr++) {
    var p = data.players[cntr];
    storeMe.players.push({
      platform: p.platform,
      memberId: p.memberId,
      pClass: p.class,
      name: p.name,
      kills: p.kills,
      deaths: p.deaths,
      assists: p.assists,
      displayScore: p.displayScore
    });
  }
  rClientPersist.zadd(key, storeMe.score, JSON.stringify(storeMe), function (err) {
    //just log it
    if (err) {
      console.log("Error storing key for " + keyWrath + ": " + err);
      return;
    }
    //otherwise update thresholds
    update390Threshold(key);
  });
}


// function storeWrathScore(isHM, instanceId, data) {
//   instanceId = parseInt(instanceId, 10);
//   var storeMe = {
//     instanceId: instanceId,
//     score: data.score * -1,
//     displayScore: data.score,
//     players: []
//   };
//   for (var cntr = 0; cntr < data.players.length; cntr++) {
//     var p = data.players[cntr];
//     storeMe.players.push({
//       platform: p.platform,
//       memberId: p.memberId,
//       pClass: p.class,
//       name: p.name,
//       kills: p.kills,
//       deaths: p.deaths,
//       assists: p.assists,
//       displayScore: p.displayScore
//     });
//   }
//   var keyWrath = isHM ? KEY_WRATHHM : KEY_WRATH;
//   rClientPersist.zadd(keyWrath, storeMe.score, JSON.stringify(storeMe), function (err) {
//     //just log it
//     if (err) {
//       console.log("Error storing key for " + keyWrath + ": " + err);
//       return;
//     }
//     //otherwise update thresholds
//     updateWrathThreshold(isHM);
//   });
// }


var ACTIVITY_CE_390_FEAT = 4000873610;
var ACTIVITY_VOG_390_FEAT = 856898338;
var ACTIVITY_KING_390_FEAT = 3978884648;

function get390KeyFromActivityHash(hash){
  if (ACTIVITY_CE_390_FEAT == hash){
    
    return KEY_CE390;

  }
  else if (ACTIVITY_VOG_390_FEAT == hash){
    return KEY_VOG390;
  }
  else if (ACTIVITY_KING_390_FEAT == hash){
    return KEY_KF390; 
  }
  return null;
}

function check390PGCR(instanceId, callback, req) {
  MATCH_INFO.matchInfo(instanceId, function (err, data) {
    if (err) {
      console.log("Error on matchinfo " + instanceId + ": " + err);
      callback(err);
      return;
    }
    var hash = data.activityHash;
    var key = get390KeyFromActivityHash(hash);
    if (key==null){
      callback("Game is not a valid 390 raid");
      return;
    }

    if (!data.complete){
      callback("Raid "+key+" was not successful.");
      return;
    }
    var period = moment(data.period);
    var madeIt = false;
    var threshold = THRESHOLDS[key];
    if (period.isBefore(threshold)) {
      store390Score(key, instanceId, data);
      madeIt = true;
    }
    console.log(data);
    callback(null, madeIt);
  }, req, true, true);
}


// function checkWrathPGCR(isHM, instanceId, callback, req) {
//   MATCH_INFO.matchInfo(instanceId, function (err, data) {
//     if (err) {
//       console.log("Error on matchinfo " + instanceId + ": " + err);
//       return;
//     }
//     var period = moment(data.score);
//     var madeIt = false;
//     var threshold = isHM ? THRESHOLDS.wrathhm : THRESHOLDS.wrath;
//     if (period.isBefore(threshold)) {
//       storeWrathScore(isHM, instanceId, data);
//       madeIt = true;
//     }
//     console.log(data);
//     callback(madeIt);
//   }, req, true);
// }


function check390(key, playerData, period, instanceId, durationSeconds, req) {
  if (DISABLED) return;
  period = period.add(durationSeconds, "seconds");
  //must be early enough to make it
  var threshold = THRESHOLDS[key];
  if (!period.isBefore(threshold)) return false;
  playerData.leaderBoard = true;
  MATCH_INFO.matchInfo(instanceId, function (err, data) {
    if (err) {
      console.log("Error on matchinfo " + instanceId + ": " + err);
      return;
    }
    store390Score(key, instanceId, data);
  }, req, true);
  return true;
}


// function checkWrath(isHM, playerData, period, instanceId, durationSeconds, req) {
//   if (DISABLED) return;
//   period = period.add(durationSeconds, "seconds");
//   //must be early enough to make it
//   var threshold = isHM ? THRESHOLDS.wrathhm : THRESHOLDS.wrath;
//   if (!period.isBefore(threshold)) return;
//   playerData.leaderBoard = true;
//   MATCH_INFO.matchInfo(instanceId, function (err, data) {
//     if (err) {
//       console.log("Error on matchinfo " + instanceId + ": " + err);
//       return;
//     }
//     //date desc shouldn't be there
//     //and the period is wrong
//     delete data.dateDesc;
//     storeWrathScore(isHM, instanceId, data);
//   }, req, true);
//
// }


function getElderLeaders(callback, archiveFile) {
  getLeaders(KEY_ELDER, callback, archiveFile);
}

function deduceClass(sub) {
  if ("Voidwalker" === sub) return "Warlock";
  if ("Sunsinger" === sub) return "Warlock";
  if ("Stormcaller" === sub) return "Warlock";

  if ("Gunslinger" === sub) return "Hunter";
  if ("Bladedancer" === sub) return "Hunter";
  if ("Nightstalker" === sub) return "Hunter";

  if ("Striker" === sub) return "Titan";
  if ("Defender" === sub) return "Titan";
  if ("Sunbreaker" === sub) return "Titan";
  console.log("Unknown subclass: " + sub);
  return "?";
}

function checkPVP(period, lastReset, a, platform, memberId, data, subclassName) {
  if (DISABLED) return;

  // in the last week?
  if (period.isAfter(lastReset)) {
    //score worth storing?
    var score = a.values.kills.basic.value;
    if (score >= getPVPThreshold()) {
      //if no time, limit, not eligible
      if (data.type == "Trials of Osiris" || data.type == "Elimination")
        return;
      //store it
      COMMON.getMemberName(memberId, function (err, name) {
        if (err) {
          console.log("Error getting name: " + err);
          return;
        }
        //last their name in the cache, ignore for now
        if ("?" == name) {
          console.log("Lost name for member id " + memberId);
          return;
        }

        //remove date desc
        var addMe = JSON.parse(JSON.stringify(data));
        delete addMe.dateDesc;


        //data is same format used in Player history, all we need to display along with user linking info
        storeScore(KEY_PVP_KILLS, period, data.kills, {
          platform: platform,
          memberId: memberId,
          pClass: deduceClass(subclassName),
          name: name,
          data: addMe
        });
      });
    }
  }
}

function getPVPLeaders(callback, archiveFile) {
  getLeaders(KEY_PVP_KILLS, callback, archiveFile);
}

function parseLeaders(result) {
  var pResults = [];
  for (var cntr = 0; cntr < result.length; cntr++) {
    var r = JSON.parse(result[cntr]);
    pResults.push(r)
  }
  return pResults;
}

function remove390Dupes(key, superCallback){
  var spot = THRESHOLDS.totals[keyWrath];
  rClientPersist.zrevrange(key, spot + 1, -1, function (err, result) {
    if (err) {
      superCallback(err);
      return;
    }
    //find dupes
    var existingSet = {};
    var dupes = [];
    for (var cntr = 0; cntr < result.length; cntr++) {
      var id = JSON.parse(result[cntr]).instanceId;
      //this is a dupe
      if (existingSet[id]) dupes.push(id);
      else existingSet[id] = true;
    }
    var deleteMe = [];
    //dupes is the PGCR of duped items
    for (var cntr = 0; cntr < result.length; cntr++) {
      var id = JSON.parse(result[cntr]).instanceId;
      //this is a dupe, delete it
      if (dupes.indexOf(id) > -1) {
        deleteMe.push(result[cntr]);
      }
    }

    async.each(deleteMe,
      function (delMe, callback) {
        rClientPersist.zrem(key, delMe, function (err, result) {
          if (err) {
            callback(err);
            return;
          }
          console.log("Deleted " + delMe);
          callback();
        });
      }, function (err) {
        if (err) {
          superCallback(err);
        } else {
          update390Threshold(key);
          superCallback(null, dupes);
        }
      });
  });
}

// function removeWrathDupes(isHM, superCallback) {
//   var keyWrath = isHM ? KEY_WRATHHM : KEY_WRATH;
//   var spot = THRESHOLDS.totals[keyWrath];
//   rClientPersist.zrevrange(keyWrath, spot + 1, -1, function (err, result) {
//     if (err) {
//       superCallback(err);
//       return;
//     }
//     //find dupes
//     var existingSet = {};
//     var dupes = [];
//     for (var cntr = 0; cntr < result.length; cntr++) {
//       var id = JSON.parse(result[cntr]).instanceId;
//       //this is a dupe
//       if (existingSet[id]) dupes.push(id);
//       else existingSet[id] = true;
//     }
//     var deleteMe = [];
//     //dupes is the PGCR of duped items
//     for (var cntr = 0; cntr < result.length; cntr++) {
//       var id = JSON.parse(result[cntr]).instanceId;
//       //this is a dupe, delete it
//       if (dupes.indexOf(id) > -1) {
//         deleteMe.push(result[cntr]);
//       }
//     }
//
//     async.each(deleteMe,
//       function (delMe, callback) {
//         rClientPersist.zrem(keyWrath, delMe, function (err, result) {
//           if (err) {
//             callback(err);
//             return;
//           }
//           console.log("Deleted " + delMe);
//           callback();
//         });
//       }, function (err) {
//         if (err) {
//           superCallback(err);
//         } else {
//           updateWrathThreshold(isHM);
//           superCallback(null, dupes);
//         }
//       });
//   });
// }

function get390Leaders(key, callback) {
  var spot = THRESHOLDS.totals[key];
  rClientPersist.zrevrange(key, spot + 1, -1, function (err, result) {
    if (err) {
      callback(err);
      return;
    }

    var pResults = [];
    for (var cntr = 0; cntr < result.length; cntr++) {
      var r = JSON.parse(result[cntr]);
      r.displayScore = moment(r.displayScore);
      pResults.push(r)
    }

    callback(null, {
      leaders: pResults
    });
  });
}


function getWrathLeaders(isHM, callback) {
  var keyWrath = isHM ? KEY_WRATHHM : KEY_WRATH;
  var spot = THRESHOLDS.totals[keyWrath];
  rClientPersist.zrevrange(keyWrath, spot + 1, -1, function (err, result) {
    if (err) {
      callback(err);
      return;
    }

    var pResults = [];
    for (var cntr = 0; cntr < result.length; cntr++) {
      var r = JSON.parse(result[cntr]);
      r.displayScore = moment(r.displayScore);
      pResults.push(r)
    }

    callback(null, {
      leaders: pResults
    });
  });
}

function getLeaders(key, callback, archiveFile) {
  if (DISABLED) {
    callback(null, []);
    return;
  }
  if (archiveFile) {
    var path = conf.dataDir + "/leader" + key + "/";
    fs.readFile(path + archiveFile, function (err, data) {
      if (err) {
        callback(err);
        return;
      }
      callback(null, {
        leaders: JSON.parse(data),
        archive: OLD_LEADERS[key],
        archiveFile: archiveFile,
        archiveLabel: moment(archiveFile, "YYYYMMDD").format("MMM DD, YYYY")
      });
    });
    return;
  }
  var spot = THRESHOLDS.totals[key];
  var realKey = getRealKey(key);
  rClientPersist.zrevrange(realKey, spot + 1, -1, function (err, result) {
    if (err) {
      callback(err);
      return;
    }
    callback(null, {
      leaders: parseLeaders(result),
      archive: OLD_LEADERS[key]
    });
  });
}

function quit() {
  rClientPersist.quit();
}

function saveLeader(key, finalCallback) {
  rClientPersist.keys(key + ':*', function (err, keys) {
    if (err) {
      callback(err);
      return;
    }
    var currentKey = getRealKey(key);
    var written = [];
    var path = conf.dataDir + "/leader" + key + "/";
    if (!fs.existsSync(path))
      fs.mkdirSync(path);
    async.each(keys, function (found, callback) {
      if (found == currentKey) {
        callback();
        return;
      }
      rClientPersist.zrevrange(found, 0, -1, function (err, result) {
        if (err) {
          callback(err);
          return;
        }
        var sResult = JSON.stringify(parseLeaders(result));
        var date = found.substring(key.length + 1, found.length);
        var filename = path + date;
        fs.writeFileSync(filename, sResult, 'utf8');
        written.push(filename);
        rClientPersist.del(found);
        callback();
      });
    }, function (err) {
      if (written && written.length > 0) {
        updateThreshold(key, getRealKey(key));
      }
      finalCallback(err, written);
    });
  });
}

function saveLeaders(callback) {
  if (DISABLED) {
    callback(null, {});
    return;
  }
  saveLeader(KEY_PVP_KILLS, function (err, written) {
    if (err) {
      callback(err);
      return;
    }

    saveLeader(KEY_ELDER, function (err, written2) {
      if (err) {
        callback(err);
        return;
      }
      loadOldLeaders();

      callback(null, {
        pvp: written,
        elder: written2,
        thresholds: THRESHOLDS
      })
    });
  })
}

function loadOldLeaders() {
  loadOldLeader(KEY_PVP_KILLS, function (err) {
    if (err) {
      console.log("Error loading old pvp leaders: " + err);
    }
    loadOldLeader(KEY_ELDER, function (err) {
      if (err) {
        console.log("Error loading old elder leaders: " + err);
      }
      console.log("Loaded old leaders");
      //console.dir(OLD_LEADERS);
    });
  });
}

var OLD_LEADERS = {
  pvp: [],
  elder: []
}

function loadOldLeader(key, callback) {
  var path = conf.dataDir + "/leader" + key + "/";
  if (!fs.existsSync(path)) callback();
  fs.readdir(path, function (err, files) {
    if (err) {
      callback(err);
      return;
    }
    files.reverse();
    var entries = [];
    for (var cntr = 0; cntr < files.length; cntr++) {
      var filename = files[cntr];
      var label = moment(filename, "YYYYMMDD").format("MMM DD, YYYY");
      entries.push({
        filename: filename,
        label: label
      });
    }
    OLD_LEADERS[key] = entries;
    callback();
  });
}