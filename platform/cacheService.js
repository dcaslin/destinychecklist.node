/**
 * Created by Dave on 6/30/2016.
 */

var conf = require('../config.js')();
var CACHE = require(conf.dataDir + "/destiny.json");

var fns = {
  cache: cache,
  explicitCache: explicitCache
};

module.exports = function (redis, common) {
  rClient = redis;
  return fns;
};


function cache(req){
  return CACHE;
}


function explicitCache(language){
  return CACHE;
}
