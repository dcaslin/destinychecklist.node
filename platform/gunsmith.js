/**
 * Created by Dave on 6/30/2016.
 */
var UTILS = require("./utils");
var conf = require('../config.js')();
var CACHE = require(conf.dataDir + "/destiny.json");
var moment = require('moment-timezone');
var async = require("async");
const fs = require('fs');

var rClient;
var COMMON;
var fns = {
  listGunsmithChars: listGunsmithChars,
  storeFoundryRolls: storeFoundryRolls,
  getFoundryRolls: getFoundryRolls
};

var ROLL_PATH = conf.dataDir + "/gsRolls.json";

function buildEmptyFoundryRolls() {
  return {
    serializeGuns: [],
    guns: [],
    hash: {},
    expires: null
  };
}

var FOUNDRY_ROLLS = buildEmptyFoundryRolls();
loadFoundryRolls();

module.exports = function (redis, common) {
  rClient = redis;
  COMMON = common;
  return fns;
};

function getFoundryRolls() {
  if (FOUNDRY_ROLLS == null) return [];
  if (moment().isAfter(FOUNDRY_ROLLS.expires)) {
    FOUNDRY_ROLLS = buildEmptyFoundryRolls();
  }
  return FOUNDRY_ROLLS.guns;
}

function saveFoundryRolls() {
  var saveMe = {
    serializeGuns: FOUNDRY_ROLLS.serializeGuns,
    expires: FOUNDRY_ROLLS.expires.format()
  };
  saveMe = JSON.stringify(saveMe);
  fs.writeFileSync(ROLL_PATH, saveMe, 'utf8');
  console.log("Saved " + saveMe.serializeGuns.length + " gunsmith gun rolls that expire on " + saveMe.expires);
}

function loadFoundryRolls() {
  fs.readFile(ROLL_PATH, function (err, data) {
    if (err) {
      return;
    }
    var parsed = JSON.parse(data);
    FOUNDRY_ROLLS.serializeGuns = parsed.serializeGuns;
    FOUNDRY_ROLLS.expires = moment(parsed.expires);
    FOUNDRY_ROLLS.serializeGuns.forEach(
      function(serializeGun){
        addGun(serializeGun);
      }
    );
    console.log("Loaded " + FOUNDRY_ROLLS.guns.length + " gunsmith gun rolls that expire on " + FOUNDRY_ROLLS.expires.format('MMMM Do YYYY, h:mm:ss a'));
  });
}

function processNodes(talentGridHash, damageType, nodes) {

  var steps = [];
  var tDesc = CACHE.TalentGrid[talentGridHash];
  var selectedNodes = {};
  for (var cntr7 = 0; cntr7 < nodes.length; cntr7++) {
    var node = nodes[cntr7];
    selectedNodes[node.nodeHash] = node;
  }
  for (var cntr7 = 0; cntr7 < tDesc.nodes.length; cntr7++) {
    var node = tDesc.nodes[cntr7];
    if (node.column < 1) continue;

    for (var cntr8 = 0; cntr8 < node.steps.length; cntr8++) {
      var step = node.steps[cntr8];
      step.col = node.column;
      var selNode = selectedNodes[node.nodeHash];
      if (selNode) {
        if (selNode.stepIndex == cntr8) {
          if (step.nodeStepName) {
            steps.push(step);
          }
        }
      }
    }
  }

  steps.sort(function (a, b) {
    return a.col - b.col;
  });

  var groups = {};
  steps.forEach(function (step) {
    if (!groups[step.col]) {
      groups[step.col] = [];
    }
    groups[step.col].push(step.nodeStepName);
  });
  var aGroups = [];
  for (var key in groups) {
    if (!groups.hasOwnProperty(key)) continue;
    aGroups.push(groups[key]);
  }
  
  return {
    damageType: COMMON.decodeDamage(damageType),
    steps: aGroups
  };
}

function addGun(serializeGun) {
  //save the general gun info, but also look up pretty info
  var jDesc = CACHE.InventoryItem[serializeGun.itemHash];
  var damageAndSteps = [];
  serializeGun.rolls.forEach(function (roll) {
    var pushMe = processNodes(serializeGun.talentGridHash, roll.damageType, roll.nodes);
    damageAndSteps.push(pushMe);
  });

  var prettyGun = {
    itemHash: serializeGun.itemHash,
    itemTypeName: jDesc.itemTypeName,
    tierTypeName: jDesc.tierTypeName,
    name: jDesc.itemName,
    damageAndSteps: damageAndSteps
  };

  FOUNDRY_ROLLS.guns.push(prettyGun);

  FOUNDRY_ROLLS.guns.sort(function (a, b) {
    if (a.name < b.name)
      return -1;
    else if (a.name > b.name)
      return 1;
    else
      return 0;
  });

  FOUNDRY_ROLLS.hash[serializeGun.itemHash] = true;
}

function storeFoundryRolls(expires, guns) {
  if (!expires) return;
  if (!guns) return;
  var mExpires = moment(expires);
  var lastReset = UTILS.getLastWeeklyReset();
  if (!mExpires.isAfter(lastReset)) return;
  //guns[]
  var valid = true;
  guns.forEach(function (gun) {
    valid = valid && gun.rolls.length >= 3;
  });
  if (!valid) return;
  var newGunAdded = false;
  guns.forEach(function (serializeGun) {
    //do we already have it?
    if (FOUNDRY_ROLLS.hash[serializeGun.itemHash]) return;
    if (!FOUNDRY_ROLLS.expires) FOUNDRY_ROLLS.expires = mExpires;
    FOUNDRY_ROLLS.serializeGuns.push(serializeGun);
    addGun(serializeGun);
    newGunAdded = true;
  });
  //if we added a new one, save it
  if (newGunAdded) {
    saveFoundryRolls();
  }
}

function listGunsmithChars(platform, memberId, name, finalCallback, req) {
  COMMON.listCharactersByMemberId(platform, memberId, name,
    function (err, data) {
      if (err) {
        finalCallback(err);
      }
      else {
        var chars = [];
        async.eachSeries(data.chars, function iterator(char, callback) {
          chars.push(char);
          COMMON.applyPublicAdvisors(platform, memberId, char.id, char, function(err, returnMe){
            if (err){
              callback(err);
              return;
            }
            COMMON.applyProgression(platform, memberId, char.id, returnMe, callback, req, true);
          }, req);
        }, function done(err) {
          finalCallback(err, chars);
        });
      }
    }, req);
}