/**
 * Created by Dave on 6/30/2016.
 */
var UTILS = require("./utils");
var cs = require("./cacheService")();

var rClient;
var COMMON;
var fns = {
  grim: grim
};

module.exports = function (redis, common) {

  rClient = redis;
  COMMON = common;
  return fns;
};

function grim(platform, memberId, callback, req) {
  var key = "grim:" + platform + ":" + memberId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      cached = JSON.parse(cached);
      callback(null, cached);

    }
    else {
      UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/Vanguard/Grimoire/' + platform + "/" + memberId,
        function (error, jo) {
          if (error) {
            callback(error);
            return;
          }
          var rm = {
            totals: {
              complete: 0,
              incomplete: 0,
              missing: 0,
              earnedPts: 0,
              totalPts: 0,
              missingPts: 0
            },
            cards: []
          };
          var j = jo.Response.data;
          var collected = {};
          var hidden = {};
          for (var cntr = 0; cntr < j.cardsToHide.length; cntr++) {
            hidden[j.cardsToHide[cntr]] = true;
          }

          for (var cntr = 0; cntr < j.cardCollection.length; cntr++) {
            var c = j.cardCollection[cntr];
            collected[c.cardId] = c;
          }

          var cards = cs.cache(req).GrimoireCard;
          for (var cntr = 0; cntr < cards.length; cntr++) {
            var c = cards[cntr];
            if (hidden[c.cardId] == true) continue;
            var coll = collected[c.cardId];
            var earned = 0;
            var pctToNextRank = 0;
            var valToNextRank = 0;
            var thresholdNextRank = 0;
            var lblNextRank = "";

            //base points for card discovery
            if (coll) {
              earned = c.points;
            }
            else {
              rm.totals.missing++;
            }

            //ranks
            var stats = [];
            if (c.hasRanks) {

              for (var cntr2 = 0; cntr2 < c.statisticCollection.length; cntr2++) {
                var templateStat = c.statisticCollection[cntr2];
                if (!templateStat.rankCollection) continue;

                var curStat = {
                  name: templateStat.statName,
                  value: 0,
                  ranks: []
                };
                stats.push(curStat);
                var colStat = null;
                if (coll) {
                  for (var cntr3 = 0; cntr3 < coll.statisticCollection.length; cntr3++) {
                    if (coll.statisticCollection[cntr3].statNumber == templateStat.statNumber)
                      colStat = coll.statisticCollection[cntr3];
                  }
                }

                if (colStat)
                  curStat.value = colStat.value;

                for (var cntr3 = 0; cntr3 < templateStat.rankCollection.length; cntr3++) {
                  var templateRank = templateStat.rankCollection[cntr3];
                  var rank = {
                    rank: templateRank.rank,
                    threshold: templateRank.threshold,
                    complete: false,
                    points: templateRank.points,
                    pctToNextRank: 0
                  };
                  curStat.ranks.push(rank);

                  if (curStat.value > 0 && curStat.value < templateRank.threshold) {
                    var curPctToNextRank = (curStat.value / templateRank.threshold);
                    rank.pctToNextRank = curPctToNextRank;
                    if (curPctToNextRank > pctToNextRank) {
                      pctToNextRank = curPctToNextRank;
                      valToNextRank = curStat.value;
                      thresholdNextRank = templateRank.threshold;
                      lblNextRank = curStat.name;
                    }
                  }
                  else if (curStat.value > 0) rank.pctToNextRank = -1;

                  if (colStat) {
                    if (colStat.rankCollection) {
                      if (colStat.rankCollection.length > cntr3) {
                        var curRank = colStat.rankCollection[cntr3];
                        rank.complete = true;
                        earned += curRank.points;
                      }
                    }
                  }
                }
              }
            }

            var complete = !(!coll || earned < c.totalPoints);
            //if (stats.length > 0)
            if (coll) {
              if (complete)
                rm.totals.complete += 1;
              else
                rm.totals.incomplete += 1;
            }

            rm.totals.earnedPts += earned;
            rm.totals.totalPts += c.totalPoints;
            rm.totals.missingPts += (c.totalPoints - earned);
            rm.cards.push({
              id: c.cardId,
              cardName: c.cardName,
              complete: complete,
              pctToNextRank: complete ? -1 : pctToNextRank,
              valToNextRank: valToNextRank,
              thresholdNextRank: thresholdNextRank,
              lblNextRank: lblNextRank,
              earnedPts: earned,
              totalPts: c.totalPoints,
              link: c.linkName,
              stats: stats
            });

          }
          rClient.set(key, JSON.stringify(rm));
          rClient.expire(key, 3600);
          callback(null, rm);
        }
      );
    }
  });
}
