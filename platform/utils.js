/**
 * Created by Dave on 10/2/2015.
 */
var moment = require('moment-timezone');
var request = require('request');

function publicRequest(url, callback) {
  var start = Date.now();
  var options = {
    url: url,
    headers: {
      'x-api-key': '997251a328d246f5ac3331b818673d12'
    }
  };

  request(options, function (error, response, body) {
    var elapsed = Date.now() - start;
    console.log("BUNGIE: " + elapsed + " ms - " + url + " | " + moment().format());
    if (error) {
      callback(error);
      return;
    }
    if (response.statusCode != 200) {
      var throwMe = new Error("Unexpected response code: " + response.statusCode);
      callback(throwMe);
      return;
    }
    var jo = JSON.parse(response.body);

    if (jo.ErrorStatus && jo.ErrorStatus != "Success") {
      if (jo.ErrorCode && jo.ErrorCode == 5) {
        var throwMe = new Error("The Bungie API is down for maintenance.");
        throwMe.status = jo.ErrorStatus;
        callback(throwMe);
        return;
      }
      if ("DestinyInvalidClaimException" == jo.ErrorStatus) {
        var throwMe = new Error("The Bungie API seems to be having a problem at the moment, please check back later. Root cause: " + jo.Message);
        throwMe.status = jo.ErrorStatus;
        callback(throwMe);
        return;
      }
      if (jo.Message) {
        if (jo.ErrorStatus && jo.ErrorStatus == "DestinyPrivacyRestriction") {
          callback(new Error("DestinyPrivacyRestriction"));
          return;
        }
        var throwMe = new Error(jo.Message);
        callback(throwMe);
        return;
      }
      callback(new Error(jo.ErrorStatus));
      return;
    }

    callback(null, jo);
  });
}


function sortByLabel(a, b) {
  if (a.label < b.label) return -1;
  if (a.label > b.label) return 1;
  return 0;
}

function sortByName(a, b) {
  if (a.name < b.name) return -1;
  if (a.name > b.name) return 1;
  return 0;
}

function countKeys(obj) {
  var keyCntr = 0;
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      keyCntr++;
    }
  }
  return keyCntr;
}

function getLastDailyReset() {
  var now = moment.utc();
  var hour = now.hour();
  if (hour < 9) {
    now.subtract(1, "day");
  }
  now.hour(9);
  now.minute(0);
  now.second(0);
  now.millisecond(0);
  return now;
}

function getResetForPeriod(period) {
  var lastReset = getLastWeeklyReset();
  while (period.isBefore(lastReset)) {
    lastReset.add(-7, "day");
  }
  return lastReset;
}

function getLastWeeklyReset() {
  var now = moment.utc();
  var changeDays = 0;

  //if it's tuesday
  if (now.day() == 2) {
    var hour = now.hour();
    if (hour > 9) {
      changeDays = 0;
    }
    //before 9, last week
    else {
      changeDays = -7;
    }
  }
  //it's after tuesday
  else if (now.day() > 2) {
    changeDays = 2 - now.day();
  }
  //it's before tuesday
  else {
    changeDays = 2 - now.day() - 7;
  }
  now.add(changeDays, "day");
  now.hour(9);
  now.minute(0);
  now.second(0);
  now.millisecond(0);
  return now;
}


var fns = {
  sortByLabel: sortByLabel,
  sortByName: sortByName,
  countKeys: countKeys,
  getLastDailyReset: getLastDailyReset,
  getLastWeeklyReset: getLastWeeklyReset,
  publicRequest: publicRequest,
  getResetForPeriod: getResetForPeriod
};

module.exports = fns;