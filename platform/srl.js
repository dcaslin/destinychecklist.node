/**
 * Created by Dave on 6/30/2016.
 */
/**
 * Created by Dave on 6/30/2016.
 */
var UTILS = require("./utils");
var conf = require('../config.js')();
var CACHE = require(conf.dataDir + "/destiny.json");
var moment = require('moment-timezone');
var async = require("async");

var rClient;
var COMMON;
var fns = {
  listSrlChars: listSrlChars
};

module.exports = function (redis, common) {
  rClient = redis;
  COMMON = common;
  return fns;
};

function listSrlChars(platform, memberId, callback, req) {
  var key = "srlChars:" + platform + ":" + memberId;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      cached = JSON.parse(cached);
      callback(null, cached);

    }
    else {
      UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/' + platform + "/Account/" + memberId + "/Summary/",
        function (error, jo) {
          if (error) {
            callback(error);
            return;
          }
          var rm = {
            player: {
              memberId: memberId,
              platform: platform
            },
            prs: {},
            chars: []
          };
          var j = jo.Response.data;
          for (var cntr = 0; cntr < j.characters.length; cntr++) {
            var char = j.characters[cntr];
            char = char.characterBase;
            var raceName = CACHE.Race[char.raceHash].raceName;
            var genderName = CACHE.Gender[char.genderHash].genderName;
            var className = CACHE.Class[char.classHash].className;

            var jchar = {
              id: char.characterId,
              label: className + " (" + char.stats.STAT_LIGHT.value + ") " + raceName.substr(0, 2) + " " + genderName.substr(0, 1),
              race: raceName,
              gender: genderName,
              class: className,
              power: char.stats.STAT_LIGHT.value,
              equipped: {
                sparrow: null
              }
            };
            var equipped = char.peerView.equipment;
            for (var cntr2 = 0; cntr2 < equipped.length; cntr2++) {
              var hash = equipped[cntr2].itemHash;
              var jDesc = CACHE.InventoryItem[hash];
              if (!jDesc)
                continue;
              var bHash = jDesc.bucketTypeHash;
              //sparrow
              if (2025709351 == bHash) {
                jchar.equipped.sparrow = {
                  type: jDesc.itemTypeName,
                  name: jDesc.itemName,
                  durability: jDesc.stats[360359141].value,
                  speed: jDesc.stats[1501155019].value,
                  boost: jDesc.stats[3017642079].value
                };
              }
              //otherwise we don't care
              else {

              }

            }
            rm.chars.push(jchar);
          }

          async.each(rm.chars, function (char, subCb) {
            applySrlProgression(platform, memberId, char, subCb);
          }, function (err) {
            if (err) {
              callback(error);
              return;
            }
            async.each(rm.chars, function (char2, subCb2) {
              applySrlHistory(rm.prs, platform, memberId, char2, subCb2);
            }, function (err) {
              if (err) {
                callback(error);
                return;
              }
              rClient.set(key, JSON.stringify(rm));
              rClient.expire(key, 3600);
              callback(null, rm);
            });

          });
        }
      );
    }
  });
}

function applySrlHistory(prs, platform, memberId, char, callback) {
  UTILS.publicRequest("https://www.bungie.net/D1/Platform/Destiny/Stats/ActivityHistory/" + platform + "/" + memberId + "/" + char.id + "/?count=100&mode=Racing",
    function (error, jo) {
      if (error) {
        callback(error);
        return;
      }
      if (jo.Response.data.activities) {
        var ja = jo.Response.data.activities;
        char.history = [];
        for (var cntr = 0; cntr < ja.length; cntr++) {
          var game = ja[cntr];
          var date = moment(game.period);
          var aDesc = CACHE.Activity[game.activityDetails.referenceId];
          var track = aDesc.activityName;

          var race = {
            racer: char.label,
            hash: game.activityDetails.instanceId,
            timeMs: game.values.raceCompletionMilliseconds.basic.value,
            timePretty: game.values.raceCompletionMilliseconds.basic.displayValue,
            track: track,
            place: game.values.standing.basic.value,
            deaths: game.values.deaths.basic.value,
            date: date.format('MM/DD/YYYY')

          };

          if (prs[track]) {
            if (race.timeMs > 0) {
              if (race.timeMs < prs[track].timeMs) {
                prs[track] = race;
              }
            }
          }
          else {
            if (race.timeMs > 0)
              prs[track] = race;
          }

          char.history.push(race);

        }
      }
      callback();
    });
}



function applySrlProgression(platform, memberId, char, callback) {
  UTILS.publicRequest('https://www.bungie.net/D1/Platform/Destiny/' + platform + "/Account/" + memberId + "/Character/" + char.id + "/Progression",
    function (error, jo) {
      if (error) {
        callback(error);
        return;
      }
      var ja = jo.Response.data.progressions;
      for (var cntr = 0; cntr < ja.length; cntr++) {
        if (ja[cntr].progressionHash == 2763619072) {
          char.prog = ja[cntr];
          var pct = Math.floor(100 * char.prog.progressToNextLevel / char.prog.nextLevelAt);
          char.prog.pct = pct;
          char.prog.style = {
            'min-width': '2em',
            'width': pct + '%'
          };
        }
      }
      callback();
    });
}
