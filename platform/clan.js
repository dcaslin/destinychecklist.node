/**
 * Created by Dave on 6/30/2016.
 */
var UTILS = require("./utils");
var async = require("async");

var rClient;
var COMMON;
var fns = {
  findClanMembers: findClanMembers
};

module.exports = function (redis, common) {
  COMMON = common;
  rClient = redis;
  return fns;
};

function findClanId(name, parentCallback, req) {
  var key = "clanId:" + name;
  if (req.nocache) rClient.del(key);
  rClient.get(key, function (err, cached) {
    if (cached) {
      parentCallback(null, cached);
    }
    else {
      UTILS.publicRequest('http://www.bungie.net/platform/Group/Name/' + encodeURIComponent(name),
        function (error, jo) {
          if (error) {
            if ("GroupNotFound" == error.message) {
              parentCallback(null, null);
              return;
            }
            parentCallback(error);
            return;
          }
          var clanId = jo.Response.detail.groupId;
          rClient.set("clanId:" + name, clanId);
          parentCallback(null, clanId);
        }
      );
    }
  });
}

function findClanMembers(platform, name, parentCallback, req) {
  findClanId(name, function (err, clanId) {
      if (err) {
        if ("GroupNotFound" == err)
          parentCallback(null);
        else
          parentCallback(err);
      }
      else {
        if (clanId == null) {
          parentCallback(null, null);
          return;
        }
        var key = "clan:" + name+":"+platform;
        if (req.nocache) rClient.del(key);

        rClient.get(key, function (err, cached) {
            if (cached) {
              tags = JSON.parse(cached);
              parentCallback(null, tags);

            }
            else {
              var page = 0;
              var hasMore = true;
              var tags = [];

              async.whilst(
                function () {
                  return hasMore;
                },
                function (callback) {

                  page++;
                  var url = "http://www.bungie.net/Platform/Group/" + clanId +
                    "/MembersV3/?lc=en&fmt=true&currentPage=" + page + "&itemsPerPage=50&platformType=" + platform;
                  UTILS.publicRequest(url,
                    function (error, jo) {
                      if (error) {
                        callback(error);
                        return;
                      }
                      hasMore = jo.Response.hasMore;
                      var results = jo.Response.results;
                      for (var cntr = 0; cntr < results.length; cntr++) {
                        var p = results[cntr].user;

                        COMMON.cacheMemberId(p.membershipId, platform, name);
                        if (platform == 1) {
                          if (p.xboxDisplayName)
                            tags.push(p.xboxDisplayName);
                        }
                        else {
                          if (p.psnDisplayName)
                            tags.push(p.psnDisplayName);
                        }
                      }
                      callback();
                    }
                  );
                },
                function (err) {
                  tags.sort();
                  //remove duplicates

                  tags.filter(function(item, pos, ary) {
                    return !pos || item != ary[pos - 1];
                  })
                  
                  rClient.set(key, JSON.stringify(tags));
                  rClient.expire(key, 86400);
                  parentCallback(null, tags);

                });
            }
          }
        );
      }
    }, req
  );
}
