/**
 * Created by Dave on 6/30/2016.
 */
var async = require("async");
var rClient;
var COMMON;
var fns = {
  listVennChars: listVennChars
};

module.exports = function (redis, common) {
  rClient = redis;
  COMMON = common;
  return fns;
};

function listPlayerVennChars(platform, player, finalCallback, req) {
  COMMON.listCharactersByMemberId(platform, player.memberId, player.name,
    function (err, data) {
      if (err) {
        finalCallback(err);
      }
      else {
        var chars = [];
        player.chars = chars;
        async.eachSeries(data.chars, function iterator(char, callback) {
          chars.push(char);
          COMMON.applyPublicAdvisors(platform, player.memberId, char.id, char, callback, req);
        }, function done(err) {
          finalCallback(err);
        });
      }
    }, req);
}

function listVennChars(platform, name1, name2, name3, finalCallback, req) {
  var players = [];
  players.push({name: name1});
  players.push({name: name2});
  if (name3)
    players.push({name: name3});
  var goodPlayers = [];

  async.each(players, function(player, callback){
    COMMON.getMemberId(platform, player.name, function (err, memberId) {
      if (err)
        player.err = err;
      else{
        player.memberId = memberId;
        goodPlayers.push(player);
      }
      callback(null, player);
    }, req);
  }, function(err){
    if (goodPlayers.length>=2){
      async.each(goodPlayers, function(player, callback){
        listPlayerVennChars(platform, player,  function(err){
          callback(err);
        }, req);
      }, function(err){
        finalCallback(err, players);
      });
    }
    else{
      finalCallback(err, players);  
    }
  });
}