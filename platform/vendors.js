/**
 * Created by Dave on 12/4/2016.
 */

var conf = require('../config.js')();
var CACHE = require(conf.dataDir + "/destiny.json");
var SECURE_REQUEST = require("./secureRequest")();
var ITEM_PARSE = require('./itemParseService.js')();
var async = require("async");


var fns = {
  refresh: refresh,
  getVendors: getVendors
};

module.exports = function (common) {
  COMMON = common;
  return fns;
};
var VENDOR_DATA = {
  weapons: [],
  armor: []
};
;

//var CHAR_IDS = ['2305843009345941076', '2305843009219352385', '2305843009219816265']; //E5 Dweezil
var CHAR_IDS = ["2305843009223785827","2305843009218644656","2305843009240194376"]; // Meggito
var CHAR_VENDORS = ['3611686524', '1821699360', '1808244981', '1998812735', '2680694281', '2190824860', '174528503', '2190824863', '3902439767', '2610555297', '2796397637', '3003633346', '1575820975', '1990950', '2668878854', '3746647075', '3658200622', '134701236', '459708109'];
//var CHAR_VENDORS = ['2610555297'];


function refresh(callback) {
  console.log("Getting vendor data.");

  var callMe = [];
  var item_pile = {};
  CHAR_IDS.forEach(function (charId) {
    CHAR_VENDORS.forEach(function (vendorId) {
      callMe.push(dumpItems.bind(null, charId, vendorId, item_pile));
    });
  });
  async.parallel(callMe, function (err, results) {
    if (err) {
      callback(err);
      return;
    }
    var aWeapons = [];
    var aArmor = [];
    var aEmblems = [];
    var aShips = [];
    var aShaders = [];
    var aArtifacts = [];
    var aXur = [];
    for (var key in item_pile) {
      if (item_pile.hasOwnProperty(key)) {
        var pushMe = item_pile[key];
        if (pushMe.vendor == "Agent of the Nine") {
          aXur.push(pushMe);
        }
        if (pushMe.item.metaType == "armor") {
          if (pushMe.item.type == "Artifact") {
            aArtifacts.push(pushMe);
          }
          aArmor.push(pushMe);
        }
        else if (pushMe.item.metaType == "weapon") {
          aWeapons.push(pushMe);
        }
        else if (pushMe.item.metaType == "emblem") {
          aEmblems.push(pushMe);
        }
        else if (pushMe.item.metaType == "ship") {
          aShips.push(pushMe);
        }
        else if (pushMe.item.metaType == "shader") {
          aShaders.push(pushMe);
        }
        else {
          throw new Error("Metatype unexpected: " + pushMe.item.metaType);
        }
      }
    }
    VENDOR_DATA = {
      weapons: aWeapons,
      armor: aArmor,
      emblems: aEmblems,
      ships: aShips,
      shaders: aShaders,
      artifacts: aArtifacts,
      xur: aXur
    };
    callback(null, VENDOR_DATA);
  });
}

function dumpItems(charId, vendorId, itemPile, callback) {
  //var FOUND_ITEMS = [];
  var venDesc = CACHE.Vendor[vendorId];
  SECURE_REQUEST.sendRequest("https://www.bungie.net/D1/Platform/Destiny/1/MyAccount/Character/" + charId + "/Vendor/" + vendorId, function (err, jo) {
    if (err) {
      if (err == "DestinyVendorNotFound") {
        console.log("Vendor id: " + vendorId + " not found for char id " + charId + ". This may be fine");
        callback();
        return;
      }
      console.log("Unexpected error: Vendor id " + vendorId + ", char id " + charId + ": " + err);
      callback();
      return;
    }
    var data = jo.Response.data;
    data.saleItemCategories.forEach(function (cat) {
      cat.saleItems.forEach(function (saleItem) {
        //saleItem.costs;
        var itm = saleItem.item;
        var itmHash = itm.itemHash;

        var key = "" + itmHash + saleItem.vendorItemIndex;
//        if (FOUND_ITEMS.indexOf(key) > -1) return;
        var jDesc = CACHE.InventoryItem[itmHash];
        if (!jDesc) return;
        //only look at weapons and armor
        //if (jDesc.itemType==14){
        //  console.log(jDesc.itemType+" "+jDesc.itemTypeName+": "+jDesc.itemName);
        //}
        if (jDesc.itemType != 3 && jDesc.itemType != 2 && jDesc.itemType != 0 && jDesc.itemType != 14) return;

        //only ships and shaders
        if (jDesc.itemType == 0) {
          if ("Ship" == jDesc.itemTypeName) {

          }
          else if ("Armor Shader" == jDesc.itemTypeName) {

          }
          else {
            return;
          }
        }
        if (itm.primaryStat && itm.primaryStat.value < 200) return;

        //console.log(saleItem.item.itemInstanceId+" "+key+" "+itmHash);
        //FOUND_ITEMS.push(key);
        var item = ITEM_PARSE.parseItem(itm);
        item.searchText += venDesc.summary.vendorName.toUpperCase();
        var costs = [];
        var glimmer = 0;
        saleItem.costs.forEach(function (cost) {

          var costMe = {
            name: CACHE.InventoryItem[cost.itemHash].itemName,
            value: cost.value
          };
          if (costMe.name === "Glimmer") {
            glimmer = cost.value;
          }
          costs.push(costMe);
        });
        itemPile[key] = {
          vendor: venDesc.summary.vendorName,
          resetDays: venDesc.summary.resetIntervalMinutes / 1440,
          costs: costs,
          glimmer: glimmer,
          item: item
        }
      });
    });
    callback(null);
    return;
  });
}

function getVendors() {
  return VENDOR_DATA;
}
