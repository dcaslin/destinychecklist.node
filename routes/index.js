module.exports = function (platformApi) {
  var express = require('express');
  var router = express.Router();

  function middle(req, title, path, overridePath) {
    var pageData = {
      title: title, 
      path: path, 
      csstheme: req.cookies.csstheme, 
      cssname: req.cookies.cssname, 
      special : false
    };
    if (!overridePath) overridePath = path;
    applySpecialPageData(req, overridePath, pageData);
    return pageData;
  }

  router.get('/', function (req, res, next) {
    if (req.headers.host && req.headers.host.includes("destinyib")) {
      handleIB(req, res, next);
    }
    else if (req.headers.host && req.headers.host.includes("gunsmith")) {
      handleGunsmith(req, res, next);
    }
    else if (req.headers.host && req.headers.host.includes("truemeaningofwar")) {
      handleTmow(req, res, next);
    }
    else if (req.headers.host && req.headers.host.includes("venndestiny")) {
      handleVenn(req, res, next);
    }
    else if (req.headers.host && req.headers.host.includes("srlcalc")) {
      handleSRL(req, res, next);
    }
    else {
      routeIndex(req, res, next);
    }
  });

  router.get('/index', function (req, res, next) {
    routeIndex(req, res, next);
  });


  router.get('/index.html', function (req, res, next) {
    routeIndex(req, res, next);
  });


  router.get('/index.htm', function (req, res, next) {
    routeIndex(req, res, next);
  });

  function routeIndex(req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net', "index");
    pageData.advisors = platformApi.getAdvisors();
    pageData.artifacts = platformApi.getVendors().artifacts;
    pageData.xur = platformApi.getVendors().xur;
    pageData.shownews = true;
    res.render('index', pageData);
  }

  router.get('/privacy', function (req, res, next) {
    res.render('privacy', middle(req, 'DestinyChecklist.net - Privacy Policy', "privacy"));
  });
  router.get('/oracle', function (req, res, next) {
    res.render('oracle', middle(req, 'DestinyChecklist.net - Oracle Spawns', "oracle"));
  });

  router.get('/about', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - About', "about");
    pageData.supporters = platformApi.getSupporters();
    res.render('about', pageData);
  });

  router.get('/specialAbout/:tag', function (req, res, next) {
    var pageData = middle(req, '', "about", req.params.tag);
    res.render('specialAbout', pageData);
  });

  router.get('/leaderPvp/:filename?', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - Leaderboard PVP', "leaderboardPvp");
    platformApi.getPVPLeaders(function (err, pvpData) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      pageData.data = pvpData;
      res.render('leaderPvp', pageData);
    }, req.params.filename);
  });


  function handleLeaderAOT(key, viewName, req, res){
    var PAGE_SIZE = 50;
    var pageData = middle(req, 'DestinyChecklist.net - Leaderboard AoT', "leaderboardAOT");
    var page = 1;
    if (req.params.page){
      page = parseInt(req.params.page);
    }
    if (page<=0) page = 1;

    platformApi.get390Leaders(key, function (err, leaderData) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      var leaders;
      if (!leaderData) leaders = [];
      else leaders = leaderData.leaders;
      var startIndex = (page-1) * PAGE_SIZE;
      var endIndex = startIndex+PAGE_SIZE;

      if (endIndex>=leaders.length){
        endIndex = leaders.length;
      }

      var finalPage =  Math.floor(leaders.length/PAGE_SIZE)+1;
      if (leaders.length%PAGE_SIZE==0) finalPage--;
      var startPage = page-2;
      var endPage = startPage+4;
      if (startPage<1){
        startPage = 1;
        endPage = 5;
      }
      else if (endPage>finalPage){
        endPage = finalPage;
        startPage = endPage-4;
      }
      if (startPage<1) startPage = 1;
      if (endPage>finalPage) endPage = finalPage;

      pageData.data = {
        leaders: leaders,
        paging : {
          startPage: startPage,
          page: page,
          endPage: endPage,
          startIndex: startIndex,
          endIndex: endIndex,
          showFastRwd: startPage>1,
          showFastFwd: endPage<finalPage,
          finalPage: finalPage
        }
      };
      res.render(viewName, pageData);
    });

  }


  function handleLeaderROI(isHM, viewName, req, res){
    var PAGE_SIZE = 50;
    var pageData = middle(req, 'DestinyChecklist.net - Leaderboard RoI', "leaderboardROI");
    var page = 1;
    if (req.params.page){
      page = parseInt(req.params.page);
    }
    if (page<=0) page = 1;

    platformApi.getWrathLeaders(isHM, function (err, leaderData) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      var leaders;
      if (!leaderData) leaders = [];
      else leaders = leaderData.leaders;
      var startIndex = (page-1) * PAGE_SIZE;
      var endIndex = startIndex+PAGE_SIZE;

      if (endIndex>=leaders.length){
        endIndex = leaders.length;
      }

      var finalPage =  Math.floor(leaders.length/PAGE_SIZE)+1;
      if (leaders.length%PAGE_SIZE==0) finalPage--;
      var startPage = page-2;
      var endPage = startPage+4;
      if (startPage<1){
        startPage = 1;
        endPage = 5;
      }
      else if (endPage>finalPage){
        endPage = finalPage;
        startPage = endPage-4;
      }
      if (startPage<1) startPage = 1;
      if (endPage>finalPage) endPage = finalPage;

      pageData.data = {
        leaders: leaders,
        paging : {
          startPage: startPage,
          page: page,
          endPage: endPage,
          startIndex: startIndex,
          endIndex: endIndex,
          showFastRwd: startPage>1,
          showFastFwd: endPage<finalPage,
          finalPage: finalPage
        }
      };
      res.render(viewName, pageData);
    });
    
  }
  router.get('/leaderAOT/ce/:page?', function (req, res, next) {
    handleLeaderAOT("ce390", 'leaderCE390', req, res);
  });

  router.get('/leaderAOT/vog/:page?', function (req, res, next) {
    handleLeaderAOT("vog390", 'leaderVOG390', req, res);
  });

  router.get('/leaderAOT/kf/:page?', function (req, res, next) {
    handleLeaderAOT("kf390", 'leaderKF390', req, res);
  });
  
  router.get('/leaderROIHM/:page?', function (req, res, next) {
    handleLeaderROI(true, 'leaderIronHm', req, res);
  });

  router.get('/leaderROI/:page?', function (req, res, next) {
    handleLeaderROI(false, 'leaderIron', req, res);
  });

  router.get('/leaderROIJSON/:isHM?', function (req, res, next) {
    var isHM = "true"==req.params.isHM;
    platformApi.getWrathLeaders(isHM, function (err, leaderData) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      var pcgrs = [];
      if (leaderData){
        leaderData.leaders.forEach(function(l){
          pcgrs.push(l.instanceId);
        });  
      }
      res.json(pcgrs);
    });
  });

  router.get('/removeWrathDupes/:isHM?', function (req, res, next) {
    var isHM = "true"==req.params.isHM;
    platformApi.removeWrathDupes(isHM, function (err, dupes) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      res.json(dupes);
    });
  });

  router.get('/leaderCoe/:filename?', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - Leaderboard CoE', "leaderboardCoe");
    platformApi.getElderLeaders(function (err, pvpData) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      pageData.data = pvpData;

      res.render('leaderCoe', pageData);
    }, req.params.filename);
  });


  router.get('/leaderFAQ', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - Leaderboard FAQ', "leaderFAQ");
    res.render('leaderFaq', pageData);
  });
  router.get('/extensionFAQ', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - Extension FAQ', "extensionFAQ");
    res.render('extensionFaq', pageData);
  });

  //
  // router.get('/vendor', function (req, res, next) {
  //   var pageData = middle(req, 'DestinyChecklist.net - Vendors,  Missions, and Bounties', "vendor");
  //   pageData.advisors = platformApi.getAdvisors();
  //   pageData.foundry = platformApi.getFoundryRolls();
  //   res.render('vendor', pageData);
  // });


  router.get('/vendor', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - Vendors', "vendor");
    res.render('vendor', pageData);
  });

  function parsePlatformAndName(req) {
    if (!req.params.platform) return null;
    var sPlatform = req.params.platform;
    var platform = parseInt(sPlatform);
    if (platform != 1 && platform != 2) return null;
    if (!req.params.name) return null;
    return {platform: platform, name: req.params.name.trim()};
  }
  
  function handleTmow(req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - TMOW', "tmow");
    res.render('tmow', pageData);
    return;
  }

  router.get('/tmow', function (req, res, next) {
    handleTmow(req, res, next);
  });

  router.get('/tmow/:platform/:name', function (req, res, next) {
    handleTmow(req, res, next);
  });

  function handleVenn(req, res, next) {
    var platName = parsePlatformAndName(req);
    if (platName != null) {
      if (req.params.name2){
        platName.name2 = req.params.name2.trim();
      }
      if (req.params.name3){
        platName.name3 = req.params.name3.trim();
      }
    }
    
    var pageData = middle(req, 'DestinyChecklist.net - Venn', "venn");
    pageData.player = platName;
    res.render('venn', pageData);
    return;
  }

  router.get('/venn', function (req, res, next) {
    handleVenn(req, res, next);
  });

  router.get('/venn/:platform/:name/:name2?/:name3?', function (req, res, next) {
    handleVenn(req, res, next);
  });

  function handleGunsmith(req, res, next) {
    var platName = parsePlatformAndName(req);
    var pageData = middle(req, 'DestinyChecklist.net - Gunsmith', "gunsmith");
    pageData.advisors = platformApi.getAdvisors();
    pageData.foundry = platformApi.getFoundryRolls();
    
    if (platName == null) {
      pageData.player = {
        platform: 0,
        memberId: 0
      };
      res.render('gunsmith', pageData);
      return;
    }
    platformApi.getMemberId(platName.platform, platName.name, function (err, data) {
      if (err) {
        if ("OtherPlatform" == err.name) {
          var otherPlatform = 1;
          if (platName.platform == 1) otherPlatform = 2;
          res.redirect(301, "/ib/" + otherPlatform + "/" + platName.name);
          return;
        }
        if ("SystemDisabled" == err.status) {
          pageData.player = platName;
          pageData.player.memberId = -1;
          res.render('gunsmith', pageData);
          return;
        }
        res.render('error', {
          err: err
        });
        return;
      }
      pageData.player = platName;
      pageData.player.memberId = data;
      res.render('gunsmith', pageData);
      return;
    }, req);
  }


  router.get('/gunsmith', function (req, res, next) {
    handleGunsmith(req, res, next);
  });

  router.get('/gunsmith/:platform/:name', function (req, res, next) {
    handleGunsmith(req, res, next);
  });


  function handleSRL(req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - SRL', "srl");
    res.render('srl', pageData);
    return;
  }
  
  router.get('/srl', function (req, res, next) {
    handleSRL(req, res, next);
    
  });
    
  router.get('/srl/:platform/:name', function (req, res, next) {
    handleSRL(req, res, next);
    //var platName = parsePlatformAndName(req);
    //if (platName == null) {
    //  res.render('error', {
    //    err: new Error("platform and name not properly specified")
    //  });
    //}
    //platformApi.getMemberId(platName.platform, platName.name, function (err, data) {
    //  if (err) {
    //    if ("OtherPlatform" == err.name) {
    //      var otherPlatform = 1;
    //      if (platName.platform == 1) otherPlatform = 2;
    //      res.redirect(301, "/srl/" + otherPlatform + "/" + platName.name);
    //      return;
    //    }
    //    res.render('error', {
    //      err: err
    //    });
    //    return;
    //  }
    //  var pageData = middle(req, 'DestinyChecklist.net - SRL', "srl");
    //  pageData.player = platName;
    //  pageData.player.memberId = data;
    //  pageData.bounties = platformApi.cache.srlBounties;
    //  res.render('srl2', pageData);
    //  return;
    //});
  });


  router.get('/grim', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - Grimoire', "grim");
    pageData.player = {
      name: "Example",
      platform: 0,
      memberId: 0
    };
    res.render('grim', pageData);
    return;
  });

  router.get('/grim/:platform/:name', function (req, res, next) {
    var platName = parsePlatformAndName(req);
    if (platName == null) {
      res.render('error', {
        err: new Error("platform and name not properly specified")
      });
    }
    platformApi.getMemberId(platName.platform, platName.name, function (err, data) {
      if (err) {
        if ("OtherPlatform" == err.name) {
          var otherPlatform = 1;
          if (platName.platform == 1) otherPlatform = 2;
          res.redirect(301, "/grim/" + otherPlatform + "/" + platName.name);
          return;
        }
        res.render('error', {
          err: err
        });
        return;
      }
      var pageData = middle(req, 'DestinyChecklist.net - Grimoire', "grim");
      pageData.player = platName;
      pageData.player.memberId = data;
      res.render('grim', pageData);
      return;
    }, req);
  });


  router.get('/checkRaidLeader', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - CheckRaidLeader', "checkRaidLeader");
    res.render('checkRaidLeader', pageData);
    return;
  });

  router.get('/cf', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - Calcified Fragments', "cf");
    pageData.player = {
      name: "Example",
      platform: 0,
      memberId: 0
    };
    res.render('cf', pageData);
    return;
  });

  router.get('/cf/:platform/:name', function (req, res, next) {
    var platName = parsePlatformAndName(req);
    if (platName == null) {
      res.render('error', {
        err: new Error("platform and name not properly specified")
      });
    }

    platformApi.getMemberId(platName.platform, platName.name, function (err, data) {
      if (err) {
        if ("OtherPlatform" == err.name) {
          var otherPlatform = 1;
          if (platName.platform == 1) otherPlatform = 2;
          res.redirect(301, "/cf/" + otherPlatform + "/" + platName.name);
          return;
        }
        res.render('error', {
          err: err
        });
        return;
      }
      var pageData = middle(req, 'DestinyChecklist.net - Calcified Fragments', "cf");
      pageData.player = platName;
      pageData.player.memberId = data;
      res.render('cf', pageData);
      return;
    }, req);
  });

  function applySpecialPageData(req, tag, pageData){
    //filter out normal scenarios
    if (tag=="ib"){
      if (!req.headers.host) return;
      if (!req.headers.host.includes("destinyib")) return;
    }
    else if (tag=="gunsmith"){
      if (!req.headers.host) return;
      if (!req.headers.host.includes("gunsmith")) return;
    }
    else if (tag=="venn"){
      if (!req.headers.host) return;
      if (!req.headers.host.includes("venndestiny")) return;
    }
    else if (tag=="tmow"){
      if (!req.headers.host) return;
      if (!req.headers.host.includes("truemeaningofwar")) return;
    }
    else if (tag=="srl"){
      if (!req.headers.host) return;
      if (!req.headers.host.includes("srlcalc")) return;
    }
    else
      return;
    
    pageData.special = true;
    pageData.aboutLink = '/specialAbout/'+tag;
    if (tag=="ib"){
      pageData.title = "Destiny IB";
      pageData.goog_anal = 'UA-21710078-7';
      pageData.metaDesc = 'Destiny Iron Banner calculator. Reach Rank 5 as efficiently as possible!';
    }
    if (tag=="gunsmith"){
      pageData.title = "Destiny Gunsmith";
      pageData.goog_anal = 'UA-21710078-8';
      pageData.metaDesc = 'Destiny Gunsmith tracker. See your weekly progress, bounties, sales, and rank with Banshee-44';
    }
    if (tag=="venn"){
      pageData.title = "Venn Destiny";
      pageData.goog_anal = 'UA-21710078-9';
      pageData.metaDesc = 'Destiny Venn Diagram, figure out which quests and bounties you share with your friends. Just in time for Rise of Iron!';
    }
    if (tag=="tmow"){
      pageData.title = "True Meaning Of War";
      pageData.goog_anal = 'UA-21710078-10';
      pageData.metaDesc = "Destiny PVP report on Lord Shaxx's weekly bounties and associated ranks. ";
    }

    if (tag=="srl"){
      pageData.title = "SRL Calc";
      pageData.goog_anal = 'UA-21710078-11';
      pageData.metaDesc = 'Destiny Sparrow Race League (SRL) calculator. Make  Amanda Holliday proud and reach max rank  as efficiently as possible!';
    }
  }

  function handleIB(req, res, next) {
    var platName = parsePlatformAndName(req);
    if (platName == null) {
      var pageData = middle(req, 'DestinyChecklist.net - Iron Banner', "ib");
      pageData.player = {
        name: "Example",
        platform: 0,
        memberId: 0
      };
      res.render('ib', pageData);
      return;
    }

    platformApi.getMemberId(platName.platform, platName.name, function (err, data) {
      if (err) {
        if ("OtherPlatform" == err.name) {
          var otherPlatform = 1;
          if (platName.platform == 1) otherPlatform = 2;
          res.redirect(301, "/ib/" + otherPlatform + "/" + platName.name);
          return;
        }
        if ("SystemDisabled" == err.status) {
          var pageData = middle(req, 'DestinyChecklist.net - Iron Banner', "ib");
          pageData.player = platName;
          pageData.player.memberId = -1;
          res.render('ib', pageData);
          return;
        }
        res.render('error', {
          err: err
        });
        return;
      }
      var pageData = middle(req, 'DestinyChecklist.net - Iron Banner', "ib");
      pageData.player = platName;
      pageData.player.memberId = data;
      res.render('ib', pageData);
      return;
    }, req);
  }

  router.get('/ib', function (req, res, next) {
    handleIB(req, res, next);
  });

  router.get('/ib/:platform/:name', function (req, res, next) {
    handleIB(req, res, next);
  });

  router.get('/tabs/:platform/:name', function (req, res, next) {
    var platName = parsePlatformAndName(req);
    if (platName == null) {
      res.render('error', {
        err: new Error("platform and name not properly specified")
      });
    }
    var platform = platName.platform;
    var name = platName.name;

    platformApi.getMemberId(platName.platform, platName.name, function (err, memberId) {
      if (err) {
        if ("OtherPlatform" == err.name) {
          var otherPlatform = 1;
          if (platform == 1) otherPlatform = 2;
          res.redirect(301, "/tabs/" + otherPlatform + "/" + name);
          return;
        }
        res.render('error', {
          err: err
        });
        return;
      }
      else
        var pageData = middle(req, 'DestinyChecklist.net - Player Report', "tabs");
      pageData.data = {};
      pageData.data.player = platName;
      pageData.data.player.memberId = memberId;

      pageData.advisors = platformApi.getAdvisors();
      res.render('tabs', pageData);
    }, req);
  });

  router.get("/srlmatch/:instanceId", function (req, res, next) {
    var instanceId = parseInt(req.params.instanceId);
    platformApi.srlMatchInfo(instanceId, function (err, data) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      var pageData = middle(req, 'DestinyChecklist.net - SRL Match', "srlmatch");
      pageData.data = data;
      res.render('srlmatch', pageData);
      return;
    }, req);
  });

  router.get("/pvpmatch/:instanceId", function (req, res, next) {
    var instanceId = parseInt(req.params.instanceId);
    platformApi.pvpMatchInfo(instanceId, function (err, data) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      var pageData = middle(req, 'DestinyChecklist.net - Match', "match");
      pageData.data = data;
      res.render('pvpmatch', pageData);
      return;
    }, req);
  });
  router.get("/match/:instanceId", function (req, res, next) {
    var instanceId = parseInt(req.params.instanceId);
    platformApi.matchInfo(instanceId, function (err, data) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      //res.json(data);
      var pageData = middle(req, 'DestinyChecklist.net - Match', "match");
      pageData.data = data;
      res.render('match', pageData);
      return;
    }, req);
  });

  router.get("/check390/:pgcr", function (req, res, next) {
    var instanceId = parseInt(req.params.pgcr);
    platformApi.check390PGCR(instanceId, function (err, data) {
      if (err) {
        res.json(err);
        return;
      }
      res.json(data);
      return;
    }, req);
  });

  router.get('/list/:platform/:tags', function (req, res, next) {
    var errMsg = null;
    var platform = 1;
    if (!req.params.platform)
      errMsg = "No platform specified";
    else {
      var sPlatform = req.params.platform;
      platform = parseInt(sPlatform);
      if (platform != 1 && platform != 2) errMsg = "Invalid platform";
    }
    if (!req.params.tags) errMsg = "No tags specified";

    if (errMsg) {
      res.render('error', {
        err: new Error(errMsg)
      });
      return;
    }

    var tags = req.params.tags.split(",");
    var pageData = middle(req, 'DestinyChecklist.net - List', "list");
    pageData.platform = platform;
    pageData.names = tags;
    res.render('list', pageData);

  });


  router.get('/clan', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - Clan Report', "clan");
    pageData.platform = 0;
    res.render('list', pageData);
    return;
  });
  
  router.get('/aotlight', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - AoT Light Up', "roilight");
    res.render('roilight', pageData);
    return;
  });
  
  router.get('/roilight', function (req, res, next) {
    var pageData = middle(req, 'DestinyChecklist.net - AoT Light Up', "roilight");
    res.render('roilight', pageData);
    return;
  });

  router.get('/clan/:platform/:name', function (req, res, next) {
    var platName = parsePlatformAndName(req);
    if (platName == null) {
      res.render('error', {
        err: new Error("platform and name not properly specified")
      });
    }
    var platform = platName.platform;
    var name = platName.name;
    platformApi.findClanMembers(platform, name, function (err, tags) {
      if (err) {
        res.render('error', {
          err: err
        });
        return;
      }
      else if (tags == null) {
        res.render('error', {
          err: new Error("No clan found for name '" + name + "'")
        });
        return;
      }
      else
        var pageData = middle(req, 'DestinyChecklist.net - Clan Report', "clan");
      pageData.platform = platform;
      pageData.clanName = name;
      pageData.names = tags;
      res.render('list', pageData);
    }, req);
  });

  //router.get('/clan2/:platform/:name', function (req, res, next) {
  //  var platName = parsePlatformAndName(req);
  //  if (platName == null) {
  //    res.render('error', {
  //      err: new Error("platform and name not properly specified")
  //    });
  //  }
  //  var platform = platName.platform;
  //  var name = platName.name;
  //  platformApi.findClanMembers(platform, name, function (err, tags) {
  //    if (err) {
  //      res.render('error', {
  //        err: err
  //      });
  //      return;
  //    }
  //    else if (tags == null) {
  //      res.render('error', {
  //        err: new Error("No clan found for name '" + name + "'")
  //      });
  //      return;
  //    }
  //    else
  //      var pageData = middle(req, 'DestinyChecklist.net - Clan Report', "clan2");
  //    pageData.platform = platform;
  //    pageData.clanName = name;
  //    pageData.names = tags;
  //    res.render('list2', pageData);
  //  }, req);
  //});
  return router;
}
;