

module.exports = function (platformApi) {
  var express = require('express');
  var router = express.Router();
  var LZString = require('lz-string');
  const fs = require('fs');
  var conf = require('../config.js')();
  var request = require('request');


  var cors = require('cors');
  
  router.api = null;

  router.get('/getMemberId/:platform/:name', function (req, res) {
    var platform = parseInt(req.params["platform"]);
    var name = req.params["name"];

    platformApi.getMemberId(platform, name, function (err, memberId) {
      if (err)
        res.json({
          error: err
        });
      else
        res.json({
          memberId: memberId
        });
    }, req);
  });


  router.get('/listChars/:platform/:memberId/:name', function (req, res) {
    var platform = parseInt(req.params.platform);
    var memberId = req.params.memberId;
    var name = req.params.name;
    platformApi.listCharactersByMemberId(platform, memberId, name, function (err, jo) {
      if (err) {
        if (err.message){
          err = err.message;
        }
        res.json({
          error: err
        });
      }
      else
        res.json(jo);
    }, req);
  });


  router.get("/getQuestItemsForTabs/:platform/:name", function (req, res) {
    var platform = parseInt(req.params["platform"]);
    var name = req.params["name"];
    platformApi.getQuestItemsForTabs(platform, name, function (err, chars) {
      if (err) res.json(err);
      else res.json(chars);
    }, req);
  });

  router.get("/questItems/:platform/:name", function (req, res) {
    var platform = parseInt(req.params["platform"]);
    var name = req.params["name"];
    platformApi.getQuestItems(platform, name, function (err, chars) {
      if (err) res.json(err);
      else res.json(chars);
    }, req);
  });

  router.get("/commonQuests/:platform/:names", function (req, res) {
    var platform = parseInt(req.params["platform"]);
    var sNames = req.params["names"];
    var names = JSON.parse(sNames);
    platformApi.getCommonQuests(platform, names, function (err, chars) {
      if (err) res.json(err);
      else res.json(chars);
    }, req);
  });

  router.get('/listCharacters/:platform/:name', function (req, res) {
    var platform = parseInt(req.params["platform"]);
    var name = req.params["name"];
    platformApi.listCharacters(platform, name, function (err, chars) {
      if (err) res.json(err);
      else res.json(chars);
    }, req);
  });


  function apiErrorHandler(err, req, res) {
    console.log('error on request %d %s %s: %s', process.domain.id, req.method, req.url, err.message);
    console.dir(err);
    console.dir(err.stack);
    res.status(500).send(err.message);
    console.log("Error sent");
  }

  router.get('/getCharacterinfo/:platform/:memberId/:characterId', function (req, res, next) {
    var platform = parseInt(req.params.platform);

    platformApi.getCharacterInfo(req, platform, req.params.memberId, req.params.characterId, function (err, data) {
      if (err)
        apiErrorHandler(err, req, res, next);
      if (!err)
        res.json(data);
    }, req);
  });


  router.get('/getCharInfoPt1/:platform/:memberId/:characterId', function (req, res, next) {
    var platform = parseInt(req.params.platform);

    platformApi.getCharInfoPt1(platform, req.params.memberId, req.params.characterId, function (err, data) {
      if (err)
        apiErrorHandler(err, req, res, next);
      if (!err)
        res.json(data);
    }, req);
  });

  router.get('/getCharInfoPt2/:platform/:memberId/:characterId', function (req, res, next) {
    var platform = parseInt(req.params.platform);

    platformApi.getCharInfoPt2(platform, req.params.memberId, req.params.characterId, function (err, data) {
      if (err)
        apiErrorHandler(err, req, res, next);
      if (!err)
        res.json(data);
    }, req);
  });


  router.get("/gunsmith/:platform/:memberId/:name", function (req, res) {
    var platform = parseInt(req.params.platform);
    var memberId = req.params.memberId;
    var name = req.params.name;
    platformApi.listGunsmithChars(platform, memberId, name, function (err, chars) {
      if (err) {
        if (err.message)
          res.json({
            error: err.message
          });
        else
          res.json({
            error: "Error retrieving gunsmith data from Bungie API"
          });
        return;
      }
      else {
        res.json({
          chars: chars
        });
        return;
      }
    }, req);
  });
  
  router.get("/grim/:platform/:memberId", function (req, res) {
    var platform = parseInt(req.params.platform);
    var memberId = req.params.memberId;
    platformApi.grim(platform, memberId, function (err, grim) {
      if (err) {
        if (err.message)
          res.json(err.message);
        else
          res.json(err);
        return;
      }
      else {
        res.json(grim);
        return;
      }
    }, req);
  });

  router.get("/grimCsv/:platform/:memberId", function (req, res) {
    var platform = parseInt(req.params.platform);
    var memberId = req.params.memberId;
    platformApi.grim(platform, memberId, function (err, data) {
      if (err) {
        if (err.message)
          res.json(err.message);
        else
          res.json(err);
        return;
      }
      else {
        var s = "";
        for (var cntr = 0; cntr < data.cards.length; cntr++) {
          var c = data.cards[cntr];
          s += c.cardName + "," + c.totalPts + "," + c.earnedPts + "\r\n";
        }
        res.set('Content-Type', 'text/plain');
        res.send(s);
        return;
      }
    }, req);
  });


  router.get("/srlChars/:platform/:memberId", function (req, res) {
    var platform = parseInt(req.params.platform);
    var memberId = req.params.memberId;
    platformApi.listSrlChars(platform, memberId, function (err, chars) {
      if (err) {
        res.json(err);
        return;
      }
      else {
        res.json(chars);
        return;
      }
    }, req);
  });

  router.get("/ibChars/:platform/:memberId", function (req, res) {
    var platform = parseInt(req.params.platform);
    var memberId = req.params.memberId;
    platformApi.listIbChars(platform, memberId, function (err, chars) {
      if (err) {
        res.json(err);
        return;
      }
      else {
        res.json(chars);
        return;
      }
    }, req);
  });

  router.get("/cf/:platform/:memberId", function (req, res) {
    var platform = parseInt(req.params.platform);
    var memberId = req.params.memberId;
    platformApi.listCalcFrags(platform, memberId, function (err, chars) {
      if (err) {
        res.json(err);
        return;
      }
      else {
        res.json(chars);
        return;
      }
    }, req);
  });


  router.get("/getCharsForList/:platform/:name", function (req, res) {
    var platform = parseInt(req.params.platform);
    var name = req.params.name;

    platformApi.getCharsForList(platform, name, function (err, chars) {
      if (err) {
        res.json(err);
        return;
      }
      else {
        res.json(chars);
        return;
      }
    }, req);
  });


  router.get("/checkRaidLeader/:platform/:name", function (req, res) {
    var platform = parseInt(req.params.platform);
    var name = req.params.name;

    platformApi.checkLeaderHistory(platform, name, function (err, chars) {
      if (err) {
        res.json(err);
        return;
      }
      else {
        res.json({
          chars: chars
        });
        return;
      }
    }, req);
  });



  router.get("/getCharsForList2/:platform/:name", function (req, res) {
    var platform = parseInt(req.params.platform);
    var name = req.params.name;

    platformApi.getCharsForList2(platform, name, function (err, chars) {
      if (err) {
        if (err.message){
          err = err.message;
        }
        else {
          err = JSON.stringify(err);
        }
        res.json({
          error: err
        });
        return;
      }
      else if (!chars || chars.length == 0) {
        res.json({
          name: "NoChars",
          message: "This player has no Destiny characters."
        });
        return;
      }
      else {
        res.json(chars);
        return;
      }
    }, req);
  });

  router.get("/leaderBoards", function (req, res) {
    platformApi.leaderBoards(function (err, msg) {
      if (err) res.json(err);
      else res.json(msg);
    });
  });


  router.get("/xyz/:apikey", cors(), function (req, res) {
    var apikey = req.params.apikey;
    if (apikey==null){
      res.json({error: "Missing api key"});
    }
    else{
      request('https://api.vendorengrams.xyz/getVendorDrops?key='+apikey, function (error, response, body) {
        if (error){
          res.json({error: error});
        }
        else{
          try {

            var j = JSON.parse(body);
            res.json(j);
          }
          catch(exc){
            res.json({error: body});
          }
        }
      }); 
    }
  });


  router.get("/refreshAdvisors", function (req, res) {
    platformApi.refreshAdvisors(function (err, jo) {
      if (err) res.json(err);
      else
        res.json(jo);
    });

  });
  router.get("/refreshVendors", function (req, res) {
    platformApi.refreshVendors(function (err, jo) {
      if (err) res.json(err);
      else
        res.json(jo);
    });

  });
  router.get("/vendor", function (req, res) {
    res.json(platformApi.getVendors());

  });

  router.get("/saveLeaders", function (req, res) {
    platformApi.saveLeaders(function (err, jo) {
      if (err) res.json(err);
      else
        res.json(jo);
    });
  });

  router.get("/venn/:platform/:name/:name2/:name3?", function (req, res) {
    var platform = parseInt(req.params.platform);
    platformApi.listVennChars(platform, req.params.name, req.params.name2, req.params.name3, function (err, chars) {
      if (err) res.json(err);
      else
        res.json(chars);
    }, req);
    return;
  });

  router.post("/guns", function (req, res) {
    if (req.body.data) {
      try {
        var s = LZString.decompressFromBase64(req.body.data);
        var data = JSON.parse(s);
        if (data.magic && data.magic == "this is magic!") {
          platformApi.storeFoundryRolls(data.expires, data.guns);
        }
      }
      catch (e) {
        //ignore
      }
    }
    res.json({status: "success"});
    return;
  });


  router.options('/mark/:platform/:memberId', cors()); // enable pre-flight request for request 
  router.options('/mark', cors()); // enable pre-flight request for DELETE request
  router.options("/xyz/:apikey", cors()); // enable pre-flight request for request

  router.get("/mark/:platform/:memberId", cors(), function (req, res) {
    var fileName = conf.dataDir + "/marks/" + req.params.platform + "-" + req.params.memberId;
    fs.readFile(fileName, {encoding: "utf8"}, function (err, data) {
      if (err) {
        res.json({});
        return;
      }
      var s = LZString.decompressFromBase64(data);
      data = JSON.parse(s);
      res.json(data);
    });
  });


  router.post("/mark", cors(), function (req, res) {
    if (req.body.data) {
      try {
        var s = LZString.decompressFromBase64(req.body.data);
        var data = JSON.parse(s);
        if (data.magic && data.magic == "this is magic!") {
          if (data.platform && data.memberId) {
            var fileName = conf.dataDir + "/marks/" + data.platform + "-" + data.memberId;
            fs.writeFile(fileName, req.body.data, function (err) {
              if (err) {
                console.dir(err);
              }
              else{
                console.log("Wrote: "+fileName+" of size "+req.body.data.length);
              }
            });
          }
        }
      }
      catch (e) {
        console.dir(e);
        //ignore
      }
    }
    res.json({status: "success"});
    return;
  });
  
  return router;
}

