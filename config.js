/**
 * Created by Dave on 8/29/2015.
 */
var request = require('request');
request.defaults({jar: true});

module.exports = function () {
  switch (process.env.NODE_ENV) {
    case 'dev':
      return {dataDir: "C:/Users/Dave/destiny"};
    case 'devMac':
      return {dataDir: "/Users/davecaslin/data"};
    case 'devMac2':
      return {dataDir: "/Users/dcaslin/data"};
    case 'prod':
      return {dataDir: "/opt/data"};
    case 'production':
      return {dataDir: "/opt/data"};
    default:
      throw "Invalid value for NODE_ENV: " + process.env.NODE_ENV;
  }
};