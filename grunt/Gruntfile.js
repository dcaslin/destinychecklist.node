/**
 * Created by Dave on 9/15/2015.
 */
module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            target: {
                files: {
                    '../public/css/site.min.css': ['vendor/css/*.css', '../css/*.css']
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['vendor/js/*.js','../js/*.js'],  
                dest: '../public/js/combined.js'
            }
        },uglify: {
            options: {
                // the banner is inserted at the top of the output
                banner: '/*! DestinyChecklist.Net <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '../public/js/combined.min.js': ['../public/js/combined.js']
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', [ 'cssmin', 'concat', 'uglify' ]);
};