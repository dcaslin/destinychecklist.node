var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var URL = require('url');
var parseDomain = require("parse-domain");


var platformApi = require("./platform/api")();

process.on('exit', function () {
  platformApi.quit();
});

var routes = require('./routes/index')(platformApi);
var api = require('./routes/api')(platformApi);

var app = express();
app.locals.pretty = true;
app.set('etag', false);

app.use(require('express-domain-middleware'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(function (req, res, next) {
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  console.log("IP: " + ip + "," + req.headers.host + "," + req.url);
  var url_parts = URL.parse(req.url, true);
  var query = url_parts.query;
  if (query.nocache && query.nocache == "true") {
    req.nocache = true;
  }
  next();
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(
  path.join(__dirname, 'public'), 
  {maxAge: '365d'}
));

var JSP_TEST = /\.jsp/gi;

app.all(/.*/, function (req, res, next) {

  var host = req.header("host");
  var parts = parseDomain(host);
  
  if (parts && parts.subdomain != "www") {
    var uri = "";
    if (req.url!=null && req.url.length>0){
      uri = req.url;
    }
    
    res.redirect(301, "http://www." + parts.domain + "." + parts.tld+uri);
  }
  else if (JSP_TEST.test(req.path)) {
    var url_parts = URL.parse(req.url, true);
    var newUrl = req.url.replace(JSP_TEST, '');
    newUrl = URL.parse(newUrl).pathname;
    var query = url_parts.query;
    if (query.platform && query.name) {
      newUrl = newUrl + "/" + query.platform + "/" + query.name;
    }
    res.redirect(301, newUrl);
  }
  else
    next();

});
app.use('/api', api);
app.use('/', routes);

function errorHandler(err, req, res, next) {
  console.log('APP.JS error on request %d %s %s: %s', process.domain.id, req.method, req.url, err.message);
  console.dir(err);
  console.dir(err.stack);
  if (res.headersSent) {
    return next(err);
  }
  res.render('error', {
    err: err
  });
  //
  //res.status(500).send(err.message);
  //console.log("Error sent to client.");
  //if(err.domain) {
  //    console.log("hmmm");
  //}
}

app.use(errorHandler);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('404');
  err.name = "Page not found";

  err.message = "Page not found";
  err.status = 404;
  errorHandler(err, req, res, next);
});

app.use(function (err, req, res) {
  res.status(err.status || 500);
  res.render('error', {
    err: err
  });
});

module.exports = app;
